
var base_url = $('#baseUrl').val();
$(".add_user").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });  
 
    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var first_name = $.trim($(".first_name").val());
    var last_name = $.trim($(".last_name").val());
    var email = $.trim($(".email").val()); 
    var mobile = $.trim($(".mobile").val());
    var password = $.trim($(".password").val());
    var npi = $.trim($(".npi").val());
    var address = $.trim($(".address").val());
    var state = $.trim($(".state").val());
    var licence = $.trim($(".licence").val());
    var user_type = "3";  
    var cpassword = $.trim($(".cpassword").val());
    mobile = mobile.replace(" ","");
    mobile = mobile.replace("(","");
    mobile = mobile.replace(")","");
    mobile = mobile.replace("-","");

    var error_msg = '';

    if(password != cpassword){
        error_msg = 'Password mismatch';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('first_name' , first_name); 
    ajaxData.append('last_name' , last_name);
    ajaxData.append('email' , email);
    ajaxData.append('mobile' , mobile);
    ajaxData.append('password' , password);
    ajaxData.append('npi' , npi);
    ajaxData.append('address' , address);
    ajaxData.append('state' , state);
    ajaxData.append('licence' , licence);
    ajaxData.append('user_type' , user_type); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'patient_signup_action',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_user").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.reload();
                    if(user_type == 2){
                        //window.location.href =  base_url+"dashboard";
                    }else{
                        //window.location.href =  base_url+"dashboard";
                    }   
                    
                }, 5000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

 
    $(".loginForm").submit(function(){
    var emailMobile = $.trim($("#loginMobile").val());
    var password = $.trim($("#loginPassword").val());
    var error_msg = '';

    if(emailMobile == '')
    {
    error_msg = 'Please Enter Email/Mobile Number';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }

    if(password == '')
    {
    error_msg = 'Please Enter Password';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }    

    if(error_msg != '')
    {
    $(".error_popup").show();
    $(".error_popup").html(error_msg);
    return false;
    }

    var qData = {
        emailMobile : emailMobile,
        password:password
    }

    $(".login_popup_spinner").show();
    $(".error_popup").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'patientLoginAction',
    data: qData,
    dataType : "text",
        success: function(response) {
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            {            
                /*$("#myModal").modal('hide');
                $("#myModal-otp").modal('show');
                setTimeout(function() {
                    $(".resendotp").show();
                }, 3000);*/
                if(resultData.user_type == 2){                    
                    window.location.href = "admin/dashboard";
                }else{
                    window.location.reload();
                }
                
            }
            else
            {
                $(".login_popup_spinner").hide();
                $(".error_popup").show();
                $(".error_popup").html(resultData.message);
                return false;
            }
        }
    });
    return false;
    });


$(".update_patient_personal_info").submit(function(){
    var ajaxData = new FormData();
    console.log("ajaxData",ajaxData);
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
 
    var family_ids = new Array();
    $('input[name="family_history[]"]:checked').each(function(){
        family_ids.push($(this).val());
    });

    var health_issues_ids = new Array();
    $('input[name="health_issues[]"]:checked').each(function(){
        health_issues_ids.push($(this).val());
    });

    var alergies_ids = new Array();
    $('input[name="alergies[]"]:checked').each(function(){
        alergies_ids.push($(this).val());
    });

  
    var first_name = $.trim($(".first_name").val());
    var last_name = $.trim($(".last_name").val());   
    var user_id = $.trim($(".user_id").val()); 
    var error_msg = '';

    if(first_name == ''){
        error_msg = 'Please Enter First Name';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    } 

    ajaxData.append('first_name' , first_name); 
    ajaxData.append('last_name' , $(".last_name").val());
    ajaxData.append('address' , $(".address").val());
    ajaxData.append('dob' , $.trim($(".dob").val()));
    ajaxData.append('age' , $.trim($(".age").val()));
    ajaxData.append('when_did_it_start' , $.trim($(".when_did_it_start").val()));
    ajaxData.append('reason_for_visit' , $.trim($(".reason_for_visit").val()));
    ajaxData.append('what_is_the_problem' , $.trim($(".what_is_the_problem").val()));
    ajaxData.append('how_often' , $.trim($(".how_often").val()));
    ajaxData.append('medication_taken' ,  $.trim($('input[name="medication_taken"]:checked').val()));
    ajaxData.append('alergies' , alergies_ids);
    ajaxData.append('family_history' ,family_ids);
    ajaxData.append('health_issues' ,health_issues_ids);
    ajaxData.append('marital_status' , $.trim($('input[name="marital_status"]:checked').val()));
    ajaxData.append('living_situation' , $.trim($('input[name="living_situation"]:checked').val()));
    ajaxData.append('smoking_status' , $.trim($('input[name="smoking_status"]:checked').val()));
    ajaxData.append('alcohol_status' , $.trim($('input[name="alcohol_status"]:checked').val()));
    ajaxData.append('drug_status' , $.trim($('input[name="drug_status"]:checked').val())); 
    ajaxData.append('operations_surgeries' , $.trim($('input[name="operations_surgeries"]:checked').val())); 
    ajaxData.append('height_inches' , $.trim($(".height_inches").val())); 
    ajaxData.append('height_feet' , $.trim($(".height_feet").val())); 
    ajaxData.append('blood_pressure' , $.trim($(".blood_pressure").val()));
    ajaxData.append('weight' , $.trim($(".weight").val()));
    ajaxData.append('notes' , $.trim($(".notes").val()));
    ajaxData.append('patient_user_id' , $.trim($(".patient_user_id").val()));
    ajaxData.append('message' , $.trim($(".message").val()));
    ajaxData.append('user_id' , user_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'update_personal_info',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            {                
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();                    
                }, 5000);
            }
            else 
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

function phoneFormatter() {
  $('.mobile').on('input', function() {
    var number = $(this).val().replace(/[^\d]/g, '')
    if (number.length == 7) {
      number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
    } else if (number.length == 10) {
      number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }
    $(this).val(number)
  });
};

$(phoneFormatter);


//Ajax onchange calls start
$(".medicines_data").hide();
$(".ship_address_section").hide();
$(".fdrug_id").on('change', function() {
    $(".medicines_data").hide();
    $(".ship_address_section").hide();
    console.log("changed")
    let drug_id = $(this).val();
    let url = base_url+"get_ajax_strength_data";
    $.ajax({
       type: "POST",
       url: url,
       data: {
        drug_id: drug_id
       }, // serializes the form's elements.
       success: function(data)
       {
        let sData = JSON.parse(data);
        if(sData.status) {
          console.log(sData.strength_data);
            //$(".aa").val(sData.vv);
             $(".fdrug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
            strengthData = sData.strength_data;
            for(let i=0; i<strengthData.length;i++) {
                $(".fdrug_strength_id").append(new Option(strengthData[i].strength, strengthData[i].drug_strength_id));
            }

        } else {
            $(".fdrug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
            console.log("data not found")
        }
       }, error: function(err) {
        console.log(err);
       }
     });
}); 


$(function () {
    changeStrengthOnCart();  
    $(".fdrug_strength_id").change(changeStrengthOnCart);
});


function changeStrengthOnCart(){ 
    $(".medicines_data").hide();
    $(".ship_address_section").hide();
    console.log("changed")
    var drug_id = $(".fdrug_id").val();
    var drug_strength_id = $(".fdrug_strength_id").val();
    var category_id = $(".category_id").val();
    var user_id = $(".user_id").val();
    let url = base_url+"get_ajax_tablets_data"; 
    $.ajax({
       type: "POST",
       url: url,
       data: {
        drug_id: drug_id,
        drug_strength_id:drug_strength_id,
        category_id:category_id,
        user_id:user_id
       }, // serializes the form's elements.
       success: function(data)
       {
        let sData = JSON.parse(data);
        if(sData.status) {
            $(".medicines_data").show();
            $(".ship_address_section").show();
            $(".price_list_data").html(sData.price_data);
        } else {
           $(".price_list_data").html('Data not availble');
            console.log("data not found")
        }
       }, error: function(err) {
        console.log(err);
       }
     });
}

//ajax onchange calls end

$(".save_cart").submit(function(){ 
    var category_id = $.trim($(".category_id").val());
    var batch_id = $.trim($(".batch_id").val());
    var drug_id = $.trim($(".fdrug_id").val());
    var drug_strength_id = $.trim($(".fdrug_strength_id").val());
    var drug_price_id = $("input[name='drug_price_id']:checked").val();  
    var error_msg = '';

    if(drug_id == ""){
        error_msg = 'Please select Drug';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }
    var ajaxData = new FormData();
    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('drug_strength_id' ,drug_strength_id);
    ajaxData.append('drug_price_id' , drug_price_id); 
    ajaxData.append('category_id' , category_id);
    ajaxData.append('batch_id' , batch_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'save_cart',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".save_cart").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"checkout?batch_id="+batch_id+"&category_id="+category_id; 
                    
                }, 2000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".save_cart_admin").submit(function(){  
    var category_id = $.trim($(".category_id").val());
    var drug_id = $.trim($(".fdrug_id").val());
    var batch_id = $.trim($(".batch_id").val());
    var no_of_refills = $.trim($(".no_of_refills").val());
    var drug_strength_id = $.trim($(".fdrug_strength_id").val());
    var drug_price_id = $("input[name='drug_price_id']:checked").val();  
    var send_to_pharmacy = $("input[name='send_to_pharmacy']:checked").val();  
    var patient_id = $(".user_id").val();

    if(typeof send_to_pharmacy === "undefined" || send_to_pharmacy == ""){
        send_to_pharmacy = 0;
    }

    var doctor_approve_status = "1";
    var error_msg = '';

    if(drug_id == ""){
        error_msg = 'Please select Drug';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }
    var ajaxData = new FormData();
    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('drug_strength_id' ,drug_strength_id);
    ajaxData.append('drug_price_id' , drug_price_id); 
    ajaxData.append('category_id' , category_id);
    ajaxData.append('patient_id' , patient_id);
    ajaxData.append('doctor_approve_status' , doctor_approve_status);
    ajaxData.append('batch_id' , batch_id);
    ajaxData.append('send_to_pharmacy' , send_to_pharmacy);
    ajaxData.append('no_of_refills' , no_of_refills);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'save_cart',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".save_cart").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.reload();
                    //window.location.href =  base_url+"checkout"; 
                    
                }, 2000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".save_address").submit(function(){ 
    var address_line1 = $.trim($(".address_line1").val()); 
    var address_line2 = $.trim($(".address_line2").val());
    var ship_city = $.trim($(".ship_city").val()); 
    var state = $.trim($(".state").val());
    var zip = $.trim($(".zip").val()); 
    var address_id = $.trim($(".address_id").val()); 
    var error_msg = '';

    if(address_line1 == ""){
        error_msg = 'Please Enter Address Line 1.';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }
    var ajaxData = new FormData();
    ajaxData.append('address_line1' , address_line1);  
    ajaxData.append('address_line2' , address_line2); 
    ajaxData.append('ship_city' , ship_city); 
    ajaxData.append('state' , state); 
    ajaxData.append('zip' , zip);  
    ajaxData.append('address_id' , address_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'save_address',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".save_address").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"my_addresses"; 
                    
                }, 4000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".update_address").submit(function(){ 
    var address_line1 = $.trim($(".address_line1").val()); 
    var address_line2 = $.trim($(".address_line2").val());
    var ship_city = $.trim($(".ship_city").val()); 
    var state = $.trim($(".state").val());
    var zip = $.trim($(".zip").val()); 
    var address_id = $.trim($(".address_id").val()); 
    var error_msg = '';

    if(address_line1 == ""){
        error_msg = 'Please Enter Address Line 1.';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }
    var ajaxData = new FormData();
    ajaxData.append('address_line1' , address_line1);  
    ajaxData.append('address_line2' , address_line2); 
    ajaxData.append('ship_city' , ship_city); 
    ajaxData.append('state' , state); 
    ajaxData.append('zip' , zip);  
    ajaxData.append('address_id' , address_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'save_address',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".save_address").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"my_addresses"; 
                    
                }, 4000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$('.ship_address_type').on('change', function() {
    var ship_address_type = this.checked ? this.value : '';
    if(ship_address_type == 2){
        
        $(".pharmacy_name").prop("required", true);
        $(".pharmacy_phone").prop("required", true); 
        $('.ship_address_section').show();
    }else{
        $('.ship_address_section').hide();
        $(".pharmacy_name").prop("required", false);
        $(".pharmacy_phone").prop("required", false); 
    }
    
});


/*$(".save_cart").submit(function(){ 
    var category_id = $.trim($(".category_id").val());
    var drug_id = $.trim($(".fdrug_id").val());
    var drug_strength_id = $.trim($(".fdrug_strength_id").val());
    var drug_price_id = $.trim($(".drug_price_id").val()); 
    var ship_name = $.trim($(".ship_name").val());
    var ship_city = $.trim($(".ship_city").val());
    var street_address = $.trim($(".street_address").val());
    var country = $.trim($(".country").val());
    var state = $.trim($(".state").val());
    var zip = $.trim($(".zip").val()); 
    var error_msg = '';

    if(drug_id == ""){
        error_msg = 'Please select Drug';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }
    var ajaxData = new FormData();
    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('drug_strength_id' ,drug_strength_id);
    ajaxData.append('drug_price_id' , drug_price_id);
    ajaxData.append('ship_name' , ship_name);
    ajaxData.append('ship_city' , ship_city);
    ajaxData.append('street_address' , street_address);
    ajaxData.append('state' , state);
    ajaxData.append('country' , country);
    ajaxData.append('zip' , zip);  
    ajaxData.append('category_id' , category_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'save_cart',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".save_cart").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.reload();
                    //window.location.href =  base_url+"dashboard"; 
                    
                }, 2000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});*/