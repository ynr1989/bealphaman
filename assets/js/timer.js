setInterval(sendLocations, 6000);

function sendLocations(){
    var mapLayer = document.getElementById("map-layer");
    var centerCoordinates = new google.maps.LatLng(37.6, -95.665);
    var defaultOptions = { center: centerCoordinates, zoom: 17 }

    map = new google.maps.Map(mapLayer, defaultOptions);
    var geocoder;
    var latLng;
        if ("geolocation" in navigator){
        navigator.geolocation.getCurrentPosition(function(position){
            var currentLatitude = position.coords.latitude;
            var currentLongitude = position.coords.longitude;

            var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
            var infoWindow = new google.maps.InfoWindow({map: map, content: infoWindowHTML});
            var currentLocation = { lat: currentLatitude, lng: currentLongitude };
            // map.setPosition(currentLocation);
            // infoWindow.close();
            map.setCenter(new google.maps.LatLng(currentLatitude, currentLongitude));

            //document.getElementById("login_latitude").value = currentLatitude;      
           // document.getElementById("login_longtitude").value = currentLongitude; 

            geocoder = new google.maps.Geocoder();
            var marker = new google.maps.Marker({
            position: currentLocation,
            map: map,
            title: 'current Location'
            });

            latLng = new google.maps.LatLng(currentLatitude,currentLongitude);

            marker.addListener('click', function() {
            infoWindow.open(map, marker);
            });
            if(geocoder) {
                geocoder.geocode({ 'latLng': latLng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //document.getElementById("login_address").value = results[0].formatted_address;
               
                    $.ajax({
                        url: "https://admin.melu.me/insertLocationTracking",
                        type: "POST",
                        data: {
                            login_latitude: currentLatitude,
                            login_longtitude: currentLongitude,
                            login_address: results[0].formatted_address
                        },
                        cache: false,
                        success: function(dataResult){
                           /* var dataResult = JSON.parse(dataResult);
                            if(dataResult.statusCode==200){
                                $("#butsave").removeAttr("disabled");
                                $('#fupForm').find('input:text').val('');
                                $("#success").show();
                                $('#success').html('Data added successfully !'); 

                            }
                            else if(dataResult.statusCode==201){
                               //alert("Error occured !");
                            }*/
                            
                        }
                    });

            }
            else {
                //document.getElementById("login_address").value ="Geocoding failed: " + status;
            }
        }); 
    }
    });
    }
}
