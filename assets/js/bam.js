var base_url = $('#baseUrl').val(); 
function isNumberKey(evt)
{       
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true; 
}
 
function alphaOnly(event) {
var key = event.keyCode;
return ((key >= 65 && key <= 90) || key == 8 || key == 9 || key == 32);
}; 

 
function checkAlert() {
  var r = confirm("Are you sure you want logout?");
  if (r == true) {    
  } else {
   return false;
  }  
}


var ptable = $('#drugPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ],
         /*Search Place holder Code Start*/
        "language": {
            "lengthMenu": '_MENU_ bản ghi trên trang',
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Search...",
                "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
            }
        }
        /*Search Place holder Code End*/
      });

      // Add event listener for opening and closing details
      $('#drugPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = ptable.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(projectFormat(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });

//Add Users

$(".add_user").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var first_name = $.trim($(".first_name").val());
    var last_name = $.trim($(".last_name").val());
    var email = $.trim($(".email").val()); 
    var mobile = $.trim($(".mobile").val());
    var password = $.trim($(".password").val());
    var npi = $.trim($(".npi").val());
    var address = $.trim($(".address").val());
    var state = $.trim($(".state").val());
    var licence = $.trim($(".licence").val());
    var user_type = $.trim($(".user_type").val());
    var contact_name = $.trim($(".contact_name").val());

    var error_msg = '';

    if(first_name == ''){
        error_msg = 'Please Enter First Name';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('first_name' , first_name); 
    ajaxData.append('last_name' , last_name);
    ajaxData.append('email' , email);
    ajaxData.append('mobile' , mobile);
    ajaxData.append('password' , password);
    ajaxData.append('npi' , npi);
    ajaxData.append('address' , address);
    ajaxData.append('state' , state);
    ajaxData.append('licence' , licence);
    ajaxData.append('user_type' , user_type);
    ajaxData.append('contact_name' , contact_name); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_user',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".addDrug").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    if(user_type == 2){
                        window.location.href =  base_url+"admin/doctors";
                    }else{
                        window.location.href =  base_url+"admin/pharmacies";
                    }   
                    
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_user").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
  
    var first_name = $.trim($(".first_name").val());
    var last_name = $.trim($(".last_name").val());
    var email = $.trim($(".email").val()); 
    var mobile = $.trim($(".mobile").val());
    var password = $.trim($(".password").val());
    var npi = $.trim($(".npi").val());
    var address = $.trim($(".address").val());
    var state = $.trim($(".state").val());
    var licence = $.trim($(".licence").val()); 
    var contact_name = $.trim($(".contact_name").val());
    var user_id = $.trim($(".user_id").val());
    var user_type = $.trim($(".user_type").val());
    var old_doctor_signature = $.trim($(".old_doctor_signature").val());
    var error_msg = '';

    if(first_name == ''){
        error_msg = 'Please Enter First Name';
    } 

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('first_name' , first_name); 
    ajaxData.append('last_name' , last_name);
    ajaxData.append('email' , email);
    ajaxData.append('mobile' , mobile);
    ajaxData.append('password' , password);
    ajaxData.append('npi' , npi);
    ajaxData.append('address' , address);
    ajaxData.append('state' , state);
    ajaxData.append('licence' , licence); 
    ajaxData.append('contact_name' , contact_name);
    ajaxData.append('user_id' , user_id);
    ajaxData.append('old_doctor_signature' , old_doctor_signature);
    
    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_user',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    if(user_type == 3){
                        window.location.href =  base_url+"admin/pharmacies";
                    }else{
                        window.location.href =  base_url+"admin/doctors";
                    }
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});




//Drugs
$(".addDrug").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var category_id = $.trim($(".category_id").val());
    var drug_name = $.trim($(".drug_name").val());
    var drug_sub_name = $.trim($(".drug_sub_name").val()); 
    var drug_description = $.trim($(".drug_description").val());
    var drug_code = $.trim($(".drug_code").val());
    var error_msg = '';

    if(category_id == ''){
        error_msg = 'Please Select Categpry';
    }else if(drug_name == ''){
        error_msg = 'Please Enter Drug name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('category_id' , category_id); 
    ajaxData.append('drug_name' , drug_name);
    ajaxData.append('drug_code' , drug_code);
    ajaxData.append('drug_sub_name' , drug_sub_name);
    ajaxData.append('drug_description' , drug_description); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_drug',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".addDrug").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_list";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_drug").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
  
    var drug_id = $.trim($(".drug_id").val());
    var category_id = $.trim($(".category_id").val());
    var drug_name = $.trim($(".drug_name").val());
    var drug_sub_name = $.trim($(".drug_sub_name").val()); 
    var drug_description = $.trim($(".drug_description").val());
    var drug_code = $.trim($(".drug_code").val());
    var error_msg = '';

    if(category_id == ''){
        error_msg = 'Please Select Categpry';
    }else if(drug_name == ''){
        error_msg = 'Please Enter Drug name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('category_id' , category_id); 
    ajaxData.append('drug_name' , drug_name);
    ajaxData.append('drug_code' , drug_code);
    ajaxData.append('drug_sub_name' , drug_sub_name);
    ajaxData.append('drug_description' , drug_description);
    ajaxData.append('drug_id' , drug_id);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_drug',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_list";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Drug Types

$(".add_drug_type").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var strength = $.trim($(".strength").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(strength == ''){
        error_msg = 'Please Enter Drug Type Name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('strength' , strength); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_drug_type',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".addDrug").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_types";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_drug_type").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
  
    var strength = $.trim($(".strength").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var drug_strength_id  = $.trim($(".drug_strength_id").val()); 
    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(strength == ''){
        error_msg = 'Please Enter Drug Type Name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('strength' , strength); 
    ajaxData.append('drug_strength_id' , drug_strength_id); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_drug_type',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_types";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Drug Doses

$(".add_drug_dose").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var drug_strength_id = $.trim($(".drug_strength_id").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var tablet_count = $.trim($(".tablet_count").val());
    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(tablet_count == ''){
        error_msg = 'Please Enter Tablets count';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('drug_strength_id' , drug_strength_id);
    ajaxData.append('tablet_count' , tablet_count); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_drug_dose',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_drug_dose").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_doses";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_drug_dose").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
  
    var tablet_count = $.trim($(".tablet_count").val());
    var tablet_set_id = $.trim($(".tablet_set_id").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var drug_strength_id  = $.trim($(".drug_strength_id").val()); 

    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(tablet_count == ''){
        error_msg = 'Please Enter Dose Name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('tablet_set_id' ,tablet_set_id); 
    ajaxData.append('tablet_count' , tablet_count);
    ajaxData.append('drug_strength_id' , drug_strength_id); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_drug_dose',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_doses";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Add Drug Prices
$(".add_drug_price").submit(function(){
    var ajaxData = new FormData();
    $.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });

    $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var drug_strength_id = $.trim($(".drug_strength_id").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var tablet_set_id = $.trim($(".tablet_set_id").val());
    var price = $.trim($(".price").val());
    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(price == ''){
        error_msg = 'Please Enter Pricet';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('drug_strength_id' , drug_strength_id);
    ajaxData.append('tablet_set_id' , tablet_set_id); 
    ajaxData.append('price' , price); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_drug_price',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_drug_dose").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_prices";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_drug_price").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
   
    var tablet_set_id = $.trim($(".tablet_set_id").val());
    var drug_id = $.trim($(".drug_id").val()); 
    var drug_strength_id  = $.trim($(".drug_strength_id").val()); 
    var price = $.trim($(".price").val()); 
    var drug_price_id = $.trim($(".drug_price_id").val());
    var error_msg = '';

    if(drug_id == ''){
        error_msg = 'Please Select Drug';
    }else if(price == ''){
        error_msg = 'Please Enter Price';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('drug_id' , drug_id); 
    ajaxData.append('tablet_set_id' ,tablet_set_id); 
    ajaxData.append('price' , price);
    ajaxData.append('drug_strength_id' , drug_strength_id);
    ajaxData.append('drug_price_id' , drug_price_id); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_drug_price',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/drug_prices";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Testimonials
$(".add_testimonials").submit(function(){
    var ajaxData = new FormData();    
    var author_name = $.trim($(".author_name").val());
    var description = $.trim($(".description").val()); 
    var error_msg = '';

    if(author_name == ''){
        error_msg = 'Please Enter Author Name';
    }else if(description == ''){
        error_msg = 'Please Enter Description';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    //Single image upload
    var image = $('#image').prop('files')[0];
    ajaxData.append('image', image);
    ajaxData.append('author_name' , author_name); 
    ajaxData.append('description' , description);  

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_testimonial',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_testimonials").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/testimonials";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_testimonial").submit(function(){
    var ajaxData = new FormData();
    var testimonial_id = $.trim($(".testimonial_id").val());
    var author_name = $.trim($(".author_name").val());
    var description = $.trim($(".description").val()); 
    var old_image = $.trim($(".old_image").val()); 
    var error_msg = '';

    if(author_name == ''){
        error_msg = 'Please Enter Author Name';
    }else if(description == ''){
        error_msg = 'Please Enter Description';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('testimonial_id' , testimonial_id); 
    ajaxData.append('old_image' , old_image);

    var image = $('#image').prop('files')[0];
    ajaxData.append('image', image);
    ajaxData.append('author_name' , author_name); 
    ajaxData.append('description' , description); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_testimonial',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/testimonials";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".add_page").submit(function(){
    var ajaxData = new FormData();    
    var page_title = $.trim($(".page_title").val());
    var description = CKEDITOR.instances['description'].getData(); 
    var error_msg = '';

    if(page_title == ''){
        error_msg = 'Please Enter Page Title';
    }else if(description == ''){
        error_msg = 'Please Enter Description';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    //var image = $('#image').prop('files')[0];
    //ajaxData.append('image', image);
    ajaxData.append('page_title' , page_title); 
    ajaxData.append('description' , description);  

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_page',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_page").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/pages";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".update_page").submit(function(){
    var ajaxData = new FormData();
    var page_id = $.trim($(".page_id").val());
    var page_title = $.trim($(".page_title").val());
    var description = CKEDITOR.instances['description'].getData(); 
    var error_msg = '';

    if(page_title == ''){
        error_msg = 'Please Enter Page Title';
    }else if(description == ''){
        error_msg = 'Please Enter Description';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('id' , page_id); 
    //ajaxData.append('old_image' , old_image);

    //var image = $('#image').prop('files')[0];
    //ajaxData.append('image', image);
    ajaxData.append('page_title' , page_title); 
    ajaxData.append('description' , description); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/update_page',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/pages";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Add Question

$(".add_questionnaire").submit(function(){
    var ajaxData = new FormData();
    /*$.each($(".attachment"), function(i, obj) {
        $.each(obj.files,function(j, file){
            ajaxData.append('attachment['+i+']', file);
        })        
    });*/

    $('.ques_options').each(function(i) {
     ajaxData.append('ques_options['+i+']', $(this).val());
    });

    var category_id = $.trim($(".category_id").val());
    var question_name = $.trim($(".question_name").val()); 
    var question_description = $.trim($(".question_description").val());
    var error_msg = '';

    if(category_id == ''){
        error_msg = 'Please Select Categpry';
    }else if(question_name == ''){
        error_msg = 'Please Enter Question';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('category_id' , category_id); 
    ajaxData.append('question_name' , question_name);
    ajaxData.append('question_description' , question_description); 

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'admin/save_questionnaire',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".add_questionnaire").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"admin/questionnaire";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(document).ready(function() {
let drug_img_cnt = 0;
let question_cnt = 0;
$(".add-drug-images").on('click', function() {     
    drug_img_cnt++;
    let item_projectItem_content = `<div class="item-data item-dyna row" id="item-c-`+drug_img_cnt+`">
                                 <div class="form-group col-md-7 col-sm-2 col-xs-8">
                                        <label>Image</label>
                                        <input class="form-control attachment" id="attachment-`+drug_img_cnt+`" type="file" name="attachment[]">
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeDrugItem('`+drug_img_cnt+`')"></i>
                                </div>
                            </div>`;
    $(".drug_img_list").append(item_projectItem_content);
});

$(".add_questionnaire_opts").on('click', function() {     
    question_cnt++;
    let question_options_content = `<div class="item-data item-dyna row" id="item-c-`+question_cnt+`">
                                 <div class="form-group col-md-12 col-sm-2 col-xs-8">
                                        <label>Option `+question_cnt+`</label>
                                        <input class="form-control ques_options" id="ques_options-`+question_cnt+`" type="text" name="ques_options[]" required>
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeQuestionItem('`+question_cnt+`')"></i>
                                </div>
                            </div>`;
    $(".questionnaire_div").append(question_options_content);
});
});

function removeDrugItem(itemNum) {
console.log("itemNum", itemNum);
$("#item-c-"+itemNum).css("display", "none");
}

function removeQuestionItem(itemNum) {
console.log("itemNum", itemNum);
$("#item-c-"+itemNum).css("display", "none");
}


//switch scripts
$(".questionStatusChange").change(function () {
    var end = this.value;
    var question_status = $(this).prop("checked"); 
    var question_id = $(this).attr('data-question_id');
    if(question_status){
        question_status = "1";
    }else{
        question_status = "0";
    }
    var qData = {
        question_status : question_status,
        question_id:question_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/questionStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".drugStatusChange").change(function () {
    var end = this.value;
    var drug_status = $(this).prop("checked"); 
    var drug_id = $(this).attr('data-drug_id');
    if(drug_status){
        drug_status = "1";
    }else{
        drug_status = "0";
    }
    var qData = {
        drug_status : drug_status,
        drug_id:drug_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/drugStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".drugTypeStatusChange").change(function () {
    var end = this.value;
    var strength_status = $(this).prop("checked"); 
    var drug_type_id = $(this).attr('data-drug_type_id');
    if(strength_status){
        strength_status = "1";
    }else{
        strength_status = "0";
    }
    var qData = {
        strength_status : strength_status,
        drug_type_id:drug_type_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/drugTypeStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".drugDoseStatusChange").change(function () {
    var end = this.value;
    var tablet_set_status = $(this).prop("checked"); 
    var dose_id = $(this).attr('data-dose_id');
    if(tablet_set_status){
        tablet_set_status = "1";
    }else{
        tablet_set_status = "0";
    }
    var qData = {
        tablet_set_status : tablet_set_status,
        dose_id :dose_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/drugDoseStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".userStatusChange").change(function () {
    var end = this.value;
    var user_status = $(this).prop("checked"); 
    var user_id = $(this).attr('data-user_id');
    if(user_status){
        user_status = "1";
    }else{
        user_status = "0";
    }
    var qData = {
        user_status : user_status,
        user_id :user_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/userStatusChange',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".testimonial_status_update").change(function () {
    var end = this.value;
    var status = $(this).prop("checked"); 
    var testimonial_id = $(this).attr('data-testimonial_id');
    if(status){
        status = "1";
    }else{
        status = "0";
    }
    var qData = {
        status : status,
        testimonial_id:testimonial_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/testimonial_status_update',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".page_status_update").change(function () {
    var end = this.value;
    var status = $(this).prop("checked"); 
    var page_id = $(this).attr('data-id');
    if(status){
        status = "1";
    }else{
        status = "0";
    }
    var qData = {
        status : status,
        id:page_id
    }
    $.ajax({
    type: 'POST',
    url: base_url+'admin/page_status_update',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

//Ajax onchange calls start

$(".drug_id").on('change', function() {
            console.log("changed")
            let drug_id = $(this).val();
            let url = base_url+"admin/get_ajax_strength_data";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                  console.log(sData.strength_data);
                    //$(".aa").val(sData.vv);
                     $(".drug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
                    strengthData = sData.strength_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".drug_strength_id").append(new Option(strengthData[i].strength, strengthData[i].drug_strength_id));
                    }

                } else {
                    $(".drug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        }); 

        $(".drug_strength_id").on('change', function() { 
            console.log("changed")
            let drug_id = $(".drug_id").val();
            let drug_strength_id = $(this).val();
            let url = base_url+"admin/get_ajax_tablets_data";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id,
                drug_strength_id:drug_strength_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                    $(".tablet_set_id").empty().append('<option selected="selected" value="">Select Tablets</option>');
                  console.log(sData.tablets_data); 
                    strengthData = sData.tablets_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".tablet_set_id").append(new Option(strengthData[i].tablet_count, strengthData[i].tablet_set_id));
                    }

                } else {
                   $(".tablet_set_id").empty().append('<option selected="selected" value="">Select Tablets</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        });

//ajax onchange calls end

//Phone number validation
function phoneFormatter() {
  $('.mobile').on('input', function() {
    var number = $(this).val().replace(/[^\d]/g, '')
    if (number.length == 7) {
      number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
    } else if (number.length == 10) {
      number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }
    $(this).val(number)
  });
};

$(phoneFormatter);
//phone validation end


//Ckeditor Code start
$(".ceditor").each(function () {
            let id = $(this).attr('id');
      
 CKEDITOR.replace(id, {
      // Define the toolbar groups as it is a more accessible solution.
      toolbarGroups: [{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'forms', groups: [ 'forms' ] },
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'insert', groups: [ 'insert' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'styles', groups: [ 'styles' ] }
      ],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Source,Save,NewPage,ExportPdf,Preview,Print,Templates,PasteText,PasteFromWord,Cut,Find,Replace,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Superscript,Subscript,CreateDiv,Link,Unlink,Anchor,Image,Flash,HorizontalRule,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles,Format,CopyFormatting,RemoveFormat,Strike'
    });
   });
//end