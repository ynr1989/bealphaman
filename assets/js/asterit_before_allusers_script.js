var base_url = $('#baseUrl').val(); //"http://asterit.melu.me/"
function isNumberKey(evt)
{      
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}

function alphaOnly(event) {
var key = event.keyCode;
return ((key >= 65 && key <= 90) || key == 8 || key == 9 || key == 32);
};


function checkAlert() {
  var r = confirm("Are you sure you want logout?");
  if (r == true) {    
  } else {
   return false;
  }  
}


$(".userStatusChange").change(function () {
    var end = this.value;
    var userStatus = $(this).prop("checked"); 
    var userUniqueId = $(this).attr('data-userUniqueId');
    if(userStatus){
        userStatus = "1";
    }else{
        userStatus = "0";
    }
    var qData = {
        userStatus : userStatus,
        userUniqueId:userUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'userStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".clientStatusChange").change(function () {
    var end = this.value;
    var clientStatus = $(this).prop("checked"); 
    var clientUniqueId = $(this).attr('data-clientUniqueId');
    if(clientStatus){
        clientStatus = "1";
    }else{
        clientStatus = "0";
    }
    var qData = {
        clientStatus : clientStatus,
        clientUniqueId:clientUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'clientStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".vendorStatusChange").change(function () {
    var end = this.value;
    var vendorStatus = $(this).prop("checked"); 
    var vendorUniqueId = $(this).attr('data-vendorUniqueId');
    if(vendorStatus){
        vendorStatus = "1";
    }else{
        vendorStatus = "0";
    }
    var qData = {
        vendorStatus : vendorStatus,
        vendorUniqueId:vendorUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'vendorStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".businessStatusChange").change(function () {
    var end = this.value;
    var businessStatus = $(this).prop("checked"); 
    var businessUniqueId = $(this).attr('data-businessUniqueId');
    if(businessStatus){
        businessStatus = "1";
    }else{
        businessStatus = "0";
    }
    var qData = {
        businessStatus : businessStatus,
        businessUniqueId:businessUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'businessStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

function testAjax(){

    $.ajax({
        url: "https://astet/insertTestAjax",
        type: "POST",
        data: {
            login_latitude:'123'
        },
        cache: false,
        success: function(dataResult){
           /* var dataResult = JSON.parse(dataResult);
            if(dataResult.statusCode==200){
                $("#butsave").removeAttr("disabled");
                $('#fupForm').find('input:text').val('');
                $("#success").show();
                $('#success').html('Data added successfully !'); 

            }
            else if(dataResult.statusCode==201){
               //alert("Error occured !");
            }*/
            console.log(dataResult);
        }
    });
}

$(".createBusiness").submit(function(){
    var businessName = $.trim($(".businessName").val());
    var ownerFirstName = $.trim($(".ownerFirstName").val());
    var ownerLastName = $.trim($(".ownerLastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var panNumber = $.trim($(".panNumber").val());
    var businessStartDate = $.trim($(".businessStartDate").val());
    var businessEndDate = $.trim($(".businessEndDate").val());
    var businessStatus = $.trim($(".businessStatus").val());
    var bankName = $.trim($(".bankName").val());
    var bankAccountNumber = $.trim($(".bankAccountNumber").val());
    var routingNumber = $.trim($(".routingNumber").val());
    var error_msg = '';

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    }else if(ownerFirstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(ownerLastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    }else if(businessStartDate == ''){
        error_msg = 'Please Enter Start Date';
    }else if(businessEndDate == ''){
        error_msg = 'Please Enter End Date';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        businessName : businessName,
        ownerFirstName:ownerFirstName,
        ownerLastName:ownerLastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        businessUrl:businessUrl,
        businessStartDate:businessStartDate,
        businessEndDate:businessEndDate,
        panNumber:panNumber,
        businessStatus:businessStatus,
        bankName:bankName,
        bankAccountNumber:bankAccountNumber,
        routingNumber:routingNumber
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveBusiness',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createBusiness").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editBusiness").submit(function(){
    var businessUniqueId = $.trim($(".businessUniqueId").val());
    var businessName = $.trim($(".businessName").val());
    var ownerFirstName = $.trim($(".ownerFirstName").val());
    var ownerLastName = $.trim($(".ownerLastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var panNumber = $.trim($(".panNumber").val());
    var businessStartDate = $.trim($(".businessStartDate").val());
    var businessEndDate = $.trim($(".businessEndDate").val());
    var businessStatus = $.trim($(".businessStatus").val());
     var bankName = $.trim($(".bankName").val());
    var bankAccountNumber = $.trim($(".bankAccountNumber").val());
    var routingNumber = $.trim($(".routingNumber").val());
    var error_msg = '';

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    }else if(ownerFirstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(ownerLastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    }else if(businessStartDate == ''){
        error_msg = 'Please Enter Start Date';
    }else if(businessEndDate == ''){
        error_msg = 'Please Enter End Date';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        businessUniqueId:businessUniqueId,
        businessName : businessName,
        ownerFirstName:ownerFirstName,
        ownerLastName:ownerLastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        businessUrl:businessUrl,
        businessStartDate:businessStartDate,
        businessEndDate:businessEndDate,
        panNumber:panNumber,
        businessStatus:businessStatus,
        bankName:bankName,
        bankAccountNumber:bankAccountNumber,
        routingNumber:routingNumber
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateBusiness',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createBusiness").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".createEmployee").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveEmployee',
    data: new FormData(this), //qData,
    dataType : "text",
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editEmployee").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber,
        userUniqueId:userUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateEmployee',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//HR
$(".createHr").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveHr',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createHr").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editHr").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber,
        userUniqueId:userUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateHr',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Accountant

$(".createAccountant").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var userStatus = $.trim($(".userStatus").val());
    var documentType0 = $.trim($("#documentType-0").val());
    var attachment0 = $.trim($("#attachment-0").val());

    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveAccountant',
    data: new FormData(this), //qData,
    dataType : "text",
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,   
        success: function(response) {        
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createAccountant").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editAccountant").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber,
        userUniqueId:userUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateAccountant',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Visa

$(".createVisa").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveVisa',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createVisa").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editVisa").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber,
        userUniqueId:userUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateVisa',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Bench
$(".createBench").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveBench',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createBench").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editBench").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        userStatus:userStatus,
        passportNumber:passportNumber,
        userUniqueId:userUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateBench',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Vendor
$(".createVendor").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var vendorStatus = $.trim($(".vendorStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        vendorStatus:vendorStatus
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveVendor',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createVendor").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editVendor").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var vendorUniqueId = $.trim($(".vendorUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var vendorStatus = $.trim($(".vendorStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        vendorStatus:vendorStatus,
        vendorUniqueId:vendorUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateVendor',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Client
$(".createClient").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var clientStatus = $.trim($(".clientStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        clientStatus:clientStatus
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveClient',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createClient").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editClient").submit(function(){
    var firstName = $.trim($(".firstName").val());
    var clientUniqueId = $.trim($(".clientUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var panNumber = $.trim($(".panNumber").val());
    var clientStatus = $.trim($(".clientStatus").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
     $(".error_message").show();
     $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        firstName : firstName,
        lastName:lastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        panNumber:panNumber,
        clientStatus:clientStatus,
        clientUniqueId:clientUniqueId
    }
    
    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateClient',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


/*Add more buttons*/

     $(document).ready(function() {
      let itemCount = 0;
     $(".add-item-content").on('click', function() {     
            itemCount++;

            var products = '<select name="documentType[]" id="documentType-'+itemCount+'" required class="form-control">';
                $("#documentType-0 option").each(function()
                {
                    products += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
                });
                products += "</select>"; 

            let item_data_content = `<div class="item-data item-dyna row" id="item-c-`+itemCount+`">
                                        <div class="form-group col-md-6 col-sm-2 col-xs-8">
                                                <label>Document Type</label>
                                                `+products+`
                                        </div>
                                        
                                        <div class="form-group col-md-6 col-sm-2 col-xs-8">
                                                <label>Attachment</label>
                                                <input class="form-control attachment" id="attachment-`+itemCount+`" type="file" name="attachment[]" required>
                                        </div>
                                        
                                        <div class="col-md-4" >
                                            <i class="fa fa-minus" onclick="removeItem('`+itemCount+`')"></i>
                                        </div>
                                    </div>`;
            $(".items-list").append(item_data_content);
        })
   });

     function removeItem(itemNum) {
        console.log("itemNum", itemNum);
        $("#item-c-"+itemNum).css("display", "none");
    }

$('.userDocumentsData').click(function(){  
   var userUniqueId = $(this).attr("id");  
   $.ajax({  
        url: base_url+"getUserDocuments",  
        method:"post",  
        data:{userUniqueId:userUniqueId},  
        success:function(data){  
             $('#documents_details').html(data);  
             $('#dataModal').modal("show");  
        }  
   });  
});  

$('.businessDocumentsData').click(function(){  
   var userUniqueId = $(this).attr("id");  
   $.ajax({  
        url: base_url+"getUserDocuments",  
        method:"post",  
        data:{userUniqueId:userUniqueId},  
        success:function(data){  
             $('#documents_details').html(data);  
             $('#dataModal').modal("show");  
        }  
   });  
});  


//Row Expand Datatable
function format(value) {
      var extradata = value.split('#');
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Gender:</td>'+
            '<td>'+extradata[0]+'</td>'+
             '<td>Business URL:</td>'+
            '<td>'+extradata[5]+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Address:</td>'+
            '<td>'+extradata[1]+'</td>'+
             '<td>Start Date:</td>'+
            '<td>'+extradata[6]+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Bank name:</td>'+
            '<td>'+extradata[2]+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Account Number:</td>'+
            '<td>'+extradata[3]+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Routing Number:</td>'+
            '<td>'+extradata[4]+'</td>'+
        '</tr>'+
    '</table>';

      //return '<div>Hidden Value: ' + value + '</div>';
  }
 
      var table = $('#businessPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ]
      });

      // Add event listener for opening and closing details
      $('#businessPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(format(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });
