<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Frontendcontroller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['profileSubmit'] = "AdminDashboardcontroller/profileSubmit";

//Frontend
$route['login'] = 'Frontendcontroller/login';
$route['index'] = 'Frontendcontroller/index';
$route['aboutus'] = 'Frontendcontroller/aboutus';
$route['erectile-dysfunction'] = 'Frontendcontroller/erectileDysfunction';
$route['pricing'] = 'Frontendcontroller/pricing';
$route['how-it-works'] = 'Frontendcontroller/howitworks';
$route['hair'] = 'Frontendcontroller/hair';
$route['contact-us'] = 'Frontendcontroller/contactus';
$route['get-started'] = 'Frontendcontroller/getstarted';
$route['faqs'] = 'Frontendcontroller/faqs';
$route['terms'] = 'Frontendcontroller/terms';
$route['consent-to-telehealth'] = 'Frontendcontroller/telehealth';
$route['privacy'] = 'Frontendcontroller/privacy';
$route['patient_signup_action'] = 'Frontendcontroller/save_user';
$route['patientLoginAction'] = 'Frontendcontroller/patientLoginAction';
$route['logout'] = 'Frontendcontroller/logout';
$route['quiz'] = 'Frontendcontroller/quiz';
$route['dashboard'] = 'Patientcontroller/dashboard';
$route['hair_loss_questionnaire'] = 'Frontendcontroller/hair_loss_questionnaire';
$route['ed_questionnaire'] = 'Frontendcontroller/ed_questionnaire';  
 
$route['get_ajax_strength_data'] ='Frontendcontroller/get_ajax_strength_data';
$route['get_ajax_tablets_data'] ='Frontendcontroller/get_ajax_tablets_data';

$route['cart'] = 'Patientcontroller/cart'; 
$route['checkout'] = 'Patientcontroller/checkout'; 
$route['confirmCheckout'] = 'Patientcontroller/confirmCheckout'; 
$route['payment_success'] = 'Patientcontroller/payment_success';
$route['order_success'] = 'Patientcontroller/order_success';
$route['my_orders'] = 'Patientcontroller/my_orders';
$route['payment_fail'] = 'Patientcontroller/payment_fail';
$route['save_cart'] = 'Patientcontroller/save_cart'; 
$route['patient_update_profile'] = 'Patientcontroller/patient_update_profile';
$route['questionnaire_complete'] = 'Patientcontroller/questionnaire_complete';
$route['my_questionnaire'] = 'Patientcontroller/my_questionnaire';
$route['my_addresses'] = 'Patientcontroller/my_addresses';
$route['add_address'] = 'Patientcontroller/add_address';
$route['edit_address'] = 'Patientcontroller/edit_address';
$route['save_address'] = 'Patientcontroller/save_address';
$route['delete_address'] = 'Patientcontroller/delete_address';
$route['update_personal_info'] = 'Patientcontroller/update_personal_info';
$route['get_appointment_slots'] = 'Patientcontroller/get_appointment_slots';

$route['get_treatment_info_by_cat'] = 'Patientcontroller/get_treatment_info_by_cat';
//Admin
$route['admin'] = 'Admincontroller/login';
$route['admin/loginAction'] = 'Admincontroller/loginAction';

//Admin Dashboard
$route['admin/dashboard'] = 'AdminDashboardcontroller/dashboard';
$route['admin/change-password'] = 'AdminDashboardcontroller/changePassword';
$route['admin/updateProfile'] = 'AdminDashboardcontroller/updateProfile';
$route['admin/logout'] = 'AdminDashboardcontroller/logout';
$route['submitChangePassword'] = "AdminDashboardcontroller/submitChangePassword";


$route['admin/add_drug'] ='AdminDashboardcontroller/add_drug';
$route['admin/save_drug'] ='AdminDashboardcontroller/save_drug';
$route['admin/edit_drug'] ='AdminDashboardcontroller/edit_drug';
$route['admin/update_drug'] ='AdminDashboardcontroller/update_drug';
$route['admin/delete_drug_image'] ='AdminDashboardcontroller/delete_drug_image';
$route['admin/delete_drug'] ='AdminDashboardcontroller/delete_drug';
$route['admin/drug_list'] ='AdminDashboardcontroller/drug_list';
$route['admin/drugStatusUpdate'] ='AdminDashboardcontroller/drugStatusUpdate';

$route['admin/add_drug_type'] ='AdminDashboardcontroller/add_drug_type';
$route['admin/save_drug_type'] ='AdminDashboardcontroller/save_drug_type';
$route['admin/edit_drug_type'] ='AdminDashboardcontroller/edit_drug_type';
$route['admin/update_drug_type'] ='AdminDashboardcontroller/update_drug_type';
$route['admin/delete_drug_type'] ='AdminDashboardcontroller/delete_drug_type';
$route['admin/drug_types'] ='AdminDashboardcontroller/drug_types';
$route['admin/drugTypeStatusUpdate'] ='AdminDashboardcontroller/drugTypeStatusUpdate';

$route['admin/add_drug_dose'] ='AdminDashboardcontroller/add_drug_dose';
$route['admin/save_drug_dose'] ='AdminDashboardcontroller/save_drug_dose';
$route['admin/edit_drug_dose'] ='AdminDashboardcontroller/edit_drug_dose';
$route['admin/update_drug_dose'] ='AdminDashboardcontroller/update_drug_dose';
$route['admin/delete_drug_dose'] ='AdminDashboardcontroller/delete_drug_dose';
$route['admin/drug_doses'] ='AdminDashboardcontroller/drug_doses';
$route['admin/drugDoseStatusUpdate'] ='AdminDashboardcontroller/drugDoseStatusUpdate';

$route['admin/questionnaire'] ='AdminDashboardcontroller/questionnaire';
$route['admin/add_questionnaire'] ='AdminDashboardcontroller/add_questionnaire';
$route['admin/save_questionnaire'] ='AdminDashboardcontroller/save_questionnaire';
$route['admin/questionStatusUpdate'] ='AdminDashboardcontroller/questionStatusUpdate';
$route['admin/delete_questionnaire'] ='AdminDashboardcontroller/delete_questionnaire';

$route['admin/add_drug_price'] ='AdminDashboardcontroller/add_drug_price';
$route['admin/save_drug_price'] ='AdminDashboardcontroller/save_drug_price';
$route['admin/edit_drug_price'] ='AdminDashboardcontroller/edit_drug_price';
$route['admin/update_drug_price'] ='AdminDashboardcontroller/update_drug_price';
$route['admin/delete_drug_price'] ='AdminDashboardcontroller/delete_drug_price';
$route['admin/drug_prices'] ='AdminDashboardcontroller/drug_prices';
$route['admin/drugPriceStatusUpdate'] ='AdminDashboardcontroller/drugPriceStatusUpdate';

$route['admin/add_doctor'] ='AdminDashboardcontroller/add_doctor'; 
$route['admin/edit_doctor'] ='AdminDashboardcontroller/edit_doctor'; 
$route['admin/doctors'] ='AdminDashboardcontroller/doctors'; 

$route['admin/add_pharmacy'] ='AdminDashboardcontroller/add_pharmacy'; 
$route['admin/edit_pharmacy'] ='AdminDashboardcontroller/edit_pharmacy'; 
$route['admin/pharmacies'] ='AdminDashboardcontroller/pharmacies'; 

$route['admin/add_patient'] ='AdminDashboardcontroller/add_patient'; 
$route['admin/edit_patient'] ='AdminDashboardcontroller/edit_patient'; 
$route['admin/patients'] ='AdminDashboardcontroller/patients'; 
$route['admin/patient_update_profile'] = 'AdminDashboardcontroller/patient_update_profile';

$route['admin/save_user'] ='AdminDashboardcontroller/save_user';
$route['admin/update_user'] ='AdminDashboardcontroller/update_user';
$route['admin/userStatusChange'] ='AdminDashboardcontroller/userStatusChange';
$route['admin/delete_user'] ='AdminDashboardcontroller/delete_user';

$route['admin/add_testimonial'] ='AdminDashboardcontroller/add_testimonial';
$route['admin/save_testimonial'] ='AdminDashboardcontroller/save_testimonial';
$route['admin/edit_testimonial'] ='AdminDashboardcontroller/edit_testimonial';
$route['admin/update_testimonial'] ='AdminDashboardcontroller/update_testimonial';
$route['admin/delete_testimonial'] ='AdminDashboardcontroller/delete_testimonial';
$route['admin/testimonials'] ='AdminDashboardcontroller/testimonials';
$route['admin/testimonial_status_update'] ='AdminDashboardcontroller/testimonial_status_update';

$route['admin/add_page'] ='AdminDashboardcontroller/add_page';
$route['admin/save_page'] ='AdminDashboardcontroller/save_page';
$route['admin/edit_page'] ='AdminDashboardcontroller/edit_page';
$route['admin/update_page'] ='AdminDashboardcontroller/update_page';
$route['admin/delete_page'] ='AdminDashboardcontroller/delete_page';
$route['admin/pages'] ='AdminDashboardcontroller/pages';
$route['admin/page_status_update'] ='AdminDashboardcontroller/page_status_update';

$route['admin/settings'] = "AdminDashboardcontroller/settings";
$route['admin/submitSettings'] = "AdminDashboardcontroller/submitSettings";
$route['admin/patient_questionnaire'] = 'AdminDashboardcontroller/patient_questionnaire';
$route['admin/patient_treatment'] = 'AdminDashboardcontroller/patient_treatment';


//Ajax Calls

$route['admin/get_ajax_strength_data'] ='AdminDashboardcontroller/get_ajax_strength_data';
$route['admin/get_ajax_tablets_data'] ='AdminDashboardcontroller/get_ajax_tablets_data';
$route['admin/questionnaire_import'] ='AdminDashboardcontroller/questionnaire_import';

$route['admin/coupons'] = 'AdminDashboardcontroller/coupons';
$route['admin/AddCouponAction'] = 'AdminDashboardcontroller/AddCouponAction';
$route['admin/addCoupon'] = 'AdminDashboardcontroller/addCoupon';
$route['admin/editCoupon/(:any)'] = 'AdminDashboardcontroller/editCoupon';
$route['admin/changeCouponStatus'] = 'AdminDashboardcontroller/changeCouponStatus';
$route['admin/deleteCoupon'] = 'AdminDashboardcontroller/deleteCoupon';


$route['doctor/deleteschedule'] = 'Doctorcontroller/deleteschedule';
$route['doctor/schedule'] = 'Doctorcontroller/schedule';

$route['get_slots'] = 'Doctorcontroller/get_slots';

$route['forgotPassword'] = 'Logincontroller/forgotPassword';
$route['submitForgotPassword'] = 'Logincontroller/submitForgotPassword';

/*$route['admin/schedule/add_schedule_lookup'] = 'Schedule/add_schedule_lookup';
$route['admin/schedule'] = 'Schedule/index';
$route['admin/schedule/edit_schedule_lookup'] = 'Schedule/edit_schedule_lookup';*/

//Web
/*
$route['business-signup'] = 'Homecontroller/businessSignup';
$route['saveNewBusiness'] = 'Homecontroller/saveNewBusiness';

$route['loginAction'] = 'Homecontroller/loginAction';
$route['dashboard'] = 'Dashboardcontroller/dashboard';


$route['updateProfile'] = "Dashboardcontroller/updateProfile";
$route['updateBusinessprofile'] = "Dashboardcontroller/updateBusinessProfile";
$route['updateBusiness'] = 'Dashboardcontroller/updateBusiness';
$route['profileSubmit'] = "Dashboardcontroller/profileSubmit";
$route['submitBusinessProfile'] = "Dashboardcontroller/submitBusinessProfile";

$route['userStatusUpdate'] ='Dashboardcontroller/userStatusUpdate';
$route['clientStatusUpdate'] ='Dashboardcontroller/clientStatusUpdate';
$route['vendorStatusUpdate'] ='Dashboardcontroller/vendorStatusUpdate';
$route['businessStatusUpdate'] ='Dashboardcontroller/businessStatusUpdate';
$route['projectStatusUpdate'] ='Dashboardcontroller/projectStatusUpdate';

$route['deleteDocument'] ='Dashboardcontroller/deleteDocument';

$route['createBusiness'] = 'Dashboardcontroller/createBusiness';
$route['saveBusiness'] = 'Dashboardcontroller/saveBusiness';
$route['businessList'] = 'Dashboardcontroller/businessList';
$route['editBusiness'] = 'Dashboardcontroller/editBusiness';
$route['updateBusiness'] = 'Dashboardcontroller/updateBusiness';
$route['deleteBusiness'] ='Dashboardcontroller/deleteBusiness';

$route['createEmployee'] ='Dashboardcontroller/createEmployee';
$route['saveEmployee'] = 'Dashboardcontroller/saveEmployee';
$route['updateEmployee'] ='Dashboardcontroller/updateEmployee';
$route['deleteEmployee'] ='Dashboardcontroller/deleteEmployee';
$route['editEmployee'] ='Dashboardcontroller/editEmployee';
$route['employeeList'] = 'Dashboardcontroller/employeeList';

$route['createHr'] ='Dashboardcontroller/createHr';
$route['saveHr'] = 'Dashboardcontroller/saveHr'; 
$route['updateHr'] ='Dashboardcontroller/updateHr';
$route['deleteHr'] ='Dashboardcontroller/deleteHr';
$route['editHr'] ='Dashboardcontroller/editHr';
$route['hrList'] = 'Dashboardcontroller/hrList';

$route['createAccountant'] ='Dashboardcontroller/createAccountant';
$route['saveAccountant'] = 'Dashboardcontroller/saveAccountant';
$route['updateAccountant'] ='Dashboardcontroller/updateAccountant';
$route['deleteAccountant'] ='Dashboardcontroller/deleteAccountant';
$route['editAccountant'] ='Dashboardcontroller/editAccountant';
$route['accountantList'] = 'Dashboardcontroller/accountantList';

$route['createVisa'] ='Dashboardcontroller/createVisa';
$route['saveVisa'] = 'Dashboardcontroller/saveVisa';
$route['updateVisa'] ='Dashboardcontroller/updateVisa';
$route['deleteVisa'] ='Dashboardcontroller/deleteVisa';
$route['editVisa'] ='Dashboardcontroller/editVisa';
$route['visaList'] = 'Dashboardcontroller/visaList';

$route['createBench'] ='Dashboardcontroller/createBench';
$route['saveBench'] = 'Dashboardcontroller/saveBench';
$route['updateBench'] ='Dashboardcontroller/updateBench';
$route['deleteBench'] ='Dashboardcontroller/deleteBench';
$route['editBench'] ='Dashboardcontroller/editBench';
$route['benchList'] = 'Dashboardcontroller/benchList';

$route['createVendor'] ='Dashboardcontroller/createVendor';
$route['saveVendor'] = 'Dashboardcontroller/saveVendor';
$route['updateVendor'] ='Dashboardcontroller/updateVendor';
$route['deleteVendor'] ='Dashboardcontroller/deleteVendor';
$route['editVendor'] ='Dashboardcontroller/editVendor';
$route['vendorList'] = 'Dashboardcontroller/vendorList';

$route['createVendor'] ='Dashboardcontroller/createVendor';
$route['saveVendor'] = 'Dashboardcontroller/saveVendor';
$route['updateVendor'] ='Dashboardcontroller/updateVendor';
$route['deleteVendor'] ='Dashboardcontroller/deleteVendor';
$route['editVendor'] ='Dashboardcontroller/editVendor';
$route['vendorList'] = 'Dashboardcontroller/vendorList';

$route['createClient'] ='Dashboardcontroller/createClient';
$route['saveClient'] = 'Dashboardcontroller/saveClient';
$route['updateClient'] ='Dashboardcontroller/updateClient';
$route['deleteClient'] ='Dashboardcontroller/deleteClient';
$route['editClient'] ='Dashboardcontroller/editClient';
$route['clientList'] = 'Dashboardcontroller/clientList';

$route['projectList'] ='Dashboardcontroller/projectList';
$route['createProject'] ='Dashboardcontroller/createProject';
$route['saveProject'] ='Dashboardcontroller/saveProject';
$route['deleteProject'] ='Dashboardcontroller/deleteProject';
$route['editProject'] ='Dashboardcontroller/editProject';
$route['updateProject'] ='Dashboardcontroller/updateProject';
$route['deleteProjectDocument'] ='Dashboardcontroller/deleteProjectDocument';

$route['getUserDocuments'] = 'Dashboardcontroller/getUserDocuments';
$route['documentPreview'] = 'Dashboardcontroller/documentPreview';


$route['passportDetails'] = 'Dashboardcontroller/passportDetails';
$route['personalDetails'] = 'Dashboardcontroller/personalDetails';
$route['educationDetails'] = 'Dashboardcontroller/educationDetails';
$route['employmentDetails'] = 'Dashboardcontroller/employmentDetails';
$route['visaDetails'] = 'Dashboardcontroller/visaDetails';
$route['myProjects'] = 'Dashboardcontroller/myProjects';

$route['addPersonalDetails'] = 'Dashboardcontroller/addPersonalDetails';
$route['addEducationDetails'] = 'Dashboardcontroller/addEducationDetails';
$route['addEmploymentDetails'] = 'Dashboardcontroller/addEmploymentDetails';
$route['addVisaDetails'] = 'Dashboardcontroller/addVisaDetails';
$route['addPassportDetails'] = 'Dashboardcontroller/addPassportDetails';

$route['educationDetailsSubmit'] = 'Dashboardcontroller/educationDetailsSubmit';
$route['employmentDetailsSubmit'] = 'Dashboardcontroller/employmentDetailsSubmit';
$route['personalDetailsSubmit'] = 'Dashboardcontroller/personalDetailsSubmit';
$route['visaDetailsSubmit'] = 'Dashboardcontroller/visaDetailsSubmit';
$route['passportDetailsSubmit'] = 'Dashboardcontroller/passportDetailsSubmit';


$route['deleteChildren'] = 'Dashboardcontroller/deleteChildren';
$route['deleteFamily'] = 'Dashboardcontroller/deleteFamily';
$route['deleteEducation'] = 'Dashboardcontroller/deleteEducation';
$route['deleteEmployment'] = 'Dashboardcontroller/deleteEmployment';
$route['deletePassport'] = 'Dashboardcontroller/deletePassport';
$route['deleteUserVisa'] = 'Dashboardcontroller/deleteUserVisa';

$route['addTimesheet'] = 'Dashboardcontroller/addTimesheet';
$route['editTimesheet'] = 'Dashboardcontroller/editTimesheet';
$route['checkTimesheet'] = 'Dashboardcontroller/checkTimesheet';
$route['saveTimesheet'] = 'Dashboardcontroller/saveTimesheet';
$route['viewTimesheet'] = 'Dashboardcontroller/viewTimesheet';

$route['addWeeklyReview'] = 'Dashboardcontroller/addWeeklyReview';
$route['saveWeeklyReview'] = 'Dashboardcontroller/saveWeeklyReview';
$route['viewWeeklyReviews'] = 'Dashboardcontroller/viewWeeklyReviews';
$route['approvalTimesheet'] = 'Dashboardcontroller/approvalTimesheet';

$route['approveComment'] = "Dashboardcontroller/approveComment";
$route['RejectComment'] = "Dashboardcontroller/RejectComment";

$route['approveReview'] = "Dashboardcontroller/approveReview";
$route['RejectReview'] = "Dashboardcontroller/RejectReview";

$route['timesheets'] = "Dashboardcontroller/timesheets";

$route['tickets'] = "Dashboardcontroller/tickets";
$route['createTicket'] = "Dashboardcontroller/createTicket";
$route['saveTicket'] = "Dashboardcontroller/saveTicket";

$route['approvalTimesshetsInfo'] = "Dashboardcontroller/approvalTimesshetsInfo";*/