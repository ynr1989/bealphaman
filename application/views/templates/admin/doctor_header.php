          <li class="sidebar-dropdown">
            <a href="#">
              <img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Patients</span> 
            </a>
            <div class="sidebar-submenu">
              <ul>                 
                <li><a href="<?php echo base_url('admin/patients'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Patients </a></li>            
              </ul>
            </div>
          </li>  
           <li>
            <a href="<?php echo base_url('doctor/schedule'); ?>">
              <img src="<?php echo base_url(); ?>assets/img/icons/dashboard.png" width="32" height="32">
              <span>Schedule</span>
            </a>
          </li> 
           <li class="sidebar-dropdown">
            <a href="#">
               <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
              <span>My Account</span>
              <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a> 
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('admin/change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password </a></li>
                <li><a href="<?php echo base_url('admin/updateProfile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a> </li>               
              </ul>
            </div>
          </li>