<li class="sidebar-dropdown">
            <a href="#">
              <img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Drug Management</span> 
            </a>
            <div class="sidebar-submenu">
              <ul>                
                <li><a href="<?php echo base_url('admin/drug_list'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/BusinessList.png" width="32" height="32">Drug List</a> </li>   
                <li><a href="<?php echo base_url('admin/drug_types'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Strength </a></li>
                <li><a href="<?php echo base_url('admin/drug_doses'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Tablets </a></li>   
                <li><a href="<?php echo base_url('admin/drug_prices'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Drug Prices </a></li>            
              </ul>
            </div>
          </li> 

          <li class="sidebar-dropdown">
            <a href="#">
              <img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Users</span> 
            </a>
            <div class="sidebar-submenu">
              <ul>                
                <li><a href="<?php echo base_url('admin/doctors'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/BusinessList.png" width="32" height="32">Doctors</a> </li>   
                <li><a href="<?php echo base_url('admin/pharmacies'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Pharmacies </a></li>
                <li><a href="<?php echo base_url('admin/patients'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Patients </a></li>            
              </ul>
            </div>
          </li> 

          
          <li>
            <a href="<?php echo base_url('admin/questionnaire'); ?>">
              <img src="<?php echo base_url(); ?>assets/img/icons/dashboard.png" width="32" height="32">
              <span>Questionnaire</span>
            </a>
          </li> 
          <li>
            <a href="<?php echo base_url('admin/testimonials'); ?>">
              <img src="<?php echo base_url(); ?>assets/img/icons/dashboard.png" width="32" height="32">
              <span>Testimonials</span>
            </a>
          </li>

           <li>
            <a href="<?php echo base_url('admin/pages'); ?>">
              <img src="<?php echo base_url(); ?>assets/img/icons/dashboard.png" width="32" height="32">
              <span>Pages</span>
            </a>
          </li>  

           <li class="sidebar-dropdown">
            <a href="#">
               <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
              <span>Upload Sheets</span> 
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('admin/questionnaire_import'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32">Import Quest </a></li>
                </ul>
            </div>
          </li>   
           
           <li class="sidebar-dropdown">
            <a href="#">
               <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
              <span>My Account</span>
              <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('admin/change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password </a></li>
                <li><a href="<?php echo base_url('admin/updateProfile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a> </li>               
              </ul>
            </div>
          </li>

           <li class="sidebar-dropdown">
            <a href="#">
               <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
              <span>Settings</span>
              <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('admin/settings'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Settings</a></li>
                            
              </ul>
            </div>
          </li>