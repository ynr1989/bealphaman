<div class="col-xl-3 col-lg-4 col-md-12 order-lg-first">
  <div class="category-list style-three mb-30">
    <h3 class="title">Dashboard</h3>

    <ul>
      <li><a href="<?php echo base_url('patient_update_profile'); ?>">  My Profile</a></li>
      <li><a href="<?php echo base_url('my_questionnaire'); ?>"> My Questionnaire</a></li>
      <li><a href="<?php echo base_url('my_addresses'); ?>"> My Address</a></li>
      <li><a href="<?php echo base_url('my_orders'); ?>"> My Orders</a></li>
      <li><a href="<?php echo base_url('logout'); ?>"> Logout</a></li> 
      <!-- <li><a href="#"><i class="fas fa-long-arrow-alt-right"></i> Respiratory</a></li>
       -->
    </ul>
  </div>
 <!--  <div class="sidebar-item">              
      <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/images/price11.jpg" alt="img"></a>
  </div>  -->
</div>