<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Be Alpha Man</title>
<link href="assets/img/favicon.png" rel="icon">
  
    <!-- stylesheet -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;531;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/style.css">
     <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css' />
<style type="text/css">
 /*  .header-right-menu{
    margin-left: 0px !important;
  }*/
  .slidersize{
    padding: 40px 0 !important;
  }
  .header-section {
    padding: 15px 0 15px 0;
}
.logotitle{
  color:#212529;
} 
.header-section {
    padding: 15px 0 15px 0;
    background: linear-gradient( 
180deg
 , #324A67, #151F2B) !important;
}
.logotitle {
    color: #ffffff !important;
}
div#navbarSupportedContent li a {
    color: #fff !important;
}
span.user-account, a#navbarDropdown {
    color: #fff !important;
    font-weight: 800 !important;
}
i.fas.fa-caret-down {
    color: #fff !important;
}
</style>
</head>
<body> 
	<?php ?>
    <!-- header-top -->
    <input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>">
    <div class="header-top style-two">
      <div class="container-fluid">
          <div class="row">
              <div class="col-lg-6">
                  <p class="info-content"><!-- Get $15 off your first order of ED treatment.&nbsp;&nbsp;&nbsp;<span style="font-weight:900;border-bottom: 1px solid #1574f6;">Start now</span> --></p>
              </div>
              <div class="col-lg-6">
                  <div class="header-top-info">
                      <div class="info-email">
                          <i class="far fa-envelope"></i>
                          <span>admin@bealphaman.com</span>
                      </div>
                      <div class="social-follow">
                          <span>Follow Us :</span>
                          <ul>
                              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                              <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                          </ul>
                      </div>
                  </div>
              
              </div>
          </div>
      </div>
    </div>
    <!-- header-top end -->

    <!-- header -->
    <div class="header-section">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-8 col-lg-12">
                    <nav class="navbar navbar-expand-lg main-menu" >
                       <!-- <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>-->
					           <a style="text-decoration: none;" class="logotitle" href="<?php echo base_url(); ?>"><h3>Be Alpha Man</h3></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <i class="fas fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                          <ul class="navbar-nav mr-xl-auto ml-lg-auto">
                            <li class="nav-item dropdown">
                                <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url(); ?>">Home</a>
                            </li>
                             <!-- <a class="nav-link " href="index.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Home
                              </a> -->
                             <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="home2.html">home 02</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="home3.html">home 03</a>
                              </div>-->
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('aboutus'); ?>">About Us</a>
                            </li>
							
                            <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('how-it-works'); ?>">How It Works</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('erectile-dysfunction'); ?>">Erectile Dysfunction</a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                              <a class="nav-link toggle" href="sex.php" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sex</a>
                              <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                                <a class="dropdown-item" href="sex.php">Sex</a></div> -->
                                <!--<div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="faq.html">faq</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="shop.html">shop</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="shop-left-sidebar.html">shop left sidebar</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="shop-right-sidebar.html">shop right sidebar</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="single-product.html">product details</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="blog.html">blog</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="blog-single.html">blog details</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="contact.html">contact</a>
                              </div>
                            </li>-->
							 <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('hair'); ?>">Hair</a>
                            </li>
							  <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('pricing'); ?>">Pricing</a>
                            </li>
							 <!-- <li class="nav-item">
                             
                            </li> -->
                            <!--<li class="nav-item">
                              <a class="nav-link" href="blog.html">Contact Us</a>
                            </li>-->
                          </ul>
                          
                        </div>
                    </nav>
                </div>
                <div class="col-xl-4 d-xl-block">
                  <div class="header-right-area">
                    <div class="header-search-area">
                      <!-- <form class="form-inline">
                        <input class="form-control header-input" type="search" placeholder="Enter Search Keyword" aria-label="Search">
                        <button class="btn btn-outline-success header-btn" type="submit">Search</button>
                      </form> -->
                      
                    </div> 
                    <div class="header-right-menu" style="margin-left: 20px;">
                      <ul>
                         <li>
                          <?php
                          $quiz1 = $this->mainModel->getQuestionsByCat("1");
                          ?>
                               <a class="nav-link btn btn-Shop" href="<?php echo base_url('ed_questionnaire'); ?>?q=quiz&step=2&eid=1&n=1&t=<?php echo count($quiz1) ;?>"><span class="getstarted-span">Get Started</span></a>
                             </li>
                       
                       <?php if($this->session->userdata('user_id')!="" && $this->session->userdata('user_type')==4): ?>
                        <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url('logout'); ?>"><span class="user-account">Logout</span></a>
                        </li>
                        <?php else: ?>
                          <li class="nav-item">
                              
                              <a class="nav-link" href="<?php echo base_url('login'); ?>"><span class="user-account">Login</span></a>
                        </li>
                        <?php endif; ?>
                        <!-- <li>
                          <a href="#"><i class="far fa-user-circle"></i></a>
                        </li> --> 
                        <?php if($this->session->userdata('user_id')!="" && $this->session->userdata('user_type')==4): ?>
                        <li class="nav-item">
                            <a class="nav-link " href="index.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More <i class="fas fa-caret-down"></i>
                              </a>  
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url('dashboard'); ?>">Dashboard</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url('my_orders'); ?>">My Orders</a>
								 
                              </div>
                          </li>
                        <?php endif; ?>
                        <li class="sales-cart">
                          <?php 
                          $count = 0;
                          $cart_amount = "0.00";
                          //if($this->session->userdata('user_id')!="" || $_COOKIE['patient_id']!=""){
                          if($this->session->userdata('user_id')!=""){
                            $user_id = $this->session->userdata('user_id')?$this->session->userdata('user_id'):$_COOKIE['patient_id'];
                            $ques_info = $this->mainModel->getUserQuestionnaireInfo($user_id);
                            $count = count( $ques_info);
                            if($count>0){
                              //$cart_amount = $cart_data[0]['drug_price'];
                            }
                            
                          }
                          
                          ?>
                          <a href="<?php echo base_url('cart'); ?>"><i class="fas fa-shopping-cart"></i> <span class="sold-item"><?php echo $count; ?></span></a>
                          <!-- <span class="sales-price">$<?php echo number_format($cart_amount); ?></span> -->
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header end -->
    </body>
</html>