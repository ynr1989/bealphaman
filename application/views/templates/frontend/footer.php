 <!-- footer -->
 <style>
 .social-icon img {
    margin-left: -11px;
    margin-top: -11px;
}
 </style>
    <div class="footer-section"> 
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-6 col-md-6">
            <div class="widget widget-info">
              <h3 class="title">About Us</h3>
              <p class="content">Our team at Be Alpha Man is working hard around the clock in order to improve patient's health outcomes.</p>
              <div class="social-icon">
                <a href="https://www.facebook.com/bealphaman" target="_blank"><img src="assets/img/fb.png" alt="images"/></a>
                <a href="#"><img src="assets/img/inst.png" alt="images"/></a>
                <a href="#"><img src="assets/img/gp.png" alt="images"/></a>
                <a href="#"><img src="assets/img/twitter.png" alt="images"/></a>
                <a href="#"><img src="assets/img/pt.png" alt="images"/></a>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-md-6">
            <div class="widget widget-info">
              <h3 class="title">Useful Links</h3>
              <div class="footer-list">
                <ul> 
                  <li><a href="<?php echo base_url('faqs'); ?>">FAQ’S</a></li>
                  <li><a href="<?php echo base_url('terms'); ?>">Terms & Conditions</a></li>
                  <li><a href="<?php echo base_url('privacy'); ?>">Privacy Policy</a></li>  
                  <li><a href="<?php echo base_url('consent-to-telehealth'); ?>">Consent To Telehealth</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-md-6">
            <div class="widget widget-info">
              <h3 class="title">Our Services</h3>
              <div class="footer-list">
                <ul>
                  <li><a href="<?php echo base_url('hair'); ?>">Hair Loss</a></li> 
                  <li><a href="<?php echo base_url('erectile-dysfunction'); ?>">Erectile Dysfunction</a></li> 
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-md-6">
            <div class="widget widget-info">
              <h3 class="title">Get in touch</h3>
              <div class="contact">
                <p class="content">Contact Us</p>
                <span style="font-weight: normal;">&nbsp;<i class="fas fa-mobile-alt"></i> &nbsp;Tel:(833)733-7847</span>
                <span style="font-weight: normal;"><i class="fas fa-envelope"></i> admin@bealphaman.com</span>
              </div>
              <!-- <div class="Address">
                <p class="content"><i class="fas fa-address-book"></i> Our Address</p>
                <span style="font-weight: normal;">6419Park Ave, West New York,</span>
                <span style="font-weight: normal;">NJ07093</span>
              </div> -->
              
            </div>
          </div>
          <div class="col-md-12">
            <div class="footer-bottom">
              <div class="row">
                <div class="col-lg-6 col-md-12 order-lg-first">
                  <div class="footer-copyright">
                    <p>&copy;2021 CopyRight <a href="#"> Be Alpha Man</a>. All rights reserved</p>
                  </div>
                </div>
                <div class="col-lg-6 col-md-12 order-first">
                  <div class="footer-nav">
                   <!--  <ul>
                      <li><a href="<?php echo base_url(); ?>">Home</a></li>
                      <li><a href="<?php echo base_url('aboutus'); ?>">About</a></li> 
                      <li><a href="<?php echo base_url('contact-us'); ?>">Contact Us</a></li>
                    </ul> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- footer end -->

   <!--  <script src="<?php echo base_url(); ?>assets/frontend/js/jquery-3.5.1.slim.min.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/main.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/frontend.js"></script>


    <script type="text/javascript"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script><script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"  type="text/javascript"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"  type="text/javascript"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"  type="text/javascript"></script> 

     
<script type="text/javascript">
jQuery(function($){
  $( "#fdate" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
  $( "#ldate" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
   $( "#dob" ).datepicker({dateFormat: 'yy-mm-dd', 
    changeYear: true,
    changeMonth: true,
   yearRange: '1950:2011'
  });
  $( "#dob-1" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
  $( ".dateFld" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});

  $( ".datefield" ).datepicker({
    format: 'yyyy-mm-dd',
    startDate:new Date()
  });

});


$(document).ready(function() {
    $('.datatableBam').DataTable();
} );
</script>