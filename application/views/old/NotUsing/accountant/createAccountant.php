<div id="page-wrapper">
<div class="page-head-line">Create Accountant</div>

            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                                              <!-- <h1 class="page-subhead-line">
                        <strong><?php if($this->session->flashdata('message')!=''):
                            echo $this->session->flashdata('message')."<hr>";
                            endif; ?></strong></h1> -->

                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-9 col-sm-6">
               <div class="panel">
                        
                        
                <form method="post" action="#" class="createAccountant" enctype="multipart/form-data">
                  
                    <div class="row">
                         <!-- <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" required name="businessId" placeholder="BusinessId" class="form-control" readonly="readonly"
                           value="<?php echo $this->mainModel->generateBusinessID(); ?>">
                        </div>
                      </div>  -->
                      
                      </div>                      
                  

                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">First name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="firstName" placeholder="First Name" maxlength="25" class="form-control firstName">
                        </div>
                      </div> 
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Last name <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="lastName" placeholder="Last Name" maxlength="25" class="form-control lastName">
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Email Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Mobile Number <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="11" onkeypress="return isNumberKey(event)" class="form-control mobile" >
                        </div>
                      </div>                      
                    </div>                    
                      <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                          </select>
                        </div>
                      </div>  
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" name="address" placeholder="Address" class="form-control address">
                        </div>
                      </div>                    
                    </div>
                      <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Passport Number </label>
                        <div class="form-group">
                          <input type="text" name="passportNumber" maxlength="12" placeholder="Passport Number" class="form-control passportNumber">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="bmd-label-floating">PAN Number </label>
                        <div class="form-group">
                          <input type="text" name="panNumber" maxlength="12" placeholder="PAN Number" class="form-control panNumber">
                        </div>
                      </div>                        
                    </div>                      
                     
                     <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Status </label>
                        <div class="form-group">
                           <select name="userStatus" required class="form-control userStatus">
                             <!--  <option value="">Select Status</option> -->
                              <option value="0">In Active</option>
                              <option value="1">Active</option>
                          </select>
                        </div>
                      </div>             
                    </div>

                     <div class="row">
                       <div class="col-md-12">
                                <div class="add-item-data" style="margin-left:18px;">
                                    <i class="fa fa-plus add-item-content"></i>
                                </div>
                                <div class="items-list">
                                    <div class="item-data row">                                        
                                        <div class="form-group col-md-4 col-sm-2 col-xs-8">
                                                <label>Document Type</label>
                                               <?php $docs = $this->mainModel->getTypes('DOCUMENT_TYPE'); ?>
                                                 <select class="form-control" name="documentType[]" id="documentType-0" required>
                                                  <option value="">Select Type</option>
                                                  <?php foreach($docs as $docInfo) {?>
                                                    <option value="<?php echo $docInfo['code'];?>"><?php echo $docInfo['value'];?></option>
                                                  <?php } ?>
                                                </select>
                                        </div>                                        
                                        <div class="form-group col-md-4 col-sm-2 col-xs-8">
                                                <label>Attachment</label>
                                                <input class="form-control attachment" id="attachment-0" name="attachment[]" type="file" required>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                          </div>

                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('accountantList'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
      
    