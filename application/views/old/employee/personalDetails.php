<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <?php $this->load->view('./templates/personalLinks',$data); ?>
                      <?php if($userTypeCode == 4): ?>
                      <a href="<?php echo base_url("addPersonalDetails"); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    <?php endif; ?>
                    </div></div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    

                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="userPrintRowExpand" >
                      <thead class=" text-primary">
                      <!--  <th>#</th>                        
                         <th>Employee ID</th> -->
                        <th>Family Type</th>
                        <th>Name</th>
			                  <th>Gender</th>	                        
                        <th>Email</th>
                        <th>Mobile</th>		
                        <th>DOB</th>                  
                        <!-- <th>Actions</th> -->
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($familyDetails as $data): ?>
                        <tr data-child-value="<?php echo $data['startDate']; ?> # <?php echo $data['endDate']; ?> # <?php echo $data['gender']; ?> # <?php echo $data['dob']; ?> # <?php echo $data['address']; ?>">
                          <!--  <td class="details-control"></td> -->
                          <!--  <td><?php echo $data['employeeId']; ?></td> -->
                            <td><?php echo $this->mainModel->getTypeName($data['familyType'],'FAMILY_TYPE');  ?></td>                          
                          <td><?php echo $data['firstName']." ".$data['lastName']; ?></td>
                          <td><?php echo $data['gender']; ?></td>
                          <td><?php echo $data['email']; ?></td>
                          <td><?php echo $data['mobile']; ?></td> 
                          <td><?php echo $data['dob']; ?></td> 
                            <!-- <td>
                              <?php $docInfo = $this->mainModel->getUserDocuments($data['userUniqueId']);
                              foreach($docInfo as $docInfos){  
                              $document = base_url()."assets/documents/".$docInfos["attachment"]; ?>
                              <a href="#" title="<?php echo $docInfos["documentType"]; ?>"><span class="getDocumentInPopup" id="<?php echo $document; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                            <?php } ?>
                            </td> -->
                          <!--  <td>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-userUniqueId="<?php echo $data['userUniqueId']; ?>" class="userStatusChange" <?php if($data['userStatus'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url($editUrl); ?>?userUniqueId=<?php echo $data['userUniqueId']; ?>&token=<?php echo $data['token']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url($deleteUrl); ?>?userUniqueId=<?php echo $data['userUniqueId']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td> --> </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>