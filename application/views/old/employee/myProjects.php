<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <?php $this->load->view('./templates/personalLinks',$data); ?>
                    </div></div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    <div class="panel">
                      <?php if($this->session->flashdata('message')!=''): ?>
                      <div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="example" >
                      <thead class=" text-primary">
                        <th>Project ID</th>
                        <th>Project Name</th>
                        <th>Start Date</th>                         
                        <th>End Date</th>
                        <th>Employee Name</th>              
                        <th>Client Name</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($projectData as $data):
                         $userInfo =  $this->mainModel->getUserInfo($data['userUniqueId']);
                         $clientInfo =  $this->mainModel->getClientInfo($data['clientUniqueId']);
                         $vendorInfo =  $this->mainModel->getVendorInfo($data['vendorUniqueId']); ?>
                        <tr data-child-value="<?php echo $vendorInfo[0]['contactName']; ?> # <?php echo $vendorInfo[0]['mobile']; ?> # <?php echo $vendorInfo[0]['email']; ?> # <?php echo $clientInfo[0]['contactName']; ?> # <?php echo $clientInfo[0]['mobile']; ?> # <?php echo $clientInfo[0]['email']; ?> # <?php echo $data['billRate']; ?> # <?php echo $data['netDays']; ?> # <?php echo $data['description']; ?>">
                           
                          <td><?php echo $data['projectId']; ?></td>
                          <td><?php echo $data['projectName']; ?></td>
                          <td><?php echo $data['startDate']; ?></td>
                          <td><?php echo $data['endDate']; ?></td>
                          <td><?php echo $userInfo[0]['firstName']." ".$userInfo[0]['lastName']; ?></td> 
                          <td><?php echo $clientInfo[0]['contactName']; ?>
                          </td>
                           </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>