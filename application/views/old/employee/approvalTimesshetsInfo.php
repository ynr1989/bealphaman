<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  </div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
                     <?php if($userTypeCode ==5 OR $userTypeCode == 2 OR $userTypeCode == 3){ ?>                  
            <form>

            <div class="row">
                  
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="year" id="year" title="Category">
                       <!--  <option value="">Select Year</option> -->
                        <option value="2020">2020</option>
                  </select>
                </div>
              </div>
             
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="month" id="month" title="Category">
                        <option value="">Select Month</option>
                      <?php  
                     $monthArray = array(
                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                );
                      foreach ($monthArray as $monthNumber=>$month) { ?>
                       <option value="<?php echo $monthNumber; ?>" <?php if($monthNumber == $drdata['id']): echo "selected"; endif; ?>><?php echo $month; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>


              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
				   <a href="<?php echo base_url('timesheets'); ?>" class="btn btn-primary pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form>
            <br><br>
          <?php } ?>

                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="userPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Employee ID</th>
                        <th>Employee Name</th>                    
                        <th>Not Submitted Weeks</th>
                        <th>Pending Approvals</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($employeeData as $data):
                          if($_GET['year']==""){
                            $year = date("Y");
                          }else{
                             $year = $_GET['year'];
                          }

                          if($_GET['month']!=""){
                            $month = $_GET['month'];
                          }else{
                             $month = '';
                          }

                          $standardHours = $this->mainModel->getTotalHours($data['userUniqueId'],$year,$month);
                          $extraHours = $this->mainModel->getTotalExtraHours($data['userUniqueId'],$year,$month);
                           ?>
                        <tr data-child-value="<?php echo $data['startDate']; ?> # <?php echo $data['endDate']; ?> # <?php echo $data['gender']; ?> # <?php echo $data['dob']; ?> # <?php echo $data['address']; ?>">
                           <td class="details-control"><?php //echo $i; ?></td>
                          <td><?php echo $data['employeeId']; ?></td>
                          <td><?php echo $data['firstName']." ".$data['lastName']; ?></td>
                          <?php 

                          $sun_days = array();
                          $sun_days_time = array();
                          $lm_date = date("Y-m-",strtotime("-1 month"));
                          $str1 = $lm_date;
                          for($i2=15; $i2<31; $i2++)
                          {
                              $ddd = $str1.$i2;
                              $date = date('Y M D d', $time = strtotime($ddd) );
                            if(strpos($date, 'Sun') )
                            {
                              $date1 = date('Y M D d', $time = strtotime($ddd) );
                              $sdate = date('Y-m-d', $time = strtotime($ddd) ); 
                              array_push($sun_days, $sdate);
                              array_push($sun_days_time, $ddd);
                            }
                          }

                          $sun_days = array_slice($sun_days, -2);
                          $sun_days_time = array_slice($sun_days_time, -2);

                          $date = date("Y-m-");
                          $str = $date;
                          for($i2=1; $i2<31; $i2++)
                          {
                              $ddd = $str.$i2;
                              $date = date('Y M D d', $time = strtotime($ddd) );
                            if(strpos($date, 'Sun') )
                            {
                              $date1 = date('Y M D d', $time = strtotime($ddd) );
                              $sdate = date('Y-m-d', $time = strtotime($ddd) );
                              array_push($sun_days, $sdate);
                              array_push($sun_days_time, $ddd);
                            }
                          }
                          echo "<td>";
                          for($su_index = 0; $su_index < count($sun_days); $su_index++){
                              $hereDate = date('Y-m-d', $time=strtotime($sun_days_time[$su_index]));
                              $sHoursByDates = $this->mainModel->getStandardHoursByDates($data['userUniqueId'],$hereDate, date('Y-m-d', strtotime($hereDate. " + 7 days")) );
                              if($sHoursByDates<=0):
                              echo date('d, M', $time=strtotime($sun_days_time[$su_index]))."<br>";
                              endif;                    
                          }
                          echo "</td>";
                          echo "<td>";
                          for($su_index = 0; $su_index < count($sun_days); $su_index++){
                              $hereDate = date('Y-m-d', $time=strtotime($sun_days_time[$su_index]));                             
                              $notAprovedDates = $this->mainModel->getStandardHoursNotApprovedByDates($data['userUniqueId'],$hereDate);
                             
                              if($notAprovedDates== 1):
                              echo "<a href='".base_url('viewTimesheet?userUniqueId='.$data['userUniqueId'].'')."''>".date('d, M', $time=strtotime($sun_days_time[$su_index]))."</a><br>";
                              endif;      
                                                     
                          } 
                          echo "</td>";
                           ?>
                          
                         </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>