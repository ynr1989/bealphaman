

<div class="page-head-line">Update <?php echo $labelName; ?></div>

            <div id="page-inner">
                <div class="row">
                   


            <div  class="col-md-8 col-sm-12">

                       <?php if($this->session->flashdata('message')!=''): ?>
                        <div class="alert alert-success">
                           <?php echo $this->session->flashdata('message'); ?>
                           </div>
                            <?php  endif; if($this->session->flashdata('message1')!=''): ?>
                               <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('message1'); ?>
                            </div><?php
                            endif; ?>
                            
                    <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
                    <form method="post" action="#" class="editEmployee" enctype="multipart/form-data">
                  <input type="hidden" name="userUniqueId" class="userUniqueId"  value="<?php echo $userInfo[0]['userUniqueId']; ?>">
                   <input type="hidden" name="createUserType" class="createUserType"  value="<?php echo $userInfo[0]['userType']; ?>">
                   
					<div class="row">
                         <div class="col-md-4">
                          <label class="bmd-label-floating">Business ID <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" readonly placeholder="First Name" maxlength="25" class="form-control" value="<?php echo $userInfo[0]['businessId']; ?>">
                        </div>
                      </div> 
                      
                      
                      <div class="col-md-4">
                        <label class="bmd-label-floating">Employee ID <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" readonly maxlength="25" class="form-control" value="<?php echo $userInfo[0]['employeeId']; ?>">
                        </div>
                      </div>  
                      
                      
                      <div class="col-md-4">
                          <label class="bmd-label-floating">First name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="firstName" placeholder="First Name" maxlength="25" class="form-control firstName" value="<?php echo $userInfo[0]['firstName']; ?>">
                        </div>
                      </div> 
                      
                      
                      <div class="col-md-4">
                        <label class="bmd-label-floating">Last name <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="lastName" placeholder="Last Name" maxlength="25" class="form-control lastName" value="<?php echo $userInfo[0]['lastName']; ?>">
                        </div>
                      </div>           
                      
                      <div class="col-md-4">
                          <label class="bmd-label-floating">Email <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email" value="<?php echo $userInfo[0]['email']; ?>">
                        </div>
                      </div>
                      
                       <div class="col-md-4">
                        <label class="bmd-label-floating">Mobile <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" value="<?php echo $userInfo[0]['mobile']; ?>">
                        </div>
                      </div>     
                      
                       <div class="col-md-4">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php if($userInfo[0]['gender'] == 'Male'): echo 'selected'; endif; ?>>Male</option>
                              <option value="Female" <?php if($userInfo[0]['gender'] == 'Female'): echo 'selected'; endif; ?>>Female</option>
                              <option value="Other" <?php if($userInfo[0]['Other'] == 'Male'): echo 'selected'; endif; ?>>Other</option>
                          </select>
                        </div>
                      </div> 
                       
                       <div class="col-md-4">
                        <label class="bmd-label-floating">Address </label>
                        <div class="form-group">
                          <input type="text" name="address" placeholder="Address" class="form-control address" value="<?php echo $userInfo[0]['address']; ?>">
                        </div>
                      </div>   
                      
                      <div class="col-md-4">
                          <label class="bmd-label-floating">Passport Number </label>
                        <div class="form-group">
                          <input type="text" name="passportNumber" placeholder="Passport Number" class="form-control passportNumber" value="<?php echo $userInfo[0]['passportNumber']; ?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <label class="bmd-label-floating">SSN Number </label>
                        <div class="form-group">
                          <input type="text" name="ssnNumber" placeholder="SSN Number" maxlength="9" class="form-control ssnNumber" value="<?php echo $userInfo[0]['ssnNumber']; ?>">
                        </div>
                      </div>   

                       <div class="col-md-4">
                           <label class="bmd-label-floating">Start Date </label>
                        <div class="form-group">
                          <input type="text" required name="startDate" autocomplete="off" id="fdate" placeholder="Start Date" class="form-control startDate" value="<?php echo $userInfo[0]['startDate']; ?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                         <label class="bmd-label-floating">End Date </label>
                        <div class="form-group">
                          <input type="text" name="endDate" autocomplete="off" id="ldate" placeholder="End Date" class="form-control endDate" value="<?php echo $userInfo[0]['endDate']; ?>">
                        </div>
                      </div>
                      
                      <div class="col-md-4">
                         <label class="bmd-label-floating">Date Of Birth</label>
                        <div class="form-group">
                          <input type="text" required name="dob" value="<?php echo $userInfo[0]['dob']; ?>" autocomplete="off" id="dob" placeholder="Date Of Birth" class="form-control dob">
                        </div>
                      </div>        
                      
                      <div class="col-md-4">
                          <label class="bmd-label-floating">Status </label>
                        <div class="form-group">
                           <select name="userStatus" required class="form-control userStatus">
                              <option value="0" <?php if($userInfo[0]['userStatus'] == 0): echo 'selected'; endif; ?>>In Active</option>
                              <option value="1" <?php if($userInfo[0]['userStatus'] == 1): echo 'selected'; endif; ?>>Active</option>
                          </select>
                        </div>
                      </div>   

                    </div>
                    
                    </div>
                
                <div class="col-md-4 col-sm-12">
            
              <table class="table table-striped table-bordered table-hover">
                <tr><th>Document Type</th><th>#</th></tr>
              
                <?php $docs = $this->mainModel->getUserDocuments($userInfo[0]['userUniqueId']);
                foreach($docs as $docInfo){ ?>
                 <tr><td><?php echo $docInfo['documentType']; ?></td><td> <a href="<?php echo DOCUMENT_URL; ?><?php echo $docInfo['attachment']; ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>&nbsp; &nbsp; <a href="<?php echo base_url('deleteDocument/'); ?>?userDocumentId=<?php echo $docInfo['userDocumentId']; ?>&documentKey=<?php echo $docInfo['documentKey']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td> </tr>
                <?php } ?>
                </table>
                <div class="add-item-data" style="margin:40px 0 0 20px; float:left">
                                    <i class="fa fa-plus add-item-content"></i>
                                </div>
                                <div class="items-list">
                                    <div class="item-data row">                                        
                                        <div class="form-group col-md-5 col-sm-12 col-xs-8">
                                                <label>Doc Type</label>
                                               <?php $docs = $this->mainModel->getTypes('DOCUMENT_TYPE'); ?>
                                                 <select class="form-control documentType" name="documentType[]" id="documentType-0">
                                                  <option value="">Select Type</option>
                                                  <?php foreach($docs as $docInfo) {?>
                                                    <option value="<?php echo $docInfo['code'];?>"><?php echo $docInfo['value'];?></option>
                                                  <?php } ?>
                                                </select>
                                        </div>                                        
                                        <div class="form-group col-md-7 col-sm-2 col-xs-8">
                                                <label>Attachment</label>
                                                <input class="form-control attachment" id="attachment" name="attachment[]" type="file">
                                        </div>                                        
                                    </div>
                                </div>

            </div>

            </div> <div class="row">
            <div class="col-md-12 col-sm-12 mb-5">
                       
                        <div>
                <form method="post" action="#" class="editEmployee" enctype="multipart/form-data">
                  <input type="hidden" class="userUniqueId"  value="<?php echo $userInfo[0]['userUniqueId']; ?>">
                   
                    
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url($cancelUrl); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                 <br/>
                 <br/>
                                  <br/>

              </div>
            </div>
            </div>
                
                 
            
                
                
                
                
                
                    
                <!-- /. ROW  -->
              


               </form>
            
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      