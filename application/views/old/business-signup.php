<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>Welcome to Aster IT</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/signup.css" rel="stylesheet" />
<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
 @media (min-width: 768px) {
 .bd-placeholder-img-lg {
 font-size: 3.5rem; 
}
}
</style>

</head>
<body>
<div class="container-fluid">

  <div class="row">
  
    <div class="col-md-8 col-sm-12" style="background-color:#adcce9" ><img align="right" class="img-fluid" src="<?php echo base_url(); ?>assets/img/login-bg.jpg" alt=""></div>
                

    <div align="center" class="col-md-4 col-sm-12 loginformbg" style="background-color:#e7f7fc; height:100vh;padding-top: 0px !important" >
    
        <img class="mb-4 mt-3 img-fluid" src="<?php echo base_url(); ?>assets/img/Logo.png">
       <!--   <img class="mb-4 img-fluid" src="<?php echo base_url(); ?>assets/img/loginimg.jpg"> -->

         <div  class="login_popup_spinner spinnerNewBg" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_popup alert innerAlert alert-danger" style="display:none;"></div>
            <div class="success_popup alert innerAlert alert-success" style="display:none;"></div>

        <h3 class="h3 mb-3 font-weight-normal">Create Business</h3>

            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <!-- <h1 class="page-subhead-line">
                        <strong><?php if($this->session->flashdata('message')!=''):
                            echo $this->session->flashdata('message')."<hr>";
                            endif; ?></strong></h1> -->

                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_popup_signup alert alert-danger" style="display:none;"></div>
            <div class="success_popup_signup alert alert-success" style="display:none;"></div>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                                <div class="panel-body">
                <form method="post" action="#" class="signupBusiness" enctype="multipart/form-data">
                  
                    <div class="row">
                       
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" required name="businessName" placeholder="Business Name" maxlength="40" ondrop="return false;"
        onpaste="return false;" class="form-control businessName">
                        </div>
                      </div>                      

 
                       <div class="col-md-12">
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email">
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                        
                       <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" >
                        </div>
                      </div>                      
                    <div class="col-md-12">
                        <div class="form-group">
                          <input type="password" required name="password" placeholder="Password" class="form-control password">
                        </div>
                      </div> 

                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="password" required name="password" placeholder="Confirm Password" class="form-control cpassword">
                        </div>
                      </div>

                       <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" required name="address" placeholder="Address" class="form-control address">
                        </div>
                      </div> 

                         <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" required name="businessUrl" placeholder="BusinessUrl" class="form-control businessUrl">
                        </div>
                      </div> 

                       <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" required name="ssnNumber" placeholder="SSN Number" maxlength="9" class="form-control ssnNumber">
                        </div>
                      </div> 

                       <div class="col-md-12 otpField">
                        <div class="form-group">
                            <input type="text" maxlength="6" placeholder="OTP" class="form-control loginOtp" name="loginOtp" id="loginOtp"/>
                           <input type="hidden" id="otpValue" value="">
                        </div>
                      </div> 
                                          
                    </div>     
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                 
                    <div class="clearfix"></div>
                  </form>

                   <p class="mt-1 mb-2 text-muted"><a href="<?php echo base_url('login'); ?>">Existing User ?</a></p>
        <p class="mt-1 mb-2 text-muted"><a href="<?php echo base_url('forgotPassword'); ?>">Forgot Password ?</a></p>

                </div>
              </div>
            </div> 
              </div>
            </div>
          </div>
        </div> 
    </div>
  </div>
</div>
</div>
</body>
</html>


<script type="text/javascript">
  
  $(".otpField").hide();
  $(".signupBusiness").submit(function(){
    var businessName = $.trim($(".businessName").val()); 
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val()); 
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var ssnNumber = $.trim($(".ssnNumber").val()); 
    var password = $.trim($(".password").val()); 
    var error_msg = '';
    var cpassword = $.trim($(".cpassword").val());

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    } else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    } else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(password != cpassword)
    {
        error_msg = 'Confirm password not matched';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    } 
    if(error_msg != '')
    {
   $(".error_popup_signup").show();
    $(".error_popup_signup").html(error_msg);
    return false;
    }
    $(".error_popup_signup").hide();
    var qData = {
        businessName : businessName,
        email:email,
        mobile:mobile, 
        address:address,
        businessUrl:businessUrl,
        ssnNumber:ssnNumber
    }

    var form = $('form')[0];  
    var formData = new FormData(form);


     $(".signup_popup_spinner").show();
    $.ajax({
    
    url: "<?php echo base_url(); ?>"+'saveNewBusiness',
    data: formData,
    type: 'POST',
    contentType: false,  
    processData: false,
    //dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".signup_popup_spinner").hide();
                    $(".error_popup_signup").hide();
                    $(".success_popup_signup").show();
                    $(".success_popup_signup").html(resultData.message);       

                    if(resultData.otpStatus == true){
                        console.log("Success Signup");
                        window.location.reload();
                    }else{
                        $(".otpField").show();
                        $("#otpValue").val("1");
                    } 
            }
            else
            {
                $(".error_popup_signup").show();
                $(".error_popup_signup").html(resultData.message);
                return false;  
            }
        }
    });
    return false;
});
</script>
