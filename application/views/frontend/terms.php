<div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Terms</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Terms</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

<div class="about-us">
      <div class="container">
        <div class="row align-items-center"> 
<h3>Terms and Conditions</h3> 
<p>Last updated April 1, 2021</p>
<p>AMONG OTHER ACTIVITIES, BE ALPHA MAN’S SERVICES ENABLE COORDINATION AND COMMUNICATION WITH A HEALTH CARE PROVIDER. IT DOES NOT REPLACE YOUR RELATIONSHIP WITH ANY PHYSICIAN. THESE SERVICES MIGHT NOT BE APPROPRIATE FOR ALL MEDICAL CONDITIONS OR CONCERNS. IF YOU HAVE A MEDICAL EMERGENCY, IMMEDIATELY CALL YOUR DOCTOR OR DIAL 911.</p>

<p>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BECAUSE THEY SET FORTH THE IMPORTANT TERMS YOU WILL NEED TO KNOW ABOUT THE SERVICES.</p>
<p>YOU UNDERSTAND THAT BY ACCESSING OR USING THE SITES OR SERVICES, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREED TO BE LEGALLY BOUND BY AND COMPLY WITH THESE TERMS OF USE. IF YOU DO NOT OR CANNOT AGREE WITH ANY PART OF THESE TERMS OF USE, YOU MAY NOT USE THE SITES OR ANY SERVICES PROVIDED ON OR THROUGH THE SITES.</p>
<p>THE TERMS OF USE ARE SUBJECT TO CHANGE AS PROVIDED HEREIN.</p>

<p>The following terms of sale (“Terms and Conditions”) are agreed to by and between Be Alpha Man Llc., a New Jersey Corporation (“Company”, “us”, “our”, or “we”) and the individual purchasing our Products and/or subscribing to our Service (“you”). By placing an order you accept and are bound by these Terms governing your purchase. If you do not agree with these Terms then do not purchase our Products or Services. We may modify these Terms from time to time as described in the Modification to Terms of Service indicated below. These Terms are an integral part of our website’s (the “Website”) Terms of Use (https://www.bealphaman.com/terms) that apply generally to the use of our Website. You should also carefully review our Privacy Policy (https://www.bealphaman.com/Privacy) before placing an order for Products or Services through this Site. </p>

<p>Our Products and Services are offered and available to you if you: (a) are 18 years of age or older; (b) have and maintain a mailing address within the United States throughout the use of our Services where we can ship our Products; and (c) agree to be bound by these Terms of Use. By purchasing our Products or Services you represent that you meet each of the above criteria.</p>

<p>PLEASE BE AWARE THAT THESE TERMS OF CONDITIONS CONTAIN AN ARBITRATION NOTICE AND CLASS ACTION WAIVER WHERE ALL DISPUTES BETWEEN YOU AND US SHALL BE RESOLVED ON AN INDIVIDUAL BASIS THROUGH BINDING ARBITRATION WITH A WAIVER OF ANY RIGHT TO PARTICIPATE IN A CLASS ACTION/COLLECTIVE LAWSUIT OR ARBITRATION.</p>

<h3>Products and Services</h3>
<p>We offer men’s health and welfare products and services in the states identified athttps://www.bealphaman.com/faq. Some of our products are only available for purchase if you have a prescription. Our services facilitate a telemedicine examination between you and a healthcare provider where the healthcare provider may issue you a prescription for certain products offered by our Pharmacy Partner. The purchase of any of our products or services is governed by these Terms.</p>

<h3>Order Acceptance and Cancellation.</h3>
<p>You agree that your order is an offer to buy, under these Terms. All orders must be accepted by us or we will not be obligated to sell our services or products to you. We may choose not to accept any orders in our sole discretion. After having received your order, we will send you a confirmation email with your order number and details of the items you have ordered. Acceptance of your order and the formation of the contract of sale between us and you will not take place unless and until you have received your order confirmation email, and the successful completion of the payment for your order. You have the option to cancel your order at any time before any products are shipped to you or before you begin using our Services.</p>

<h3>Auto-Fill</h3>
<p>If you receive a prescription that is filled by our Pharmacy Partners, then you may have the option of scheduling automatic refilling of the prescription (“Auto-Fill”) as described on our Website. You consent to us automatically billing your credit card (which is on file with our third-party payment processor) for each Auto-Fill unless your cancel the Auto-Fill by contacting us at www.BeAlphaMan.com before the product is shipped to you by our Pharmacy Partner.</p>
<p>As a courtesy, approximately 2 days prior to the Auto-Fill, we will send you an email to the email address that you provided upon your initial purchase advising you of the Auto-Fill. Our email shall provide you with a link to cancel your membership.</p>
<p>UNLESS YOU NOTIFY US BEFORE A CHARGE THAT YOU WANT TO CANCEL YOUR AUTO-FILL, YOU UNDERSTAND YOUR AUTO-FILL WILL AUTOMATICALLY CONTINUE AND YOU AUTHORIZE US (WITHOUT NOTICE TO YOU, UNLESS REQUIRED BY APPLICABLE LAW) TO COLLECT THE THEN-APPLICABLE FEES AND ANY TAXES, USING ANY ELIGIBLE PAYMENT METHOD WE HAVE ON RECORD FOR YOU.</p>

<h3>Prices and Payment Terms.</h3>
<p>All prices posted on this Site are subject to change without notice. The price charged for the products or services will be the price in effect at the time the order is placed and will be sent out in your order confirmation email. Price increases will only apply to orders placed after such changes. Posted prices do not include taxes or charges for shipping and handling. All such taxes and charges will be added to your merchandise total and will be itemized in your shopping cart and in your order confirmation email. We are not responsible for pricing, typographical, or other errors in any offer by us and we reserve the right to cancel any orders arising from such errors.</p>

<p>You agree that you are responsible for all fees due to receive healthcare services and pharmacy services, including fees charged by each of our Pharmacy Partners that are made available through our services. Your payment to us may include such fees, which we collect on their behalf. Any healthcare service or pharmacy services not made available through our services are not included in any payments collected by us, and you may be charged separately for such services.</p>
<p>We do not represent that the health care services or products made available through our services will qualify for reimbursement under any federal or state healthcare program. As a result, you agree and acknowledge that you will pay directly for any medical services and products provided to you, and that neither us nor any of our affiliates will bill any federal or state healthcare program for such services or products.</p>

<p>Terms of payment are within our sole discretion and, unless otherwise agreed by us in writing, payment must be received by us before our acceptance of an order. We accept all payments identified at check-out on our Site for all purchases. At the time you place your order for our services or products, your credit card will be charged and kept on file within our internal encrypted servers or our third-party processor. All service fees are non-refundable and fully accrued once services are provided.</p>
<p>You represent and warrant that (i) the credit card information you supply to us is true, correct, and complete, (ii) you are duly authorized to use such credit card for the purchase, (iii) charges incurred by you will be honored by your credit card company, (iv) you will pay charges incurred by you at the posted prices, including all applicable taxes, if any, and (v) you authorize us and our affiliates, or out third party payment processors to charge the amount due.</p>

<h3>Shipping</h3>
<p>We will arrange for shipment of a product that does not require a prescription. You are responsible for the payment of all shipping and handling charges associated with the shipping of such product. Title and risk of loss pass to you upon delivery of the product to the address identified in your order. Shipping and delivery dates are estimates only and cannot be guaranteed. We are not liable for any delays in shipments. Our Pharmacy Partners will ship all products that require a prescription.</p>

<h3>Disclaimer of Warranty.</h3>
<p>Our products and services are provided “as is” without warranties of any kind, either express or implied, all of which are specifically disclaims, including but not limited to, the implied warranties of merchantability and fitness for a particular purpose. We do not warrant that the products or services will meet your specific requirements. Our entire liability, and your exclusive remedy, for a problem that you may experience with the products, Website, or services shall be to contact us at admin@bealphaman.com and in our discretion we may elect to replace the products (if permitted by law), or terminate our services.</p>

<p>SOME STATES DO NOT ALLOW LIMITATIONS ON HOW LONG AN IMPLIED WARRANTY LASTS, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.</p>

<h3>Limitation of Liability</h3>

<p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL WE, OUR LICENSORS, PROVIDERS, OR OUR AFFILIATES, HAVE ANY LIABILITY TO YOU OR ANY THIRD PARTY FOR ANY DAMAGES, INCLUDING CONSEQUENTIAL, INDIRECT, LOSS OF PROFITS, LOSS OF REVENUES, AND PUNITIVE DAMAGES, ARISING FROM OR RELATED OUR PRODUCTS, SERVICES, SITE OR CONTENT.</p>
<p>THE FOREGOING LIMITATIONS WILL APPLY WHETHER SUCH DAMAGES ARISE OUT OF BREACH OF CONTRACT, TORT (INCLUDING NEGLIGENCE), PERSONAL INJURY, OR OTHERWISE AND REGARDLESS OF WHETHER SUCH DAMAGES WERE FORESEEABLE OR WE WERE ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW CERTAIN LIMITATIONS OF LIABILITY SO SOME OR ALL OF THE ABOVE LIMITATIONS OF LIABILITY MAY NOT APPLY TO YOU. OUR MAXIMUM LIABILITY FOR ANY CLAIMS ARISING FROM YOUR PURCHASE, USE, OR INABILITY TO USE OUR PRODUCTS, SERVICES, OR SITE, SHALL NOT EXCEED $100.</p>
<h3>Products Not for Resale or Export.</h3>

<p>You represent and warrant that you are buying Products or Services from the Site for your own household use only and not for resale or export. Products and Services purchased from the Site may be controlled for export purposes by export regulations, including but not limited to, the Export Control Reform Act of 2018 (ECRA) (Title XVII, Subtitle B of Pub. L. No. 115-232), the Export Administration Regulations (15 C.F.R. 768-799) for which ECRA is permanent statutory authority, the International Traffic in Arms Regulations (22 C.F.R. 120-128 and 130) and their successor and supplemental regulations (collectively, "Export Regulations").</p>

<h3>Privacy.</h3>

<p>We respect your privacy and are committed to protecting it. Our Privacy Policy, https://www.bealphaman.com/Privacy, governs the processing of all personal data collected from you in connection with your purchase of products or services through the Site.</p>

<h3>Force Majeure.</h3>
<p>We will not be liable or responsible to you, nor be deemed to have defaulted or breached these Terms, for any failure or delay in our performance under these Terms when and to the extent such failure or delay is caused by or results from acts or circumstances beyond our reasonable control, including, without limitation, acts of God, flood, fire, earthquake, explosion, governmental actions, war, invasion or hostilities (whether war is declared or not), terrorist threats or acts, riot or other civil unrest, national emergency, revolution, insurrection, epidemic, lockouts, strikes or other labor disputes (whether or not relating to our workforce), or restraints or delays affecting carriers or inability or delay in obtaining supplies of adequate or suitable materials, materials or telecommunication breakdown or power outage.</p>
<h3>Governing Law and Jurisdiction.</h3>
<p>All matters arising out of or relating to these Terms are governed by and construed in accordance with the internal laws of the State of New Jersey without giving effect to any choice or conflict of law provision or rule that would cause the application of the laws of any jurisdiction other than those of the State of New Jersey.</p>

<h3>Dispute Resolution and Binding Arbitration.</h3>

<p>Summary. Most customer’s concerns can be resolved quickly and simply by reaching out to us at 1-833-733-7847. In the event that we are unable to resolve your complaint, or we are unable to resolve our dispute with you, then we each agree to resolve all disputes through binding arbitration or small claims court instead of through the traditional court system. Arbitration is more informal than a traditional lawsuit brought in court, because it is before a neutral arbitrator instead of a judge or jury, allows for more limited discovery than in court, and is subject to very limited review by courts. Any arbitration under this Agreement will take place on an individual basis; class arbitrations and class actions are not permitted.</p>

<p>Arbitration. Excluding intellectual property infringement and indemnification claims, any claim, dispute or controversy (whether in contract, tort or otherwise, whether pre-existing, present or future, and including statutory, consumer protection, common law, intentional tort, injunctive and equitable claims) between you and us arising from or relating in any way to our Products or Services, including the use or purchase thereof, will be resolved exclusively and finally by binding arbitration.</p>

<p>YOU HEREBY ACKNOWLEDGE YOUR UNDERSTANDING AND AGREE THAT BY ACCEPTING THIS AGREEMENT AND THE ARBITRATION PROVISIONS HEREIN, THE FAA WILL GOVERN THE INTERPRETATION AND ENFORCEMENT OF THIS PROVISION, AND THAT YOU AND COMPANY AGREE THAT, BY ENTERING INTO THIS AGREEMENT, YOU AND COMPANY ARE EACH HEREBY IRREVOCABLY WAIVING THE RIGHT TO A TRIAL BY JURY AND THE RIGHT TO PARTICIPATE IN ANY CLASS OR REPRESENTATIVE ACTION IN CONNECTION WITH ANY DISPUTE BETWEEN US. YOU AND COMPANY ALSO AGREE THAT ANY ARBITRATION CONDUCTED HEREUNDER </p>

<p>WILL BE BROUGHT ONLY ON AN INDIVIDUAL BASIS, AND WILL NOT BE BROUGHT OR PROCEED ON BEHALF OF A CLASS OR IN A REPRESENTATIVE CAPACITY.</p>

<p>The arbitration will be administered by the American Arbitration Association ("AAA") in accordance with the Consumer Arbitration Rules (the "AAA Rules") then in effect before a single arbitrator applying the substantive law of the State of New Jersey (The AAA Rules are available at adr.org or by calling the AAA at 1-800-778-7879.) The Federal Arbitration Act will govern the interpretation and enforcement of this section.</p>

<p>Excluding the power to consider the enforceability of the class arbitration waiver and any challenge to the class arbitration, the arbitrator will have exclusive authority to resolve any dispute relating to arbitrability and/or enforceability of this arbitration provision, including any unconscionability challenge or any other challenge that the arbitration provision or the Agreement is void, voidable or otherwise invalid. The arbitrator will be empowered to grant whatever relief would be available in court under law or in equity. Any award of the arbitrator(s) will be final and binding on each of the parties and may be entered as a judgment in any court of competent jurisdiction. Any challenge to the class arbitration waiver may only be raised in a court of competent jurisdiction.</p>

<p>You may elect to pursue your claim in small-claims court rather than arbitration if you provide us with written notice of your intention do so within 60 days of your purchase. The arbitration or small-claims court proceeding will be limited solely to your individual dispute or controversy.</p>
<h3>Assignment.</h3>

<p>You will not assign any of your rights or delegate any of your obligations under these Terms without our prior written consent. Any purported assignment or delegation in violation of this Section is null and void. No assignment or delegation relieves you of any of your obligations under these Terms.</p>

<h3>No Waivers.</h3>

<p>The failure by us to enforce any right or provision of these Terms will not constitute a waiver of future enforcement of that right or provision. The waiver of any right or provision will be effective only if in writing and signed by a duly authorized representative of Company.</p>

<h3>No Third Party Beneficiaries.</h3>

<p>These Terms do not and are not intended to confer any rights or remedies upon any person other than you.</p>

<h3>Notices.</h3>

<p>To You. We may provide any notice to you under these Terms by: (i) sending a message to the email address you provide or (ii) by posting to the Site. Notices sent by email will be effective when we send the email and notices we provide by posting will be effective upon posting. It is your responsibility to keep your email address current.</p>
<p>To Us. To give us notice under these Terms, you must contact us by email at admin@bealphaman.com which will be effective upon our receipt.</p>

<h3>Severability.</h3>

<p>If any provision of these Terms is invalid, illegal, void or unenforceable, then that provision will be deemed severed from these Terms and will not affect the validity or enforceability of the remaining provisions of these Terms.</p>

<h3>Entire Agreement.</h3>

<p>Our order confirmation, these Terms, our Website Terms of Use and our Privacy Policy will be deemed the final and integrated agreement between you and us on the matters contained in these Terms and shall supersede and extinguish any previous communications or agreements concerning the subject matter contained herein.</p>
</div>
</div>
</div>

