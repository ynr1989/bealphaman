    <!-- slider -->
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Contact</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- slider end -->

    <!-- contact-info -->
    <div class="contact-info">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="single-contact-info">
              <div class="info-icon">
                <i class="fas fa-phone-alt"></i>
              </div>
              <div class="info-dec">
                <h4>Phone</h4>
                <p>(201)854-3535</p>
                <p>(201)854-3535</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="single-contact-info">
              <div class="info-icon">
                <i class="far fa-envelope-open"></i>
              </div>
              <div class="info-dec">
                <h4>Email</h4>
                <p>admin@bealphaman.com</p>
                <!-- <p>support@mediglass.com</p> -->
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="single-contact-info">
              <div class="info-icon">
                <i class="fas fa-map-marker-alt"></i>
              </div>
              <div class="info-dec">
                <h4>Address</h4>
                <p>6419Park Ave, West New York</p>
                <p>NJ07093</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- contact-info end -->

    <!-- map end -->
    <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23797.787943261996!2d-87.67301056717703!3d41.79117239070044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e2fb4be08af53%3A0xb147453362f42592!2sWest%20Englewood%2C%20Chicago%2C%20IL%2060636%2C%20USA!5e0!3m2!1sen!2sbd!4v1604133409332!5m2!1sen!2sbd" height="420" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <!-- map end -->

    <!-- contact-form -->
    <div class="contact-form-area">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="contact-form-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/contact-big1.png" alt="img">
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="contact-form">
              <div class="heading-part">
                <p>GET IN TOUCH</p>
                <h2>Send Us a Message</h2>
              </div>
              <form action="#" class="form-details">
                <input type="text" placeholder="Your Name" class="form-control-text">
                <input type="email" placeholder="Your E-mail" class="form-control-text">
                <input type="text" placeholder="Subject" class="form-control-text">
                <textarea placeholder="Your Message" class="form-control-textarea"></textarea>
                
                <a href="#" class="btn btn-Shop Message">Send Message<i class="fas fa-long-arrow-alt-right"></i></a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- contact-form end -->
