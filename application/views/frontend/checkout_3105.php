<style>
  .breadcrumb-item{ 
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white;
    margin-left: 15px;
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97; 
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
.section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}
.shiptext{
   word-wrap: break-word;
}
 .backcart_btn{
  width: 15%;
      font-size: 17px;
  }   
</style>
<?php 

$batch_id = $_GET['batch_id'];
$category_id = $_GET['category_id'];
$cart_data = $this->mainModel->checkUserCheckoutData($user_id,$batch_id,$category_id);
//$cart_data = $this->mainModel->checkUserCartByCat($user_id,"");
 

?> 
<form method="post" action="<?php echo base_url('confirmCheckout'); ?>" >
 <div class="slider slidersize">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12"> 
            <a class="nav-link btn btn-Shop backcart_btn" href="<?php echo base_url('cart'); ?>">Back to Cart</a>
          </div>

           <div class="col-md-4"> 
            <div class="slider-title ">
              <h3 class="title" style="text-align: left;">Shipping Info</h3>  
            </div>

             <div class="col-md-12">
              <?php $row = $this->mainModel->getTypes('SHIP_ADDRESS_TYPE'); 
                foreach($row as $data){
                  if($data['code'] == 1):
                ?>
              <div class="form-check" style="margin-top:10px;">
                <input class="form-check-input ship_address_type" type="radio" name="ship_address_type" id="flexRadioDefault<?php echo $data['code']; ?>"  <?php if($data['code']==1): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
                <label class="form-check-label" for="flexRadioDefault<?php echo $data['code']; ?>">
                  <?php echo $data['value']; ?>
                </label>
              </div>
            <?php endif;  } ?>
            </div>

             
            <div class="container row" style="margin-top:20px;">
              <div class="col-md-12">
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Free 2 Day Shipping<br></p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Discreet Packaging</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Guaranteed Pricing</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Non Childproof, single-dose packaging</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Email Notifications on the shipping status</p>
            </div>

           

              <div class="col-md-12 row" style="margin-top:10px;">
              <div class="col-md-8 col-sm-8">
                 <strong>Select Your Home Address</strong>  <span><a href="<?php echo base_url('add_address'); ?>">Add New Address</a></span>
              </div>
              <select class="form-control user_address_id" name="user_address_id" required="required">
               <option value="">Select Address</option>
              <?php 
                foreach($addresses as $data){
                ?>
               <option value="<?php echo $data['address_id']; ?>"><?php echo $data['address_line1']; ?>, <?php echo $data['city']; ?>, <?php echo $data['state']; ?> - <?php echo $data['zip']; ?></option>
             
            <?php } ?>
            </select>
            </div>

            <?php if($cart_data[0]['payment_status'] == 0): ?>

            <div class="col-md-12">
              <?php $row = $this->mainModel->getTypes('SHIP_ADDRESS_TYPE'); 
                foreach($row as $data){
                  if($data['code'] == 2):
                ?>
              <div class="form-check" style="margin-top:10px;">
                <input class="form-check-input ship_address_type" type="radio" name="ship_address_type" id="flexRadioDefault<?php echo $data['code']; ?>"  <?php if($data['code']==1): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
                <label class="form-check-label" for="flexRadioDefault<?php echo $data['code']; ?>">
                  <?php echo $data['value']; ?>
                </label>
              </div>
            <?php endif; } ?>
            </div>

            <?php elseif($cart_data[0]['ship_address_type'] == 2):
              $pharmacyInfo = $this->mainModel->getUserShipPharmacyAddress($cart_data[0]['cart_id']); ?>
               <h3><b>Patient Pharmacy Address</b></h3>
        <div class="col-md-12 foundation_sm">
           <ul>
            <li><?php echo $pharmacyInfo[0]['pharmacy_name'];  ?> </li>
            <li><?php echo $pharmacyInfo[0]['street_address'];  ?> , <?php echo $pharmacyInfo[0]['city'];  ?> </li>
             <li><?php echo $pharmacyInfo[0]['state'];  ?> , <?php echo $pharmacyInfo[0]['zip'];  ?> </li> 
           <li><i class="fa fa-phone" aria-hidden="true"></i><?php echo $pharmacyInfo[0]['phone'];  ?>  </li>
           <li>Fax: <?php echo $pharmacyInfo[0]['fax'];  ?> </li></ul>
           <?php
             endif; ?>


            <!-- <div class="col-md-12">
              <?php $row = $this->mainModel->getTypes('SHIP_ADDRESS_TYPE'); 
                foreach($row as $data){
                ?>
              <div class="form-check" style="margin-top:10px;">
                <input class="form-check-input ship_address_type" type="radio" name="ship_address_type" id="flexRadioDefault<?php echo $data['code']; ?>"  <?php if($data['code']==1): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
                <label class="form-check-label" for="flexRadioDefault<?php echo $data['code']; ?>">
                  <?php echo $data['value']; ?>
                </label>
              </div>
            <?php } ?>
            </div> -->

          


           </div> 
        </div>


         <div class="col-md-4 ship_address_section">
           
            <div class="slider slidersize ">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="slider-title ">
                      <h4 class="title">Enter Your Pharmacy Address</h4>
                      <h6>Please enter name and address of your pharmacy of choice.</h6>
                    </div>
                     <div class="row">
                     <div class="form-group col-md-12">
                    <label for="usr">Pharmacy Name: (*)</label>
                    <input type="text" class="form-control pharmacy_name" name="pharmacy_name" placeholder="Pharmacy Name">
                    </div>
                  
                   <div class="form-group col-md-12">
                    <label for="usr">Pharmacy Phone: (*)</label>
                    <input type="text" class="form-control pharmacy_phone" name="phone" placeholder="Phone Number" >
                    </div>
                      
                  <div class="form-group col-md-12">
                    <label for="usr">Street Address:</label>
                    <input type="text" class="form-control street_address" name="street_address" placeholder="Street Address" >
                    </div>

                    <div class="form-group col-md-6">
                    <label for="usr">City</label>
                    <input type="text" class="form-control ship_city" name="city" placeholder="City">
                    </div>

                    <!-- <div class="form-group col-md-6">
                    <label for="usr">Country</label>
                    <input type="text" class="form-control country" name="country" placeholder="Country" required="required">
                    </div> -->
                    <div class="form-group col-md-6">
                    <label for="usr">State</label>
                    <input type="text" class="form-control state" name="state" placeholder="State">
                    </div>
                    <div class="form-group col-md-6">
                    <label for="usr">Zip</label>
                    <input type="text" class="form-control zip" name="zip" placeholder="Zip Code">
                    </div>
                   
                  </div>
                  <!--  <button type="submit" class="btn btn-warning">Submit</button> -->

                 <!--  <div class="container row">
              <div class="col-md-6 col-sm-6"> 
              </div>

               <?php if($cart_data[0]['payment_status'] == 1 && $cart_data[0]['doctor_approve_status'] == 0){ ?>
              <div class="col-md-7 col-sm-7" style="text-align: right;">
                 <a href="#" class="btn btn-warning">Wait for Doctor Approval</a>
              </div> 
              <?php }else{  ?>
                <div class="col-md-6 col-sm-6" style="text-align: right;">
                 <button type="submit" class="btn btn-warning">Pay</button>
              </div>
              <?php }  ?> 
            </div> -->

                  </div>
                </div>
              </div>
            </div>
         </div>

         
          <div class="col-md-4">
            <div class="slider-title ">
              <h3 class="title">Finish your visit</h3> 
              <h5 class="title">Your treatment if prescribed</h5>
            </div>
             
            <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong><?php echo $cart_data[0]['drug_name']; ?></strong><br>
                 <?php echo $cart_data[0]['strength']; ?>
              </div>
              <input type="hidden" name="payment_id" value="<?php echo $this->encryption->encrypt($cart_data[0]['cart_id']); ?>">
              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>$<?php echo $cart_data[0]['price']; ?></strong>
              </div> 
              <div class="col-md-12">
                <p style="background: #1574f6;
    color: #fff;
    padding: 10px;">You won't be charged for or shipped any prescription products unless prescribed by a doctor through our platform.</p>
              </div>
            </div>

            <hr>

            <div class="container row">
              <div class="col-md-8 col-sm-8">
                 <strong>Online doctor visit</strong> 
              </div>

              <div class="col-md-4 col-sm-4" style="text-align: right;">
                 </strong>$<?php echo $cart_data[0]['doctor_consultation_fee']; ?></strong>
              </div> 
            </div>
            <hr>
            <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong>Shipping</strong> 
              </div>

              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>FREE</strong>
              </div>   
            </div>


             <hr>
             <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong>Due</strong> 
              </div>

              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>$<?php
                 if($cart_data[0]['payment_status'] == 0){
                  echo $cart_data[0]['doctor_consultation_fee'];
                  }else{
                    echo $cart_data[0]['price'];
                  } ?></strong>
              </div> 
            </div>

            <hr>
             <div class="container row">
              <div class="col-md-6 col-sm-6"> 
              </div>

               <?php if($cart_data[0]['payment_status'] == 1 && $cart_data[0]['doctor_approve_status'] == 0){ ?>
              <div class="col-md-7 col-sm-7" style="text-align: right;">
                 <a href="#" class="btn btn-warning">Wait for Doctor Approval</a>
              </div> 
              <?php }else{  ?>
                <div class="col-md-6 col-sm-6" style="text-align: right;">
                 <button type="submit" class="btn btn-warning">Pay</button>
              </div>
              <?php }  ?> 
            </div>
          </div>


          <div class="col-md-4"> 

            
            <div class="container row">
              
              <?php if($cart_data[0]['appointment_date']!="" && $cart_data[0]['payment_status'] == 2): ?>
                <h4>Select Appointment Date/Time</h4>
                <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label  requiredField" for="date">
                   <strong>Appointment Date :</strong> <?php echo $cart_data[0]['appointment_date']; ?> <?php echo  date("g:i a", strtotime($cart_data[0]['appointment_time'])); ?>
                   
                  </label>
                  
                 </div>
               </div>

                <div class="col-md-12 col-sm-12 col-xs-12 slots_data">
                   <label class="control-label col-sm-4 requiredField" for="date">
                   Select Time
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                </div>

              <div class="col-md-4 col-sm-4" style="text-align: right;">
                 </strong></strong>
              </div>
              <?php elseif($cart_data[0]['payment_status'] == 0): ?>
                <h4>Select Appointment Date/Time</h4>
                <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label col-sm-4 requiredField" for="date">
                   Select Date
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                  <div class="col-sm-10">
                   <div class="input-group">
                    <div class="input-group-addon">
                     <i class="fa fa-calendar">
                     </i>
                    </div>
                    
                    <input class="form-control appointment_date" readonly="readonly" autocomplete="off" placeholder="Select Date" name="appointment_date" type="text" required value="<?php echo $cart_data[0]['appointment_date']; ?>" />
                    
                   </div>
                  </div> 
                 </div>
               </div>

                <div class="col-md-12 col-sm-12 col-xs-12 slots_data">
                   <label class="control-label col-sm-4 requiredField" for="date">
                   Select Time
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                </div>

              <div class="col-md-4 col-sm-4" style="text-align: right;">
                 </strong></strong>
              </div>
              <?php endif; ?>

                
            </div>

          </div>
           
          


      </div>
    </div>

    </form>
<?php /*else: ?>
 <div class="slider slidersize">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          
          <div class="alert alert-success" role="alert">
             Thank you. Please wait for doctor approval
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endif;*/ ?>

<script type="text/javascript">
  $(".slots_data").hide();
  $(".appointment_date").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate:new Date(),
    onSelect: function(date, instance) {
      console.log(date, instance);
        $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url('get_appointment_slots'); ?>",
              data: "a_date="+date,
              success: function(result)
              { $(".slots_data").show();
                var resultData = $.parseJSON(result);
                if(resultData.status == true)
                {
                  $(".slots_data").html(resultData.rdata);
                }else{
                  $(".slots_data").html(resultData.message);
                }
              }
         });  
     }
});
</script>
     