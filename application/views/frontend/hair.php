<style>
  .about-text{
          border-style:solid;
          border-color: green;
          border-width: 2px; 
          border-radius:10px 10px;
          text-align:center;
  }
  .our-team{
    padding:0px !important;
  }
  .about{
    margin-left:15px !important;
  }
  .button1{
    margin-left:15px !important;
  }
  .thumbnail-content {
    box-shadow: 10px 10px 5px #aaaaaa;
    margin: 20px;
}
.thumbnail-content img {
    margin-left: 70px;
    margin-bottom: 15px;
}
h4.name {
    font-weight: 500 !important;
    text-align: center;
    margin: 3px;
    margin-right: 30px;
}
.button {
    float: right;
    margin-top: 49px;
    margin-right: 20px;
    margin-bottom: 40px;
}
.faq-content .accordion .card .card-header .btn-link::after {
    right: 2% !important;
}
div#boxq {
    background: #f5f9ff;
    display: flex;
    border: 2px solid #1574f6 !important;
    margin: 20px 0px 0px;
    box-shadow: 10px 10px 5px #aaaaaa;
    vertical-align: middle;
    text-align-last: auto;
}
.our-team {
    text-align: center;
    background-color: #f5f9ff0d;
    padding: 50px 0 100px;
}
.section-title.style-three {
    padding: 10px 0 0px !important;
}
h2.title {
    margin-bottom: -30px !important;
}
.about-content h3 {
    margin-top: 20px !important;
}
.col-lg-12.faq-content {
    margin-bottom: 40px;
}

@media only screen and (max-width: 600px) {
  div#boxq {
    
    display: flow-root;
    
    margin-left: 15px;
}
div#accordionExample {
    margin-left: 20px;
}
.inner-header-top.style-three {
    margin-left: 20px;
}
}
</style>

<?php
$quiz1 = $this->mainModel->getQuestionsByCat("2");
$hairurl = base_url('ed_questionnaire')."?q=quiz&step=2&eid=2&n=1&t=".count($quiz1);
?>

    <!-- about-us -->
    <div class="about-us">
      <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content">
                <!-- <span class="pre-text">Hair</span> -->
                 <h1 class="main-title">Hair Loss</h1> 
                <p class="simple-content">Male-patterned baldness is nothing to 
                 be ashamed about—but it’s also nothing to ignore.  Our Treatments
                 have been proven to be over 80% effective. 
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12">
                  <div class="thumbnail">
                    
                    <div class="thumbnail-content">
                       <img src="<?php echo base_url(); ?>assets/frontend/images/strut3.svg" alt="img">
                      <h4 class="name">Treatments starting at $20/month</h4>
                      <!-- <span class="profession">Doctor</span> -->
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div>
           <!--  <div class="col-lg-6 col-md-12">
              <p class="about-text">Treatments starting at $20/month</p>
            </div> -->
            <div class="col-xl-6 col-lg-6 col-md-12">
                  <div class="thumbnail">
                    
                    <div class="thumbnail-content">
                       <img style="width:58px;height:60px" src="<?php echo base_url(); ?>assets/frontend/images/doctor.png" alt="img">
                      <h4 class="name">Speak to healthcare professional</h4>
                      <!-- <span class="profession">Doctor</span> -->
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div>
           <!--  <div class="col-lg-6 col-md-12">
              <p class="about-text">Speak to a Licensed Healthcare Provider</p>
            </div> -->
          </div>
          <div class="button">
             <a href="<?php echo $hairurl; ?>" class="btn btn-Shop Read-More">Get Started<i class="fas fa-long-arrow-alt-right"></i></a>
          </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/minoxidil.png" alt="img">
            </div>
          </div>
        
             
             <div class="col-lg-12 faq-content">

      <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">About Hair Loss</h4>
              </div>              
            </div> 

           <!--  <div class="single-blog-item"> 
              <p class="blog-content">Erectile Dysfunction (ED) is the inability to attain or maintain an erection needed for satisfying sexual performance. Research has found that it affects an estimated 18 million men in the United States alone.  </p> 
            </div>
 -->
            <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       What Causes Hair Loss?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      <i class="fas fa-hand-point-right"></i> Male pattern hair loss is believed to be due to a combination of genetics and the male hormone dihydrotestosterone (DHT) a hormone derived from testosterone. With age, most people notice some hair loss because hair growth slows and start to lose its color. At some point, hair follicles stop growing hair which causes the hair on our scalp to thin.  
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                       Can Hair Loss be Treated?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      <i class="fas fa-hand-point-right"></i> The good news is that male pattern hair loss can be treated with FDA-approved medications. Minoxidil, the generic of Rogaine®, is a topical treatment that is applied directly to the scalp. Minoxidil is best for new hair loss or to treat mild to moderate balding because it works by stimulating hair regrowth. For men who are experiencing thinning hair, minoxidil can help to restore some hair density. Another medication for hair loss is finasteride, an oral medication that treats hair loss by blocking the hormones that inhibit hair growth. Finasteride can help to prevent additional balding and can even stimulate hair regrowth.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                       How long does it take to see results?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                      <i class="fas fa-hand-point-right"></i> You may need to take finasteride daily for three months or more before you see a benefit. Finasteride can only work over the long term if you continue taking it. If the drug has not worked for you in twelve months, further treatment is unlikely to be of benefit.
                    </div>
                  </div>
                </div>
                
                
                
              </div>

              <div class="our-team">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-md-12">
                    <div class="section-title style-three">
                        <!-- <p class="content">Meet Our Team</p> -->
                        <h2 class="title">How does Be Alpha Man work?</h2>
                        <p>We offer prescription and over-the-counter medications clinically proven to help you prevent and reverse hair loss.</p>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
     <!-- <div class="about-us">
      <div class="container"> -->
        <div class="row align-items-center">
		<div class="col-lg-12" id="boxq">
          <div class="col-lg-8 col-md-12">
            <div class="about-image">
              <h3>Finasteride & Minoxidil </h3>
              <p>A daily combination of finasteride (generic Propecia), a prescription treatment for male pattern baldness, and minoxidil (generic Rogaine), a topical treatment for the crown of your head.</p>
            </div>
             
          </div>
          <div class="col-lg-4 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h3 >$35/month</h3> Starting at
<!-- <div class="button">
             <a href="#" class="btn btn-Shop Read-More">Get Started<i class="fas fa-long-arrow-alt-right"></i></a>
          </div> -->
                 
              
              </div>
            </div>
          </div>
</div>
          <div class="col-lg-12" id="boxq">
          <div class="col-lg-8 col-md-12">
            <div class="about-image about">
              <h3>Finasteride  </h3>
              <p>Generic Propecia</p>
              <p>Prescription treatment used for receding hairline. </p>
            </div>
             
          </div>
          <div class="col-lg-4 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h3 >$20/month</h3> Starting at


                <!--  <div class="button button1">
             <a href="#" class="btn btn-Shop Read-More">Get Started<i class="fas fa-long-arrow-alt-right"></i></a>
          </div> -->
              
              </div>
            </div>
          </div>
</div>
        <div class="col-lg-12" id="boxq">
          <div class="col-lg-8 col-md-12">
            <div class="about-image about">
              <h3>Minoxidil </h3>
              <p>Generic Rogaine</p>
              <p>Over-the-counter treatment used for thinning crowns. </p>
            </div>
             
          </div>
          <div class="col-lg-4 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h3 >$15/month

                 </h3>
             <!--  <div class="button button1">
             <a href="#" class="btn btn-Shop Read-More">Get Started<i class="fas fa-long-arrow-alt-right"></i></a>
          </div> -->
              </div>
            </div>
          </div>
		  </div>
       <!--  </div> -->
      </div>
    <!-- </div>

          </div> -->
        </div>
      </div>
    </div>
    
   