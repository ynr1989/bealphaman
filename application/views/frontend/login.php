<?php include("header.php"); ?>
<style>
.login-form {
    box-shadow: 10px 10px 5px #aaaaaa;
    padding: 20px;
    border: 1px solid #7b7979;
    width: 80%;
}
.register-form {
    box-shadow: 10px 10px 5px #aaaaaa;
    padding: 20px;
    border: 1px solid #7b7979;
}
.about-us {
    margin-bottom: 60px;
}
button.btn.btn-Shop {
    float: right;
    margin-top: -40px;
}
a.lost-your-password {
    margin-left: 40px;
}
.col-lg-6.col-md-6.col-sm-6.lost-your-password-wrap {
    margin-top: -30px;
    float: right;
}
@media only screen and (max-width: 600px) {
  .register-form {
    margin-top: 37px;
    width: 130%;
    margin-left: -40px;
}
.login-form {
    width: 100%;
}
}
</style>
    <!-- slider --> 
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Login/Register</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div> 
    <!-- slider end -->

    <!-- about-us -->
    <div class="about-us">
      <div class="container">
        
        <div class="container">
<div class="row">
<div class="col-lg-6 col-md-12">
<div class="login-form">
<h2><i class="fas fa-sign-in-alt"></i> Login</h2>
<form class="form-signin loginForm">
  <div  class="login_popup_spinner spinnerNewBg" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_popup alert innerAlert alert-danger" style="display:none;"></div>
            <div class="success_popup alert innerAlert alert-success" style="display:none;"></div>
<div class="form-group">
<label>Email or Mobile</label>
<input type="text" class="form-control" id="loginMobile" placeholder="Email/Mobile" required="required">
</div>
<div class="form-group">
<label>Password</label>
<input type="password" class="form-control" id="loginPassword" placeholder="Password" required="required">
</div>
<div class="row align-items-center">
<div class="col-lg-6 col-md-6 col-sm-6 remember-me-wrap">
<p>
<input type="checkbox" id="test2">
<label for="test2">Remember me</label>
</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 lost-your-password-wrap">
<a href="<?php echo base_url('forgotPassword'); ?>" class="lost-your-password">Forgot password?</a>
</div>
</div>
<button type="submit" class="btn btn-Shop" style="float: none;margin-top: 0px;">Log In <i class="fas fa-chevron-right"></i></button>
</form>
</div>
</div>
<div class="col-lg-6 col-md-12" style="border-left: 1px dotted blue;margin-left: -27px;padding-left: 83px;">
<div class="register-form">
<h2><i class="fas fa-users-cog"></i> Register</h2>

<div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

<form method="post" action="#" class="add_user" enctype="multipart/form-data">
  <div class="form-group">
<label>First Name</label>
<input type="text" class="form-control first_name" placeholder="First Name" required="required">
</div>
<div class="form-group">
<label>Last Name</label>
<input type="text" class="form-control last_name" placeholder="Last Name" required="required">
</div>
<div class="form-group">
<label>Email</label>
<input type="email" class="form-control email" placeholder="Email" required="required">
</div>
<div class="form-group">
<label>Phone(Mobile)</label>
<input type="text" class="form-control mobile" placeholder="Mobile" required="required">
</div>
<div class="form-group">
<label>Password</label>
<input type="password" class="form-control password" placeholder="Password" required="required">
</div> 
<div class="form-group" style="margin-bottom: 65px;">
<label>Confirm Password</label>
<input type="password" class="form-control cpassword" placeholder="Password" required="required">
</div> 
<button type="submit" class="btn btn-Shop">Register<i class="fas fa-caret-right"></i></button>
</form>
</div>
</div>
</div>
</div>

      </div>
    </div>
 
    <?php include("footer.php"); ?>