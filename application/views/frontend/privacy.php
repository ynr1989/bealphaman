<div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Privacy</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Privacy</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

<div class="about-us">
      <div class="container">
        <h3>Privacy Policy</h3> 
        <div class="row align-items-center"> 

<p>Last Modified: APRIL 1, 2021</p>
<p>This Privacy Policy (the “Privacy Policy”) applies to all visitors and users of the Be Alpha Man Llc., ("Company", “us”, “we” or “our”) application (“Application”), our website, https://www.BeAlphaMan.com (“Website”) and our services provided through the Application and Website (“Services”) and is incorporated into our Terms and Conditions (“Terms & Conditions”) .</p>

<p>This Privacy Policy describes the types of information we may collect from your use of the Application, Website, or Services, and how we use, maintain, protect and disclose such information. Your access and/or use of the Application, Website or Services signifies that you have read, acknowledged and accepted the terms and conditions of this Privacy Policy. If you do not agree with this Privacy Policy, then do not access or use the Application, Website or Services, and delete the Application from your Mobile Device. We may alter, amend or supplement this Privacy Policy in our sole discretion (“Revised Privacy Policy”). We will provide notice on the Site of any Revised Privacy Policy at least thirty (30) days before the Revised Privacy Policy goes into effect. Your continued use of the Application, Website, or Services after the Revised Privacy Policy becomes effective signifies that you acknowledge, agree and accept the Revised Privacy Policy.</p>
<h3>18 years old or older</h3>
<p>The Application, Website, and Services are for individuals who are 18 years or older. We do not knowingly collect information from individuals who are less than 18 years old, and you are prohibited from providing any information to us for such individuals.</p>
<h3>Information that we may collect</h3>

<h6>Personal Information. </h6>
<p>Personal Information is information that identifies your name, postal address, email address, social media address, username and password, phone number, gender, date of birth, information regarding your preferences, and any other information that you provide to us. </p>
<h6>Medical Information</h6>
<p>Medical Information is your medical data, such as the doctors, dentists, or other healthcare providers that you have visited, the reasons and dates of such visit, your medical history and other medical and health information you choose to share with us.</p>
<h6>Anonymous Information. </h6>

<p>Anonymous Information is any information that we collect from you, including Personal Information and Medical Information, from which all information that could identify you as the source of such information has been removed. You hereby assign to us all rights, including intellectual property rights, in and to the Anonymous Information, so that we are the sole and exclusive owners of such information. To the extent that state or local laws prohibits you from assigning ownership rights in such the Anonymous Information, you grant us, our successors and assigns, a royalty-free worldwide, sublicensable, transferrable license to host, transfer, process, analyze, distribute, communicate and use Anonymous Information in perpetuity without any compensation to you, your successors, your heirs or your assigns. If you do not consent to the collection and use of Anonymous Information, then you are prohibited from using the Site or Services.</p>

<h6>Public Communications</h6>
<p>The Site or Services may contain features permitting you to communicate with the public at large or with a group of people, including bulletin board services, chat areas, news groups, forums, communities, personal web pages, and calendars (“Public Communications”). If you post or provide information through any Public Communication, then the information will most likely become public without any privacy restrictions whatsoever. We have the right, but not the obligation, to use all Public Communications in our discretion without restriction.</p>

<h3>Automatically Collected Information</h3>
<p>When you download, access, and use the Application or the Website, it may use technology to automatically collect:</p>

<h6>• Usage Details. </h6>
<p>When you access and use the Application, Website or Services, we may automatically collect certain details of your access to and use of the Application, Website or Services, including traffic data, location data, logs, and other communication data and the resources that you access and use on or through the App. or Website. When you access the Website, we may collect information about your equipment (such as information about your computer, internet connection, operating system, browser type and IP address), and browsing actions and patterns.</p>

<h6>• Mobile Device Information (Application only). </h6>
<p>We may collect information about your Mobile Device and internet connection, including the device's unique device identifier, IP address, operating system, browser type, mobile network information, and the device's telephone number.</p>

<h6>• Stored Information and Files. </h6>
<p>The App also may access metadata and other information associated with other files stored on your device. This may include, for example, photographs, audio and video clips, personal contacts, and address book information.</p>

<h6>• Location Information. </h6>
<p>If you do not want us to collect this information do not use the Application or Website, and delete the Application from your Mobile Device.</p>

<h3>How Automatically Collected Information is gathered</h3>

<p>Automatically Collected Information is gathered through cookies, flash cookie, web beacons and web widgets, and may be gathered by us or our third-party business partners. We do not control third parties' collection or use of your information to serve interest-based advertising. However, these third parties may provide you with ways to choose not to have your information collected or used in this way. Some browsers use a DNT (Do Not Track) feature that sends a signal or preference to the websites regarding DNT. Because there is no current accepted industry standard on how to respond to the different DNT signals, we do not currently respond to them on our Site.</p>


<h6>• Cookies</h6>
<p>Cookies are pieces of data your web browser stores on your hard drive. We also offer certain features that are only available through the use of a "cookie." Cookies can also help us provide information which is targeted to your interests. You are always free to decline our cookies if your browser permits, although in that case you may not be able to use certain features on our site and you will not be able to submit an order online. Please be aware that if you link to another site from our website, you may encounter "cookies" or other similar devices placed by third parties. We do not control the use of cookies by third parties.</p>

<h6>• Flash Cookies</h6>

<p>Flash Cookies Certain features of our website may use locally stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, from, and on our website. Flash cookies are not managed by the same browser settings as are used for browser cookies.</p>

<h6>• Web Beacons. </h6>
<p>Pages of our Site and our emails may contain small electronic files known as web beacons (also referred to as clear gifs, pixel tags, and single-pixel gifs) that permit us, for example, to count users who have visited those pages or opened an email and for other related website statistics (for example, recording the popularity of certain website content and verifying system and server integrity).</p>

<h6>• Web Widget</h6>
<p>Pages of the Site may contain stand-alone applications commonly referred to as Web Widgets or “Widgets.” Widgets provide specific services or advertisements from third-parties. Personal Information may be collected through a Widget and the collection and use of your information through this technology is governed by the privacy policy of the company that created or of the company for which is providing the specific services or advertisements. We recommend that you review all such information which is generally included in links provided by the Widget.</p>

<h3>Third-Party Information Collection</h3>
<p>When you use the Application, Website or its content, certain third parties may use automatic information collection technologies to collect information about you or your device. These third parties may include:</p>

<ul>
  <li>Advertisers, ad networks, and ad servers</li>
 <li>Analytics companies</li>
 <li>Your mobile device manufacturer</li>
 <li>Your mobile service provider.</li>
</ul>

<p>The information they collect may be associated with your Personal Information or they may collect information, including personal information, about your online activities over time and across different websites, apps, and other online services websites. They may use this information to provide you with interest-based (behavioral) advertising or other targeted content.</p>

<p>We do not control these third parties' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.</p>

<h3>How We Use Your Information</h3>

<p>We use the information we collect as follows:</p>
<ul>
  <li>To establish your account and credentials and to verify the same;</li>
  <li>to improve the quality of the administration, interaction and use of the Application, Website or Services, including tracking your usage, administering a survey, estimating audience size and usage patterns, store information about your preferences, performing quality control and improvement, and responding to any of your inquiries</li>
  <li>To provide you the Services, including any information relating to the Services;</li>
  <li>To send you notifications about your account and any Updates to the Application or Services, including any changes to the same;</li>
  <li>To recognize you when you visit the Site or use the Services;</li>
  <li>To provide you information, and to offer you third-party goods and services, unless you opt out of such contact by following the Opt-Out procedure below;</li>
  <li>To provide to contractors, service providers, and other third parties we use to support our business who are contractually bound by to keep Personal Information and Sensitive Personal Information confidential and use it only for the purposes for which we disclose it to them.</li>
  <li>To disclose to a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information held by us is among the assets transferred;</li>
  <li>To comply with any court order, law, or legal process, including to respond to any government or regulatory request; to enforce or apply our Terms of Use; protect our, our customers, or other rights, property, or safety;</li>
  <li>To analyze purchase history, measure success of marketing and promotional efforts of our products or Services and any third-party products or services offered to you through our Site or Services;</li>
  <li>to scrub any information that specifically identifies you to include in an aggregated database that we may use or permit others to use for any commercial purposes, in our sole discretion;</li>
  
</ul>


<h3>Disclosure of your Information</h3>
<p>We may disclose or license your information to our affiliates, partners and other third parties, including subcontractors, to:</p>

<ul>
  <li>Assist us with the maintenance and operation of the Application, Website or Services;</li>
  <li>Market and promote the Application, Website and Services, including the measurement of the success of such efforts;</li>
  <li>Market and promote the goods or services of others, including the measurement of the success of such efforts;</li>
  <li>Comply with any court order, law, or legal process, including to respond to any government or regulatory request; to enforce or apply our Terms of Use; protect us, our customers, or others’ rights, property, or safety; or</li>
  <li>To disclose to a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information held by us is among the assets transferred.</li></ul>
  <p>If the information that we disclose to others consists of your Personal Information, then we will request that the party receiving such information be contractually bound to appropriate confidentiality provisions restricting the additional disclosure of such information. We shall have no restrictions on the use and disclosure of Automatically Collected Information, Anonymous Information or Public Communications.</p>

  <h3>Use and Disclosure of your PHI</h3>
  <p>Although we are not a healthcare provider and are not a “covered entity” as defined in HIPAA, the healthcare providers offered through our services may be a “covered entity” and therefore subject to the provisions of Health Insurance Portability and Accountability Act (“HIPAA”). The healthcare provider will share your protected health information (“PHI”) with us subject to the Business Associates Agreement in place with the healthcare provider. We will only use your PHI to provide our Services to you, and to provide to our Pharmacy Partners.</p>

  <h3>Accessing your Information</h3>
  <p>You can update your Personal Information by logging into the Website and visiting your Account Profile, or through your Account Profile located within the Application. You may also send us an email at admin@bealphaman.com to request access to, correct or delete any Personal Information that you have provided to us. We cannot delete your Personal Information except by also deleting your account. We may not accommodate a request to change information if we believe the change would violate any law or legal requirement or cause the information to be incorrect.</p>

  <h3>California Privacy Rights</h3>
  <p>California Civil Code Section 1798.83 permits users of our Application or Website that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an email to admin@bealphaman.com or write us at:</p>

  <p>Customer Support </p>
  <p>3 Tulip Lane</p>
  <p>Denville, NJ 07834</p>

  <h3>Data Security</h3>

  <p>We have implemented measures designed to secure your information from accidental loss and from unauthorized access, use, alteration, and disclosure. All information you provide to us is stored on our secure servers behind firewalls. Despite our efforts we cannot guarantee that loss, misuse, or unauthorized access will not occur, and we cannot and are not responsible for our third-party vendors.</p>

  <p>The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a password for access to the Application, certain parts of our Website, or Services, you are responsible for keeping this password confidential. We ask you not to share your password with anyone. We urge you to be careful about giving out information in public areas of the Website or Services like message boards. The information you share in public areas may be viewed by any user of our Website.</p>

  <p>Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted through the Services. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained in the Services.</p>

  <h3>Opting-Out of Newsletter, Email Communications, Push Notices, and Third-Party Promotions</h3>

  <p>We may send you emails, newsletters, promotional materials, or push notifications featuring our goods or Services or the goods and the goods and services of others. By using the Application, Website or Services you acknowledge and agree that such materials may be sent to you, your Mobile Device, and your email address. You can discontinue your receipt of specific materials by responding to the email address in the body of the email requesting such discontinuance. If you desire not to receive any newsletters, push notification, or communications promoting third-party goods or services, then you can opt-out by emailing us at admin@bealphaman.com</p>
 <h3> Third Party Links</h3>
  <p>The Site may contain links to other websites, advertisements and applications offered by third parties (“Third Party Links”) that may collect and use your information. Third Party Links are beyond our control and are not subject to this Privacy Policy.</p>
  <h3>Contact Us</h3>
  <p>If you have any comments, concerns or questions about this Privacy Policy, please contact us at: admin@bealphaman.com.</p>
</ul>
</div>
</div>
</div>

