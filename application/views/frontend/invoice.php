<?php include("header.php"); ?>

    <!-- slider --> 
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">invoice</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">invoice</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- slider end -->

    <!-- about-us -->
    <div class="about-us" style="padding: 0px;">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/about-img1.png" alt="img">
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content"> 
                <h1 class="main-title">What We Do</h1>
                <p class="simple-content">BeAlphaMan was created due to the high demand for men’s wellness. Many companies have developed online services providing exactly what men need but at the same time lightening their wallets.  With many years of experience in the healthcare field we decided to make the process much easier and more accessible to all men. With our network of pharmacies, we now can provide the same FDA approved meds for a much affordable price.
</p>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- about-us end -->

    <!-- client-logo -->
    
    <!-- client-logo end-->

    <!-- our-team-->
    
      




    <!-- testimonial -->
    <!-- <div class="testimonial-section style-two">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="inner-header-top style-two">
              <div class="header-title">
                <h3 class="title">Testimonials</h3>
                <p>Why People Believe in Us!</p>
              </div>
              <div class="header-button">
                <a href="#" class="btn btn-product"><i class="fas fa-chevron-left"></i></a>
                <a href="#" class="btn btn-product active"><i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="testimonial-slide mbm-165 owl-carousel">
              <div class="single-testimonial style-two">
                <div class="testimonial-icon">
                  <img src="images/h3-te-icon1.png" alt="img">
                </div>
                <div class="testimonial-content">
                  <p class="content">Slore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                  <div class="testimonial-thumbnail">
                    <div class="testimonial-pic">
                      <img src="images/test-pic1.png" alt="img">
                    </div>
                    <div class="testimonial-name">
                      <span>Byron Brahona</span>
                      <span class="profession">HD Manager</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="single-testimonial style-two">
                <div class="testimonial-icon">
                  <img src="images/h3-te-icon1.png" alt="img">
                </div>
                <div class="testimonial-content">
                  <p class="content">Asi enim ad minim veniam, quis nostrud exerci tation uLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
                  <div class="testimonial-thumbnail">
                    <div class="testimonial-pic">
                      <img src="images/01_Home_1.png" alt="img">
                    </div>
                    <div class="testimonial-name">
                      <span>Zuzeth Mendez</span>
                      <span class="profession">Sonographer</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="single-testimonial style-two">
                <div class="testimonial-icon">
                  <img src="images/h3-te-icon1.png" alt="img">
                </div>
                <div class="testimonial-content">
                  <p class="content">Slore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                  <div class="testimonial-thumbnail">
                    <div class="testimonial-pic">
                      <img src="images/test-pic1.png" alt="img">
                    </div>
                    <div class="testimonial-name">
                      <span>Richard K. Le</span>
                      <span class="profession">HD Manager</span>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            
          </div>
        </div>
      </div>
    </div> -->
    <!-- testimonial end-->

    <!-- newsletter-section -->
    <!--<div class="newsletter-section style-two">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-title style-two">
              <h2 class="title">Newsletter!</h2>
              <p class="content my-30y55">It only takes a second to be the first to find <br>
                out about our latest news</p>
            </div>
            <div class="meditree-subscription style-two">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Your email address...">
                <div class="input-group-append">
                  <button class="input-group-text">SUBMIT</button>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>-->
    <!-- newsletter-section end -->

    <!-- shop-features -->
    <!-- <div class="shop-features style-two ">
      <div class="container">
        <div class="row justify-content-lg-center">
          <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon1.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title"> Complete Online Questionnaire
</h5>
                <p class="content">We will be asking you some simple questions about your lifestyle, symptoms, and past medical history in order to create a Patient Health Profile. (Similar to what you have to fill out at a doctor’s office) </p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon2.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Speak to a Licensed Healthcare Provider</h5>
                <p class="content">Once we have gathered your information, we will connect you with a qualified healthcare provider.  During your consultation, you will get a chance to discuss your symptoms, concerns, and potential treatment options.</p>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon3.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Medication Discreetly Delivered to You 
</h5>
                <p class="content">Members will have the option to have their medicine delivered directly to their door with our FREE Delivery Service. 
</p>
              </div>
            </div>
          </div> -->
          <!--<div class="col-xl-3 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon4.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Secure Payments</h5>
                <p class="content">100% payment protection</p>
              </div>
            </div>
          </div>-->
        <!-- </div>
      </div>
    </div> -->
    <!-- shop-Features end -->

    <?php include("footer.php"); ?>