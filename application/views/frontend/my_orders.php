   <style>
  .breadcrumb-item{
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white;
    margin-left: 15px;
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97;
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
   .section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
   
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}

table#DataTables_Table_0,table.datatableBam.table.table-striped.table-bordered {
    box-shadow: 1px 2px 3px #ddd;
}
</style> 
<?php 

//$ques_info = $this->mainModel->getUserOrderQuestionnaireInfo($user_id);
//echo "oo==$user_id"; die();
$ques_info = $this->mainModel->getPaymentInfoByCartId($user_id);

?> 

 <div class="slider slidersize">
      <div class="container-fluid">
        <div class="row"> 
           <?php 
          $this->load->view('templates/frontend/sidebar'); ?>
          <div class="col-xl-9 col-lg-8 col-md-12"> 
            
<div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">My Orders</h3>
              </div>
              
            </div>
          </div>
            <table class="datatableBam table table-striped table-bordered">
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
              <tr><th>Order Date</th><th>Category</th> <th>Appointment</th><th>Doctor Fee</th><th>Payment Status</th></tr>
             
             <?php $i=0; foreach ($ques_info as $quesinfo) { $i++; 
             $cart_data = $this->mainModel->getCartInfoByBatchIdCartId($user_id,$quesinfo['cart_id'],$quesinfo['batch_id']);
            // / $pay_info = $this->mainModel->getPaymentInfoByCartId($cart_data[0]['cart_id']);
             $pay_info = $this->mainModel->getPaymentInfoByCartId($user_id,$quesinfo['cart_id'],"2");
 
              ?> 
              <tr><td>
                <?php
                $date=date_create($quesinfo['created_at']);
echo date_format($date,"m-d-Y");
?></td><td>Doctor Visit</td>
                 <td><?php echo $quesinfo['appointment_date']; ?>, <?php echo $quesinfo['appointment_time']; ?></td>
                <td>$<?php echo $quesinfo['doctor_consultation_fee']; ?></td>
                <td>Paid</td></tr>

                <tr><th>Order Date</th><th>Category</th><th>Drug Info</th><th>Drug Price</th><th>Payment Status</th></tr>
                <tr><td> <?php
                if($cart_data[0]['payment_status'] == 2){
                $date=date_create($pay_info[0]['created_at']);
echo date_format($date,"m-d-Y");
}else{echo "-"; }
?></td><td>Precription Payement</td>
                <td><?php echo $cart_data[0]['drug_name']; ?>, <?php echo $cart_data[0]['strength']; ?>
                </td>
                <td>$<?php echo $cart_data[0]['price']; ?></td> 
                <td>
                  <?php if($cart_data[0]['payment_status'] == 1 && $quesinfo['send_to_pharmacy'] ==1){ ?>
                   <a href="<?php echo base_url('cart'); ?>">To Be Paid</a>
                   <?php }else if($cart_data[0]['payment_status'] == 1 && $quesinfo['send_to_pharmacy'] ==0){ echo "Waiting on Doctor Approval"; }else if($cart_data[0]['payment_status'] == 2){ echo "Paid";}else{} ?></td></tr>

         <?php } ?>
             </table>

<hr/> 
             
          </div>
           
  
        </div>
      </div>
    </div>
      

    <!-- <div class="slider slidersize ship_address_section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Shipping info & Address</h2>
              <h6>Where would you like to have your medication shipped, i</h6><h6>f prescribed? use your legal name</h6>
            </div>
             <div class="row">
             <div class="form-group col-md-6">
            <label for="usr">Name:</label>
            <input type="text" class="form-control ship_name" id="ship_name" required="required">
            </div>
          <div class="form-group col-md-6">
            <label for="usr">City</label>
            <input type="text" class="form-control ship_city" id="usr" required="required">
            </div>
          <div class="form-group col-md-6">
            <label for="usr">Street Address:</label>
            <input type="text" class="form-control street_address" id="usr" required="required">
            </div>
            <div class="form-group col-md-6">
            <label for="usr">Country</label>
            <input type="text" class="form-control country" id="usr" required="required">
            </div>
            <div class="form-group col-md-3">
            <label for="usr">State</label>
            <input type="text" class="form-control state" id="usr" required="required">
            </div>
            <div class="form-group col-md-3">
            <label for="usr">Zip</label>
            <input type="text" class="form-control zip" id="usr" required="required">
            </div>
           
          </div>
           <button type="submit" class="btn btn-warning">Submit</button>
          </div>
        </div>
      </div>
    </div> -->
    </form>
<?php /*else: ?>
 <div class="slider slidersize">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          
          <div class="alert alert-success" role="alert">
             Thank you. Please wait for doctor approval
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endif;*/ ?>

   
     