
<style type="text/css">
  
button.btn.btn-primary.pull-right {
    background: #fb641b;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 20%);
    border: none !important;
    color: #fff;
    padding: 7px 14px;
    width: 92px;
    margin-top: 20px;
    margin-left: 2px;
    float: right !important;
}
.label {
    font-size: 1rem !important;
    font-weight: 400 !important;
    margin-top: 5px !important;
    color: #000 !important;
    margin-bottom: 4px;
}
.inner-header-top.style-three {
    margin-bottom: 16px !important;
   
}
.custom-control.custom-checkbox.mb-3 {
    margin: 0px 4px;
    padding: 0px 20px;
	margin-bottom: 5px !important;
}
.form-check.form-check-inline {
    margin-bottom: 5px !important;
    padding: 0px 0px !important;
}
label.label {
    text-transform: uppercase !important;
}

label.label {
    font-weight: 400 !important;
    font-family: 'Rubik';
}
</style> 

<form class="update_patient_personal_info" method="post">

   <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
<div class="panel">
                       <div class="panel-body">
<div class="row">
<div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">Profile Details</h3>
              </div>
              
            </div>
          </div>
 <input type="hidden" class="patient_user_id" value="<?php if(isset($_GET['user_id'])){ echo $_GET['user_id']; }; ?>" />
    <div class="col-md-4">
     <div class="form-group">
      <label  class="label">First Name</label>
      <input type="text" class="form-control first_name" value="<?php echo $rdata[0]['first_name']; ?>" name="first_name" id="first_name" placeholder="First Name">
    </div>
  </div>
    <div class="col-md-4">
      <label  class="label">Last Name</label>
      <input type="text" class="form-control last_name" value="<?php echo $rdata[0]['last_name']; ?>" name="last_name" id="last_name" placeholder="Last Name">
    </div>
    <div class="col-md-4">
    <label  class="label">Address</label>
    <input type="text" class="form-control address" name="address" id="address" placeholder="Address">
  </div>
  <div class="col-md-4">
    <label  class="label">Phone Number</label>
    <input type="phone" class="form-control mobile" value="<?php echo $rdata[0]['mobile']; ?>" name="mobile" id="mobile" placeholder="Mobile number" disabled="disabled">
  </div>
    <div class="col-md-4">
      <label  class="label">Email</label>
      <input type="email" disabled="disabled" value="<?php echo $rdata[0]['email']; ?>" class="form-control email" name="email" id="email" placeholder="Email">
    </div>
  
     <div class="col-md-4">
    <label  class="label">What is the problem?</label>
    <input type="text" class="form-control what_is_the_problem" name="what_is_the_problem" id="what_is_the_problem" placeholder="What is the problem?" value="<?php echo $pdata[0]['what_is_the_problem']; ?>">
  </div>
  <div class="col-md-4">
      <label  class="label">How often?</label>
      <input type="text" class="form-control how_often" name="how_often" id="how_often" placeholder="How often?" value="<?php echo $pdata[0]['how_often']; ?>">
    </div>
    <div class="col-md-4">
    <label for="inputStart" class="label">When did it start?</label> 
    <select name="when_did_it_start" class="form-control when_did_it_start">
       <option value="">Select Option</option>
<?php $row = $this->mainModel->getTypes('WHEN_DID_IT_START'); 
foreach($row as $data){
?>
<option value="<?php echo $data['code']; ?>" <?php if($data['code']==$pdata[0]['when_did_it_start']): echo "selected"; endif; ?>><?php echo $data['value']; ?></option>
<?php } ?>
</select>

  </div>
  <div class="col-md-4">
      <label for="inputWeight" class="label">Weight (Pounds)</label>
      <input type="text" class="form-control weight" id="weight" value="<?php echo $pdata[0]['weight']; ?>" placeholder="Weight in Pounds">
    </div>
    <div class="col-md-4">
      <label for="inputBloodPressure" class="label">Blood Pressure</label>
      <input type="text" class="form-control blood_pressure" value="<?php echo $pdata[0]['blood_pressure']; ?>" id="blood_pressure" placeholder="Example 120-80">
    </div>
    <div class="col-md-4">
      <label for="inputReason" class="label">Reason for visit</label>
     
<select name="reason_for_visit" class="form-control reason_for_visit">
       <option value="">Select Reason for visit</option>
<?php $row = $this->mainModel->getTypes('REASON_FOR_VISIT'); 
foreach($row as $data){
?>
<option value="<?php echo $data['code']; ?>" <?php if($data['code']==$pdata[0]['reason_for_visit']): echo "selected"; endif; ?>><?php echo $data['value']; ?></option>
<?php } ?>
</select>
    </div>
    
    <div class="col-md-2">
     <label for="inputDOB" class="label">DOB</label>
    <input type="text" class="form-control dob" id="dob" placeholder="MM-DD-YYYY" value="<?php echo $rdata[0]['dob']; ?>">
    </div>
    <div class="col-md-2">
       <label  class="label">Age</label>
      <input type="text" class="form-control age" name="age" id="age" value="<?php echo $rdata[0]['age']; ?>" placeholder="Age">
    </div>
    <div class="col-md-3">
     <label for="inputHeight" class="label">Height (Feet and Inches)</label>
     <input type="number" class="form-control height_feet" id="height_feet" value="<?php echo $pdata[0]['height_feet']; ?>" placeholder="Feet...">
    </div>
    <div class="col-md-1">
       <label  class="label">Inches</label>
      <input type="number" class="form-control height_inches" value="<?php echo $pdata[0]['height_inches']; ?>" id="height_inches" placeholder="Inches">
    </div>
     
    
</div>
<div class="row" style="margin-top:20px;">

 <div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">Medical History</h3>
              </div>
              
            </div>
          </div>
   

<div class="col-md-4" style="margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
        <label  class="label">FAMILY HISTORY:</label>
                         
  <?php $row = $this->mainModel->getTypes('FAMILY_HISTORY'); 
foreach($row as $data){
?> 
<div class="custom-control custom-checkbox mb-3">
      <input type="checkbox" class="custom-control-input family_history" id="family_history<?php echo $data['code']; ?>"  name="family_history[]" value="<?php echo $data['code']; ?>" <?php if(!empty($pdata[0]['family_history']) && in_array($data['code'], explode(",", $pdata[0]['family_history']))): echo "checked"; endif; ?>>
      <label class="custom-control-label" for="family_history<?php echo $data['code']; ?>"><?php echo $data['value']; ?> </label>
    </div>

    <?php } ?>  
  </div>

<div class="col-md-4" style="margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
        <label  class="label">ALERGIES:</label>
                         
  <?php $row = $this->mainModel->getTypes('ALERGIES'); 
foreach($row as $data){
?> 
<div class="custom-control custom-checkbox mb-3">
      <input type="checkbox" class="custom-control-input alergies" id="alergies<?php echo $data['code']; ?>"  name="alergies[]" value="<?php echo $data['code']; ?>" <?php if(!empty($pdata[0]['alergies']) && in_array($data['code'], explode(",", $pdata[0]['alergies']))): echo "checked"; endif; ?>>
      <label class="custom-control-label" for="alergies<?php echo $data['code']; ?>"><?php echo $data['value']; ?> </label>
    </div>

    <?php } ?>  
  </div>

   <div class="col-md-4" style="margin: 0px 0px;padding: 6px 15px 0px;border-bottom:1px solid rgb(167 169 171);">
        <label  class="label" style="text-transform: uppercase;">Check any health problem</label>
                         
  <?php $row = $this->mainModel->getTypes('HEALTH_ISSUES'); 
foreach($row as $data){
?> 
<div class="custom-control custom-checkbox mb-3">
      <input type="checkbox" class="custom-control-input" id="health_issues<?php echo $data['code']; ?>"  name="health_issues[]" value="<?php echo $data['code']; ?>" <?php if(!empty($pdata[0]['health_issues']) && in_array($data['code'], explode(",", $pdata[0]['health_issues']))): echo "checked"; endif; ?>>
      <label class="custom-control-label" for="health_issues<?php echo $data['code']; ?>"><?php echo $data['value']; ?> </label>
    </div>

    <?php } ?>  
  </div> 

            <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom:1px solid rgb(167 169 171);">

      <label for="inputOften" class="label">Marital Status: </label>

      <?php $row = $this->mainModel->getTypes('MARITAL_STATUS'); 
foreach($row as $data){
?>
      <div class="form-check form-check-inline">
  <input class="form-check-input marital_status" type="radio" name="marital_status" id="marital_status<?php echo $data['code']; ?>" <?php if($data['code']==$rdata[0]['marital_status']): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="marital_status<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?>

    </div>      

    <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
      <label for="inputOften" class="label">Living Situation: </label>
        <?php $row = $this->mainModel->getTypes('LIVING_SITUATION'); 
foreach($row as $data){
?>
      <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" <?php if($data['code']==$pdata[0]['living_situation']): echo "checked"; endif; ?> name="living_situation" id="living_situation" value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="living_situation<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>

<?php } ?>
 
 
    </div> 

    <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
      <label for="inputOften" class="label">Smoking: </label>
          <?php $row = $this->mainModel->getTypes('SMOKING_STATUS'); 
foreach($row as $data){
?>
      <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" <?php if($data['code']==$pdata[0]['smoking_status']): echo "checked"; endif; ?> name="smoking_status" id="smoking_status" value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="smoking_status<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?>
 
    </div> 

    <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
       <label for="inputOften" class="label">Alcohol: </label>
 <?php $row = $this->mainModel->getTypes('ALCOHOLIC_TYPE');
  foreach($row as $data){ ?>
     
      <div class="form-check form-check-inline">
  <input class="form-check-input" <?php if($data['code']==$pdata[0]['alcohol_status']): echo "checked"; endif; ?> type="radio" name="alcohol_status" id="alcohol_status" value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="alcohol_status<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?>
</div>
 <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
      <label for="inputOften" class="label">Drugs: </label>
      <?php $row = $this->mainModel->getTypes('DRUG_STATUS');
  foreach($row as $data){ ?>
      <div class="form-check form-check-inline">  
  <input class="form-check-input" <?php if($data['code']==$pdata[0]['drug_status']): echo "checked"; endif; ?> type="radio" name="drug_status" id="drug_status" value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="drug_status<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?> 

</div>

<div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
      <label for="inputOften" class="label">Operations/surgeries: </label>
      <?php $row = $this->mainModel->getTypes('SURGERIES');
  foreach($row as $data){ ?>
      <div class="form-check form-check-inline">  
  <input class="form-check-input" <?php if($data['code']==$pdata[0]['operations_surgeries']): echo "checked"; endif; ?> type="radio" name="operations_surgeries" id="operations_surgeries" value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="operations_surgeries<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?> 

</div>

 <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">

      <label for="inputOften" class="label">Medication Taken: </label>

      <?php $row = $this->mainModel->getTypes('MEDICATION_TAKEN'); 
foreach($row as $data){
?>
      <div class="form-check form-check-inline">
  <input class="form-check-input medication_taken" type="radio" name="medication_taken" id="medication_taken<?php echo $data['code']; ?>" <?php if($data['code']==$pdata[0]['medication_taken']): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
  <label class="form-check-label" for="medication_taken<?php echo $data['code']; ?>"><?php echo $data['value']; ?></label>
</div>
<?php } ?>

    </div>
 <div class="col-md-3" style="display: grid;margin: 0px 0px;padding: 6px 15px 0px;border-bottom: 1px solid rgb(167 169 171);">
&nbsp;
</div>

<div class="col-md-12 mb-10">
      <label for="inputNotes" class="label">Message</label>
      <textarea class="form-control message" id="message" placeholder="Add your message.."><?php echo $pdata[0]['message']; ?></textarea>
    </div>
 <div class="col-md-12 mb-10">
   <div style="margin-top: 10px;">
<button type="submit" class="btn btn-primary pull-right" style="margin-left:20px;">Submit</button>
</div>
</div>
</form>
</div></div>