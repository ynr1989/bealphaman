   <style>
  .breadcrumb-item{
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white;
    margin-left: 15px;
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97;
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
   .section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
   
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}
h5.title,h2.title {
    font-size: 1.3rem;
    font-family: monospace;
    font-weight: 600;
}
li.breadcrumb-item.active {
    height: 135px !important;
    border: 1px solid #0f0f0f2b;
}
a.btn.btn-warning {
    width: 10%;
    float: right !important;
    margin-top: 12px;
    margin-bottom: 20px;
    background: linear-gradient( 
180deg
 , #324A67, #1d2733) !important;
    color: #fff;
    font-weight: 700;
    
}
div#cartaha {
    box-shadow: 2px 2px 10px lightblue;
}
select.form-control.fdrug_id,select.form-control.fdrug_strength_id {
    border-radius: 0px;
    height: 45px;
    margin-top: -5px;
}

@media only screen and (max-width: 600px) {
 a.btn.btn-warning {
    width: 50%;
}

}

<!--emptycartpage-->
.cmtk_group {
    width: 100%;
    padding: 100px 0;
}
.cmtk_dt {
    padding: 50px 0 117px;
    text-align: center;
}
.thnk_coming_title {
    font-size: 10em;
    font-weight: 600;
    color: #1d2733 !important;
    font-family: 'Roboto', sans-serif;
    text-align: center;
    line-height: 1em;
    text-shadow: 1px 10px 6px rgb(0 0 0 / 20%);
    padding-bottom: 29px;
    margin-top: 0;
}
.thnk_title1 {
    font-size: 24px;
    font-weight: 600;
    color: #324A67;
    font-family: 'Roboto', sans-serif;
    text-align: center;
    line-height: 26px;
    margin-top: 0;
    text-shadow: 1px 10px 6px rgb(0 0 0 / 20%);
}
.thnk_des {
    font-size: 16px;
    font-weight: 400;
    color: #324A67;
    font-family: 'Roboto', sans-serif;
    text-align: center;
    line-height: 26px;
    margin-top: 0;
    text-shadow: 1px 10px 6px rgb(0 0 0 / 20%);
    margin-top: 30px;
    margin-bottom: 0;
}
.thnk_des span {
    color: #fe807e;
}

.thnk_coming_title1 {
    font-size: 2.5em;
    font-weight: 600;
    color: #1d2733 !important;
    font-family: 'Roboto', sans-serif;
    text-align: center;
    line-height: 1em;
    text-shadow: 1px 10px 6px rgb(0 0 0 / 20%);
    padding-bottom: 29px;
    margin-top: 0;
}
.col-md-5 .cmtk_dt {
    padding: 10px 0 117px !important;
}
.cmtk_group {
    text-align: center;
}
a.shopnow {
    padding: 10px 15px;
    background: #324A67;
    border-radius: 25px;
    color: #fff;
    font-weight: 600;
    font-family: 'Rubik';
}
</style>
<?php  
$ques_info = $this->mainModel->getUserQuestionnaireInfo($user_id);
if(count($ques_info)>0 && !isset($_GET['category_id'])){ 
?>
<div class="slider slidersize">
  <div class="container-fluid">
   <div class="row">
     <?php foreach ($ques_info as $catInfo) {   ?> 
        <div class="col-xl-6 col-lg-12">
          <div class="call-to-action home layout-two">
            <div class="content">
                <h2 class="main-title"><?php echo $catInfo['category_name']; ?><br></h2>
                <p class="simple-content"><?php echo $catInfo['short_description']; ?></p><br>
                <?php if($catInfo['questionnaire_status'] == 0): ?> 
                  <a style="margin-top:10px;" href="<?php echo base_url('cart'); ?>?category_id=<?php echo $catInfo['category_id']; ?>&batch_id=<?php echo $catInfo['batch_id']; ?>" class="btn btn-Shop">Book Doctor Appointment<i class="fas fa-long-arrow-alt-right"></i></a>
                   <?php elseif($catInfo['questionnaire_status'] == 1): ?> 
                   <a style="margin-top:10px;" href="#" class="btn btn-Shop">Wait for doctor approval<i class="fas fa-long-arrow-alt-right"></i></a>
                    <?php elseif($catInfo['questionnaire_status'] == 2): ?> 
                   <a style="margin-top:10px;" href="<?php echo base_url('checkout'); ?>?category_id=<?php echo $catInfo['category_id']; ?>&batch_id=<?php echo $catInfo['batch_id']; ?>" class="btn btn-Shop">Continue for Medicines payment<i class="fas fa-long-arrow-alt-right"></i></a>
                  <?php endif; ?> 
            </div>
           <div class="banner-image">
               <img style="width:87%" src="<?php echo base_url(); ?>assets/frontend/images/banner-img2.png" alt="img">
           </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<?php
//}else{
}else if(count($ques_info)>0){
$category_id = $_GET['category_id'];
$batch_id = $_GET['batch_id'];
$catInfo = $this->adminModel->getCategoryInfo($category_id);
$cart_data = $this->mainModel->checkUserCartByCat($user_id,$category_id);
$drugs = $this->adminModel->getDrugsListByCat($category_id);
//if(count($cart_data) == 0):
?> 
<form method="post" action="#" class="save_cart" enctype="multipart/form-data">
 <div class="slider slidersize">
      <div class="container-fluid">
        <div class="row" style="background: #bad6e2b0;margin: 0px 0px;padding: 20px 15px;" id="cartaha">
          <div class="col-md-6">
            <div class="slider-title ">
              <h5 class="title">Choice Of Medications for <?php echo $catInfo[0]['category_name']; ?></h5> 
            </div>
            <div class="col-md-12 col-sm-12">
                <label class="bmd-label-floating">Select Drug <span class="mandatory-label">*</span></label>
                <input type="hidden" class="category_id" value="<?php echo $category_id; ?>">
                 <input type="hidden" class="batch_id" value="<?php echo $batch_id; ?>">
                 <input type="hidden" class="user_id" value="">
              <div class="form-group">
                 <select name="drug_id" class="form-control fdrug_id" required="required">
                    <option value="">Select Drug</option>
                    <?php foreach ($drugs as $catInfo) { ?>                                
                    <option value="<?php echo $catInfo['drug_id']; ?>" <?php if(count($cart_data)>0 && $catInfo['drug_id'] == $cart_data[0]['drug_id']): echo "selected"; endif; ?>><?php echo $catInfo['drug_name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div> 

             <div class="col-md-12 col-sm-12">
                  <label class="bmd-label-floating">Dosage Strength<span class="mandatory-label">*</span></label>
                <div class="form-group">
                   <select name="drug_strength_id" class="form-control fdrug_strength_id" required="required">
                      <option value="">Select Strength</option>
                      <?php if(count($cart_data)>0): ?>
                        <option value="<?php echo $cart_data[0]['drug_strength_id']; ?>" <?php if(count($cart_data)>0): echo "selected"; endif; ?>><?php echo $cart_data[0]['strength']; ?></option>
                      <?php endif; ?>
                  </select>
                </div>
              </div> 
          </div>

          <div class="col-md-6 medicines_data ">
            <div class="slider-title ">
              <h2 class="title">Select Quantity<br/><br/></h2> 
            </div>
              <div class="thumbnail-content"> 
                <span class="price_list_data"></span>
            </div>
          </div>
           <?php if($cart_data[0]['payment_status'] == 0 || count($cart_data) == 0){ ?>
          <div class="col-md-3 medicines_data" style="text-align: center;">
            <button type="submit" class="btn btn-warning">Continue</button>
          </div>
          <?php }else{ ?>
            <div class="col-md-12 medicines_data" style="text-align: right;"><br/>
            <a href="<?php echo base_url('checkout'); ?>?category_id=<?php echo $category_id; ?>&batch_id=<?php echo $batch_id; ?>" class="btn btn-warning">Continue <i class="fas fa-forward"></i></a>
          </div>
          <?php } ?>  
        </div>
      </div>
    </div>
      

    <!-- <div class="slider slidersize ship_address_section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Shipping info & Address</h2>
              <h6>Where would you like to have your medication shipped, i</h6><h6>f prescribed? use your legal name</h6>
            </div>
             <div class="row">
             <div class="form-group col-md-6">
            <label for="usr">Name:</label>
            <input type="text" class="form-control ship_name" id="ship_name" required="required">
            </div>
          <div class="form-group col-md-6">
            <label for="usr">City</label>
            <input type="text" class="form-control ship_city" id="usr" required="required">
            </div>
          <div class="form-group col-md-6">
            <label for="usr">Street Address:</label>
            <input type="text" class="form-control street_address" id="usr" required="required">
            </div>
            <div class="form-group col-md-6">
            <label for="usr">Country</label>
            <input type="text" class="form-control country" id="usr" required="required">
            </div>
            <div class="form-group col-md-3">
            <label for="usr">State</label>
            <input type="text" class="form-control state" id="usr" required="required">
            </div>
            <div class="form-group col-md-3">
            <label for="usr">Zip</label>
            <input type="text" class="form-control zip" id="usr" required="required">
            </div>
           
          </div>
           <button type="submit" class="btn btn-warning">Submit</button>
          </div>
        </div>
      </div>
    </div> -->
    </form>
<?php } else{ ?>
 <div class="slider slidersize">
 <div class="container">
			<div class="row" role="alert">
			<div class="col-md-7">
					<div class="cmtk_group">
						<div class="cmtk_dt">
						
							<h1 class="thnk_coming_title1">Your Cart Is Currently Empty!<br/><br/><br/></h1>
							
							<p class="thnk_des">Beofore proceed you must add some product to your shopping cart.<br/>You will find a lot of interesting products on our "Shop Page"<br/><br/>
							
							<!-- <a href="invoice.html" class="shopnow">Shop Now</a></p> -->
						</div>
						
						
					</div> 	
				</div>	
				<div class="col-md-5">
					<div class="cmtk_group">
						
						<div class="cmtk_dt">
							<a href="http://localhost:81/bamn_old/"><img src="assets/img/emptycart.jpg" style="width:100%;height:100%;"/></a>
						</div>
						
					</div> 	
				</div>	
			</div>	
		</div>


    
  </div>

<?php  } ?>

   
     