<style>
  .about-text{
          border-style:solid;
          border-color: green;
          border-width: 2px; 
          border-radius:10px 10px;
          text-align:center;
  }
  .our-team{
    padding:0px !important;
  }
</style>

<?php include("header.php"); ?>
    <!-- about-us -->
    <div class="about-us">
      <div class="container">
        <div class="row align-items-center">
           
             <div class="col-lg-12 faq-content">
              <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">Learn about BeAlphaMan</h4>
              </div>              
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       What is BeAlphaMan?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      -BeAlphaMan is a one-stop telehealth service for men's wellness and care, providing treatment options for both hairloss and erectile dysfunction.  
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Why use BeAlphaMan?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      - BeAlphaMan provides men with high-quality prescription drugs at affordable prices, with the convenience of rapid and discreet home assessment and delivery.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                       Can we trust the medicines used? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                      -BeAlphaMan uses FDA approved medications from Verified-Accredited Wholesale Distributors (VAWD). 
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                       What states do you service? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="card-body">
                      -As of March 2021, we can ship to only New Jersey and New York.  If we don’t currently service your state please check back with us in a few months. 
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingFive">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      Why is BeAlphaMan only available in certain states?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                    <div class="card-body">
                      -Due to telemedicine laws, a physician has to be licensed in the state where the patient is located. Our medical providers are licensed in only select states, but we will be expanding soon.
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingSix">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                       How do I get started? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                    <div class="card-body">
                      -Our process is easy as 1-2-3! First, simply fill out our medical questionnaire that will be looked over by our medical doctors. Second, have an online consultation to see if you qualify to receive the medications. Three, have your medicine discreetly delivered to your home, and start enjoying life. 
                    </div>
                  </div>
                </div>
              </div>
      </div>

           <div class="col-lg-12 faq-content">
              <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">Learn about Telehealth</h4>
              </div>              
            </div>
            <div class="accordion" id="accordionExample1">
                <div class="card">
                  <div class="card-header" id="headingSeven">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                      Is telehealth legal and safe?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#accordionExample1">
                    <div class="card-body">
                      -Yes. BeAlphaMan’s medical platform allows our licensed physicians to give patients the same high-quality care they expect from in-person appointments without the hassle of leaving their homes. Just like any doctor’s visit, the physician will evaluate the patient and answer any questions and ultimately provide a proper treatment plan.  
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingEight">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      How do I have my consultation? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample1">
                    <div class="card-body">
                      -Each state has its own telehealth laws. It may include phone calls, video chats, emails, and text messages.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingNine">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                      Who are the doctors prescribing medications for BeAlphaMan?  
                      </button>
                    </h2>
                  </div>
                  <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample1">
                    <div class="card-body">
                      -BeAlphaMan uses US licensed and board certified medical doctors. 
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingTen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                      Can I use any pharmacy?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample1">
                    <div class="card-body">
                      -Yes, you may use any pharmacy of your choice but be ready to pay 3-5 times the price you would be paying with us. With our preferred pharmacy network, you will be getting the same medicine delivered discreetly to your home at a fraction of the price.
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingEleven">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseTEleven">
                      What happens if there's an emergency?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample1">
                    <div class="card-body">
                      -Please call 911 immediately.
                    </div>
                  </div>
                </div>
              </div>
      </div>

       <div class="col-lg-12 faq-content">
              <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">Pricing and Subscription Details
               </h4>
              </div>              
            </div>
            <div class="accordion" id="accordionExample2">
                <div class="card">
                  <div class="card-header" id="headingTwelve">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve">
                      Is BeAlphaMan covered by my insurance?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseTwelve" class="collapse show" aria-labelledby="headingTwelve" data-parent="#accordionExample2">
                    <div class="card-body">
                      -At this time, BeAlphaMan does not accept any insurances, but be aware that our services are less expensive than most peoples’ copayments.  
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThirteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                     How much is the online doctor visit?  
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -The cost for the online visit is only $25. If your physician determines you are not a candidate, you will receive a refund for the full amount. Other sites offer free online visits but at the end the final cost of everything is nearly double the price compared to ours. 
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingFourteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                     How much is the prescription medication?   
                      </button>
                    </h2>
                  </div>
                  <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -The cost varies depending on the medication prescribed. To get the whole list of prices go to the menu on our main page.  
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingFifteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                      How quickly will a doctor review my history and prescribe medication? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -You will be contacted within one business day. 
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingSixteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                     Are you required to have a subscription?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -No, you may use our services whenever you would like. However, many choose to have a subscription for discounts and to make sure they never run out of their treatment.
                    </div>
                  </div>
                </div>

                 <div class="card">
                  <div class="card-header" id="headingSeventeen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                     Does BeAlphaMan allow you to cancel your subscription?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -Yes, we offer the option to cancel, pause, or delay your subscription order.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingEighteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
                    Can I delay my next shipment?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseEighteen" class="collapse" aria-labelledby="headingEighteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -Yes, just let us know when you are ready for the next shipment. 
                    </div>
                  </div>
                </div>

                 <div class="card">
                  <div class="card-header" id="headingNineteen">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseNineteen" aria-expanded="false" aria-controls="collapseNineteen">
                     Can I change the frequency of my orders? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseNineteen" class="collapse" aria-labelledby="headingNineteen" data-parent="#accordionExample2">
                    <div class="card-body">
                      -Yes, we are here to make this as easy as possible. 
                    </div>
                  </div>
                </div>
                 <div class="card">
                  <div class="card-header" id="headingTwenty">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwenty" aria-expanded="false" aria-controls="collapseTwenty">
                    Can I order multiple months at one time?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwenty" class="collapse" aria-labelledby="headingTwenty" data-parent="#accordionExample2">
                    <div class="card-body">
                      -Yes, for most products you can order a 90-day supply. 
                    </div>
                  </div>
                </div>
                 <div class="card">
                  <div class="card-header" id="headingTwentyOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyOne" aria-expanded="false" aria-controls="collapseTwentyOne">
                    What is your return policy? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentyOne" class="collapse" aria-labelledby="headingTwentyOne" data-parent="#accordionExample2">
                    <div class="card-body">
                      -Unfortunately, the law doesn’t allow prescriptions to be returned.  
                    </div>
                  </div>
                </div>
              </div>
      </div>


        <div class="col-lg-12 faq-content">
              <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">Shipping</h4>
              </div>              
            </div>
            <div class="accordion" id="accordionExample3">
                <div class="card">
                  <div class="card-header" id="headingTwentyTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwentyTwo" aria-expanded="true" aria-controls="collapseTwentyTwo">
                      Where do you ship?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseTwentyTwo" class="collapse show" aria-labelledby="headingTwentyTwo" data-parent="#accordionExample3">
                    <div class="card-body">
                      -As of April 2021, we are only shipping to New Jersey and New York. If this is not one of your states, please check back with us in a few months.  
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwentyThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyThree" aria-expanded="false" aria-controls="collapseTwentyThree">
                     Do you ship internationally? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentyThree" class="collapse" aria-labelledby="headingTwentyThree" data-parent="#accordionExample3">
                    <div class="card-body">
                      - No, not at this time.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingTwentyFour">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyFour" aria-expanded="false" aria-controls="collapseTwentyFour">
                       Can I expedite shipping? 
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentyFour" class="collapse" aria-labelledby="headingTwentyFour" data-parent="#accordionExample3">
                    <div class="card-body">
                      -Unfortunately, we do not offer next day delivery. We currently use FedEx 2-day express shipping and USPS to deliver in the United States. 
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingTwentyFive">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyFive" aria-expanded="false" aria-controls="collapseTwentyFive">
                      How do I track my order?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentyFive" class="collapse" aria-labelledby="headingTwentyFive" data-parent="#accordionExample3">
                    <div class="card-body">
                      -Once your doctor has approved your treatment, you will be provided with a tracking number.  
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingTwentySix">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentySix" aria-expanded="false" aria-controls="collapseTwentySix">
                     Do I need to be home to sign for the order?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentySix" class="collapse" aria-labelledby="headingTwentySix" data-parent="#accordionExample3">
                    <div class="card-body">
                      -We do not require a signature upon receipt.
                    </div>
                  </div>
                </div>
                
                 <div class="card">
                  <div class="card-header" id="headingTwentySeven">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentySeven" aria-expanded="false" aria-controls="collapseTwentySeven">
                      Is the Free 2-Day Shipping guaranteed?  
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwentySeven" class="collapse" aria-labelledby="headingTwentySeven" data-parent="#accordionExample3">
                    <div class="card-body">
                      -We will do our part but unfortunately FedEx and USPS are not offering guarantees on delivery times due to their increased volume during the pandemic.  
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>
</div>
    
    <?php include("footer.php"); ?>