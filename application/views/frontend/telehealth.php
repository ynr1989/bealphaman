
<div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Telehealth</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Telehealth</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

<div class="container"> 
<h3>Consent to Telehealth</h3> 
<p>Date of last revision: March 31, 2021</p>
<p>YOU UNDERSTAND THAT BY CHECKING THE "AGREE" BOX FOR THESE TERMS OF USE AND/OR ANY OTHER SUCH FORM OF THE SAME PRESENTED TO YOU FROM TIME TO TIME ON THE SITE YOU ARE AGREEING TO THESE TERMS OF USE AND THAT SUCH ON-GOING ACTIONS IN USING THE SITE CONSTITUTE A LEGAL SIGNATURE AND ON-GOING AGREEMENT TO THESE TERMS OF USE (IN WHATEVER FORM)</p>

<p>All capitalized terms used in this Consent to Telehealth but not defined herein have the meanings assigned to them in the Terms of Use For avoidance of any doubt, the terms "BeAlphaMan", "we", "us", or "our" refer to Be Alpha Man LLC and the terms "you" and "yours" refer to the person using the Service</p>
<p>Telemedicine involves the delivery of healthcare services using electronic communications, information technology or other means between a healthcare provider and a member who are not in the same physical location Telemedicine may be used for diagnosis, treatment, follow-up and/or member education, and may include, but is not limited to:</p>
<ul><li>	Electronic transmission of medical records, photo images, personal health information or other data between a member and a healthcare provider;</li>
<li>	Interactions between a member and healthcare provider via audio, video and/or data communications; and</li>
<li>	Use of output data from medical devices, sound and video files</li></ul>
<p>The electronic systems used in the BeAlphaMan Service will incorporate network and software security protocols to protect the privacy and security of health information and imaging data, and will include measures to safeguard the data to ensure its integrity against intentional or unintentional corruption</p>
<h5>Possible Benefits of Telemedicine</h5>
<ul><li>	Can be easier and more efficient for you to access medical care and treatment</li>
<li>	You can obtain medical care and treatment at times that are convenient for you</li>
<li>	You can interact with providers without the necessity of an in-office appointment
Possible Risks of Telemedicine</li>
<li>	Information transmitted to your provider(s) may not be sufficient to allow for appropriate medical decision making by the provider(s)</li>
<li>	The inability of your provider(s) to conduct certain tests or assess vital signs in-person may in some cases prevent the provider(s) from providing a diagnosis or treatment or from identifying the need for emergency medical care or treatment for you</li>
<li>	Your provider may not able to provide medical treatment for your particular condition via telemedicine and you may be required to seek alternative care</li>
<li>Delays in medical evaluation/treatment could occur due to failures of the technology</li>
<li>	Security protocols or safeguards could fail causing a breach of privacy</li>
<li>	Given regulatory requirements in certain jurisdictions, your provider(s) treatment options, especially pertaining to certain prescriptions may be limited
By accepting this Consent to Telehealth, you acknowledge your understanding and agreement to the following:</li>
<li>	I have read this special Consent to Telehealth carefully, and understand the risks and benefits of the use of telemedicine in the medical care and treatment provided to me through BeAlphaMan platform by "Providers"</li>
<li>	I give my informed consent to the use of telemedicine by providers affiliated with BeAlphaMan</li>
<li>	I understand that the delivery of healthcare services via telemedicine is an evolving field and that the use of telemedicine in my medical care and treatment may include uses of technology not specifically described in this consent</li>
<li>	I understand that while the use of telemedicine may provide potential benefits to me, as with any medical care service no such benefits or specific results can be guaranteed My condition may not be cured or improved, and in some cases, may get worse</li>
<li>	I understand that "Providers" may determine in his or her sole discretion that my condition is not suitable for treatment using telemedicine, and that I may need to seek medical care and treatment in-person or from an alternative source</li>
<li>	I understand that the same confidentiality and privacy protections that apply to my other health care services also apply to these telemedicine services</li>
<li>	I understand that I have access to all of my health and wellness information pertaining to the telemedicine services in accordance with applicable laws and regulations</li>
<li>	I understand that I can withhold or withdraw this consent at any time by emailing BeAlphaMan with such instruction Otherwise, this consent will be considered renewed upon each new telemedicine consultation with "Providers"</li>
<li>	I agree and authorize my health care provider to share information regarding the telemedicine exam with other individuals for treatment, payment and health care operations purposes</li>
<li>	I agree and authorize my health care provider to release information regarding the telemedicine exam to BeAlphaMan and its affiliates</li>
</ul>
</div>
</div>

