 <style>
 div#newadress {
    font-size: 14px;
    color: #2874f0;
    font-weight: 500;
    padding: 16px 16px;
    border-bottom: 1px solid #f0f0f0;
    cursor: pointer;
    background: #fff;
    box-shadow: 1px 1px 1px #ccc;
	margin: 5px 0px;
}
div#oldadress {
    font-size: 14px;
    color: #000;
    font-weight: 500;
    padding: 16px 16px;
    border-bottom: 1px solid #f0f0f0;
    cursor: pointer;
    background: #fff;
    box-shadow: 1px 1px 1px #ccc;
	margin: 5px 0px;
}
a#edit {
    float: right;
}
 </style>
 
 <div class="slider slidersize">
      <div class="container-fluid">
        <div class="row"> 
          <?php 
          $this->load->view('templates/frontend/sidebar'); ?>
         <div class="col-xl-9 col-lg-8 col-md-12 order-first"> 
          <div class="row">
            <div class="col-md-12" id="newadress">
              <a href="<?php echo base_url('add_address'); ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add a New Address </a>
            </div>
            <?php if(count($addresses)>0): foreach ($addresses as $addressInfo) { ?>
			
              <div class="col-md-4" id="oldadress">
              <address>
                <strong><?php echo $addressInfo['address_line1']; ?></strong> <a id="edit" href="<?php echo base_url('edit_address'); ?>?ad_id=<?php echo $addressInfo['address_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i> EDIT</a><br>
                <?php if($addressInfo['address_line2']): ?>
                <?php echo $addressInfo['address_line2']; ?><br>
              <?php endif; ?>
                <?php echo $addressInfo['city']; ?>, <?php echo $addressInfo['state']; ?>, - <?php echo $addressInfo['zip']; ?><br>
                <!-- <abbr title="Phone">P:</abbr> <?php echo $addressInfo['ship_phone']; ?> -->
              </address> 
              <!-- <address>
                <strong>Full Name</strong><br>
                <a href="mailto:#">exam.ple@example.com</a>
              </address> -->
              </div> 
            <?php } else: ?>
                <div class="col-md-4">
                  <h4>No addresses found</h4>
                </div>
          <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
 
   
     