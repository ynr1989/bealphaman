<style>
h2.title {
    font-size: 1.75rem;
    font-family: 'Rubik' !important;
}
button.saveadd {
    background: #fb641b;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 20%);
    border: none;
    color: #fff;
    padding: 7px 14px;
}
label {
    margin-top: 20px;
    margin-bottom: 0px;
}
form.save_address {
    margin-top: -40px;
}
@media only screen and (max-width: 600px) {
  .col-md-12 {
    display: initial !important;
}
button#savebutton {
    margin-bottom: 15px;
}
}
</style>

<div class="product-single mt-0 pt-0">
     <div class="product-section">
      <div class="container">
        <div class="row">
             <?php 
          $this->load->view('templates/frontend/sidebar'); ?>
         <div class="col-xl-9 col-lg-8 col-md-12 order-first"> 
		 
		  <div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">Shipping Address</h3>
              </div>
              
            </div>
          </div>
          
              <form method="post" class="save_address">

                   <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
            
			<div class="col-md-12" style="display:flex">
             <div class="col-md-6">
            <label for="usr">Address Line1: <span>*</span></label>
            
			<textarea class="form-control address_line1" id="usr" name="addressLine1" rows="4" cols="10" tabindex="5" required="" autocomplete="street-address" placeholder="Address Line1"></textarea>
            </div>

             <div class="col-md-6">
            <label for="usr">Address Line2:</label>
			<textarea class="form-control address_line2" id="usr" name="address_line2" rows="4" cols="10" tabindex="5" autocomplete="street-address" placeholder="Address Line2"></textarea>
            
            </div>
			</div>
            
		   <div class="col-md-12" style="display:flex">
			 <div class="col-md-6">
            <label for="usr">City: <span>*</span></label>
            <input type="text" class="form-control ship_city" id="City" required="required" placeholder="City">
            </div>
			<div class="col-md-3">
            <label for="usr">State: <span>*</span></label>
            <input type="text" class="form-control state" id="State" required="required" placeholder="State">
            </div>
			 
			
			 
			 <div class="col-md-3">
            <label for="usr">Zipcode: <span>*</span></label>
            <input type="text" class="form-control zip" id="zip" maxlength="8" required="required" placeholder="Zipcode">
            </div>
			
			</div>
		  
		  
		  <br/>
            <button class="saveadd" type="submit" tabindex="10" id="savebutton" style="float: right;margin-right: 25px;margin-top: 10px;">Save New Address</button>
          </div>
           
          </div>
        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   
     