<style>

@media only screen and (max-width: 600px) {
  p.simple-content {
    font-size: 12px !important;
}
}
.call-to-action {
   
    padding: 45px 0 45px 20px !important;
   
} 
</style>
    <?php include("header.php"); ?>
<?php
$cat1DrugsInfo = $this->mainModel->getDrugsListByCat("1");
$cat2DrugsInfo = $this->mainModel->getDrugsListByCat("2");
?> 
    <!-- slider 
      <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Dashboard</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>  
    </div> 
    slider end -->

    <!-- product-single -->
    <div class="product-single mt-0 pt-0"> 
     <div class="product-section">
      <div class="container-fluid">
        <div class="row">
          <?php $this->load->view('templates/frontend/sidebar'); ?>  

         <div class="col-xl-9 col-lg-8 col-md-12 order-first">
           <div class="row" style="border-bottom: 1px dotted #1e2843;margin-bottom: 11px;margin-left: 4px;margin-right: 5px;font-family: 'Rubik';"> 

              <div class="header-title">

                <h5 class="title" style="margin-bottom: 10px;"><i class="fas fa-user-shield"></i> Welcome <strong><?php echo $rdata[0]['first_name']; ?> </strong></span></h5>

              <!--  <h6>You already completed Questionnaire -
               <?php foreach ($categories as $catInfo) {
                  $quiz1 = $this->mainModel->getQuestionsByCat($catInfo['category_id']);
                  $checkIsQues= $this->mainModel->checkIsQuesByUser($catInfo['category_id'],$user_id); ?> 
                           <?php if(count($checkIsQues) > 0): ?>
                            <strong><?php echo $catInfo['category_name']; ?> ,</strong>
                            <?php endif; ?> 

                       
                <?php } ?><br>
                Please click on treatment plan to Continue.
              </h6>
              -->
          </div> 
        </div>
                   <div class="row">
              <?php  
                $ques_info = $this->mainModel->getUserQuestionnaireInfo($user_id);
                if(count($ques_info)>0 && !isset($_GET['category_id'])){
                ?>
                
                     <?php foreach ($ques_info as $catInfo) {   ?> 
                        <div class="col-xl-6 col-lg-6">
                          <div class="call-to-action home layout-two">
                            <div class="content">
                                <h2 class="main-title"><?php echo $catInfo['category_name']; ?><br></h2>
                                <p class="simple-content"><?php echo $catInfo['short_description']; ?></p><br>
                                <?php if($catInfo['questionnaire_status'] == 0): ?> 
                                  <a style="margin-top:10px;" href="<?php echo base_url('cart'); ?>?category_id=<?php echo $catInfo['category_id']; ?>&batch_id=<?php echo $catInfo['batch_id']; ?>" class="btn btn-Shop">Book Doctor Appointment<i class="fas fa-long-arrow-alt-right"></i></a>
                                   <?php elseif($catInfo['questionnaire_status'] == 1): ?> 
                                   <a style="margin-top:10px;" href="#" class="btn btn-Shop">Waiting for doctor approval<i class="fas fa-long-arrow-alt-right"></i></a>
                                    <?php elseif($catInfo['questionnaire_status'] == 2): ?> 
                   <a style="margin-top:10px;" href="<?php echo base_url('checkout'); ?>?category_id=<?php echo $catInfo['category_id']; ?>&batch_id=<?php echo $catInfo['batch_id']; ?>" class="btn btn-Shop">Continue for Medicines payment<i class="fas fa-long-arrow-alt-right"></i></a>
                                  <?php endif; ?> 
                            </div>
                           <div class="banner-image">
                               <img style="width:87%" src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $catInfo['image']; ?>" alt="img">
                           </div>
                          </div>
                        </div>
                      <?php } ?>
                  
              <?php }

                    $ques_info1 = $this->mainModel->getUserNonQuestionnaireInfo($user_id);
                     if(count($ques_info1)>0){ ?>

                    <?php  count($ques_info1); foreach ($ques_info1 as $catInfo) {
                    $quiz1 = $this->mainModel->getQuestionsByCat($catInfo['category_id']);   ?> 
                        <div class="col-xl-6 col-lg-6">
                          <div class="call-to-action home layout-two">
                            <div class="content">
                                <h2 class="main-title"><?php echo $catInfo['category_name']; ?><br></h2>
                                <p class="simple-content"><?php echo $catInfo['short_description']; ?></p><br>
                                    <a style="margin-top:10px;" href="<?php echo base_url('ed_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo $catInfo['category_id']; ?>&n=1&t=<?php echo count($quiz1) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                                   
                            </div>
                           <div class="banner-image">
                               <img style="width:87%" src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $catInfo['image']; ?>" alt="img">
                           </div>
                          </div>
                        </div>
                      <?php } ?>

              <?php } ?>

                </div>
                


             <!--  <div class="row">
               <?php foreach ($categories as $catInfo) {
                  $quiz1 = $this->mainModel->getQuestionsByCat($catInfo['category_id']);
                  $checkIsQues= $this->mainModel->checkIsQuesByUser($catInfo['category_id'],$user_id); ?> 
                  <div class="col-xl-6 col-lg-6">
                    <div class="call-to-action home layout-two">
                      <div class="content">
                          <h2 class="main-title"><?php echo $catInfo['category_name']; ?><br></h2>
                          <p class="simple-content"><?php echo $catInfo['short_description']; ?></p><br>
                          <?php if(count($checkIsQues) > 0): ?>
                            <a href="<?php echo base_url('my_questionnaire'); ?>?eid=<?php echo $catInfo['category_id']; ?>&view=1" class="btn btn-Shop">View Info<i class="fas fa-long-arrow-alt-right"></i></a> 
                            <a style="margin-top:10px;" href="<?php echo base_url('cart'); ?>?category_id=<?php echo $catInfo['category_id']; ?>" class="btn btn-Shop">Choose Treatement Plan<i class="fas fa-long-arrow-alt-right"></i></a>
                            <?php else: ?>
                              <a href="<?php echo base_url('ed_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo $catInfo['category_id']; ?>&n=1&t=<?php echo count($quiz1) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                            <?php endif; ?> 

                      </div>
                     <div class="banner-image">
                         <img style="width:87%" src="<?php echo base_url(); ?>assets/frontend/images/banner-img2.png" alt="img">
                     </div>
                    </div>
                  </div>
                <?php } ?>
              </div> -->


              </div>              
            </div>

        </div>
      </div>
    </div>
          
        </div>
      </div>
    </div>
    <!-- product-single end -->

    <!-- newsletter-section -->
    <!-- <div class="newsletter-section style-two">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-title style-two">
              <h2 class="title">Newsletter!</h2>
              <p class="content my-30y55">It only takes a second to be the first to find <br>
                out about our latest news</p>
            </div>
            <div class="meditree-subscription style-two">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Your email address...">
                <div class="input-group-append">
                  <button class="input-group-text">SUBMIT</button>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div> -->
    <!-- newsletter-section end -->

    <!-- shop-features -->
    <!-- <div class="shop-features style-two">
      <div class="container">
        <div class="row justify-content-lg-center">
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon1.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Free Shipping</h5>
                <p class="content">Free shipping world wide</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon2.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Easy Return</h5>
                <p class="content">Money Back Guarantee</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon3.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Support 24/7</h5>
                <p class="content">Online Support 24/7</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="shop-single-features style-two">
              <div class="ssf-img">
                <img src="images/h2-icon4.png" alt="img">
              </div>
              <div class="ssf-content">
                <h5 class="title">Secure Payments</h5>
                <p class="content">100% payment protection</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- shop-Features end -->
  </div>
 <?php include("footer.php"); ?>
   
<style>
* {
  box-sizing: border-box;
}

.columns {
  float: left;
  width: 23.3%;
  padding: 0px;
}

.price {
  list-style-type: none;
  border: 1px solid #eee;
  margin: 0;
  padding: 0;
  -webkit-transition: 0.3s;
  transition: 0.3s;
}

.price:hover {
  box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
  background-color: #111;
  color: white;
  font-size: 25px;
}

.price li {
  border-bottom: 1px solid #eee;
  padding: 10px;
  text-align: center;
}

.price .grey {
  background-color: #eee;
  font-size: 20px;
}

.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 25px;
  text-align: center;
  text-decoration: none;
  font-size: 18px;
}

@media only screen and (max-width: 600px) {
  .columns {
    width: 100%;
  }
}
</style>
   