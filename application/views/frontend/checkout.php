<style>
  .breadcrumb-item{ 
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white;
    margin-left: 15px;
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97; 
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
.section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}
.shiptext{
   word-wrap: break-word;
}
 .backcart_btn{
  width: 15%;
      font-size: 17px;
  }   
  h3.title {
    font-size: 1.5rem;
    font-family: 'Rubik' !important;
    font-weight: 600;
    text-align: left;
    margin-top: 20px;
}
input#datacart,.input-group,select.form-control{
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    margin-left: -16px;
}
select.form-control {
    background: #fff !important;
    margin: 12px -15px;
    width: 108%;
}
i.fa.fa-calendar {
    margin-top: 10px;
}
label.form-check-label {
    font-weight: 700;
}
strong {
    font-weight: 400 !important;
    font-family: 'Heebo' !important;
}
.slider-title h5 {
    text-align: left;
    font-size: 14px;
    font-family: 'Rubik';
}
.slider-title h6 {
    font-size: 14px;
    text-align: left;
    font-family: 'Rubik';
    font-weight: 400;
}
hr {
    margin: 10px 0px !important;
    border-top: 1px dotted rgb(0 0 0 / 50%) !important;
}
button.btn.btn-warning {
    height: 48px;
    width: 200px;
    text-transform: uppercase;
    font-size: 16px;
    font-weight: 500;
    float: right;
    background: #0d1f35;
    box-shadow: 0px 1px 2px 1px rgb(28 28 28 / 20%);
    border: none;
    color: #fff;
}
.slider.slidersize .col-md-4 {
    border: 1px solid #ccc;
    margin: 0px 0px 70px;
    box-shadow: 1px 2px 3px #28a745;
}
.slider.slidersize {
    padding-top: 10px !important;
}
label.control-label.col-sm-4.requiredField {
    margin-left: -12px;
}
.widget .title::after {
    content: ' ';
    display: block;
    background-color: #1574f6;
    height: 2px;
    width: 30px;
    position: absolute;
    top: 40px;
    left: 0;
}

.membership_chk_bg {
    background: #fff;
    width: 100%;
    float: left;
    padding: 30px;
    border-radius: 10px;
    border: 1px solid #efefef;
    transition: all .2s ease-in-out;
    margin-bottom: 30px;
}
.address_text {
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    float: left;
    width: 100%;
    text-align: left;
    line-height: 24px;
    margin-top: 10px;
}
.rght1528 {
    position: sticky;
    top: 90px;
}
.order_dt_section {
    float: left;
    width: 100%;
    margin-top: 7px;
}
.order_title {
    float: left;
    width: 100%;
    padding: 20px 0;
    border-bottom: 1px solid #efefef;
	display:flex;
}
.order_title .order_price {
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    text-align: right;
    float: right;
    width: 30%;
    margin-bottom: 0;
}
.membership_chk_bg {
    background: #fff;
    width: 100%;
    float: left;
    padding: 30px;
    border-radius: 10px;
    border: 1px solid #efefef;
    transition: all .2s ease-in-out;
    margin-bottom: 30px;
}
.scr_text {
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    text-align: center;
    float: left;
    width: 100%;
    margin-top: 27px;
}
.order_title h4 {
    font-size: 16px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    margin-bottom: 0;
    line-height: 24px;
    width: 70%;
    text-align: left;
}

.order_title h6 {
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_title h2 {
    font-size: 20px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_title .order_price5 {
    font-size: 20px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    text-align: right;
    float: right;
    width: 30%;
    margin-bottom: 0;
}
.checkout_title h4 {
    font-size: 18px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 10px !important;
    color: #333;
    text-align: left;
    line-height: 26px;
}
.order_dt_section {
    float: left;
    width: 100%;
    margin-top: 7px;
}
.chckot_btn {
    height: 40px;
    padding: 0 20px;
    border: 0;
    margin-top: 30px;
    float: right;
    color: #fff;
    border-radius: 20px;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    background: #ed2a26;
    display: block;
}
.order_title h4 {
    font-size: 16px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    margin-bottom: 0;
    line-height: 24px;
    width: 70%;
    text-align: left;
}
.order_title h3 {
    font-size: 18px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_dt_section {
    margin-top: -10px !important;
}
@media only screen and (max-width: 600px) {
  .input-group {
    width: 107% !important;
}
button.btn.btn-warning {
    margin-bottom: 10px;
}
.order_title {
    display: block;
}
}



</style>
<?php 

$batch_id = $_GET['batch_id'];
$category_id = $_GET['category_id'];
$cart_data = $this->mainModel->checkUserCheckoutData($user_id,$batch_id,$category_id); 
$catInfo = $this->adminModel->getCategoryInfo($category_id); 
?> 
<div class="col-md-12"> 
            <a class="nav-link" href="<?php echo base_url('cart'); ?>" style="font-weight:700;"><i class="fas fa-arrow-circle-left"></i> Back to Cart</a>
          </div>


<form method="post" action="<?php echo base_url('confirmCheckout'); ?>" >
 <input type="hidden" name="payment_id" value="<?php echo $this->encryption->encrypt($cart_data[0]['cart_id']); ?>">
  <?php if($cart_data[0]['send_to_pharmacy'] == 0){ ?>
 <div class="slider slidersize">
      <div class="container-fluid">
 
        <div class="row"> 
<div class="col-md-4">

			

            <div class="slider-title ">
			
              <h3 class="title" style="text-align: left;">1. Shipping Info</h3>  
            </div>

            <div>
              
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Free 2 Day Shipping<br></p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Discreet Packaging</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Guaranteed Pricing</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Non Childproof, single-dose packaging</p>
              <p class="shiptext"><i class="fa fa-check" aria-hidden="true"></i> Email Notifications on the shipping status</p>
            </div>
              <?php $row = $this->mainModel->getTypes('SHIP_ADDRESS_TYPE'); 
                foreach($row as $data){
                  if($data['code'] == 1):
                ?>
              <div class="form-check" style="margin-top:10px;">
                <input class="form-check-input ship_address_type" type="radio" name="ship_address_type" id="flexRadioDefault<?php echo $data['code']; ?>"  <?php if($data['code']==1): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
                <label class="form-check-label" for="flexRadioDefault<?php echo $data['code']; ?>">
                  <?php echo $data['value']; ?>
                </label>
              </div>
            <?php endif;  } ?>  
           

              <div class="col-md-12" style="margin-top:10px;"> 
                 <strong style="font-weight: 700 !important;margin-left: -12px;"><i class="fas fa-caret-right"></i> Select Your Home Address :</strong>  
				 
                <span style="float:right;"><a style="font-size:12px;font-weight:600;" href="<?php echo base_url('add_address'); ?>"><i class="fas fa-plus-square"></i> Add New Address <i class="fas fa-street-view"></i><br/><br/></a></span>
              <select class="form-control user_address_id" name="user_address_id" required="required">
               <option value="">Select Address</option>
              <?php 
                foreach($addresses as $data){
                ?>
               <option value="<?php echo $data['address_id']; ?>"><?php echo $data['address_line1']; ?>, <?php echo $data['city']; ?>, <?php echo $data['state']; ?> - <?php echo $data['zip']; ?></option>
             
            <?php } ?>
            </select>
			
            </div>


            <?php if($cart_data[0]['payment_status'] == 0): ?>

            
              <?php $row = $this->mainModel->getTypes('SHIP_ADDRESS_TYPE'); 
                foreach($row as $data){
                  if($data['code'] == 2):
                ?>
              <div class="form-check" style="margin-top:20px;">
                <input class="form-check-input ship_address_type" type="radio" name="ship_address_type" id="flexRadioDefault<?php echo $data['code']; ?>"  <?php if($data['code']==1): echo "checked"; endif; ?> value="<?php echo $data['code']; ?>">
                <label class="form-check-label" for="flexRadioDefault<?php echo $data['code']; ?>">
                  <?php echo $data['value']; ?>
                </label>
              </div>
            <?php endif; } ?>
            

            <?php elseif($cart_data[0]['ship_address_type'] == 2):
              $pharmacyInfo = $this->mainModel->getUserShipPharmacyAddress($cart_data[0]['cart_id']); ?>
               <h3><b>Patient Pharmacy Address</b></h3>
           <div class="col-md-12 foundation_sm">
           <ul>
            <li><?php echo $pharmacyInfo[0]['pharmacy_name'];  ?> </li>
            <li><?php echo $pharmacyInfo[0]['street_address'];  ?> , <?php echo $pharmacyInfo[0]['city'];  ?> </li>
             <li><?php echo $pharmacyInfo[0]['state'];  ?> , <?php echo $pharmacyInfo[0]['zip'];  ?> </li> 
           <li><i class="fa fa-phone" aria-hidden="true"></i><?php echo $pharmacyInfo[0]['phone'];  ?>  </li>
           <li>Fax: <?php echo $pharmacyInfo[0]['fax'];  ?> </li></ul>
           <?php
             endif; ?>  


             <?php if($cart_data[0]['appointment_date']!="" && $cart_data[0]['payment_status'] == 2): ?>
                 <strong>Appointment Info:</strong>
                <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label  requiredField" for="date">
                   <strong>Appointment Date :</strong> <?php echo $cart_data[0]['appointment_date']; ?> <?php echo  date("g:i a", strtotime($cart_data[0]['appointment_time'])); ?>
                   
                  </label>
                  
                 </div>
               </div>

                

              
              <?php elseif($cart_data[0]['payment_status'] == 0): ?>
                 
              <?php endif; ?>


<div class="col-md-12 ship_address_section">
           
            <div class="slider slidersize">
              <div class="container">
                <div class="row">
                  <div class="col-md-12" style="margin-left:-27px;">
                    <div class="slider-title">
                      <h5 class="title">Enter Your Pharmacy Address</h5>
                      <h6>Please enter name and address of your pharmacy of choice.</h6>
                    </div>
                     <div class="row">
                     <div class="form-group col-md-12">
                    <label for="usr">Pharmacy Name: (*)</label>
                    <input type="text" class="form-control pharmacy_name" name="pharmacy_name" placeholder="Pharmacy Name">
                    </div>
                  
                   <div class="form-group col-md-12">
                    <label for="usr">Pharmacy Phone: (*)</label>
                    <input type="text" class="form-control pharmacy_phone" name="phone" placeholder="Phone Number" >
                    </div>
                      
                  <div class="form-group col-md-12">
                    <label for="usr">Street Address:</label>
                    <input type="text" class="form-control street_address" name="street_address" placeholder="Street Address" >
                    </div>

                    <div class="form-group col-md-6">
                    <label for="usr">City</label>
                    <input type="text" class="form-control ship_city" name="city" placeholder="City">
                    </div>
 
                    <div class="form-group col-md-6">
                    <label for="usr">State</label>
                    <input type="text" class="form-control state" name="state" placeholder="State">
                    </div>
                    <div class="form-group col-md-6">
                    <label for="usr">Zip</label>
                    <input type="text" class="form-control zip" name="zip" placeholder="Zip Code">
                    </div>
                   
                  </div> 
                  </div>
                </div>
              </div>
            </div>
         </div>
           </div> 

       

         

         <div class="col-md-4">
		 <h3 class="title">2. Appointment Info</h3>
		 
                <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label col-sm-4 requiredField" for="date">
                 
                   <span class="asteriskField">
                    <strong>Select Date</strong>
                   </span>
                  </label>
                  <div class="col-sm-10">
                   <div class="input-group" style="width: 130%;margin: 0px;padding: 0px 9px 0px 11px;margin-left: -14px;">
                    <div class="input-group-addon">
                     
                    </div>
                    <input class="form-control appointment_date" id="datacart" readonly="readonly" autocomplete="off" placeholder="Select Date" name="appointment_date" type="text" required value="<?php echo $cart_data[0]['appointment_date']; ?>" style="background: none;border: none;"/><i class="fa fa-calendar">
                     </i>
                    
                   </div>
                  </div> 
                 </div>
				 
				 <div class="col-md-12 col-sm-12 col-xs-12 slots_data">
				 <span class="asteriskField">
                    <strong>Select Date</strong>
                   </span>
                   <label class="control-label col-sm-4 requiredField" for="date">
                   Select Time
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                </div>
	 
				
               </div>
			   </div>
          <div class="col-md-4">
            <div class="slider-title ">
              <h3 class="title">3. Finish your visit</h3> 
              <h6 class="title">Your treatment if prescribed</h6>
            </div>
             
            <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong><?php echo $cart_data[0]['drug_name']; ?></strong><br>
                 <?php echo $cart_data[0]['strength']; ?>
              </div>
             
              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>$<?php echo $cart_data[0]['price']; ?></strong>
              </div> 
              <div class="col-md-12">
                <p style="background: #1574f6;
    color: #fff;
    padding: 10px;font-size:12px;">You won't be charged for or shipped any prescription products unless prescribed by a doctor through our platform.</p>
              </div>
            </div>

            <hr>

            <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong>Online doctor visit</strong> 
              </div>

              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>$<?php echo $cart_data[0]['doctor_consultation_fee']; ?></strong>
              </div> 
            </div>
            <hr>
            <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong>Shipping</strong> 
              </div>

              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>FREE</strong>
              </div>   
            </div>


             <hr>
             <div class="container row">
              <div class="col-md-6 col-sm-6">
                 <strong>Due Now</strong> 
              </div>

              <div class="col-md-6 col-sm-6" style="text-align: right;">
                 </strong>$<?php
                 if($cart_data[0]['payment_status'] == 0){
                  echo $cart_data[0]['doctor_consultation_fee'];
                  }else{
                    echo $cart_data[0]['price'];
                  } ?></strong>
              </div> 
            </div>

            <hr>
             <div class="container row">
              <div class="col-md-6 col-sm-6"> 
              </div>

               <?php if($cart_data[0]['payment_status'] == 1 && $cart_data[0]['doctor_approve_status'] == 0){ ?>
              <div class="col-md-7 col-sm-7" style="text-align: right;">
                 <a href="#" class="btn btn-warning">Wait for Doctor Approval</a>
              </div> 
              <?php }else{  ?>
                <div class="col-md-6 col-sm-6" style="text-align: right;">
                 <button type="submit" class="btn btn-warning validPay">Pay</button>
                 <button type="button" class="btn btn-warning validatePay">Pay</button>
              </div>
              <?php }  ?> 
            </div>
          </div>


        


      </div>
    </div>
	
	<?php }else{
    $user_info = $this->mainModel->getUserInfoById($user_id);
$user_address = $this->mainModel->getUserAddressById($user_id,$cart_data[0]['address_id']);
   ?>
	<div class="summary">
      <div class="container-fluid">
<div class="row">
<div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h6 class="title">Summary</h6>
              </div>
              
            </div>
			
			<div class="container">			
				<div class="row">
					<div class="col-lg-8">
						<div class="membership_chk_bg">
							<div class="checkout_title">
								<h4>Billing Details</h4>
								<img src="images/line.svg" alt="">
							</div>
							
							<div class="address_text">
								<?php echo $user_info[0]['first_name']." ".$user_info[0]['last_name']; ?><br>
							<?php echo $user_info[0]['first_name']." ".$user_info[0]['last_name']; ?><br>
                <?php echo $user_address[0]['address_line1']; ?>, <?php echo $user_address[0]['city']; ?>,
                <br> <?php echo $user_address[0]['state']; ?>- <?php echo $user_address[0]['zip']; ?> 
                <br>
                  United States
							</div>
						</div>
							<div class="membership_chk_bg">
							
							
						
							<div class="chckout_order_dt">
								<div class="checkout_title">
									<h4>Order Details</h4>
									
								</div>
								<div class="order_dt_section">
									<div class="order_title">
										<h6>Category: <br/><strong style="font-weight: 500 !important;color: #000;"><i class="fas fa-file-medical-alt"></i> <?php echo $catInfo[0]['category_name']; ?></strong></h6>
										<h6>Drug Info : <br/><strong style="font-weight: 500 !important;color: #000;"><?php echo $cart_data[0]['drug_name'].",".$cart_data[0]['strength']; ?></strong></h6>
										<h6>Appointment : <br/><strong style="font-weight: 500 !important;color: #000;"><i class="far fa-calendar-plus"></i><?php echo $cart_data[0]['appointment_date']; ?>, <i class="far fa-clock"></i> <?php echo $cart_data[0]['appointment_time']; ?></strong></h6>
										<div class="order_price">$<?php echo $cart_data[0]['drug_price']; ?></div>
									</div>
									<div class="order_title">
										<h6>Online Doctor Visit (Already Paid)</h6>
										<div class="order_price">$<?php echo $cart_data[0]['doctor_consultation_fee']; ?></div>
									</div>
									<!-- <div class="order_title">
										<h6>Discount</h6>
										<div class="order_price">$10</div>
									</div> -->
									<!-- <div class="order_title">
										<h3>Total</h3>
										<div class="order_price">$<?php echo $cart_data[0]['drug_price']+$cart_data[0]['doctor_consultation_fee']; ?></div>
									</div> -->
									<!--<button class="chckot_btn" type="submit">Confirm Checkout</button>-->
								</div>
							</div>
						</div>						
					</div>

          <?php
                 if($cart_data[0]['payment_status'] == 0){
                  $price =  $cart_data[0]['doctor_consultation_fee'];
                  }else{
                    $price = $cart_data[0]['price'];
                  } ?>

					<div class="col-lg-4">
						<div class="membership_chk_bg rght1528">
								<div class="checkout_title">
									<h4>Order Summary</h4>
									<img src="images/line.svg" alt="">
								</div>
								<div class="order_dt_section">
									<div class="order_title">
										<h4>Precription Price</h4>
										<div class="order_price">$<?php echo $price; ?></div>
									</div>
									<!-- <div class="order_title">
										<h6>Discount Price</h6>
										<div class="order_price">$5</div>
									</div> -->
									<div class="order_title">
										<h2>Total</h2>
										<div class="order_price5">$<?php echo $price; ?></div>
									</div>
									<div class="scr_text">
                    <?php if($cart_data[0]['payment_status'] == 1 && $cart_data[0]['send_to_pharmacy'] == 0): ?>
                    <i class="uil uil-lock-alt"></i> 
                 <a class="btn btn-warning">Pay after Appointment</a>
                 <?php else: ?>
                  <button type="submit" class="btn btn-warning">Pay</button>
                 <?php endif; ?>
               
            </div>
								</div>
						
									
							
						</div>
					</div>								
				</div>				
			</div>
          <hr/>
        <?php } ?>
		  
	
		  
		  <hr/>
		  
		  </div>
		  </div>
		  </div>
		  </div>
    </form>
 

<script type="text/javascript">
  $(".slots_data").hide();
  $(".validPay").hide();
  $(".appointment_date").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate:new Date(),
    onSelect: function(date, instance) {
      console.log(date, instance);
        $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url('get_appointment_slots'); ?>",
              data: "a_date="+date,
              success: function(result)
              { $(".slots_data").show();
                var resultData = $.parseJSON(result);
                if(resultData.status == true)
                {
                  $(".validatePay").hide();
                  $(".validPay").show();
                  $(".slots_data").html(resultData.rdata);
                }else{
                  $(".validatePay").show();
                  $(".validPay").hide();
                  $(".slots_data").html(resultData.message);
                }
              }
         });  
     }
});
  
$(".validatePay").click(function(){
  alert("Please schdule appointment.");
});  
</script>
     