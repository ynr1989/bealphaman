<style>
  .breadcrumb-item{
   border: 1px solid #1574f6;
    width:45%; 
    height:45%;
    padding-top:30px;
    margin-bottom:20px;
    padding-bottom:30px;
    background: white;
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97;
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
   .section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}
.call-to-action.layout-two .banner-image {
    position: absolute;
    top: 17%;
    left: 50%;
	right:10%;
}
/*.col-md-12 {
    background: #ffffffc9 !important;
    padding: 15px 0px;
    box-shadow: 2px 10px 20px #eee;
}*/
input.btn.btn-primary {
    width: 10%;
    float: left !important;
    margin-left: 45%;
    margin-top: 12px;
    margin-bottom: 20px;
    background: linear-gradient(
180deg
, #324A67, #1d2733) !important;
    font-weight: 600;
}
.slider-title h3 {
    font-size: 1.3rem !important;
    font-weight: 600;
	color:#1d2733 !important;
	margin: 10px 50px;
	text-align: left !important;
}
label {
    margin: 0px 25px;
    margin-left: 60px;
    text-indent: -60px;
}
h5.title {
    font-size: 1.0rem;
    font-weight: 400 !important;
    margin: 10px 100px;
    line-height: 24px;
    line-break: anywhere;
}
</style>
<?php
if(@$_GET['q']== 'quiz' && @$_GET['step']== 2 && isset($_POST['qsub'])) 
{
  $category_id=@$_GET['eid'];
  $sn=@$_GET['n'];
  $total=@$_GET['t'];
  $ans=$_POST['ans'];
  $qid=@$_GET['qid'];

  $qdata = ["category_id" => $category_id, "question_id" => $qid, "option_id" => $ans];  
  $_SESSION['questionarray'][] = $qdata;

  if($sn != $total)
  {
    $sn++;
    if($category_id == 1){
      $qurl = base_url('ed_questionnaire');
    }else{
      $qurl = base_url('hair_loss_questionnaire');
    }
   
    header("location:$qurl?q=quiz&step=2&eid=$category_id&n=$sn&t=$total")or die('Error152');
    exit();
  }else{  
    $_SESSION['is_quiz'] = "1"; 
    //unset($_SESSION['ukey']);
    $_SESSION['quiz_category'] = $category_id; 
    if($_SESSION['user_id']!=""){
     $this->mainModel->insertUserQuestionnaire($_SESSION['quiz_category'],$_SESSION['questionarray']);
     redirect(base_url('questionnaire_complete'), 'Location');
    }else{
      header("location:".base_url('login')) or die('Error152');
    }
    //echo "<pre>"; print_r($_SESSION['questionarray']); echo "</pre>";die();
  }
}


if(@$_GET['q']== 'quiz' && @$_GET['step']== 2) 
{    
    $category_id=@$_GET['eid'];
    $sn=@$_GET['n'];
    $total=@$_GET['t'];

    if(isset($_SESSION['user_id'])){ 

    }else if(!isset($_SESSION['ukey']) && $sn == 1){
      $_SESSION['ukey'] = $this->mainModel->generateEmailKey();
    }else if($_SESSION['ukey'] == ""){
      header("location:".base_url())or die('Error152');
    }
    //echo "dd=d=".$_SESSION['ukey'];
    $question = $this->mainModel->getQuestionById($category_id,$sn);
    $qid=$question[0]['question_id'];
    if($category_id == 1){
      $qurl = base_url('ed_questionnaire');
    }else{
      $qurl = base_url('hair_loss_questionnaire');
    }
    $question_options = $this->mainModel->getOptionsByQyestion($question[0]['question_id']);
     echo '<form action='.$qurl.'?q=quiz&step=2&eid='.$category_id.'&n='.$sn.'&t='.$total.'&qid='.$qid.' method="POST"  class="form-horizontal"> ';
      ?>
    <!-- slider --> 
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h3 class="title"><?php echo $question[0]['question_name']; ?></h3>
              <h5 class="title"><?php echo $question[0]['question_description']; ?></h5>
            </div>

            <?php foreach ($question_options as $options) { ?> 
             <div class="thumbnail-content">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"> <label ><input required type="radio" class="radio" name="ans" value="<?php echo $options['option_id']; ?>"><span class="label"><?php echo $options['question_option']; ?></span></label>
                <!-- <img src="<?php echo base_url(); ?>assets/frontend/images/avatar.jpg" class="section-img1" > --></li>                
              </ol>
            </nav>
            </div>
           <?php } ?>            
          </div>
          </div>
          <div style="text-align: center;">  
              <?php
              $num = $_GET['n'];
              $num = $num - 1;
              $query = $_GET;
              $query['n'] = $num; 
              $query_result = http_build_query($query); 
              $baseUrl = base_url('ed_questionnaire')."?";
              if($num!=0){
                $prevLink =  $baseUrl.$query_result;               
               echo'<a href="'.$prevLink.'" style="background: linear-gradient( 180deg , #324A67, #1d2733) !important; font-weight: 600;color:#fff;" class="btn"><span class="glyphicon glyphicon-lock" aria-hidden="true" style="color: #fff;">Previous</span></a>';
              }
               ?>
            

           <?php //echo'<input type="submit" name="qsub" value="Next" class="btn btn-primary" style="margin-top: 0px;"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></form>';

            echo'<input type="submit" name="qsub" value="Next" class="btn" style="margin-top: 0px;background: linear-gradient( 180deg , #324A67, #1d2733) !important; font-weight: 600;color:#fff;"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></form>';
             ?> 
          </div>


          <div >

             <!-- <a class="btn btn-Shop" href="<?php echo base_url('quiz'); ?>"><span class="getstarted-span">
            Next</span></a> -->
            <!-- <h6 class="sign">Already have an account?<a class="under" href="<?php echo base_url('login'); ?>"><span class="signin"> Sign in</span></a></h6> -->
          </div>
        
      </div>
    </div>
    <!-- slider end -->
<?php }else{ ?>
    <!-- about-us -->
    <div class="about-us"> 
      <div class="container"> 
                    <div class="row">
                      <?php foreach ($categories as $catInfo) {
                      $quiz1 = $this->mainModel->getQuestionsByCat($catInfo['category_id']); ?> 
                      <div class="col-xl-6 col-lg-6">
                        <div class="call-to-action home layout-two">
                          <div class="content">
                              <h2 class="main-title"><?php echo $catInfo['category_name']; ?> </h2>
                              <p class="simple-content"><?php echo $catInfo['short_description']; ?></p>
                              <a href="<?php echo base_url('ed_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo $catInfo['category_id']; ?>&n=1&t=<?php echo count($quiz1) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                          </div>
                         <div class="banner-image">
                             <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $catInfo['image']; ?>" alt="img">
                         </div>
                        </div>
                      </div>
                    <?php } ?>
                      <!-- <div class="col-xl-6 col-lg-6">
                        <div class="call-to-action home layout-two">
                          <div class="content">
                              <h2 class="main-title">Hair Loss <br>
                             Treatment</h2>
                              <p class="simple-content">Stay safe, stay home. Let's stop it together</p>
                              <a href="<?php echo base_url('hair_loss_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo HAIR_LOSS_CATEGORY; ?>&n=1&t=<?php echo count($quiz2) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                          </div>
                        <div class="banner-image">
                            <img src="<?php echo base_url(); ?>assets/frontend/images/banner-img3.png" alt="img">
                        </div>
                      </div>
                      </div> -->
                    </div>
                </div>


      </div>
    <?php } ?>
    </div>

    