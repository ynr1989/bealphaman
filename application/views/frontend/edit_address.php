<style>
button.btn.btn-warning {
    background: #fb641b !important;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 20%);
    border-radius: 0px !important;
    color: #fff;
    padding: 7px 14px !important;
    float: right !important;
}
.inner-header-top.style-three {
    margin-bottom: 20px !important;
}
@media only screen and (max-width: 600px) {
  button.btn.btn-warning {
    margin-bottom: 15px;
}
}
</style>

<div class="product-single mt-0 pt-0">
     <div class="product-section">
      <div class="container">
        <div class="row">
             <?php 
          $this->load->view('templates/frontend/sidebar'); ?>
         <div class="col-xl-9 col-lg-8 col-md-12 order-first"> 
           
			  <div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">Shipping info & Address</h3>
              </div>
              
            </div>
          </div>
			  
			  
              <form method="post" class="update_address">

                <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
            <input type="hidden" name="address_id" class="address_id" value="<?php echo $rdata[0]['address_id']; ?>">
            
             <div class="row">

          <div class="form-group col-md-6">
            <label for="usr">Address Line 1:</label> 
            <textarea class="form-control address_line1" id="usr" name="addressLine1" rows="4" cols="10" tabindex="5" required="" autocomplete="street-address" placeholder="Address Line1"><?php echo $rdata[0]['address_line1']; ?></textarea>
            </div> 

            <div class="form-group col-md-6">
            <label for="usr">Address Line 2:</label>
           <textarea class="form-control address_line2" id="usr" name="address_line2" rows="4" cols="10" tabindex="5" autocomplete="street-address" placeholder="Address Line2"><?php echo $rdata[0]['address_line2']; ?></textarea>
            </div> 
          <div class="form-group col-md-6">
            <label for="usr">City</label>
            <input type="text" class="form-control ship_city" value="<?php echo $rdata[0]['city']; ?>" id="usr" required="required">
            </div>
          
            <div class="form-group col-md-3">
            <label for="usr">State</label>
            <input type="text" class="form-control state" value="<?php echo $rdata[0]['state']; ?>" id="usr" required="required">
            </div>
            <div class="form-group col-md-3">
            <label for="usr">Zipcode</label>
            <input type="text" class="form-control zip" maxlength="8" value="<?php echo $rdata[0]['zip']; ?>" id="usr" required="required">
            </div>
           
          </div>
           <button type="submit" class="btn btn-warning">Submit</button>
          </div>
        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   
     