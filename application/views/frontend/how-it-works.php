<?php include("header.php"); ?>
<style>
h4.name {
    margin-top: 23px !important;
    font-weight: 500 !important;
}
.thumbnail-content {
    box-shadow: 10px 10px 5px #aaaaaa;
}
.thumbnail-content img:hover {
    transform: rotateY( 180deg );
}
.about-content .content .main-title {
    font-size: 48px !important;
}
.thumbnail .thumbnail-content {
  
    min-height: 184px;
}
</style>
    <!-- slider --> 
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">How It Works</h2>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">How It Works</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- slider end -->

    <!-- about-us -->
    <!--<div class="about-us">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/about-img1.png" alt="img">
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content">
                
                <h1 class="main-title">Coming Soon</h1>
                <p class="simple-content"></p>
              </div>
            

            </div>
          </div>
        </div>
      </div>
    </div>-->
	
	<div class="our-team">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-md-12">
                    <div class="section-title style-three">
                        <!-- <p class="content">Meet Our Team</p> -->
                        <h2 class="title">How to be an Alpha Man? </h2>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                  <div class="thumbnail">
                    <!-- <div class="thumbnail-image">
                      <img src="images/team3.jpg" alt="img">
                    </div> -->

                    <div class="thumbnail-content">
                       <img style="width:58px;height:60px" src="<?php echo base_url(); ?>assets/frontend/images/questionnaire.png" alt="img">
                      <h4 class="name">Complete Online Questionnaire</h4>
                      <!-- <span class="profession">Doctor</span> -->
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                  <div class="thumbnail">
                    <!-- <div class="thumbnail-image">
                      <img src="images/team1.jpg" alt="img">
                    </div> -->
                     
                    <div class="thumbnail-content">
                      <img style="width:58px;height:60px" src="<?php echo base_url(); ?>assets/frontend/images/doctor.png" alt="img">
                      <h4 class="name">Speak to a Licensed Healthcare Provider</h4>
                      <!-- <span class="profession">Doctor</span> -->
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                  <div class="thumbnail">
                    <!-- <div class="thumbnail-image">
                      <img src="images/team2.jpg" alt="img">
                    </div> -->
                   
                    <div class="thumbnail-content">
                       <img style="width:58px;height:60px" src="<?php echo base_url(); ?>assets/frontend/images/shipping.png" alt="img">
                      <h4 class="name">Medication Discreetly Delivered to You</h4>
                      <!-- <span class="profession">Doctor</span> -->
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div>
               <!--  <div class="col-xl-3 col-lg-4 col-md-6">
                  <div class="thumbnail">
                     <div class="thumbnail-image">
                      <img src="images/team3.jpg" alt="img">
                    </div>
                    <div class="thumbnail-content">
                      <h4 class="name">Sharon A. Robinson</h4>
                      <span class="profession">Doctor</span>
                    </div>
                    <div class="thumbnail-social-icon">

                    </div>
                  </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- our team end-->
     <div class="about-us">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/strut4.png" alt="img">
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h1 class="main-title"> Complete Online Questionnaire
                 </h1>
                <p class="simple-content">We will be asking you some simple questions about your lifestyle, symptoms, and past medical history in order to create a Patient Health Profile. (Similar to what you have to fill out at a doctor’s office)

                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


     <div class="about-us">
      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h1 class="main-title"> Speak to a Licensed Healthcare Provider

                 </h1>
                <p class="simple-content">Once we have gathered your information, we will connect you with a qualified healthcare provider. During your consultation, you will get a chance to discuss your symptoms, concerns, and potential treatment options.

                </p>
              </div>
            </div>
          </div>
             <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/strut5.png" alt="img">
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="about-us">
	<div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="about-image">
              <img src="<?php echo base_url(); ?>assets/frontend/images/strut6.png" alt="img">
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="about-content">
              <div class="content">
               <!--  <span class="pre-text">About Us</span> -->
                <h1 class="main-title"> Medication Discreetly Delivered to You
                 </h1>
                <p class="simple-content">Members will have the option to have their medicine delivered directly to their door with our FREE Delivery Service.

                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php include("footer.php"); ?>