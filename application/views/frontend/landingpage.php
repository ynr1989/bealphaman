<style type="text/css">
  .banner-section{
    background: #fff !important;
  }
  .call-to-action.home{
    background: none;
  }
  .main-title, .layout-two p{
    color:#fff !important;
  }
  .layout-two{
    background-color: #0e0f11 !important;
    border-radius: 20px;
     padding: 1px 0 34px 22px;
  }
  .call-to-action.call-to-action-large {
    padding: 39px 0 87px 0px !important;
  }
 .call-to-action.layout-two .banner-image{
  position: absolute;
    top: 3%;
    left: 70%;
 }
 .simple-content{
  color: #fff !important;
 }
 .lbanner{
      background-image: url("<?php echo base_url(); ?>/assets/img/banner.jpg");
    background-repeat: no-repeat;
    background-size: auto 100%;
 }
 .header-section {
    padding: 15px 0 15px 0;
    background: linear-gradient(
180deg
, #324A67, #151F2B) !important;
 }
 .logotitle {
    color: #ffffff !important;
}
div#navbarSupportedContent li a {
    color: #fff !important;
}
.layout-two {
    background-color: #151F2B !important;
    
}
.feature-section {
    background: #151F2B !important;
    padding: 40px 0px;
}
.call-to-action.layout-two .banner-image {
    position: absolute;
    top: 14% !important;
    left: 60% !important;
}
.feature-thumbnail .image {
    box-shadow: 1px 1px 2px !important;
}
.feature-thumbnail .image:hover {
    transform: scale(1.1) !important;
}

h2.main-title {
    text-shadow: 1px 1px 5px #1e2843;
}
.testimonial-section {
    background-color: #f5f9ff !important;
    padding: 30px 0px 180px !important;
    margin-top: 35px !important;
}
.blog-section {
    margin: 0px !important;
}
.footer-section {
    background-color: #fff !important;
    padding: 40px 0 0px !important;
}
.header-section .header-right-area .header-right-menu ul li a i {
    color: #ffffff !important;
    font-size: 20px;
}
span.user-account,a#navbarDropdown {
    color: #fff !important;
    font-weight: 800 !important;
}
.header-top .social-follow ul li a {
    
    padding: 0px 2px !important;
    
}
.feature-thumbnail {
    background-color: #C1DFE9 !important;
    
}
.Product-offer.mt-30 {
    background: #BAD6E2;
    padding: 25px 0px;
    margin-top: 0px !important;
}
.col-lg-4.col-md-6 img {
    box-shadow: 1px 4px 10px #aaaaaa;
}

i.fas.fa-shopping-cart {
    color: #fff !important;
}
<!--mediaquery-->
@media only screen and (max-width: 600px) {
  .col-xl-8.col-lg-12.col-md-12.lbanner {
        background-repeat: round;
    margin-left: 0px;
  background-image:url(assets/img/Mobilebanner.jpg);
}
.header-right-menu {
    margin-left: -60px !important;
  margin-right: -10px;
}
img#op1 {
    width: 100%;
    margin: 15px 0px;
}

}

@media only screen and (max-width: 600px) {
  .col-xl-8.col-lg-12.col-md-12.lbanner {
        background-repeat: round;
    margin-left: 0px;
  background-image:url(assets/img/Mobilebanner.jpg);
}
.header-right-menu {
    margin-left: -60px !important;
  margin-right: -10px;
}
img#op1 {
    width: 100%;
    margin: 15px 0px;
}
}
.single-testimonial .testimonial-info {
  
    padding: 9px 30px !important;
   
}
.testimonial-info {
    min-height: 285px;
}
p#boxmodel {
    border: 1px solid rgb(41 132 48);
    padding: 10px !important;
    text-align: center;
    border-radius: 15px;
}
</style>
<?php
unset($_SESSION['questionarray']);
$quiz1 = $this->mainModel->getQuestionsByCat(ED_CATEGORY); 
$quiz2 = $this->mainModel->getQuestionsByCat(HAIR_LOSS_CATEGORY); 
?>
    <!-- banner -->
    <div class="banner-section pt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 lbanner">
                    <div class="call-to-action home call-to-action-large">
                        <div class="content">
                            <!-- <span class="pre-text">Premium Quality</span> -->
                            <h5 class="main-title" style="color:#000 !important;margin-left:40px;text-shadow: 1px 1px 1px #08c;">The Future of <br/>Men's Healthcare.</h5>
                            <p class="simple-content" style="color:#000 !important;margin-left:40px;text-shadow: 1px 1px 1px #08c;">Meds prescribed and shipped discreetly to you.</p>
                         <!--    <a href="<?php echo base_url('ed_questionnaire'); ?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a> -->
                        </div>
                      <!-- <div class="banner-image">
                           <img src="images/banner-img1.png" alt="img">
                       </div>--> 
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12 col-md-12">
                    <div class="row">
                      <?php foreach ($categories as $catInfo) {
                      $quiz1 = $this->mainModel->getQuestionsByCat($catInfo['category_id']); ?> 
                      <div class="col-xl-12 col-lg-6">
                        <div class="call-to-action home layout-two">
                          <div class="content">
                              <h2 class="main-title" style=" font-size: 20px;"><?php echo $catInfo['category_name']; ?><br></h2>
                              <p class="simple-content"><?php echo $catInfo['short_description']; ?></p><br>
                              <a href="<?php echo base_url('ed_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo $catInfo['category_id']; ?>&n=1&t=<?php echo count($quiz1) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                          </div>
                         <div class="banner-image">
                             <img style="width:87%;border-radius: 8px" src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $catInfo['image']; ?>" alt="img">
                         </div>
                        </div>
                      </div>
                    <?php } ?>
                      <!-- <div class="col-xl-12 col-lg-6">
                        <div class="call-to-action home layout-two">
                          <div class="content">
                              <h2 class="main-title">Hair Loss <br>
                             Treatment</h2>
                              <p class="simple-content">Stay safe, stay home.<br/> Let's stop it together</p>
                              <a href="<?php echo base_url('hair_loss_questionnaire'); ?>?q=quiz&step=2&eid=<?php echo HAIR_LOSS_CATEGORY; ?>&n=1&t=<?php echo count($quiz2) ;?>" class="btn btn-Shop">Start Now<i class="fas fa-long-arrow-alt-right"></i></a>
                          </div>
                        <div class="banner-image">
                            <img src="<?php echo base_url(); ?>assets/frontend/images/banner-img3.png" alt="img">
                        </div>
                      </div>
                      </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner end -->
<!--about sale-->
<div class="Product-offer mt-30">
      <div class="container-fluid"> 
    <div class="section-title">
              <h3 class="title mb-4" style="font-size: 30px;font-weight: bold;font-family: &quot;Heebo&quot;, sans-serif;color: #1e2843;margin-bottom: 0;">How to be an Alpha Man?</h3>
               
            </div>
        <div class="row justify-content-md-center">
    
          <div class="col-lg-4 col-md-6">
            <p id="boxmodel">Complete Online Questionnaire</p>
          </div>
          <div class="col-lg-4 col-md-6">
           <p id="boxmodel">Speak to a Licensed Healthcare Provider</p>
          </div>
          <div class="col-lg-4 col-md-6">
            <p id="boxmodel">Medication Discreetly Delivered to You</p>
          </div>
      </div>
      
      </div>
    </div>
<!--end-->
    <!-- feature section -->
   <!--  <div class="feature-section">
      <div class="container-fluid">
        <div class="row justify-content-lg-center">
     <div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title" style="color:#fff;">Our Services</h3>
              </div>
              
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon1.png" alt="img">
              </div>
              <a href="#" class="title">Pharmacy <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon2.png" alt="img">
              </div>
              <a href="#" class="title">Blood Pressure <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon3.png" alt="img">
              </div>
              <a href="#" class="title">Independent Living <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon4.png" alt="img">
              </div>
              <a href="#" class="title">Pharmacy <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon5.png" alt="img">
              </div>
              <a href="#" class="title">Accessories <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
          <div class="col-xl-2 col-lg-3 col-md-4">
            <div class="feature-thumbnail">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/frontend/images/icon6.png" alt="img">
              </div>
              <a href="#" class="title">Pharmacy <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- feature section end-->
<br><br>
    <!-- Features Products-->
       <div class="container-fluid">
        <div class="row justify-content-lg-center">
           <div class="col-md-12">
            <div class="inner-header-top style-three">
              <div class="header-title">
                <h3 class="title">Our Products</h3>
              </div>
              
            </div>
          </div>
          <div class="col-12">
            <!-- <div class="owl-carousel features-product-carousel owl-theme">
                <div class="item">
                  <div class="single-products style-two">
                    <div class="products-image">
                     <img src="images/pro-img11.jpg" alt="img">
                    </div>
                    <div class="product-content">
                      <a href="#"><h5 class="product-name">Skin</h5></a>
                      <span class="price">$45.00</span>
                      <a href="single-product.html" class="btn btn-cart"><i class="fas fa-shopping-cart"></i> Add to cart</a>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="single-products style-two">
                    <div class="products-image">
                      <img src="images/pro-img4.jpg" alt="img">
                    </div>
                    <div class="product-content">
                      <a href="#"><h5 class="product-name">Primary Care</h5></a>
                      <span class="price">$45.00</span>
                      <a href="single-product.html" class="btn btn-cart"><i class="fas fa-shopping-cart"></i> Add to cart</a>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="single-products style-two">
                    <div class="products-image">
                      <img src="images/pro-img3.jpg" alt="img">
                    </div>
                    <div class="product-content">
                      <a href="#"><h5 class="product-name">Mental Health</h5></a>
                      <span class="price">$45.00</span>
                      <a href="single-product.html" class="btn btn-cart"><i class="fas fa-shopping-cart"></i> Add to cart</a>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="single-products style-two">
                    <div class="products-image">
                      <img src="images/pro-img4.jpg" alt="img">
                    </div>
                    <div class="product-content">
                      <a href="#"><h5 class="product-name">Hair</h5></a>
                      <span class="price">$45.00</span>
                      <a href="single-product.html" class="btn btn-cart"><i class="fas fa-shopping-cart"></i> Add to cart</a>
                    </div>
                  </div>
                </div>
            </div> -->


          </div> 
        </div>
      </div>
     
    <!-- Features Products end-->

    <!-- Product offer -->
    <div class="Product-offer mt-30">
      <div class="container-fluid"> 
        <div class="row justify-content-md-center">
          <div class="col-lg-4 col-md-6">
            <img src="assets/frontend/images/minoxidil1.png" alt="img" id="op1">
          </div>
          <div class="col-lg-4 col-md-6">
           <img src="assets/frontend/images/minoxidil.png" alt="img" id="op1">
          </div>
          <div class="col-lg-4 col-md-6">
            <img src="assets/frontend/images/minoxidil2.png" alt="img" id="op1">
          </div>
      </div>
      <br/>
      <div class="row justify-content-md-center">
      <div class="col-lg-4 col-md-6">
            <img src="assets/frontend/images/minoxidil4.png" alt="img" id="op1">
          </div>
      
      <div class="col-lg-4 col-md-6">
            <img src="assets/frontend/images/minoxidil5.png" alt="img" id="op1">
          </div>
      
      <div class="col-lg-4 col-md-6">
            <img src="assets/frontend/images/minoxidil6.png" alt="img" id="op1">
          </div>
      
      
      
        </div>
      </div>
    </div>
    <!-- Product offer end-->
 

    <!-- testimonial -->
    <div class="testimonial-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-title">
              <h3 class="title" style="font-size: 30px;font-weight: bold;font-family: &quot;Heebo&quot;, sans-serif;color: #1e2843;margin-bottom: 0;">Testimonials</h3>
               <p class="content mb-5">What customer say about our amazing products</p>
            </div>
            <div class="testimonial-slide owl-carousel">
              <?php foreach ($testdata as $testdatas) { ?>
              <div class="single-testimonial">
                <div class="testimonial-icon">
                  <img src="<?php echo base_url(); ?>assets/frontend/images/icon7.png" alt="img">
                </div>
                <div class="testimonial-info">
                  <div class="testimonial-pic">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/ticon.png" alt="img">
                  </div>
                  <div class="testimonial-content">
                    <div class="testimonial-name">
                      <span><?php echo $testdatas['author_name']; ?></span>
                    </div>
                    <p class="content"><?php echo $testdatas['description']; ?></p>  
                  </div>
                </div>
              </div>
               <?php } ?>
               
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- testimonial end-->

    <!-- -blog-section -->
    <div class="blog-section">
      <div class="container">
       <!-- <div class="row justify-content-md-center">
          <div class="col-md-12">
            <div class="inner-header-top style-two">
              <div class="header-title">
                <h3 class="title">Latest Blog Posts</h3>
              </div>
              <div class="header-button">
                <a href="#" class="btn btn-product"><i class="fas fa-chevron-left"></i></a>
                <a href="#" class="btn btn-product active"><i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="owl-carousel blog-carousel owl-theme">
              <div class="item">
                <div class="single-blog">
                  <div class="blog-img">
                    <img src="images/blog-img1.jpg" alt="img">
                  </div>
                  <div class="blog-info">
                    <span class="date">February 20, 2020</span>
                    <h4 class="title"><a href="blog-single.html"> Proof that Vitamin D is a key factor in beating Covid-19</a></h4>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="single-blog">
                  <div class="blog-img">
                    <img src="images/blog-img2.jpg" alt="img">
                  </div>
                  <div class="blog-info">
                    <span class="date">February 20, 2020</span>
                    <h4 class="title"><a href="blog-single.html"> The key to Covid-19 prevention is in all of us</a></h4>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="single-blog">
                  <div class="blog-img">
                    <img src="images/blog-img3.jpg" alt="img">
                  </div>
                  <div class="blog-info">
                    <span class="date">February 20, 2020</span>
                    <h4 class="title"><a href="blog-single.html">How Vitamin C can protect against the risk of coronavirus</a></h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>-->
      </div>
    </div>
    <!-- blog end -->

