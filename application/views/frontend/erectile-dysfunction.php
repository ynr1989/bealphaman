<?php include("header.php"); ?>
<style>
.faq-content .accordion .card .card-header .btn-link::after {
    right: 2% !important;
   }
.inner-header-top.style-three {
    margin-top: 40px !important;
}
h2.blog-title {
    min-height: 80px !important;
}
p.blog-content {
    min-height: 206px !important;
}
.blog {
    padding: 100px 0 15px;
}
div#accordionExample {
    margin-top: -160px !important;
}

@media only screen and (max-width: 600px) {
  .inner-header-top.style-three {
    margin-left: 20px;
}
.single-blog-item {
    margin-bottom: 120px;
    margin-left: 20px;
}
.card {
    margin-left: 15px;
}
}
</style>
<?php
$quiz1 = $this->mainModel->getQuestionsByCat("1");
$edurl = base_url('ed_questionnaire')."?q=quiz&step=2&eid=1&n=1&t=".count($quiz1);
?>
    <!-- slider -->
    <div class="slider slidersize">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="slider-title ">
              <h2 class="title">Erectile Dysfunction</h2>
            </div>
            
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Erectile Dysfunction</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- slider end -->

    <!-- blog-section -->
    <div class="blog mt-0">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="single-blog-item">
              <div class="blog-image">
                <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/images/strut6.png" alt="img"></a>
              
              </div>
            </div>
          </div>
            <div class="col-lg-6"> 

              <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">Erectile Dysfunction</h4>
              </div>              
            </div> 

              <p class="blog-content" style="min-height:0px !important;">Don’t let anything stop you from performing like an AlphaMan. Get ED meds delivered directly to you!
           </p>
           <ul>
           <li>
            FDA approved meds</li>
            <li>No doctor visits</li>
            <li>Prescribed by a licensed professional</li>
            <li>Free and discreet delivery</li>
            <li>Cheaper than most of our competitors</li>
           </ul>
              <a href="<?php echo $edurl; ?>" class="btn btn-Shop Read-More">Start My Visit<i class="fas fa-long-arrow-alt-right"></i></a>
            </div>

            <div class="col-lg-12 faq-content">

      <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title pb-0">About Erectile Dysfunction</h4>
              </div>              
            </div> 

            <div class="single-blog-item"> 
              <p class="blog-content">Erectile Dysfunction (ED) is the inability to attain or maintain an erection needed for satisfying sexual performance. Research has found that it affects an estimated 18 million men in the United States alone.  </p> 
            </div>

            <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        What Causes ED?
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      <i class="fas fa-hand-point-right"></i> Erectile Dysfunction is caused by age, obesity, smoking, neurological disorders, psychological states  -including stress and depression- and vascular disease among others. The enzyme PDE5 is responsible for not providing enough blood flow into the penis for a proper erection. 
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Can it be Treated?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      <i class="fas fa-hand-point-right"></i> The good news is that there are many treatments for ED and they include a group of drugs called PDE5 inhibitors. Following sexual stimulation, PDE5 inhibitors increase blood flow to the penis, causing an erection.
                    </div>
                  </div>
                </div>
                
                
                
              </div>

          </div>

 <div class="col-lg-12">
      <div class="inner-header-top style-three">
              <div class="header-title">
                <h4 class="title">Our ED Treatment Options</h4>
              </div>              
            </div>
          </div>

            <div class="col-lg-4">

            <div class="single-blog-item">

              <h2 class="blog-title"><a href="#">Sidenafil 20mg</a></h2>
              <p class="blog-content">Sildenafil (20mg) is the active ingredient of Viagra, but it is NOT the actual generic of Viagra! Most of our competitors sell this one as their main and more affordable product. The most common dose for a man with ED is 50-100mg, so WHY take 3 to 5 pills at a time just to achieve the same effects of taking a single pill?</p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-4">
            <div class="single-blog-item">
              <h2 class="blog-title"><a href="#">Sildenafil<br> (Generic Viagra)</a></h2>
              <p class="blog-content">Sildenafil is the actual generic form of the brand Viagra.  It is our most common treatment for ED since a single pill is all that is required. It’s available in 25mg, 50mg, and 100mg.  </p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
            </div>
          <div class="col-lg-4">
            <div class="single-blog-item"> 
              <h2 class="blog-title"><a href="#">Tadalafil<br>(Generic Cialis)</a></h2>
              <p class="blog-content">Tadalafil is the generic form of the brand Cialis and is a medication used to treat erectile dysfunction. It offers particularly long lasting effects in comparison to the other treatment options available and can produce results for up to 36 hours after taking a single pill. Perfect for a weekend getaway!</p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
            </div>
          <div class="col-lg-4">
            <div class="single-blog-item">
              <h2 class="blog-title"><a href="#">Tadalafil Daily<br>(Generic Cialis)</a></h2>
              <p class="blog-content">Tadalafil Daily is available in two strengths of 2.5 mg and 5 mg.  It is taken once a day at the same time regardless if you are planning to have sex or not BUT just know that you will always be ready when the time comes.  </p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
            </div>
          <div class="col-lg-4">
            <div class="single-blog-item">
              <h2 class="blog-title"><a href="#">Cialis</a></h2>
              <p class="blog-content">Cialis is the original product manufactured by Eli Lilly and is better known as the “weekend pill” or the “yellow pill.”</p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
            </div>
          <div class="col-lg-4">
            <div class="single-blog-item"> 
              <h2 class="blog-title"><a href="#">Viagra</a></h2>
              <p class="blog-content">Viagra is the original product manufactured by Pfizer and prescribed by doctors for over 20 years. Better know as the “Blue Pill.”</p>
              <!--<a href="#" class="btn btn-Shop Read-More">Read More<i class="fas fa-long-arrow-alt-right"></i></a>-->
            </div>
           
          </div>
         <!--  <div class="col-lg-4">
            <div class="sidebar">
              <div class="sidebar-item">
                <form class="form-inline">
                  <input class="form-control sidebar-form mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  <button class="btn btn-search my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                </form>
              </div>
              <div class="sidebar-item">
                <div class="categoris">
                  <h3 class="sidebar-title">categoris</h3>
                  <div class="categori-list">
                    <ul>
                      <li><a href="shop.html"> Diabetes Care</a></li>
                      <li><a href="shop.html"> First Aid</a></li>
                      <li><a href="shop.html"> Incontinence</a></li>
                      <li><a href="shop.html"> Respiratory</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="sidebar-item">
                <div class="Latest-Posts">
                  <h3 class="sidebar-title">Latest Posts</h3>
                  <div class="post-list">
                    <div class="single-post">
                      <div class="image">
                        <a href="#"><img src="images/blog-s1.jpg" alt="img"></a>
                      </div>
                      <div class="content">
                        <h4 class="title"><a href="blog-single.html"> Corem ipsum dolor tetur adipisicing elit</a></h4>
                        <span><i class="far fa-calendar-alt"></i> 28 JANUARY, 2020</span>
                      </div>
                    </div>
                    <div class="single-post">
                      <div class="image">
                        <a href="#"><img src="images/blog-s2.jpg" alt="img"></a>
                      </div>
                      <div class="content">
                        <h4 class="title"><a href="blog-single.html"> Fdipisicing elit, sed do eiusmod tempor</a></h4>
                        <span><i class="far fa-calendar-alt"></i> 28 JANUARY, 2020</span>
                      </div>
                    </div>
                    <div class="single-post">
                      <div class="image">
                        <a href="#"><img src="images/blog-s3.jpg" alt="img"></a>
                      </div>
                      <div class="content">
                        <h4 class="title"><a href="blog-single.html"> Eonsectetur adipisicing elit, sed do eiusmod</a></h4>
                        <span><i class="far fa-calendar-alt"></i> 28 JANUARY, 2020</span>
                      </div>
                    </div>
                    <div class="single-post">
                      <div class="image">
                        <a href="#"><img src="images/blog-s4.jpg" alt="img"></a>
                      </div>
                      <div class="content">
                        <h4 class="title"><a href="blog-single.html"> gnim ad minim veniam, quis nostrud</a></h4>
                        <span><i class="far fa-calendar-alt"></i> 28 JANUARY, 2020</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="sidebar-item">
                <div class="Tags">
                  <h3 class="sidebar-title">Tags</h3>
                  <div class="tag-list">
                    <a href="#" class="btn btn-tag">lab</a>
                    <a href="#" class="btn btn-tag">kits</a>
                    <a href="#" class="btn btn-tag">aids</a>
                    <a href="#" class="btn btn-tag">Diabetes</a>
                    <a href="#" class="btn btn-tag">soaps</a>
                    <a href="#" class="btn btn-tag">gloves</a>
                    <a href="#" class="btn btn-tag">pain</a>
                  </div>
                </div>
              </div>
             
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <!-- blog-section end -->


   <?php include("footer.php"); ?>