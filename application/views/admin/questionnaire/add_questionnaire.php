<div class="page-head-line">Add Medicines</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="add_questionnaire" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">

                  <div class="row">

                    <div class="col-md-12 col-sm-12">
                          <label class="bmd-label-floating">Select Category <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="category_id" class="form-control category_id" required="required">
                              <option value="">Select Category</option>
                              <?php foreach ($cats as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['category_id']; ?>"><?php echo $catInfo['category_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                      <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Question <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <textarea name="question_name" required="required" placeholder="Add Question here.." class="form-control question_name"></textarea>
                        </div>
                      </div> 

                      <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Question Description</label>
                        <div class="form-group">                        
                          <textarea name="question_description" placeholder="More Info here.."  class="form-control question_description"></textarea>
                        </div>
                      </div>

                      <div class="col-md-12 col-12">
                          <label>Option 1 <i class="fa fa-plus add_questionnaire_opts"></i></label>
                    <div class="questionnaire_div">
                        <div class="item-data">                              
                            <div class="form-group ">
                                   
                                    <input class="form-control ques_options" id="ques_options-0" name="ques_options[]" type="text">
                            </div>                                        
                        </div>
                    </div>
                      </div>

                  
                    </div> 

            </div>
            
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/questionnaire'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        