<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <a href="<?php echo base_url('admin/add_questionnaire'); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div></div>
                <!-- /. ROW  -->


                <form>

            <div class="row">
                  
              

              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control selectpicker" name="category_id" id="category_id" title="Category">
                        <option value="">Select Category</option>
                      <?php   
                      foreach($cats as $drdata){ ?>
                       <option value="<?php echo $drdata['category_id']; ?>" <?php if($_GET && $_GET['category_id'] == $drdata['category_id']): echo "selected"; endif; ?>><?php echo $drdata['category_name']; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div> 

              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
           <a href="<?php echo base_url('admin/questionnaire'); ?>" class="btn btn-primary pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form>
            <br><br>

              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
               
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="drugPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Category</th>
                        <th>Batch</th>
                        <th>Question</th>                
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php
                          if(isset($_GET['category_id'])){
                             $questions = $this->adminModel->getQuestionsBYCat($_GET['category_id']);
                          }else{
                             $questions = $this->adminModel->getQuestions();
                          }                         
                           $i=1; foreach($questions as $data): ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                          <td><?php echo $data['category_name']; ?></td> 
                          <td><?php echo $data['batch_id']; ?></td> 
                          <td><?php echo $data['question_name']; ?></td>
                           <td>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-question_id="<?php echo $data['question_id']; ?>" class="questionStatusChange" <?php if($data['question_status'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url('admin/edit_drug'); ?>?question_id=<?php echo $data['question_id']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url('admin/delete_questionnaire'); ?>?question_id=<?php echo $data['question_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td> </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>