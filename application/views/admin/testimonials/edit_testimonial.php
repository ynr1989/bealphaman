<div class="page-head-line">Edit Testimonial</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
             <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div> 

             <form method="post" action="#" class="update_testimonial" enctype="multipart/form-data">
                 <div  class="col-md-8 col-sm-12">
                  <input type="hidden" class="testimonial_id" id="testimonial_id" value="<?php echo $rdata[0]['testimonial_id'];?>">
                  <input type="hidden" id="old_image" class="old_image" value="<?php echo $rdata[0]['image'];?>">
                  <div class="row">
                    
                    
                   <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Author Name </label>
                        <div class="form-group">                        
                          <input type="text" name="author_name" placeholder="Author Name" maxlength="25" class="form-control author_name" value="<?php echo $rdata[0]['author_name']; ?>">
                        </div>
                      </div>  

                       <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Description </label>
                        <div class="form-group">                        
                          <textarea type="text" style="min-height: 200px;" name="description" placeholder="Description"  class="form-control description"><?php echo $rdata[0]['description']; ?></textarea>
                        </div>
                      </div> 

                       <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Auther Image</label>
                        <div class="form-group">                        
                          <input type="file" name="image" id="image" maxlength="25" class="form-control image">
                          <?php
                          if($rdata[0]['image']!=""):?>
                          <img style="width: 50px;height: 50px;" src="<?php echo TESTIMONIALS_IMG_URL.$rdata[0]['image'];?> ">
                          <?php else: echo ''; endif; ?>
                        </div>
                      </div> 
           
</div> 
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/testimonials'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        