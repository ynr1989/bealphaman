<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <h1 class="page-head-line">Testimonials List</h1>  
                      <a href="<?php echo base_url('admin/add_testimonial'); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div></div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
               
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="drugPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Image</th>
                        <th>Author Name</th>
                        <th>Description</th>            
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($rdata as $data): ?>
                        <tr>
                           <td><?php echo $i++; ?></td> 
                          <td><?php
                          if($data['image']!=""):?>
                          <img style="width: 50px;height: 50px;" src="<?php echo TESTIMONIALS_IMG_URL.$data['image'];?> ">
                          <?php else: echo ''; endif; ?></td>
                          <td><?php echo $data['author_name']; ?></td>
                          <td><?php echo $data['description']; ?></td> 
                           <td>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-testimonial_id="<?php echo $data['testimonial_id']; ?>" class="testimonial_status_update" <?php if($data['status'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url('admin/edit_testimonial'); ?>?testimonial_id=<?php echo $data['testimonial_id']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url('admin/delete_testimonial'); ?>?testimonial_id=<?php echo $data['testimonial_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td> </tr>
                        <?php   endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>