  <div class="page-head-line">Patients List</div>  
            <div id="page-inner">
              <?php if($data['user_type'] == 1): ?>
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <a href="<?php echo base_url('admin/add_patient'); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div>
                </div>
              <?php endif; ?>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
               
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="drugPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th> 
                        <th>Email</th>  
                        <th>Mobile</th>
                        <th>Address</th>
                        <th>State</th>  
                        <th>DOB</th>    
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($userList as $data):
                         //$catInfo =  $this->adminModel->getCategoryInfo($data['category_id']);?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                          <td><?php echo $data['first_name']; ?></td>
                          <td><?php echo $data['last_name']; ?></td>
                          <td><?php echo $data['email']; ?></td> 
                          <td><?php echo $data['mobile']; ?></td> 
                          <td><?php echo $data['address']; ?></td> 
                          <td><?php echo $data['dob']; ?></td> 
                          <td><?php echo $data['state']; ?></td> 
                           <td>
                            <?php if($data['user_type'] == 1): ?>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-user_id="<?php echo $data['user_id']; ?>" class="userStatusChange" <?php if($data['user_status'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url('admin/edit_pharmacy'); ?>?user_id=<?php echo $data['user_id']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url('admin/delete_user'); ?>?user_id=<?php echo $data['user_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      <?php else: ?>

                      <?php endif; ?>

                      <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Action
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="<?php echo base_url('admin/patient_update_profile').'?user_id='.$data['user_id'];?>">View Full Info</a>
                        <?php
                        $user_id = $data['user_id'];
                        $cnt1 = $this->adminModel->checkUserQuestionaire("1",$data['user_id']);
                        $cnt2 = $this->adminModel->checkUserQuestionaire("2",$data['user_id']);

                        $treatment1 = $this->mainModel->checkUserCartByCat($data['user_id'],"1");
                        $treatment2 = $this->mainModel->checkUserCartByCat($data['user_id'],"2");
                        if($cnt1 == 1):
                        ?> 
                          <a class="dropdown-item" href="<?php echo base_url('admin/patient_questionnaire').'?category_id=1&user_id='.$user_id;?>"> ED Questionary</a>
                          <?php if(count($treatment1) == 1): ?>
                            <a class="dropdown-item" href="<?php echo base_url('admin/patient_treatment').'?category_id=1&user_id='.$user_id;?>"> ED Treatment</a>
                        <?php endif; endif; 
                         if($cnt2 == 1):
                        ?> 
                          <a class="dropdown-item" href="<?php echo base_url('admin/patient_questionnaire').'?category_id=2&user_id='.$user_id;?>"> Hair Questionary</a>
                          <?php if(count($treatment2) == 1): ?>
                            <a class="dropdown-item" href="<?php echo base_url('admin/patient_treatment').'?category_id=2&user_id='.$user_id;?>"> Hair Treatment</a>
                        <?php endif; endif; ?>
                        </div>
                      </div>

                      </td> </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>