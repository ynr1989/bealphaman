<style>
.page-head-line {
    border-bottom: 1px dotted #000;
    padding-bottom: 10px;
}
.btntab {
    
    padding: 6px 11px !important;
    
    margin: 1px !important;
    
}
nav#sidebar {
    min-height: 162px !important;
    max-height: 162% !important;
    height: 162% !important;
}
.custom-control-label::before {
    position: absolute;
    top: .25rem;
    left: 0;
    display: block;
    width: 1.5rem;
    height: 1.5rem;
    pointer-events: none;
    content: "";
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: #ffffff;
    border: 1px solid #ccc;
}
</style>
<div class="row">
  <div class="col-md-10">
<div class="page-head-line">Update Patient Profile</div>
</div>
  <div class="col-md-2">
<div class="page-head-line1"><a href="<?php echo base_url('admin/patients'); ?>" type="button" class="btntab btn-1 tabButtons"><i class="fa fa-step-backward" aria-hidden="true"></i> Go Back</a></div>
</div>
</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel"> 
                
          <?php 
          //include_once("../../frontend/patient_update_profile_form.php");
          $this->load->view('admin/patients/admin_patient_update_profile_form',$data);
           ?>
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

      