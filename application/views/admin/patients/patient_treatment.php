 <style type="text/css">
   .checkbox, .radio {
    display: unset;
}
.label{
  color:#000;
}
div#mainadd {
    background: #eee;
    padding: 9px;
}
.badge_seller {
    border-radius: 3px;
    font-size: 10px;
    position: absolute;
    padding: 2px 10px;
    background: #fa8305;
    border: none;
    color: #fff;
    font-family: 'Roboto', sans-serif;
    font-weight: 600;
    line-height: 1.5;
    right: 10px;
    text-align: center;
    text-transform: uppercase;
    top: 145px;
    z-index: 2;
    vertical-align: baseline;
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
    box-shadow: 0 0 1px 1px rgb(20 23 28 / 10%), 0 3px 1px 0 rgb(20 23 28 / 10%);
}
.badge_seller:after {
    background: inherit;
    content: '';
    height: 12px;
    position: absolute;
    top: 3px;
    transform: rotate(
45deg
);
    width: 12px;
    z-index: 1;
    display: block;
    left: -5px;
    border-bottom-left-radius: 2px;
    border-top-left-radius: 3px;
    border-bottom-right-radius: 3px;
}
span.shotaddress {
    font-weight: 500;
    font-family: sans-serif;
}
span.pincode {
    font-weight: 700;
}
 </style> 
 <style>
  .breadcrumb-item{
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white; 
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97;
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
   .section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
   
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
  
}

.foundation{ 
padding: 0 0 5px;
}
 
.foundation h3{
  padding-left:3%;margin-top: 0px;padding-top: 5px;padding-bottom: 5px;font-size:16px;
  
}
.foundation_sm ul{
  margin:0px;
  padding:0px;
}
.foundation_sm li{
  list-style:none;
}
.foundation_sm li i{
   padding-right: 6px;
}
.btntab {
    padding: 6px 11px !important;
    margin: 1px !important;
}
h2.title {
    font-size: 1.6rem;
    font-weight: 600;
    margin-top: 6px;
}
.col-md-4 {
    min-height: 350px;
    background: #add8e600;
    box-shadow: 3px 3px 8px 1px #b9bcbd;
    padding-top: 15px;
}


.membership_chk_bg {
    background: #fff;
    width: 100%;
    float: left;
    padding: 30px;
    border-radius: 10px;
    border: 1px solid #efefef;
    transition: all .2s ease-in-out;
    margin-bottom: 30px;
}
.address_text {
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    float: left;
    width: 100%;
    text-align: left;
    line-height: 24px;
    margin-top: 10px;
}
.rght1528 {
    position: sticky;
    top: 90px;
}
.order_dt_section {
    float: left;
    width: 100%;
    margin-top: 7px;
}
.order_title {
    float: left;
    width: 100%;
    padding: 20px 0;
    border-bottom: 1px solid #efefef;
	display:flex;
}
.order_title .order_price {
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    text-align: right;
    float: right;
    width: 30%;
    margin-bottom: 0;
}
.membership_chk_bg {
    background: #fff;
    width: 100%;
    float: left;
    padding: 30px;
    border-radius: 10px;
    border: 1px solid #efefef;
    transition: all .2s ease-in-out;
    margin-bottom: 30px;
}
.scr_text {
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    text-align: center;
    float: left;
    width: 100%;
    margin-top: 27px;
}
.order_title h4 {
    font-size: 16px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    margin-bottom: 0;
    line-height: 24px;
    width: 70%;
    text-align: left;
}

.order_title h6 {
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #686f7a;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_title h2 {
    font-size: 20px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_title .order_price5 {
    font-size: 20px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    text-align: right;
    float: right;
    width: 30%;
    margin-bottom: 0;
}
.checkout_title h4 {
    font-size: 18px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 10px !important;
    color: #333;
    text-align: left;
    line-height: 26px;
}
.order_dt_section {
    float: left;
    width: 100%;
    margin-top: 7px;
}
.chckot_btn {
    height: 40px;
    padding: 0 20px;
    border: 0;
    margin-top: 30px;
    float: right;
    color: #fff;
    border-radius: 20px;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    background: #ed2a26;
    display: block;
}
.order_title h4 {
    font-size: 16px;
    font-weight: 600;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    margin-bottom: 0;
    line-height: 24px;
    width: 70%;
    text-align: left;
}
.order_title h3 {
    font-size: 18px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    color: #333;
    float: left;
    width: 70%;
    text-align: left;
    line-height: 24px;
    margin-bottom: 0;
}
.order_dt_section {
    margin-top: -10px !important;
}

.inner-header-top.style-three {
    margin-bottom: 40px;
    border-bottom: 2px solid #e6e6e6;
    padding-bottom: 18px;
    position: relative;
}
.inner-header-top .header-title .title {
    font-size: 30px;
    font-weight: bold;
    font-family: "Heebo", sans-serif;
    color: #1e2843;
    margin-bottom: 0;
}
@media only screen and (max-width: 600px) {
 .order_title {
    display: block !important;
}
}
nav#sidebar {
    min-height: 162px !important;
    max-height: 185% !important;
    height: 185% !important;
}
</style>
<div class="row">
  <div class="col-md-10">
<div class="page-head-line" style="border-bottom: 1px dotted #000;
    padding-bottom: 10px;">Manage Patient Treatment</div>
</div>
  <div class="col-md-2">
<div class="page-head-line"><a href="<?php echo base_url('admin/patients'); ?>" type="button" class="btntab btn-1 tabButtons"><i class="fa fa-step-backward" aria-hidden="true"></i> Go Back</a></div>
</div>
</div>

<form method="post" action="#" class="save_cart_admin" enctype="multipart/form-data">
 
<div class="container-fluid">
  <div class="row">
<?php $category_id =  $_GET['category_id'];
$batch_id =  $_GET['batch_id'];
$user_id =  $_GET['user_id'];
$catInfo = $this->adminModel->getCategoryInfo($category_id);
$cart_data = $this->mainModel->getCartInfoByBatchId($user_id,$batch_id); 
$pharmacyInfo = $this->mainModel->getPharmacyInfo();
$uencrid = $user_id;

$user_info = $this->mainModel->getUserInfoById($user_id);
$user_address = $this->mainModel->getUserAddressById($user_id,$cart_data[0]['address_id']);

if($cart_data[0]['send_to_pharmacy']!=1){
?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="slider-title ">
          <h5 class="title">Choice Of Medications for <?php echo $catInfo[0]['category_name']; ?> : <br/><br/></h5> 
        </div>
        <div class="col-md-4 col-sm-12" style="">
            <label class="bmd-label-floating">Select Drug <span class="mandatory-label">*</span></label>
            <input type="hidden" class="category_id" value="<?php echo $category_id; ?>">
            <input type="hidden" class="user_id" value="<?php echo $uencrid; ?>">
             <input type="hidden" class="batch_id" value="<?php echo $cart_data[0]['batch_id']; ?>">  
          <div class="form-group">
             <select name="drug_id" class="form-control fdrug_id" required="required">
                <option value="">Select Drug</option>
                <?php foreach ($drugs as $drugInfo) { ?>                                
                <option value="<?php echo $drugInfo['drug_id']; ?>" <?php if(count($cart_data)>0 && $drugInfo['drug_id'] == $cart_data[0]['drug_id']): echo "selected"; endif; ?>><?php echo $drugInfo['drug_name']; ?></option>
              <?php } ?>
            </select>
          </div>        
              <label class="bmd-label-floating">Dosage Strength<span class="mandatory-label">*</span></label>
            <div class="form-group">
               <select name="drug_strength_id" class="form-control fdrug_strength_id" required="required">
                  <option value="">Select Strength</option>
                  <?php if(count($cart_data)>0): ?>
                    <option value="<?php echo $cart_data[0]['drug_strength_id']; ?>" <?php if(count($cart_data)>0): echo "selected"; endif; ?>><?php echo $cart_data[0]['strength']; ?></option>
                  <?php endif; ?>
              </select>
            </div>
          </div> 

          <div class="col-md-4 medicines_data ">
        <div class="slider-title ">
          <h2 class="title">Select Quantity</h2> 
        </div>
          <div class="thumbnail-content"> 
            <span class="price_list_data"></span>
        </div>
      </div>

       <div class="col-md-4 col-sm-12">
         <label class="bmd-label-floating">SIG </label>
         <input type="text" class="form-control sig" name="sig" value="<?php echo $cart_data[0]['sig']; ?>" required>

        <label class="bmd-label-floating">No of Refills </label>
         <input type="text" class="form-control no_of_refills" name="no_of_refills" value="<?php echo $cart_data[0]['no_of_refills']; ?>">
       
        <input type="checkbox" class="" name="daw" value="1" <?php if($cart_data[0]['daw'] == 1): echo "checked"; endif; ?>> DAW
         

         <input type="checkbox" class="" name="send_to_pharmacy" value="1" <?php if($cart_data[0]['send_to_pharmacy'] == 1): echo "checked"; endif; ?>> Send to Pharmacy
       <br/>
   <br/>

   <label for="form" class="addressqq"><div class="_1XFPmK"></div><div class="ased"><div class="addefname"><div id="mainadd"><p class="rcode"><span class="namemain"><?php echo $user_info[0]['first_name']." ".$user_info[0]['last_name']; ?></span></p>
   <div class="badge_seller">Address</div>
   <span class="shotaddress"><?php echo $user_address[0]['address_line1']; ?>, <?php echo $user_address[0]['city']; ?>, <?php echo $user_address[0]['state']; ?> - <span class="pincode"><?php echo $user_address[0]['zip']; ?></span></span></div></div></div></label>
   <br/>
        <div class="col-md-12 medicines_data" style="text-align: right;">
        <br/><button type="submit" class="btn btn-primary pull-right">Update</button>
      </div>
      </div> 
      </div> 
       <div class="col-md-6 foundation" style="max-height: 220px;">
         <?php if($cart_data[0]['ship_address_type'] == 1): ?>

   <h3><b>Pharmacy Address</b></h3>
    <div class="col-md-12 foundation_sm">
       <ul>
        <li><?php echo $pharmacyInfo[0]['first_name']." ".$pharmacyInfo[0]['last_name'];  ?> </li>
        <li><?php echo $pharmacyInfo[0]['address'];  ?> </li>
       <li><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo $pharmacyInfo[0]['email'];  ?></li>
       <li><i class="fa fa-phone" aria-hidden="true"></i><?php echo $pharmacyInfo[0]['mobile'];  ?>  </li>
       <li>Fax: <?php echo $pharmacyInfo[0]['fax'];  ?> </li></ul> 
    </div>
         <?php endif; ?> 
      </div>
    </div>
  </div>

    <?php }else{ ?>
 
<div class="summary">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="inner-header-top style-three">
          <div class="header-title">
            <h6 class="title">Summary</h6>
          </div>              
        </div>			
			<div class="container">			
				<div class="row">
					<div class="col-lg-8">
						<div class="membership_chk_bg">
							<div class="checkout_title">
								<h4>Billing Details</h4>
								<img src="<?php echo base_url(); ?>assets/frontend/images/line.svg" alt="">
							</div>
							
							<div class="address_text">
								<?php echo $user_info[0]['first_name']." ".$user_info[0]['last_name']; ?><br>
								<?php echo $user_address[0]['address_line1']; ?>, <?php echo $user_address[0]['city']; ?>,
                <br> <?php echo $user_address[0]['state']; ?>- <?php echo $user_address[0]['zip']; ?> 
                <br>
								United States
							</div>
						</div>
							<div class="membership_chk_bg">
							
							
						
							<div class="chckout_order_dt">
								<div class="checkout_title">
									<h4>Order Details</h4>
									
								</div>
								<div class="order_dt_section">
									<div class="order_title">
										<h6>Category: <br/><strong style="font-weight: 500 !important;color: #000;"><i class="fas fa-file-medical-alt"></i> <?php echo $catInfo[0]['category_name']; ?></strong></h6>
										<h6>Drug Info : <br/><strong style="font-weight: 500 !important;color: #000;"><?php echo $cart_data[0]['drug_name'].",".$cart_data[0]['strength']; ?></strong></h6>
										<h6>Appointment : <br/><strong style="font-weight: 500 !important;color: #000;"><i class="far fa-calendar-plus"></i> <?php echo $cart_data[0]['appointment_date']; ?><br> <i class="far fa-clock"></i> <?php echo $cart_data[0]['appointment_time']; ?></strong></h6>
										<div class="order_price">$<?php echo $cart_data[0]['drug_price']; ?></div>
									</div>
									<div class="order_title">
										<h6>Online Doctor Visit</h6>
										<div class="order_price">$<?php echo $cart_data[0]['doctor_consultation_fee']; ?></div>
									</div>
									<!-- <div class="order_title">
										<h6>Discount</h6>
										<div class="order_price">$10</div>
									</div> -->
									<div class="order_title">
										<h3>Total</h3>
										<div class="order_price">$<?php echo $cart_data[0]['drug_price']+$cart_data[0]['doctor_consultation_fee']; ?></div>
									</div>
									<!--<button class="chckot_btn" type="submit">Confirm Checkout</button>-->
								</div>
							</div>
						</div>						
					</div>
					<!-- <div class="col-lg-4">
						<div class="membership_chk_bg rght1528">
								<div class="checkout_title">
									<h4>Order Summary</h4>
									<img src="images/line.svg" alt="">
								</div>
								<div class="order_dt_section">
									<div class="order_title">
										<h4>Orignal Price</h4>
										<div class="order_price">$<?php echo $cart_data[0]['drug_price']; ?></div>
									</div>
									<div class="order_title">
										<h6>Discount Price</h6>
										<div class="order_price">$5</div>
									</div>
									<div class="order_title">
										<h2>Total</h2>
										<div class="order_price5">$10</div>
									</div>
									<div class="scr_text"><i class="uil uil-lock-alt"></i>Secure checkout</div>
								</div>							
						</div>
					</div> -->								
				</div>				
			</div>
      </div>
    </div>
  </div>
</div> <!--summary end-->
<?php } ?>
 <table>
 
 </table>
 
    </form>

<script type="text/javascript">
/*jQuery(document).ready(function($){
  
});
  
$(".fdrug_id").change(function(){
   $('select').find("option[value=<?php echo $cartInfo[0]['drug_id']; ?>]").attr('selected','selected');
}).change();*/
</script>