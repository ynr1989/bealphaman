<style>
.btn-primary {
    color: #fff;
    background-color: #106ec8;
    border-color: #357ebd;
    border-radius: 20px;
    padding: 0px 8px;
    font-size: 14px;
    font-weight: 600;
    margin: 10px 0px;
    font-family: monospace;
    box-shadow: 1px 2px 3px #535252;
}
thead.text-primary {
    background: #3445b424;
    color: #000 !important;
}
thead.text-primary th, tr td {
    border: 1px solid #00000026 !important;
}
.page-head-line {
    border-bottom: 1px dotted #000;
    padding-bottom: 10px;
}
div#patientBookInfo {
    background: #4444458a;
}

@media only screen and (max-width: 600px) {
  .btn-primary {
    display: block;
}
.modal-dialog.vertical-align-center {
    max-width: 100% !important;
}
.modal-content {
    top: 490px !important;
}
}
.modal-content {
    top: 225px;
}
.modal-dialog.vertical-align-center {
    max-width: 65%;
}

.boxmode {
    border: 2px solid #000;
    padding: 4px 20px;
}
.modal-content {
    width: 80% !important;
    left: 100px !important;
}

@media print {
  
  div#patientBookInfo {
    position: absolute;
    left: 0;
    top: 0;
  }
  div#patientBookInfo {
    width: 100% !important;
}
}


    
</style>

  <div class="page-head-line">Patients List</div>  
            <div id="page-inner">
              <?php if($data['user_type'] == 1): ?>
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <a href="<?php echo base_url('admin/add_patient'); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div>
                </div>
              <?php endif; ?>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
               
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="drugPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Name</th> 
                        <th>Email</th>  
                        <th>Mobile</th>  
                        <th>Appointment</th> 
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php  $i=1; foreach($userList as $data): 
                        $user_id = $data['user_id'];
                        $cnt1 = $this->adminModel->checkUserQuestionaire("1",$data['user_id']);
                        $cnt2 = $this->adminModel->checkUserQuestionaire("2",$data['user_id']);

                        $treatment1 = $this->mainModel->checkUserCartByCat($data['user_id'],"1");
                        $treatment2 = $this->mainModel->checkUserCartByCat($data['user_id'],"2");
                        $cartInfo = $this->mainModel->getUserCart($data['user_id']); 
                        ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                          <td><?php echo $data['first_name']; ?> <?php echo $data['last_name']; ?></td>
                          <td><?php echo $data['email']; ?></td> 
                          <td><?php echo $data['mobile']; ?></td>  
                             <td> <?php echo $cartInfo[0]['appointment_date']; ?> <?php echo date("g:i a", strtotime($cartInfo[0]['appointment_time'])); ?></td> 
                            <?php if($data['user_type'] == 1): ?><td>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-user_id="<?php echo $data['user_id']; ?>" class="userStatusChange" <?php if($data['user_status'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url('admin/edit_pharmacy'); ?>?user_id=<?php echo $data['user_id']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url('admin/delete_user'); ?>?user_id=<?php echo $data['user_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                     </td>
                      <?php endif; ?>  
                      <td>
                        
                        <a  class="btn btn-primary" style="float: left;margin-right: 5px;" href="<?php echo base_url('admin/patient_update_profile').'?user_id='.$data['user_id'];?>"> <i class="fa fa-eye" aria-hidden="true"></i> Info</a>

                          <?php if($cnt1 == 1){ ?>
                           <a class="btn btn-primary" style="float: left;margin-right: 5px;" href="<?php echo base_url('admin/patient_questionnaire').'?category_id=1&user_id='.$user_id;?>"><i class="fa fa-question-circle" aria-hidden="true"></i> ED Ques</a>
                          <?php } ?>

                          <?php if($cnt2 == 1){ ?>
                          <a class="btn btn-primary" style="float: left;margin-right: 5px;" href="<?php echo base_url('admin/patient_questionnaire').'?category_id=2&user_id='.$user_id;?>"><i class="fa fa-question-circle" aria-hidden="true"></i> Hair Ques</a>
                          <?php } ?>

                           <?php if($cnt1 >0 && count($treatment1) >0){ ?>
                         <a class="btn btn-primary" style="float: left;margin-right: 5px;" href="<?php echo base_url('admin/patient_treatment').'?category_id=1&user_id='.$user_id;?>&batch_id=<?php echo $treatment1[0]['batch_id'];?>"><i class="fa fa-user-md" aria-hidden="true"></i> ED Treatment</a>

                         <button type="button"  class="btn btn-lg btn-primary patientBookInfoModel"  data-category-id="1"  data-user-id="<?php echo $user_id; ?>" data-toggle="modal" data-target="#patientBookInfo"><i class="fa fa-address-card" aria-hidden="true"></i> ED Prescription</button>

                          <?php } ?>

                           <?php if($cnt2 >0 && count($treatment2) >0){ ?>
                       <a class="btn btn-primary" style="float: left;margin-right: 5px;" href="<?php echo base_url('admin/patient_treatment').'?category_id=2&user_id='.$user_id;?>&batch_id=<?php echo $treatment2[0]['batch_id'];?>"><i class="fa fa-user-md" aria-hidden="true"></i> Hair Treatment</a>

                       <button type="button"  class="btn btn-lg btn-primary patientBookInfoModel" data-category-id="2"  data-user-id="<?php echo $user_id; ?>" data-toggle="modal" data-target="#patientBookInfo"><i class="fa fa-address-card" aria-hidden="true"></i> Hair Prescription</button>
                         
                          <?php }  ?> 
                          

                      </td>

                       </tr>

                      
                       <tr>
                         </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            





          </div>
       </div>





<script type="text/javascript">
$(document).ready(function(){
  $(".patientBookInfoModel").click(function (){
    var category_id = $(this).data("category-id");
    var user_id = $(this).data("user-id"); 
       $.ajax({
          url: '<?php echo base_url('get_treatment_info_by_cat'); ?>',
          type: 'post',
          data: {user_id: user_id, category_id:category_id},
          success: function(response){ 
            //$('.modal-body').html(response);
            $('#patientBookInfo').modal('show'); 
          }
      });
  });
});
</script>

      <div id="patientBookInfo" class="modal fade">
         <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div  align="left"  class="modal-header">  
                      <div align="left"><h4 class="modal-title">Send to</h4>  
                      <!-- <span><a href="" id="docLink" download><i class="fa fa-download" aria-hidden="true"></i></a></span> -->
                 </div>  
                <a href="#" class="close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/img/close.png" width="24" height="24" /></a>
                </div> 
				
				<div class="container right-container col-md-12" id="printableArea" style="display:block;">
				<div class="modal-body">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12" style="text-align: left;">
        <b>E & M PHARMACY</b><br>
        <b>205 South Essex Ave</b><br>
        <b>ORANGE, NJ, 07095</b><br>
      </div>
	  <div class="col-md-8">Phone: <b>(973)677-2800</b> Fax: <b>(973)677-8864</div>  
    </div>
	<div class="boxmode">
    <div class="row">
      <div class="col-md-8"><b>Rx Pree: Slobodan Miric, 060</b></div>
      <div class="col-md-4"><b><u>Ord:</u></b> 02/18/2020</div>
	  
	  <div class="col-md-8">35 W Main StreeSuite 103</div>
      <div class="col-md-4"><b><u>NPI#</u></b> 1701090909</div>
	  
	  <div class="col-md-4">Denville, NJ 0784</div>
      <div class="col-md-4">Ph: (973)625-0858</div>
	  <div class="col-md-4"><b><u>LIC#</u></b> 25MA08380900</div>
	  
	  <div class="col-md-4">Dr. Miric Neurology Center</div>
      <div class="col-md-4">Fax: -</div>
	  <div class="col-md-4"><b><u>DEA#</u></b> 25MA08380900<br/>SPI: 25MA08380900</div>
	  
    </div>
	
<hr style="border-bottom:5px solid #000;margin: 0px !important;"/>
	
	
	<div class="row"><div class="col-md-12"><b>Patient: Maddali, Prakash</b></div><br/><br/></div>
	
	
   <div class="row">
   
      <div class="col-md-6"><b>DOB:</b> 07/08/1967</div>
      <!-- <div class="col-md-6"><b>Gender</b> M</div>  -->
	  
	  <div class="col-md-8"><b>Address:</b> 3 tulip lane, Denvilla, NJ, 07834</div> 
	  
	  <div class="col-md-12"><b>Phone:</b> (973)441-8470</div><br><br>
	  
	  <div class="col-md-12"><b>Qty:</b> 4.000    (Four)</div>
	  
	  <div class="col-md-2"><b>Days:</b> 0</div>
      <div class="col-md-2"><b>Refiils:</b> 5</div> 
	  <div class="col-md-4"><b>Class:</b>   0</div> 
	  <div class="col-md-12"><b>Drug:</b> Nascobal 500 mcg/spary nasal spary</div>
	  <br/><br/>
	  <div class="col-md-12"><b>Sig:</b> 1 suite nasal spary weekly<br/><br/></div>
	  
	  </div>
	  <hr/>
	  <div class="row">
	  <div class="col-md-2"><b>Signature</b></div>o
	  <div class="col-md-4">-</div>
	  <div class="col-md-2"><b>Date</b></div>
	  <div class="col-md-4">-</div>
	  
	  
	  
	  </div>
	<hr style="border-bottom:5px solid #000;margin: 0px !important;"/>
	
	 <div class="row">
      
      <div class="col-md-8" style="text-align:center;">This Prescription Will be Filled Generically Unless Prescriber Writes "DAW(Y) in the Box Dispense As Wriiten</div>
	  <div class="col-md-4"><span style="border: 3px solid #000;padding-left: 65px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO</span> <br/>DAW</div>
    </div>
	
	</div>
	
	<!-- <div class="row">
	
	<div class="col-md-2">2/19/2020</div>
	  <div class="col-md-2">11:31 AM</div>
	  <div class="col-md-4" style="text-align: right;"><b>Messagae Type:</b></div>
	  <div class="col-md-4">New Prescrption<br/>From : 65363763673637</div>
	  
	  
	   <div class="col-md-8">Mesg.Id: 1N202002181030262F2805357</div>
	  <div class="col-md-4">To : 3124546</div>
	  
	  <div class="col-md-12">Prs. Order Ref: Nascobal 500 mcg/spary nasal spary</div>
	  
	  <div class="col-md-6">&nbsp;</div>
	  <div class="col-md-6">Resp.Status: Ready To Process</div>
	  
	  
	  <div class="col-md-12">Nascobal 500 mcg/spary nasal spary</div>
	  
	</div> -->
  </div>
</div>
          </div>     
                <div class="modal-footer">
				<button class="btn btn-default" onclick="printDiv('printableArea')"><i class="fa fa-print" aria-hidden="true" style="    font-size: 17px;"> Print</i></button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    </div>
	<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>