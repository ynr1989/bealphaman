 <style type="text/css">
   .checkbox, .radio {
    display: unset;
}
.label{
  color:#000;
}
 </style> 
 <style>
  .breadcrumb-item{
   border: 2px solid #e5fff2;
    width:100%; 
    height:45%;
    padding-top:30px;
    margin-bottom:4px;
    padding-bottom:30px; 
    background: white; 
  }
   .breadcrumb-item:hover{
     border: 1px solid pink;
   }
  .radio{
    margin-left: 25px;
     background: #004c97;
  }
  .label{
    color:black;
    margin-left: 20px;
  }
  .sign{
    margin-left:470px;
    font-size:13px;
  }
  .signin{
     color:rgb(204 131 92);
     border-bottom: 0.125rem solid transparent;
  }
  .section-img1,.section-img2,.section-img3{
    width:30px;
  }
  .section-img1{
    margin-left:200px;
  }
   .section-img2{
    margin-left:140px;
  }
  .section-img3{
    margin-left:90px;
  }
  .head{
    margin-top:0px !important;
    margin-left:20px;
  }
  .title1{
    text-align: center;
  }
   
  .price{
   margin-left:40%;
  }
  .btn1{
    width:100px;
     height:30px;
     margin-top:80px;
     margin-left:-10px;
  }
  .image{
    margin:0 10px 20px 10px;
  }
  .cart{
    background-color:#43bf71;
    color:white;
    margin-top:0px;
  }
  .title2{
    text-align:center;
    margin-top:20px;
  }

  ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver; 
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}


 /* .under:hover{
     border-bottom: 2px solid pink;
  }*/
  @media only screen and (max-width: 600px) {
    .breadcrumb-item{
      width:100%;
    }
     .breadcrumb-item:hover{
     border: 3px solid pink !important;
   }
    .sign{
    margin-left:95px;
  }
 .section-img2{
    margin-left:160px;
  }
  .section-img3{
    margin-left:125px;
  }
  .label{
    font-size:10px !important;
  }
}
</style>
<div class="row">
  <div class="col-md-10">
<div class="page-head-line">Manage Patient Treatment</div>
</div>
  <div class="col-md-2">
<div class="page-head-line"><a href="<?php echo base_url('admin/patients'); ?>" type="button" class="btntab btn-1 tabButtons">Go Back</a></div>
</div>
</div>
 <form method="post" action="#" class="save_cart" enctype="multipart/form-data">
<div class="container">
  <div class="row">
<?php $category_id =  $_GET['category_id'];
$user_id =  $_GET['user_id'];
$catInfo = $this->adminModel->getCategoryInfo($category_id);
$cart_data = $this->mainModel->checkUserCartByCat($user_id,$category_id);
?>  
   
    
          
              <div class="col-md-6">

                <div class="col-md-12 col-sm-12">
                    <label class="bmd-label-floating">The Pills For ED <span class="mandatory-label">*</span></label>
                    <input type="hidden" class="category_id" value="<?php echo $category_id; ?>">
                  <div class="form-group">
                     <select name="drug_id" class="form-control fdrug_id" required="required">
                        <option value="">Select Drug</option>
                        <?php foreach ($drugs as $catInfo) { ?>                                
                        <option value="<?php echo $catInfo['drug_id']; ?>" <?php if($cartInfo[0]['drug_id'] == $catInfo['drug_id']): echo 'selected'; endif; ?>><?php echo $catInfo['drug_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div> 

                 <div class="col-md-12 col-sm-12">
                      <label class="bmd-label-floating">Dosage Strength<span class="mandatory-label">*</span></label>
                    <div class="form-group">
                       <select name="drug_strength_id" class="form-control fdrug_strength_id" required="required">
                          <option value="">Select Strength</option>
                           
                      </select>
                    </div>
                  </div> 
              </div>

              <div class="col-md-6 medicines_data ">
                <div class="slider-title ">
                  <h2 class="title">Select Medicine</h2> 
                </div>
                  <div class="thumbnail-content"> 
                    <span class="price_list_data"></span>
                </div>
              </div>

            </div>
          </div>
        </div>
          

        <div class="ship_address_section">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="slider-title ">
                  <h2 class="title">Shipping info & Address</h2>
                  <h6>Where would you like to have your medication shipped, i</h6><h6>f prescribed? use your legal name</h6>
                </div>
                 <div class="row">
                 <div class="form-group col-md-6">
                <label for="usr">Name:</label>
                <input type="text" class="form-control ship_name" id="ship_name" required="required">
                </div>
              <div class="form-group col-md-6">
                <label for="usr">City</label>
                <input type="text" class="form-control ship_city" id="usr" required="required">
                </div>
              <div class="form-group col-md-6">
                <label for="usr">Street Address:</label>
                <input type="text" class="form-control street_address" id="usr" required="required">
                </div>
                <div class="form-group col-md-6">
                <label for="usr">Country</label>
                <input type="text" class="form-control country" id="usr" required="required">
                </div>
                <div class="form-group col-md-3">
                <label for="usr">State</label>
                <input type="text" class="form-control state" id="usr" required="required">
                </div>
                <div class="form-group col-md-3">
                <label for="usr">Zip</label>
                <input type="text" class="form-control zip" id="usr" required="required">
                </div>
               
              </div>
               <button type="submit" class="btn btn-warning">Submit</button>
              </div>
            </div>
          </div>
        </div>
        </form>
    <?php //else: ?>
     
           <!--  <div class="col-md-6">
              
              <div class="alert alert-success" role="alert">
                 Thanks for your treatment selection
              </div>
            </div> -->
          

    <?php //endif; ?>
      
    </div>
  </div>
</div>

<script type="text/javascript">
/*jQuery(document).ready(function($){
  
});
  
$(".fdrug_id").change(function(){
   $('select').find("option[value=<?php echo $cartInfo[0]['drug_id']; ?>]").attr('selected','selected');
}).change();*/
</script>