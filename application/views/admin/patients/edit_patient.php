<div class="page-head-line">Edit Patient</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
            <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="update_user" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">
                  <input type="hidden" class="user_id" value="<?php echo $userInfo[0]['user_id']; ?>">
                  <input type="hidden" class="user_type" value="<?php echo $userInfo[0]['user_type']; ?>">
                  <div class="row">

                    <div class="col-md-6 col-12"> 
                      <label class="bmd-label-floating">First Name <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['first_name']; ?>" required name="first_name" placeholder="Enter First Name" maxlength="50" class="form-control first_name">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Last Name <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['last_name']; ?>" required name="last_name" placeholder="Enter Last Name" maxlength="25" class="form-control last_name">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Email <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['email']; ?>" required name="email" placeholder="Enter Email Address" class="form-control email">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Mobile <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['mobile']; ?>" required name="mobile" placeholder="Enter Mobile" maxlength="25" class="form-control mobile">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">NPI <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['npi']; ?>" required name="npi" placeholder="Enter NPI" class="form-control npi">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">State <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['state']; ?>" required name="state" placeholder="Enter State" class="form-control state">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Licence <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" value="<?php echo $userInfo[0]['licence']; ?>" required name="licence" placeholder="Enter Licence" class="form-control licence">
                      </div>
                  </div>
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Password  </label>
                      <div class="form-group">                        
                        <input type="text" name="password" placeholder="Enter Password" class="form-control password">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Address <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <textarea  name="address" placeholder="Enter Address"  class="form-control address"> <?php echo $userInfo[0]['address']; ?></textarea>
                      </div>
                  </div>
                    
  
                    </div> 
 
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/patients'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        