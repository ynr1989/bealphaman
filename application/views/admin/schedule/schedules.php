 
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
    <!--Font Awesome (added because you use icons in your prepend/append)-->
   <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
   
   
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  
    <div class="container-fluid">
    <style type="text/css">
    .control-label{
    text-align: left !important;
    }
	ol.breadcrumb {
    border-bottom: 1px dotted #000;
    padding-bottom: 10px;
    background: none;
}
li.active {
    font-size: 20px !important;
    color: #000 !important;
    font-weight: 800;
    margin-left: -11px;
}
input.datepicker {
    border: 1px solid #ccc;
}
.datepicker-days th.next, th.prev {
    background: none !important;
}
    </style>  
    <?php
    if($this->input->post('submit')!=""){ ?>
    <script type="text/javascript">
      alert('Schedule added successfully.');
    </script>
    <?php } ?>              
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            
            <ol class="breadcrumb">
                <li class="active">
                     Doctor Schedule
                </li>
            </ol>
        </div>
    </div>
    <!-- Page Heading end-->

    <!-- panel start -->
    <div class="panel panel-primary">

        <!-- panel heading starat -->
        <div class="panel-heading">
            <h3 class="panel-title">Add Schedule</h3>
        </div>
        <!-- panel heading end -->

        <div class="panel-body">
        <!-- panel content start -->
            <div class="bootstrap-iso">
             <div class="container-fluid">
              <div class="row">
              
                <form class="form-horizontal" method="post">


                   <div class="col-md-3 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label col-sm-4 requiredField" for="date">
                   Date
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                  <div class="col-sm-10">
                   <div class="input-group">
                    <div class="input-group-addon">
                     <i class="fa fa-calendar">
                     </i>
                    </div>
                    <input class="datepicker" name="schedule_date" required="required" data-date-format="mm-dd-yyyy" autocomplete="off">
                   </div>
                  </div>
                 </div>
               </div>

                <div class="col-md-3 col-sm-12 col-xs-12">                 
                 <div class="form-group">
                  <label class="control-label col-sm-12 requiredField" for="starttime">
                   Shift Start
                   <span class="asteriskField">
                    *
                   </span>
                  </label> 
                  <div class="col-sm-10">
                   <div class="input-group"  data-align="top" data-autoclose="true">
                    <div class="input-group-addon">
                     <i class="fa fa-clock-o">
                     </i>
                    </div>
                   <!--  <input class="form-control" autocomplete="off" placeholder="Select Start Time" id="starttime" name="shift_start" type="text" required/> -->

                             <div class="indexpicker">
        <input id="shift_start" class="form-control" type="text" autocomplete="off" placeholder="Select Start Time" name="shift_start" /> </div>  
                   </div>
                  </div>
                 </div>
               </div>

                <div class="col-md-3 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label col-sm-12 requiredField" for="endtime">
                   Shift End
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                  <div class="col-sm-10">
                   <div class="input-group"  data-align="top" data-autoclose="true">
                    <div class="input-group-addon">
                     <i class="fa fa-clock-o">
                     </i>
                    </div>
                    <!-- <input class="form-control" autocomplete="off" placeholder="Select End Time" id="endtime" name="shift_end" type="text" required/> -->
                     <div class="indexpicker">
        <input id="shift_end" class="form-control" type="text" autocomplete="off" placeholder="Select End Time" name="shift_end" /> </div>
                   </div>
                  </div>
                 </div>
               </div>


                <div class="col-md-2 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="control-label col-sm-12 requiredField" for="bookavail">
                   Availabilty
                   <span class="asteriskField">
                    *
                   </span>
                  </label>
                  <div class="col-sm-10">
                   <select class="select form-control" id="bookavail" name="is_availability" required>
                    <option value="1">
                     Yes
                    </option>
                    <option value="0">
                     No
                    </option>
                   </select>
                  </div>
                 </div>
               </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                 <div class="form-group">
                   <label class="control-label col-sm-5 requiredField" for="bookavail">
                   
                  </label>
                  <div class="col-md-12 col-sm-offset-2">
                   <input class="btn btn-primary" name="submit" type="submit" value="Submit"> 
                  </div>
                 </div>
               </div>
                </form>
               </div>
              </div>
             </div>
            </div>                        
        <!-- panel content end -->
        <!-- panel end -->
        </div>
    </div>
    <!-- panel start -->

     <!-- panel start -->
    <div class="panel panel-primary filterable">

        <!-- panel heading starat -->
        <div class="panel-heading">
            <h3 class="panel-title">List of Schedules</h3>
            <!-- <div class="pull-right">
            <button class="btn btn-default btn-xs btn-filter"><span class="fa fa-filter"></span> Filter</button>
            </div> -->
        </div>
        <!-- panel heading end -->

        <div class="panel-body">
        <!-- panel content start -->
           <!-- Table -->
        <table class="table table-hover table-bordered">
           <!--  <thead>
                <tr class="filters">
                    <th><input type="text" class="form-control" placeholder="scheduleId" disabled></th>
                    <th><input type="text" class="form-control" placeholder="scheduleDate" disabled></th>
                    <th><input type="text" class="form-control" placeholder="scheduleDay" disabled></th>
                    <th><input type="text" class="form-control" placeholder="startTime." disabled></th>
                    <th><input type="text" class="form-control" placeholder="endTime" disabled></th>
                    <th><input type="text" class="form-control" placeholder="bookAvail" disabled></th>
                </tr>
            </thead> -->

            <thead>
                <tr class="filters">
                    <th>ID</th>
                    <th>Date</th> 
                    <th>Day</th>
                    <th>Shift Start</th>
                    <th>Shift End</th>
                    <th>Availability</th>
                    <th>Delete</th>
                </tr>
            </thead>
            
            <?php 
            foreach ($schedules as $doctorschedule) {  
              $shift_start  = date("g:i a", strtotime($doctorschedule['shift_start']));
              $shift_end  = date("g:i a", strtotime($doctorschedule['shift_end']));
              $is_availability = $doctorschedule['is_availability']?'Yes':'No';
                echo "<tbody>";
                echo "<tr>";
                    echo "<td>" . $doctorschedule['doctor_schedule_id'] . "</td>";
                    echo "<td>" . $doctorschedule['schedule_date'] . "</td>";
                    echo "<td>" . $doctorschedule['day'] . "</td>";
                    echo "<td>" . $shift_start . "</td>";
                    echo "<td>" . $shift_end . "</td>";
                    echo "<td>" . $is_availability . "</td>";
                    echo "<form method='POST'>";
                    echo "<td class='text-center'><a href='#' id='".$doctorschedule['doctor_schedule_id']."' class='delete'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>
            </td>";
               
            } 
                echo "</tr>";
           echo "</tbody>";
       echo "</table>";
      /* echo "<div class='panel panel-default'>";
       echo "<div class='col-md-offset-3 pull-right'>";
       echo "<button class='btn btn-primary' type='submit' value='Submit' name='submit'>Update</button>";
        echo "</div>";
        echo "</div>";*/
        ?>
        <!-- panel content end -->
        <!-- panel end -->
        </div>
    </div>
    <!-- panel start -->
</div>
 

 <script src="<?php echo base_url(); ?>assets/js/timepicki.js"></script>
<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy/mm/dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
 
 <script type="text/javascript">
$(function() {
$(".delete").click(function(){
var element = $(this);
var id = element.attr("id");
var info = 'id=' + id;
if(confirm("Are you sure you want to delete this?"))
{
 $.ajax({
   type: "POST",
   url: "<?php echo base_url('doctor/deleteschedule') ?>",
   data: info,
   success: function(){
 }
});
  $(this).parent().parent().fadeOut(300, function(){ $(this).remove();});
 }
return false;
});
});
</script>
<script>
$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    startDate: '-3d'
});

</script>
<script type="text/javascript">
             
          
              //$('#timepicker1').timepicker();
              $('#shift_start').timepicki();
              $('#shift_end').timepicki();


            
        </script>

       
        <link href="<?php echo base_url(); ?>assets/css/timepicki.css"  rel="stylesheet">

