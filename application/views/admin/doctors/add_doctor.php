<div class="page-head-line">Add Doctor</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel"> 
                
          <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

             <form method="post" action="#" class="add_user" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">

                  <div class="row">

                  <div class="col-md-6 col-12">
                    <input type="hidden" name="user_type" class="user_type" value="<?php echo DOCTOR; ?>">
                      <label class="bmd-label-floating">First Name <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="first_name" placeholder="Enter First Name" maxlength="50" class="form-control first_name">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Last Name <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="last_name" placeholder="Enter Last Name" maxlength="25" class="form-control last_name">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Email <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="email" placeholder="Enter Email Address" class="form-control email">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Mobile <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="mobile" placeholder="Enter Mobile" maxlength="25" class="form-control mobile">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">NPI <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="npi" placeholder="Enter NPI" class="form-control npi">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">State <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="state" placeholder="Enter State" class="form-control state">
                      </div>
                  </div> 
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Licence <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <input type="text" required name="licence" placeholder="Enter Licence" class="form-control licence">
                      </div>
                  </div> 
                 <!--  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Password  </label>
                      <div class="form-group">                        
                        <input type="text" name="password" placeholder="Enter Password" class="form-control password">
                      </div>
                  </div>  -->
                  <div class="col-md-6 col-12">
                      <label class="bmd-label-floating">Address <span class="mandatory-label">*</span></label>
                      <div class="form-group">                        
                        <textarea  name="address" placeholder="Enter Address"  class="form-control address"></textarea>
                      </div>
                  </div>  
  
                </div> 
            </div>
              
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/doctors'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        <script type="text/javascript">
        /*$(document).ready(function () {
          $('.farmerName').typeahead({
                    source: function (query, result) {
                        $.ajax({
                            url: "<?php echo base_url('getFarmersNames'); ?>",
                  data: 'query=' + query,            
                            dataType: "json",
                            type: "POST",
                            success: function (data) {
                    result($.map(data, function (item) {
                      return item;
                                }));
                            }
                        });
                    }
                });
        });*/

        /*$(".drug_id").on('change', function() {
            console.log("changed")
            let drug_id = $(this).val();
            let url = "<?php echo base_url('admin/get_ajax_strength_data'); ?>";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                  console.log(sData.strength_data);
                    //$(".aa").val(sData.vv);
                    strengthData = sData.strength_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".drug_strength_id").append(new Option(strengthData[i].strength, strengthData[i].drug_strength_id));
                    }

                } else {
                    $(".drug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        }); 

        $(".drug_strength_id").on('change', function() { 
            console.log("changed")
            let drug_id = $(this).val();
            let drug_strength_id = $(".drug_strength_id").val();
            let url = "<?php echo base_url('admin/get_ajax_tablets_data'); ?>";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id,
                drug_strength_id:drug_strength_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                  console.log(sData.tablets_data); 
                    strengthData = sData.tablets_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".tablet_set_id").append(new Option(strengthData[i].tablet_count, strengthData[i].tablet_set_id));
                    }

                } else {
                   $(".tablet_set_id").empty().append('<option selected="selected" value="">Select Tablets</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        });*/
        </script>