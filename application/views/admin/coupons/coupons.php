      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="card">
              <header class="card-header">
                  <span class="card-title">Coupons</span>
             <!-- <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span> -->
              </header>
              <div class="card-body">
              <div class="adv-table">
              <a class="btn btn-primary pull-right" href="<?php echo base_url('admin/addCoupon');?>">Add Coupon</a>

              <table  class="display table table-bordered table-striped" id="printexample">
              <thead>
              <tr>
                  <th>Sr No.</th>
                  <th>Coupon Code</th>
                  <th>Coupon Percentage</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Max Amount</th>
                  <th>Coupon Description</th>
                  <th>Status</th>
                  <th>Actions</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                  if(is_array($categories) && count($categories) >0){
                  foreach($categories as $key => $val):?>
              <tr class="gradeX">
                  <td><?php echo $key+1;?></td>
                  <td><?php echo $val['couponCode'];?></td>
                  <td><?php echo $val['couponPercentage'];?></td>
                  <td><?php echo $val['startDate'];?></td>
                  <td><?php echo $val['endDate'];?></td>
                  <td><?php echo $val['maxAmount'];?></td>
                  <td><?php echo $val['couponDesc'];?></td>
                <?php if($val['couponStatus'] == 1){?>
                    <td>
                        <input type="button" class="btn btn-primary btn-xs coupon_status_change" data-cat_id="<?php echo $val['couponId'];?>" data-change_status=0 value="Active">                      
                    </td>
                  <?php }else{?>
                  <td>  <input type="button" class="btn btn-primary btn-xs coupon_status_change" data-cat_id="<?php echo $val['couponId'];?>" data-change_status=1 value="InActive">
                  <?php }?>
                 
                  <td>
                      <button class="delete_cat btn btn-danger" type="button" data-cat_id="<?php echo $val['couponId'];?>" title="Delete">Delete</button>
                        <!--<a class="delete_cat" href="javascript:" data-cat_id="<?php echo $val['couponId'];?>"><button class="btn btn-primary far fa-trash-o">Delete</button></a>-->
                        <a href="<?php echo base_url('admin/editCoupon').'/'.$val['couponId'];?>"><button  class="btn btn-success">Edit</button></a>
                        <!--<a href="<?php echo base_url('admin/editCoupon').'/'.$val['couponId'];?>"><button class="btn btn-primary fa fa-edit">Edit</button></a>-->
                  </td>
              </tr>
              <?php endforeach;}else{?>
              <tr>No categories found</tr>
              <?php }?>
              </tbody>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- page end-->
          </section>
      </section>

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
      
      <!--main content end-->
       <script type="text/javascript">
      $(document).on('click','.coupon_status_change',function(){
        var cat_id = $(this).data('cat_id');
        var status = $(this).data('change_status');
         $.ajax({
            url: "<?php echo base_url('admin/changeCouponStatus'); ?>",
            type: 'post',
            data: {'cat_id':cat_id,'status':status},
            success: function(response) {
                // alert(response);
                //  var data = JSON.parse(response);
                // alert(data.message);
                // if(data.success){
                     location.reload();
                // }
            }            
        });
    });

      $(document).on('click','.delete_cat',function(){
           var cat_id = $(this).data('cat_id');
          swal({
          title: "Are you sure want o delete this category?",
          text: "You will not be able to recover this category!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
         
            $.ajax({
                url: "<?php echo base_url('admin/deleteCoupon'); ?>",
                type: 'post',
                data: {'cat_id':cat_id},
                success: function(response) {
                    // alert(response);
                         location.reload();
                }            
            }) 
          } else {
            swal("Cancelled", "", "error");
          }
        });
    })
    </script>