<section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                              <span><a href="<?php echo base_url('coupons');?>">coupons</a></span> / 
                            <span class="card-title"><?php if(isset($categories['couponId'])){echo "Edit Coupon";}else{echo "Add Coupon";}?></span>
                          </header>
                          <div class="card-body">
                              <form class="form-horizontal tasi-form" method="post" action="<?php echo base_url('admin/AddCouponAction');?>" enctype="multipart/form-data">
                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Coupon Code</label>
                                      <div class="col-sm-10">
                                          <input type="text" name="couponCode" required value="<?php if(isset($categories['couponCode'])){echo $categories['couponCode'];}?>"  class="form-control">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Coupon Percentage</label>
                                      <div class="col-sm-10">
                                          <input type="number" name="couponPercentage" required value="<?php if(isset($categories['couponPercentage'])){echo $categories['couponPercentage'];}?>"  class="form-control">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Start Date</label>
                                      <div class="col-sm-10">
                                          <input type="text" id="fdate" readonly="readonly" autocomplete="off" name="startDate" required value="<?php if(isset($categories['startDate'])){echo $categories['startDate'];}?>"  class="form-control">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">End Date</label>
                                      <div class="col-sm-10">
                                          <input type="text" id="ldate" autocomplete="off" readonly="readonly" name="endDate" autocomplete="off" required value="<?php if(isset($categories['endDate'])){echo $categories['endDate'];}?>"  class="form-control">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Max Amount</label>
                                      <div class="col-sm-10">
                                          <input type="text" name="maxAmount" required value="<?php if(isset($categories['maxAmount'])){echo $categories['maxAmount'];}?>"  class="form-control">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Coupon Description</label>
                                      <div class="col-sm-10">
                                          <input type="text" name="couponDesc" required value="<?php if(isset($categories['couponDesc'])){echo $categories['couponDesc'];}?>"  class="form-control">
                                      </div>
                                  </div>

                                  <!-- <div class="form-group row">
                                      <label class="col-sm-2 col-sm-2 control-label">Coupon Status</label>
                                      <div class="col-sm-10">
                                          <input type="text" name="couponStatus" required value="<?php if(isset($categories['couponStatus'])){echo $categories['couponStatus'];}?>"  class="form-control">
                                      </div>
                                  </div> -->
             <!--                      <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div> -->
<input type="hidden" value="<?php if(isset($categories['couponId'])){echo $categories['couponId'];}?>" name='couponId'>
<input type="submit" class="btn btn-success">
<a href="<?php echo base_url('admin/coupons');?>" class="btn btn-danger">Cancel</a>
                              </form>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
    
   