
<div class="page-head-line">Add Questionnaire</div>

            <div id="page-inner">
            <div class="row"> 
               
                       
           <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">
                   <?php if(isset($_GET['msg'])): ?>
                  <div class="success_message alert alert-success">Successfully imported questions</div>
                <?php endif; ?>
                  <div class="row">
                      <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Upload Sheet <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="file" name="question_file" required="required" class="form-control question_file"></textarea>
                        </div>
                      </div>  
                    </div> 
              </div>
             
             <a download href="<?php echo base_url('assets/uploads/demo_files/questionnaire_sample.xlsx') ?>" class="btn btn-primary ml-4"><i class="fas fa-download"></i> Sample File</a>
                  
                    <button type="submit" name="sub" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/questionnaire'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
                  
                </div>
              </div>

<?php 

if(isset($_POST['sub'])){
  set_time_limit(0);
ob_start(); 
error_reporting(0);
  require_once ('Classes/PHPExcel.php');
  require_once ('Classes/PHPExcel/IOFactory.php');
  $time = time();
  $target_dir = "assets/uploads/questionnaire_import/";  
  if(isset($_FILES["question_file"]["name"]) && $_FILES["question_file"]["name"]!=''):
      $target_file = $target_dir . $time."_".basename($_FILES["question_file"]["name"]); 
      $attachment1 = $time."_".basename($_FILES["question_file"]["name"]);
      move_uploaded_file($_FILES["question_file"]["tmp_name"], $target_file);
      
   
  $inputFileName = $target_dir."/$attachment1";
  //  Read your Excel workbook
  try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
  } catch(Exception $e) {
      die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
  }

  //uploadQuestionsData("0");
  //uploadQuestionsData("1");
  $message = "";
  //function uploadQuestionsData($sheet_index){
  $sheet_index = "0";
    $category_id = 0;
    if($sheet_index == 0){
      $category_id = 1;
    }

    if($sheet_index == 1){
      $category_id = 2;
    }
    //  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet($sheet_index); 
    $highestRow = $sheet->getHighestRow(); 
    $highestColumn = $sheet->getHighestColumn();
    //$highestRow = 10;
    //  Loop through each row of the worksheet in turn
    $batch_id = $this->adminModel->generateBatchId();
    for ($row = 1; $row <= $highestRow; $row++){ 
        if($row>1){
          
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
           /*echo "<pre>";
            print_r($rowData);
            echo "</pre>"; */
            $question_name = $rowData[0][0]; 
            $question_description = $rowData[0][1]; 
            if($question_name!=""): 
              $data = array(
                  "batch_id" =>$batch_id,
                  "category_id" => $category_id,
                  "question_name" => $question_name,
                  "question_description" => $question_description,
                  "question_status" => 1
                  );
              $this->db->insert('questions',$data);
              $question_id = $this->db->insert_id();
              $options_data = $rowData[0][2]; 
              $options_data_arr = explode("||", $options_data);
              for($k=0;$k<=count($options_data_arr);$k++){
                if(!empty($options_data_arr[$k])){
                $data1 = array(
                  "question_id" =>$question_id,
                  "question_option" => $options_data_arr[$k]
                  );
              $this->db->insert('question_options',$data1);
              }
            }
            endif;
        }
    }
    //$message = "Data processed successfully.";
  //}



    //22
    $sheet_index = "1";
    $category_id = 0;
    if($sheet_index == 0){
      $category_id = 1;
    }

    if($sheet_index == 1){
      $category_id = 2;
    }
    //  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet($sheet_index); 
    $highestRow = $sheet->getHighestRow(); 
    $highestColumn = $sheet->getHighestColumn();
    //$highestRow = 10;
    //  Loop through each row of the worksheet in turn
     $batch_id = $this->adminModel->generateBatchId();
    for ($row = 1; $row <= $highestRow; $row++){ 
        if($row>1){
         
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
           /*echo "<pre>";
            print_r($rowData);
            echo "</pre>"; */
            $question_name = $rowData[0][0]; 
            $question_description = $rowData[0][1]; 
            if($question_name!=""): 
              $data = array(
                  "batch_id" =>$batch_id,
                  "category_id" => $category_id,
                  "question_name" => $question_name,
                  "question_description" => $question_description,
                  "question_status" => 1
                  );
              $this->db->insert('questions',$data);
              $question_id = $this->db->insert_id();
              $options_data = $rowData[0][2]; 
              $options_data_arr = explode("||", $options_data);
              for($k=0;$k<=count($options_data_arr);$k++){
                if(!empty($options_data_arr[$k])){
                $data1 = array(
                  "question_id" =>$question_id,
                  "question_option" => $options_data_arr[$k]
                  );
              $this->db->insert('question_options',$data1);
            }
              }
            endif;
        }
    }
    //$message = "Data processed successfully.";

    //end
endif;
redirect(base_url("admin/questionnaire_import?msg=s"));
}
?>

        