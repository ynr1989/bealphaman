                      <!--<div class="page-head-line">Dashboard</div> -->
                       <!--  <h1 class="page-subhead-line">Notifications </h1> -->
            <div id="page-inner">
                <div class="row"> 
                     <?php if($user_type == 1){ ?>
                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('admin/doctors'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/client.png" width="64" height="64">
                                <h5>Doctors</h5>
                            </a>
                        </div>
                    </div> 
 
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-pink p-3 m-1">
                            <a href="<?php echo base_url('admin/pharmacies'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/employeedetails.png" width="64" height="64">
                                <h5>Pharmacies</h5>
                            </a>
                        </div>
                    </div>

                     <div class="col-md-4 col-6 text-center ">
                        <div class="mb-pink p-3 m-1">
                            <a href="<?php echo base_url('admin/patients'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/employeedetails.png" width="64" height="64">
                                <h5>Patients</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('admin/drug_list'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/client.png" width="64" height="64">
                                <h5>Drug Management</h5>
                            </a>
                        </div>
                    </div> 

                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-grey  p-3 m-1">
                            <a href="<?php echo base_url('settings'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/bench.png" width="64" height="64">
                                <h5>Settings</h5>
                            </a>
                        </div>
                    </div>

                    <div  class="col-md-4 col-6 text-center ">
                        <div class="mb-pink  p-3 m-1">
                            <a href="<?php echo base_url(''); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/accounts.png" width="64" height="64">
                                <h5>Accountant</h5>
                            </a>
                        </div>
                    </div>

                     <div class="col-md-4 col-6 text-center shdow rounded ">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('admin/updateProfile'); ?>">
                                 <img src="<?php echo base_url(); ?>assets/img/dashboard/personaldetails.png" width="64" height="64">
                                <h5>Update Profile</h5>
                            </a>
                        </div>
                    </div>
                     <?php }else if($user_type == 2){ ?>
                        <div class="col-md-4 col-6 text-center ">
                        <div class="mb-pink p-3 m-1">
                            <a href="<?php echo base_url('admin/patients'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/employeedetails.png" width="64" height="64">
                                <h5>Patients</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-6 text-center shdow rounded ">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('admin/updateProfile'); ?>">
                                 <img src="<?php echo base_url(); ?>assets/img/dashboard/personaldetails.png" width="64" height="64">
                                <h5>Update Profile</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-6 text-center shdow rounded ">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('doctor/schedule'); ?>">
                                 <img src="<?php echo base_url(); ?>assets/img/dashboard/personaldetails.png" width="64" height="64">
                                <h5>Schedule</h5>
                            </a>
                        </div>
                    </div>
                     <?php } ?>
                </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->