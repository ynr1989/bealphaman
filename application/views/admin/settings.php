<!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">

                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                           <h5>Settings</h5>
                                        </div>
                                        <div class="card-body">
                                           <?php if($this->session->flashdata('message')!=''): ?>
                                           <div class="alert alert-success col-md-6"><strong>
                            <?php echo $this->session->flashdata('message');
                            ?></strong></div><?php endif; ?>
                                           
                                           </div>
                                            <div class="row">
                                                <div class="col-md-12">
                  <form enctype="multipart/form-data" method="post" action="<?php echo base_url('admin/submitSettings'); ?>">
                  

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Site Name <span class="mandatory-label">*</span></label>
                         <input type="text" name="site_name" value="<?php echo $sdata[0]['site_name']; ?>" placeholder="Be Alpha Man" class="form-control" required="required">
                        </div>
                      </div>
                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Paypal Business Email <span class="mandatory-label">*</span></label>
                         <input type="text" name="paypal_business_email" value="<?php echo $sdata[0]['paypal_business_email']; ?>" placeholder="Paypal Business Email" class="form-control" >
                        </div>
                      </div>
                       
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Doctor Consultation Fee  <span class="mandatory-label">*</span></label>
                         <input type="text" name="doctor_consultation_fee" value="<?php echo $sdata[0]['doctor_consultation_fee']; ?>" placeholder="Doctor Consultation Fee" class="form-control" >
                        </div>
                      </div>                     
                     </div>
                     


                  
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
      </div>

<script type="text/javascript">
  $(function () {
      $('#datetimepicker1').datetimepicker();
  });

  $(document).on("change", ".tramount", function() {
    var sum = 0;
    $(".tramount").each(function(){
        sum += +$(this).val();
    });
    $(".total").val(sum);
  });


</script>

       