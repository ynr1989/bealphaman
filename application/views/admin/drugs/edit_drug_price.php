<div class="page-head-line">Edit Drug</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="update_drug_price" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">
                  <input type="hidden" class="drug_price_id" value="<?php echo $priceInfo[0]['drug_price_id']; ?>">
                  <div class="row">

                    <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Select Drug <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_id" class="form-control drug_id" required="required">
                              <option value="">Select Category</option>
                              <?php foreach ($drugs as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['drug_id']; ?>" <?php if($catInfo['drug_id'] == $priceInfo[0]['drug_id']): echo "selected"; endif; ?>><?php echo $catInfo['drug_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>  
                      <?php $strengthInfo = $this->adminModel->get_ajax_strength_data($priceInfo[0]['drug_id']); ?>
                       <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Strength<span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_strength_id" class="form-control drug_strength_id" required="required">
                              <option value="">Select Strength</option>
                              <?php foreach ($strengthInfo as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['drug_strength_id']; ?>" <?php if($catInfo['drug_strength_id'] == $priceInfo[0]['drug_strength_id']): echo "selected"; endif; ?>><?php echo $catInfo['strength']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>  
                    <?php $tabletInfo = $this->adminModel->get_ajax_tablets_data($priceInfo[0]['drug_id'],$priceInfo[0]['drug_strength_id']); ?>
                  <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Tablet(s) <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                         <select name="tablet_set_id" class="form-control tablet_set_id" required="required">
                              <option value="">Select Tablet</option>
                              <?php foreach ($tabletInfo as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['tablet_set_id']; ?>" <?php if($catInfo['tablet_set_id'] == $priceInfo[0]['tablet_set_id']): echo "selected"; endif; ?>><?php echo $catInfo['tablet_count']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                      <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Price <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="number" required name="price" placeholder="Enter Price" maxlength="25" class="form-control price" value="<?php echo $priceInfo[0]['price']; ?>">
                        </div>
                      </div> 
  
                    </div> 
  
                    </div> 
 
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/drug_doses'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        