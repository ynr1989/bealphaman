<div class="page-head-line">Add Tablets</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
          <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

             <form method="post" action="#" class="add_drug_dose" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">

                  <div class="row">

                    <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Select Drug <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_id" class="form-control drug_id" required="required">
                              <option value="">Select Category</option>
                              <?php foreach ($drugs as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['drug_id']; ?>"><?php echo $catInfo['drug_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>  

                      <!-- <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Strength<span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_strength_id" class="form-control drug_strength_id" required="required">
                              <option value="">Select Strength</option>
                          </select>
                        </div>
                      </div> -->
                    
                  <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Tablet(s) <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="number" required name="tablet_count" placeholder="10.." maxlength="25" class="form-control tablet_count">
                        </div>
                      </div> 
  
                    </div> 

            </div>
              
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/drug_doses'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        