<div class="page-head-line">Add Medicines</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="addDrug" enctype="multipart/form-data">
                 <div  class="col-md-8 col-sm-12">

                  <div class="row">

                    <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Select Category <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="category_id" class="form-control category_id" required="required">
                              <option value="">Select Category</option>
                              <?php foreach ($cats as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['category_id']; ?>"><?php echo $catInfo['category_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                      <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Drug Code </label>
                        <div class="form-group">                        
                          <input type="text" name="drug_code" placeholder="Drug Code" maxlength="25" class="form-control drug_code">
                        </div>
                      </div> 
                    
                  <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Drug Name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="drug_name" placeholder="Drug Name" maxlength="25" class="form-control drug_name">
                        </div>
                      </div> 
 
                    <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Drug Sub Name</label>
                        <div class="form-group">                        
                          <input type="text" name="drug_sub_name" placeholder="Drug Sub Name" maxlength="25" class="form-control drug_sub_name">
                        </div>
                      </div> 

                       <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Drug Description </label>
                        <div class="form-group">                        
                          <textarea type="text" name="drug_description" placeholder="Drug Description"  class="form-control drug_description"></textarea>
                        </div>
                      </div> 

                    </div> 

            </div>
            <div  class="col-md-4 col-12">

            <div style="margin:40px 0 0 20px; float:left"><i class="fa fa-plus add-drug-images"></i> </div>
                    <div class="drug_img_list">
                        <div class="item-data row">                              
                            <div class="form-group col-md-7 col-sm-12">
                                    <label>Images</label>
                                    <input class="form-control attachment" id="attachment-0" name="attachment[]" type="file">
                            </div>                                        
                        </div>
                    </div>
            </div>

           

                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/drug_list'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        