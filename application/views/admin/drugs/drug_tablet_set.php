<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <h1 class="page-head-line">Tablets Set List</h1>
                      <a href="<?php echo base_url('admin/add_drug_dose'); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div></div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
               
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="drugPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Drug Name</th> 
                        <th>Tablet(s)</th>          
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($drugList as $data):
                         //$catInfo =  $this->adminModel->getCategoryInfo($data['category_id']);?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                          <td><?php echo $data['drug_name']; ?></td> 
                          <td><?php echo $data['tablet_count']; ?></td> 
                           <td>
                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-tablet_set_id="<?php echo $data['tablet_set_id']; ?>" class="drugDoseStatusChange" <?php if($data['tablet_set_status'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                          <a title="Edit User" href="<?php echo base_url('admin/edit_drug_dose'); ?>?tablet_set_id=<?php echo $data['tablet_set_id']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url('admin/delete_drug_dose'); ?>?tablet_set_id=<?php echo $data['tablet_set_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td> </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>