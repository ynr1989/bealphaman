<div class="page-head-line">Add Drug Price</div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
          <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

             <form method="post" action="#" class="add_drug_price" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">

                  <div class="row">

                    <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Select Drug <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_id" class="form-control drug_id" required="required">
                              <option value="">Select Drug</option>
                              <?php foreach ($drugs as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['drug_id']; ?>"><?php echo $catInfo['drug_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>  

                      <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">Strength<span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="drug_strength_id" class="form-control drug_strength_id" required="required">
                              <option value="">Select Strength</option>
                              <?php /*foreach ($drugtypes as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['drug_strength_id']; ?>"><?php echo $catInfo['strength']; ?></option>
                            <?php }*/ ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-12">
                          <label class="bmd-label-floating">No.Of Doses <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="tablet_set_id" class="form-control tablet_set_id" required="required">
                              <option value="">No.Of Doses</option>
                              <?php /*foreach ($tablets as $catInfo) { ?>                                
                              <option value="<?php echo $catInfo['tablet_set_id']; ?>"><?php echo $catInfo['tablet_count']; ?></option>
                            <?php }*/ ?>
                          </select>
                        </div>
                      </div>
                    
                  <div class="col-md-6 col-12">
                          <label class="bmd-label-floating">Price <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="number" required name="price" placeholder="Enter Price" maxlength="25" class="form-control price">
                        </div>
                      </div> 
  
                    </div> 

            </div>
              
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/drug_doses'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        <script type="text/javascript">
        /*$(document).ready(function () {
          $('.farmerName').typeahead({
                    source: function (query, result) {
                        $.ajax({
                            url: "<?php echo base_url('getFarmersNames'); ?>",
                  data: 'query=' + query,            
                            dataType: "json",
                            type: "POST",
                            success: function (data) {
                    result($.map(data, function (item) {
                      return item;
                                }));
                            }
                        });
                    }
                });
        });*/

        /*$(".drug_id").on('change', function() {
            console.log("changed")
            let drug_id = $(this).val();
            let url = "<?php echo base_url('admin/get_ajax_strength_data'); ?>";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                  console.log(sData.strength_data);
                    //$(".aa").val(sData.vv);
                    strengthData = sData.strength_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".drug_strength_id").append(new Option(strengthData[i].strength, strengthData[i].drug_strength_id));
                    }

                } else {
                    $(".drug_strength_id").empty().append('<option selected="selected" value="">Select Strength</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        }); 

        $(".drug_strength_id").on('change', function() { 
            console.log("changed")
            let drug_id = $(this).val();
            let drug_strength_id = $(".drug_strength_id").val();
            let url = "<?php echo base_url('admin/get_ajax_tablets_data'); ?>";
            $.ajax({
               type: "POST",
               url: url,
               data: {
                drug_id: drug_id,
                drug_strength_id:drug_strength_id
               }, // serializes the form's elements.
               success: function(data)
               {
                let sData = JSON.parse(data);
                if(sData.status) {
                  console.log(sData.tablets_data); 
                    strengthData = sData.tablets_data;
                    for(let i=0; i<strengthData.length;i++) {
                        $(".tablet_set_id").append(new Option(strengthData[i].tablet_count, strengthData[i].tablet_set_id));
                    }

                } else {
                   $(".tablet_set_id").empty().append('<option selected="selected" value="">Select Tablets</option>');
                    console.log("data not found")
                }
               }, error: function(err) {
                console.log(err);
               }
             });
        });*/
        </script>