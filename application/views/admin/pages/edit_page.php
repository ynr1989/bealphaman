<div class="page-head-line">Edit Page</div>

            <div id="page-inner">
            <div class=" ">
            
         <div class="panel">
                
                       
             <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div> 

             <form method="post" action="#" class="update_page" enctype="multipart/form-data">
                 <div  class="col-md-12 col-sm-12">
                  <input type="hidden" class="page_id" id="id" value="<?php echo $rdata[0]['id'];?>"> 
                  <div class="row">
                    
                    
                    
                       <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Page Title </label>
                        <div class="form-group">                        
                          <input type="text" name="page_title" value="<?php echo $rdata[0]['page_title']; ?>" placeholder="Page Title" maxlength="25" class="form-control page_title">
                        </div>
                      </div>  

                       <div class="col-md-12 col-12">
                          <label class="bmd-label-floating">Description </label>
                        <div class="form-group">                        
                          <textarea type="text" style="min-height: 200px;" name="description" id="description" placeholder="Description"  class="form-control description ceditor"><?php echo $rdata[0]['description']; ?></textarea>
                        </div>
                      </div> 
           
</div> 
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('admin/testimonials'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

         <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>  
         