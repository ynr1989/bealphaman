<?php
set_time_limit(0);
ob_start();
/** Include PHPExcel */
error_reporting(0);
require_once ('Classes/PHPExcel.php');
require_once ('Classes/PHPExcel/IOFactory.php');


$time = time();
$target_dir = "assets/uploads/questionnaire_import/";  
if(isset($_FILES["select_file"]["name"]) && $_FILES["select_file"]["name"]!=''):
    $target_file = $target_dir . $time."_".basename($_FILES["select_file"]["name"]); 
    $attachment1 = $time."_".basename($_FILES["select_file"]["name"]);
    move_uploaded_file($_FILES["select_file"]["tmp_name"], $target_file);
    
 
$inputFileName = public_path('excels')."/$attachment1";
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
//$highestRow = 10;
//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++){ 
    if($row>1){
        //  Read a row of data into an array
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
       echo "<pre>";
        print_r($rowData);
        echo "</pre>"; 
        $awc_id = $rowData[0][0]; 
        if($awc_id!=""):
         
        $userInfo = DB::select(DB::raw("SELECT * FROM users WHERE awc_id =$awc_id"));
        $users_id = $userInfo[0]->id;
        $project_id = $userInfo[0]->project;
        $district_id = $userInfo[0]->district; 
        $type = 1;
        $order_id =  App\Http\Controllers\ImportController::generateOrderID();
        $two_hundred_ml_qty = $rowData[0][1]; 
        echo "$users_id = $district_id <br>";
            if($users_id!="" && $district_id !=""){
                DB::table('awc_orders')->insert(
                    array(
                    "users_id" =>$users_id,
                    "awc_id" => $awc_id,
                    "project_id" => $project_id,
                    "district_id" => $district_id,
                    "type" => $type,
                    "order_id" => $order_id,
                    "two_hundred_ml_qty" => $two_hundred_ml_qty,
                    "status" => 0,
                    "five_hundred_ml_qty" => 0,
                    "thousand_hundred_ml_qty" => 0,
                    "awc_total" => $two_hundred_ml_qty
                    ));
            }
            endif;
    }
}
$message = "Data processed successfully."; 
header("Location:import-data?msg=1");
die();
else:
    $message = "Error while uploading...";
    header("Location:import-data?msg=2");
endif;
?>
