<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");

class Patientcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct(); 
        if(empty($this->session->userdata('user_type'))){
    		redirect(base_url(), 'Location');
    	}
        $this->load->library('encryption'); 
    	$this->load->model('AdminDB_model', 'adminModel');
		$this->load->model('MainDB_model', 'mainModel');
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function global_functions(){    	
    	$data['user_id'] = $this->session->userdata('user_id'); 
        $data['user_type'] = $this->session->userdata('user_type');
        //$data['user_id'] = $_COOKIE['patient_id']; 
        //$data['user_type'] = $_COOKIE['user_type'];
        $data['settings'] = $this->adminModel->getSettings();
        return $data;
    }

	public function dashboard() {
        $data = $this->global_functions();
        if($_SESSION['is_quiz'] == 1){
            unset($_SESSION['is_quiz']);    
            //echo "dd=d==".$_SESSION['quiz_category'];
            //print_r($_SESSION['questionarray']); die();        
            $this->mainModel->insertUserQuestionnaire($_SESSION['quiz_category'],$_SESSION['questionarray']);
            redirect(base_url('questionnaire_complete'), 'Location');
        }
        $data['rdata'] = $this->adminModel->getUserInfo($data['user_id']);
        $data['categories'] = $this->adminModel->getCategories();
    	$this->load->view('templates/frontend/header',$data);
		$this->load->view('frontend/dashboard',$data);	
		$this->load->view('templates/frontend/footer',$data);
	}
 
    public function questionnaire_complete() {
        $data['category_id'] = 1;
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/questionnaire_complete',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function my_questionnaire() { 
        $data = $this->global_functions();
        $data['categories'] = $this->adminModel->getCategories();
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/my_questionnaire',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function my_addresses(){
        $data = $this->global_functions();
        $data['addresses'] = $this->mainModel->getUserAddresses($data['user_id']);
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/my_addresses',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function save_address(){
        $response = array(); 
        $data = $this->global_functions();
        $address_id = $this->input->post('address_id');
        $address_line1 = $this->input->post('address_line1'); 
        $ship_city = $this->input->post('ship_city'); 
        $address_line2 = $this->input->post('address_line2');
        $state = $this->input->post('state'); 
        $zip = $this->input->post('zip'); 
        $form_data = array(
            'address_line1' =>  $address_line1,
            'address_line2' =>  $address_line2, 
            'user_id' => $data['user_id'], 
            'city' =>  $ship_city,
            'state' =>  $state,
            'zip' =>  $zip  
        ); 
        if($address_id!=""){
            $this->mainModel->updateAddress($form_data,$address_id,$data['user_id']); 
        }else{
            $this->mainModel->insertAddress($form_data); 
        }
        
        $this->session->set_flashdata('message', "Successfully added address.");
        $response['status'] = true; 
        $response['message'] = 'Successsfully updated.'; 
       echo json_encode($response);
    }

    public function add_address(){
        $data = $this->global_functions(); 
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/add_address',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function edit_address(){
        $data = $this->global_functions(); 
        $address_id = $_GET['ad_id'];
        $data['rdata'] = $this->mainModel->getUserAddressById($data['user_id'],$address_id);
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/edit_address',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function patient_update_profile() {
        $data = $this->global_functions();
        $data['rdata'] = $this->adminModel->getUserInfo($data['user_id']);
        $data['pdata'] = $this->adminModel->getPatientInfo($data['user_id']);
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/patient_update_profile',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function cart() { 
        $data = $this->global_functions();  
        $data['drugCatInfo'] = $this->mainModel->getUserQuestionire($data['user_id']);  
        //$data['drugtypes'] = $this->adminModel->getDrugStrength();
        //$data['tablets'] = $this->adminModel->getDrugTablets(); 
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/cart',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function SplitTime($StartTime, $EndTime, $Duration=DOCTOR_APPOINTMENT_TIME){
        $ReturnArray = array ();// Define output
        $StartTime    = strtotime ($StartTime); //Get Timestamp
        $EndTime      = strtotime ($EndTime); //Get Timestamp

        $AddMins  = $Duration * 60;

        while ($StartTime <= $EndTime) //Run loop
        {
            $ReturnArray[] = date ("G:i", $StartTime);
            $StartTime += $AddMins; //Endtime check
        }
        return $ReturnArray;
    }

    public function get_slots($date){
        $date = str_replace("-","/",$date);
        //$new_date=date_create($date); 
        $ap_date = date('Y-m-d', strtotime($date)); //date_format($new_date,"Y-m-d");  
        $schedules = $this->mainModel->getDoctorSchdulesByDate($date);
        if(count($schedules)>0){
             foreach ($schedules as $schedule) {
            $shift_start = $schedule['shift_start'];
            $shift_end = $schedule['shift_end'];
            $times[] = $this->SplitTime("$ap_date $shift_start", "$ap_date $shift_end");
            } 
            //Doing merge here
            $times_arrays = array_merge([], ...$times);
            foreach ($times_arrays as $times_array) {
                $available_slots[] = $times_array;
            }
            return $available_slots;
        }else{
            return [];
        }
       
    }

    public function get_appointment_slots(){
        $data = $this->global_functions(); 
        $a_date = $this->input->post("a_date");
        $times = $this->get_slots($a_date); 
        if(count($times)>0) {
            $sdata = '<select name="appointment_time" required class="form-control"  readonly="readonly">';
            $sdata .= '<option value="">Select Time</option>';
            foreach ($times as $timesInfo) {
                $time_in_12_hour_format = date("g:i a", strtotime($timesInfo)); 
               $sdata .= '<option  value="'.$timesInfo.'">'.$time_in_12_hour_format.'</option>';
            }
            $sdata .= '</select>'; 
            $response['status'] = true;
            $response['rdata'] = $sdata; 
        }else{
            $response['status'] = false;
            $response['message'] = 'No Slots Available for this date.';
        }   
       echo json_encode($response);
    }

    public function checkout() { 
        $data = $this->global_functions(); 
        //$times = $this->SplitTime("2021-05-12 12:15", "2021-05-12 15:30");
         //$times = $this->get_slots("2021-05-13");
        //echo "<pre>"; print_r($times); echo "</pre>";

       // $arraysToMerge = [ [1, 2], [2, 3], [5,8] ];
         //echo "<pre>"; print_r($arraysToMerge); echo "</pre>";
        $data['addresses'] = $this->mainModel->getUserAddresses($data['user_id']);
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/checkout',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function payment_success(){ 
        $data = $this->global_functions();  
        echo "<pre>"; 
        //print_r($data);
        
        $payment_string = http_build_query($_POST,'',', ');
        //echo htmlentities($payment_string);
        //echo "ddd===".$this->session->userdata('user_id')."---".$_COOKIE['patient_id']."<br>";
        $txn_id = $this->input->post("txn_id");
        $ainfo = explode("_", $this->input->post("item_number"));
        $cart_id = $ainfo[0];
        $check = $this->mainModel->checkPaymentTxnId($txn_id);
        $item_name = $this->input->post("item_name");
        //$user_id = $this->session->userdata('user_id')?$this->session->userdata('user_id'):$_COOKIE['patient_id'];
        $this->session->set_userdata('user_type', "4"); 
        $this->session->set_userdata('user_id', $ainfo[1]);
        $get_cart_data = $this->mainModel->getCartInfoById($cart_id);
        if($check == 0){
            $cart_data = array(
                'user_id' =>  $ainfo[1],
                'cart_id' =>  $cart_id,
                'payer_email' =>  $this->input->post("payer_email"),
                'verify_sign' =>  $this->input->post("verify_sign"),
                'payment_fee' =>  $this->input->post("payment_fee"),
                'amount' =>  $this->input->post("payment_gross"),
                'payment_for' =>  $item_name,
                'txn_id' =>  $txn_id,
                'payment_status' =>  $this->input->post("payment_status"),
                'payment_string' =>  $payment_string
            ); 
            if(trim($item_name) == "Precription Amount"){
                $pstatus = 2;
                $q_status = 3;
            }else{
                $pstatus = 1;
                $q_status = 1;
            }
/*echo "$pstatus ===$q_status <br>";

            echo "<pre>";
        print_r($_POST); 
        //print_r($_SESSION);
         //echo ($this->session->userdata('ship_pharmacy_address'));
         echo "</pre>"; die();*/

            //print_r($cart_data);
            $insertUser = $this->mainModel->insertPayment($cart_data);
            $ship_address_type = $this->session->userdata('ship_address_type');
            if($pstatus == 1){
                $form_data = array( 
                    'payment_status' => $pstatus,
                    'ship_address_type' => $ship_address_type,
                    'address_id' => $this->session->userdata('user_address_id'),
                    'appointment_date' =>  $ainfo[2],
                    'appointment_time' =>  $ainfo[3]

                );
            }else{
                $form_data = array( 
                    'payment_status' => $pstatus
                );
            }

            $q_form_data1 = array( 
                    'questionnaire_status' => $q_status
                );
             
            $this->mainModel->updateQuestionnaireStatus($q_form_data1,$get_cart_data[0]['batch_id'],$ainfo[1]);
            $this->mainModel->updatePaymentStatusCart($form_data,$cart_id,$ainfo[1]);
            if($ship_address_type == 2){
                $shipPharmacyData = $this->session->userdata('ship_pharmacy_address');
                $shipPharmacyData['cart_id'] = $cart_id;
                $this->mainModel->insertShipPharmacy($shipPharmacyData);
            }
        }

        redirect(base_url('order_success')."?txn_id=$txn_id");
        exit();
    }

    public function order_success(){
        $data['txn_id'] = $this->input->get('txn_id');
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/order_success',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function payment_fail(){  
        $data = $this->global_functions();
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/payment_fail',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function my_orders(){  
        $data = $this->global_functions();
        $this->load->view('templates/frontend/header');
        $this->load->view('frontend/my_orders',$data);   
        $this->load->view('templates/frontend/footer');
    }

    public function confirmCheckout(){
        $data = $this->global_functions(); 
        $this->session->set_userdata('user_address_id', $_POST['user_address_id']); 
        $this->session->set_userdata('ship_address_type', $_POST['ship_address_type']); 
        $ship_address_type = $this->input->post("ship_address_type");

        if($ship_address_type == 2){
            $ship_pharmacy_address = array(
                "user_id" => $data['user_id'],
                "pharmacy_name" => $_POST['pharmacy_name'],
                "street_address" => $_POST['street_address'],
                "city" => $_POST['city'],
                "state" => $_POST['state'],
                "zip" => $_POST['zip'],
                "phone" => $_POST['phone']
            );
            $this->session->set_userdata('ship_pharmacy_address', $ship_pharmacy_address);
        }
        $appointment_time = "";
        $appointment_date = "";
        $userInfo = $this->adminModel->getUserInfo($data['user_id']); 
        $payment_id = $this->encryption->decrypt($this->input->post("payment_id"));
        $cart_data = $this->mainModel->checkUserCartById($data['user_id'],$payment_id);
        if($cart_data[0]['payment_status'] == 0){
            $appointment_time = date("H:i", strtotime($this->input->post("appointment_time")));
            $appointment_date = $this->input->post("appointment_date");
            $item_name = "Doctor Consultation Fee";
        }else if($cart_data[0]['payment_status'] == 1){
            $item_name = "Precription Amount";
        }else{
            $item_name = "Error";
        }

        if($cart_data[0]['payment_status'] == 0){
          $price =  $cart_data[0]['doctor_consultation_fee'];
          }else{
            $price = $cart_data[0]['price'];
          }

        $query = array();
        $query['business'] = 'ynreddy1989nthwall@gmail.com';
        $query['cmd'] = '_xclick';
        $query['item_name'] = $item_name;
        $query['rm'] = "2";
        $query['cbt'] = "2";
        $query['item_number'] = $payment_id."_".$data['user_id']."_".$appointment_date."_".$appointment_time;
        $query['credits'] = "510";
        $query['userid'] = $data['user_id'];
        $query['amount'] = $price;
        $query['no_shipping'] = "1";
        $query['currency_code'] = "USD";
        $query['handling'] = "0";        
        $query['cancel_return'] = base_url('payment_fail');
        $query['return'] = base_url('payment_success');
        $query['first_name'] = $userInfo[0]['first_name'];
        $query['last_name'] = $userInfo[0]['last_name'];
        $query['email'] = $userInfo[0]['email'];
        $query['address1'] = "Hyderabad";
        //$query['item_name'] = "Be Alpha Man";
        $query['zip'] = "";
        // Prepare query string
        $query_string = http_build_query($query);
         //header('Location: https://www.paypal.com/cgi-bin/webscr?' . $query_string);exit();
        header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?' . $query_string);exit();
    }

    public function get_treatment_info_by_cat(){
        $user_id = $this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        $catInfo = $this->adminModel->getCategoryInfo($category_id);
        $cart_data = $this->mainModel->checkUserCartByCat($user_id,$category_id); 
        $fdata = "";
        $fdata .="<h3>Drug Information</h3>";
        $fdata .= '<table class="table table-user-information" align="center">
                <tbody> 
                    <tr>
                        <td>Drug Name</td>
                        <td>'.$cart_data[0]['drug_name'].'</td>
                    </tr>
                    <tr>
                        <td>Strength</td>
                        <td>'.$cart_data[0]['strength'].'</td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>$ '.$cart_data[0]['price'].'</td> 
                    </tr>
                </tbody>
            </table>';

        $fdata .="<h3>Appointment Information</h3>";
        $fdata .= '<table class="table table-user-information" align="center">
                <tbody> 
                    <tr>
                        <td>Appointment Date</td>
                        <td>'.$cart_data[0]['appointment_date'].'</td>
                    </tr>
                    <tr>
                        <td>Time</td>
                        <td>'.$cart_data[0]['appointment_time'].'</td>
                    </tr>  
                </tbody>
            </table>';

            echo $fdata;
    }

    public function save_cart(){
        $response = array(); 
        $data = $this->global_functions();
        $category_id = $this->input->post('category_id');
        $batch_id = $this->input->post('batch_id');
        $drug_id = $this->input->post('drug_id');
        $drug_strength_id = $this->input->post('drug_strength_id');
        $drug_price_id = $this->input->post('drug_price_id'); 
        $patient_id = $this->input->post('patient_id');
        $send_to_pharmacy = $this->input->post('send_to_pharmacy');
        $no_of_refills = $this->input->post('no_of_refills');
        $doctor_approve_status=0;
        if(empty($patient_id)){
            $patient_id = $data['user_id'];
        }
        if($this->input->post('doctor_approve_status')!=""){
            $doctor_approve_status = 1;
        } 
        $priceInfo = $this->adminModel->getDrugPriceInfo($drug_price_id);

        $check_cart_data = $this->mainModel->checkUserCartByCat($patient_id,$category_id);

        if($batch_id!=""){
            $cart_data = array(
                'category_id' =>  $category_id,
                'batch_id' => $batch_id,
                'user_id' => $patient_id,
                'doctor_consultation_fee' =>$data['settings'][0]['doctor_consultation_fee'], 
                'drug_id' =>  $drug_id,
                'drug_price' =>  $priceInfo[0]['price'],
                'drug_strength_id' =>  $drug_strength_id,
                'doctor_approve_status' =>  $doctor_approve_status,
                'drug_price_id' =>  $drug_price_id,
                'send_to_pharmacy' =>$send_to_pharmacy,
                'no_of_refills' => $no_of_refills
            ); 
        }else{
            $cart_data = array(
                'category_id' =>  $category_id,
                'user_id' => $patient_id,
                'doctor_consultation_fee' =>$data['settings'][0]['doctor_consultation_fee'], 
                'drug_id' =>  $drug_id,
                'drug_price' =>  $priceInfo[0]['price'],
                'drug_strength_id' =>  $drug_strength_id,
                'doctor_approve_status' =>  $doctor_approve_status,
                'drug_price_id' =>  $drug_price_id,
                'send_to_pharmacy' =>$send_to_pharmacy
            ); 
        }
        
        if(count($check_cart_data)>0):
            $q_form_data1 = array( 
                    'questionnaire_status' => 2
                );
             
            $this->mainModel->updateQuestionnaireStatus($q_form_data1,$batch_id,$patient_id);
            $insertUser = $this->mainModel->updateCart($cart_data,$category_id,$patient_id);
        else:

            $insertUser = $this->mainModel->insertCart($cart_data);
        endif;
        if($insertUser) {
            $this->session->set_flashdata('message', "Successfully added cart.");
            $response['status'] = true; 
            $response['message'] = 'Successsfully registered';  
        }else{
            /*$response['status'] = false;
            $response['message'] = 'Something wrong';*/
            $response['status'] = true; 
            $response['message'] = 'Successsfully registered';
        } 
       echo json_encode($response);
    }

    /*public function save_cart(){
        $response = array(); 
        $data = $this->global_functions();
        $category_id = $this->input->post('category_id');
        $drug_id = $this->input->post('drug_id');
        $drug_strength_id = $this->input->post('drug_strength_id');
        $drug_price_id = $this->input->post('drug_price_id');
        $ship_city = $this->input->post('ship_city');
        $name = $this->input->post('ship_name');
        $street_address = $this->input->post('street_address');
        $state = $this->input->post('state');
        $country = $this->input->post('country');
        $zip = $this->input->post('zip'); 
        $form_data = array(
            'name' =>  $name,
            'user_id' => $data['user_id'],
            'street_address' =>$street_address, 
            'city' =>  $ship_city,
            'state' =>  $state,
            'zip' =>  $zip,
            'country' =>  $country 
        );
         
        $address_id = $this->mainModel->insertAddress($form_data); 
        if($address_id) {   
            $cart_data = array(
                'category_id' =>  $category_id,
                'user_id' => $data['user_id'],
                'address_id' =>$address_id, 
                'drug_id' =>  $drug_id,
                'drug_strength_id' =>  $drug_strength_id,
                'drug_price_id' =>  $drug_price_id 
            );         
            $insertUser = $this->mainModel->insertCart($cart_data);
            $this->session->set_flashdata('message', "Successfully added cart.");
            $response['status'] = true; 
            $response['message'] = 'Successsfully registered';  
        }else{
            $response['status'] = false;
            $response['message'] = 'Something wrong';
        } 
       echo json_encode($response);
    }*/

    public function update_personal_info(){
        $data = $this->global_functions();
        $response = array();  
        $patient_user_id = $this->input->post('patient_user_id');
        if(!empty($patient_user_id)){
            $user_id = $patient_user_id;
        }else{
            $user_id = $_SESSION['user_id'];
        } 
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $address = $this->input->post('address');
        $age = $this->input->post('age');
        $what_is_the_problem = $this->input->post('what_is_the_problem');
        $how_often = $this->input->post('how_often');
        $medication_taken = $this->input->post('medication_taken');
        $alergies = $this->input->post('alergies'); 
        $family_history = $this->input->post('family_history');
        $marital_status = $this->input->post('marital_status');
        $living_situation = $this->input->post('living_situation');
        $smoking_status = $this->input->post('smoking_status');
        $alcohol_status = $this->input->post('alcohol_status');
        $drug_status = $this->input->post('drug_status');

        /*if(!empty($family_history) ){
            $family_history = implode(",", $family_history);
        }*/

        $userInfo = $this->adminModel->getUserInfo($user_id); 
         $form_data1 = array(
            'user_id' =>  $user_id,
            'first_name' =>  $first_name,
            'last_name' =>$last_name, 
            'address' =>  $address,
            'marital_status' =>  $marital_status,
            'age' =>  $age,
            'dob' =>  $this->input->post('dob')
            );  
         $this->adminModel->updateUser($form_data1,$user_id); 
        $form_data = array(
            'user_id' =>  $user_id,             
            'what_is_the_problem' =>  $what_is_the_problem,
            'how_often' =>  $how_often,
            'medication_taken' =>  $medication_taken,
            'alergies' =>$alergies,
            'family_history' =>  $family_history,            
            'living_situation' =>  $living_situation,
            'smoking_status' =>  $smoking_status,
            'alcohol_status' =>  $alcohol_status,
            'drug_status' =>  $drug_status,
            'health_issues' =>  $this->input->post('health_issues'),
            'when_did_it_start' =>  $this->input->post('when_did_it_start'),
            'height_inches' =>  $this->input->post('height_inches'),
            'height_feet' =>  $this->input->post('height_feet'),
            'reason_for_visit' =>  $this->input->post('reason_for_visit'),
            'blood_pressure' =>  $this->input->post('blood_pressure'),  
            'weight' =>  $this->input->post('weight'),
            'notes' =>  $this->input->post('notes'),
            'message' =>  $this->input->post('message'),
            'operations_surgeries' =>  $this->input->post('operations_surgeries')
        );
        $check = $this->mainModel->checkUserPersonalInfo($user_id);  
        if($check=='0') {  
            $updateUser = $this->mainModel->insertPersonalInfo($form_data);           
            if($updateUser > 0 ) {
                $this->session->set_flashdata('message', "Data updated successsfully.");
                $response['message'] = "Data updated successfully.";
                $response['status'] = true;
                $response['drug_id'] = $updateUser;
            }else{
                $this->session->set_flashdata('message', "Data updated successsfully.");
                $response['message'] = "Data updated successfully.";
                $response['status'] = true;
            }           
        }else if($check=='1'){
            $updateUser = $this->mainModel->updatePersonalInfo($form_data,$user_id);  
            $response['status'] = true;
            $response['message'] = 'Data updated successfully';
        }
       echo json_encode($response);
    }
}