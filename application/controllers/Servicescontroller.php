<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");

class Servicescontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();	
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: *");
        header('Access-Control-Allow-Headers: *');
		$this->load->model('ServicesDB_model', 'servModel');
    }
    
    public function login() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->servModel->checkUser($mobile,$password);
		$checkBusiness = $this->servModel->checkBusiness($mobile,$password);
		$checkBusinessAvail = $this->servModel->checkBusinessAvail($mobile,$password);
		
		$userinfo = $this->servModel->getUserDetailsByKey($checkUser);
		$businessInfo = '';
		if($checkBusiness !='no_business'){
		    $businessInfo = $this->servModel->getBusinessInfo($checkBusiness);
		}
		
		if($checkBusinessAvail=='business_avail'){
		     $response['businessUniqueId'] = $businessInfo[0]['businessUniqueId'];
		    $response['businessId'] = $businessInfo[0]['businessId'];
		    $response['userTypeCode'] = $businessInfo[0]['userTypeCode'];
		    $response['userType'] = $businessInfo[0]['userType'];
		    $response['businessName'] = $businessInfo[0]['businessName'];
	    //	$response['usertypeName'] = $this->servModel->getTypeName($userinfo[0]['userTypeCode'],'USER_TYPE');
			$response['username'] = $businessInfo[0]['ownerFirstName']." ".$businessInfo[0]['ownerLastName'];
   			$response['mobile'] = $businessInfo[0]['mobile'];
   			$response['email'] = $businessInfo[0]['email'];
   			$response['businessStatus'] = $businessInfo[0]['businessStatus'];
   			$response['panNumber'] = $businessInfo[0]['panNumber'];
   			$response['gender'] = $businessInfo[0]['gender'];
   			$response['message'] = "Loggedin Successfully";
   			$response['status'] = true;			
		}else if($checkUser=='no_user'){
			$response['message'] = "Please check Mobile/Password";
			$response['status'] = false;			
		}else{
		    $response['userUniqueId'] = $userinfo[0]['userUniqueId'];
		    $response['userId'] = $userinfo[0]['userId'];
		    $response['userType'] = $userinfo[0]['userType'];
		    $response['userTypeCode'] = $userinfo[0]['userTypeCode'];
		    $response['businessId'] = $userinfo[0]['businessId'];
		    $response['businessUniqueId'] = $userinfo[0]['businessUniqueId'];
	    	$response['usertypeName'] = $this->servModel->getTypeName($userinfo[0]['userTypeCode'],'USER_TYPE');
			$response['username'] = $userinfo[0]['firstName']." ".$userinfo[0]['lastName'];
   			$response['mobile'] = $userinfo[0]['mobile'];
   			$response['email'] = $userinfo[0]['email'];
   		//	$response['restKey'] = $userinfo[0]['restKey'];
   			$response['userStatus'] = $userinfo[0]['userStatus'];
   			$response['panNumber'] = $userinfo[0]['panNumber'];
   			$response['gender'] = $userinfo[0]['gender'];
   			$response['message'] = "Loggedin Successfully";
   			$response['status'] = true;
		}
		echo json_encode($response);
	}
	
	 public function businessLogin() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->servModel->checkBusinessUser($mobile,$password);
		$userinfo = $this->servModel->getBusinessInfo($checkUser);
		if($checkUser=='no_user'){
			$response['message'] = "Please check Mobile/Password";
			$response['status'] = false;			
		}else{
		    $response['businessDetails'] = $userinfo;
   			$response['message'] = "Loggedin Successfully";
   			$response['status'] = true;
		}
		echo json_encode($response);
	}
	
	
	public function signup() {		
		$response = array();
        $restkey = md5($this->servModel->generateRESTKey());
        $generatePw = $this->servModel->generatePassword();
        $usertype = $this->input->post('userTypeCode');
        if(empty($usertype)){
            $usertype = 6;
        }
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
		$form_data = array(
		    'userTypeCode' =>$usertype,
		    'userId' =>  $this->servModel->generateUserID(),
			'firstName' =>  $this->input->post('firstName'),
			'lastName' => $this->input->post('lastName'),
			'businessId' => $this->input->post('businessId'),
			'businessUniqueId' => $this->input->post('businessUniqueId'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5(trim($generatePw)),
			'address' => $this->input->post('address'),
			'restKey' => $restkey,
			'userStatus' => 1,
			'panNumber' => $this->input->post('panNumber'),
			'gender' => $this->input->post('gender'),
			'test_password' =>$generatePw,
			'userType' => $this->servModel->getTypeName($usertype,'USER_TYPE')
		);
		$check = $this->servModel->checkUserAvail($this->input->post('mobile'),$this->input->post('email'),$this->input->post('panNumber'));
		if($check=='0') {
			$otp = $this->servModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
		
		
    		$config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
    		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
    		$row = $query->result_array();
    		$email_message = $row[0]['message'];
    		$subject =  $row[0]['subject'];
    		$finalmessage = str_replace('EMAIL',$email,$email_message);
    		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
    		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
    		
    		//$list = array($email, 'info@efileservices.in');
    		$this->load->library('email',$config);
    		$this->email->set_newline("\r\n");
    		$this->email->from(FROM_EMAIL,'Efile Services');
    		$this->email->to($email);
    		$this->email->subject($subject);
    		$this->email->message($finalmessage);
    		$this->email->send();
		
			$insert = $this->servModel->insertUser($form_data);	
			if($insert>0) {
				$response['message'] = 'success';
				$response['status'] = true;
                $response['userid'] = $restkey;
			}			
		}else{
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
               echo json_encode($response);
	}
	
	public function createBusiness(){
    	$response = array();
        $usertype = "2";
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->servModel->generateBusinessID();
        $generatePw = $this->servModel->generatePassword();
        $form_data = array(
            'userTypeCode' =>2,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
		);
		 
		$check = $this->servModel->checkUserAvail($this->input->post('mobile'),$this->input->post('email'),$this->input->post('panNumber'));
		if($check=='0') {
			$otp = $this->servModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertBusines = $this->servModel->insertBusiness($form_data);
			
			if($insertBusines>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Efile Services');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'success';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}			
		}else{
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}
	
    public function getusers(){
        $response = array();
    	$product = $this->servModel->getUsers();
		if($product == 0){
			$response['status'] = false;
			$response['message'] = "Data not available";
		}else{
			$response['users'] = $product;	
			$response['status'] = true;
		}		
		echo json_encode($response);
    }
    
    public function getBusinessList(){
        $response = array();
    	$product = $this->servModel->getBusinessList();
		if($product == 0){
			$response['status'] = false;
			$response['message'] = "Data not available";
		}else{
			$response['users'] = $product;	
			$response['status'] = true;
		}		
		echo json_encode($response);
    }
    
     public function userStatusUpdate(){
    	$status= $this->input->post('userStatus');
    	$userUniqueId= $this->input->post('userUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'userStatus' =>$status
		    );
		    $this->servModel->userStatusUpdate($form_data,$userUniqueId);
        
        echo json_encode($response);
    }
    
    public function businessStatusUpdate(){
    	$status= $this->input->post('businessStatus');
    	$userUniqueId= $this->input->post('businessUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'businessStatus' =>$status
		    );
		    $this->servModel->businessStatusUpdate($form_data,$userUniqueId);
        echo json_encode($response);
    }
    
	
	public function mobileEmailVerify(){
	    $mobile = $this->input->post('mobile');
	    $email = $this->input->post('email');
	    $check = '';
	    $variable = "";
	    if($mobile != ''){
	        $variable = "Mobile";
	        $check = $this->servModel->checkUserMobile($mobile);
	    }else if($email != ''){ 
	        $variable = "Email";
	        $check = $this->servModel->checkUserEmail($email);
	    }else{
        	
	    }
	    if($check == '0'){
	        $response['status'] = true;
			$response['message'] = 'success';
	    }else if($check == '1'){
	        $response['status'] = false;
			$response['message'] = "$variable already exist.";
	    }else{
	        $response['status'] = false;
			$response['message'] = 'Request Data Not Matching';
	    }
	    echo json_encode($response);
	}
	
	public function sendSms(){
       $mobile = $this->input->post('mobile');
	    $email = $this->input->post('email');
    	$sendSms = $this->servModel->sendSmsEmail($mobile,$email);
    	$response['status'] = true;
		$response['message'] = 'success';
		$response['sms'] = $sendSms;
		echo json_encode($response);
    }
    
    public function otpVerify(){
	    $mobile = $this->input->post('mobile');
	    $sms = $this->input->post('otp');
    	$verify = $this->servModel->otpVerify($mobile,$sms);
    	if($verify == 1){
    	    $response['status'] = true;
		    $response['message'] = 'success';
    	}else{
    	    $response['status'] = false;
	    	$response['message'] = 'Please enter valid OTP';
    	}
		echo json_encode($response);
	}
	
	public function userChangePassword(){		
		$oldpassword     = $this->input->post('oldPassword');
		$newpassword = $this->input->post('newPassword');
		$userUniqueId = $this->input->post('userUniqueId');		
		if($oldpassword == 'otp'){
			$oldpasswordCheck = 1;
		}else{
			$oldpasswordCheck = $this->servModel->checkOldPassword($oldpassword,$userUniqueId);
		}			
		if($oldpasswordCheck == 1){
			$check = $this->servModel->updateUserPassword($newpassword,$userUniqueId);
			if($check == '1'){
				echo json_encode(array('status'=>true,'message' => "Password sucessfully updated."));
			}else{
				echo json_encode(array('status'=>false,'message' => "Error"));
			}				
		}else{
			echo json_encode(array('status'=>false,'message' => "Old password is wrong."));
		}		
	}
	
	public function getprofile(){
    	$response = array();
    	$userid = $this->input->post('userUniqueId');
    	$product = $this->servModel->getProfile($userid);	 
		if($product == 0){
			$response['status'] = false;
			$response['message'] = "Data not available";
		}else{
			$response['profile'] =$product;
			//$response['businessInfo'] = $this->servModel->getBusinessInfo($product[0]['businessId']);
		}		
		echo json_encode($response);
	}
	
	public function getBusinessProfile(){
    	$response = array();
    	$userid = $this->input->post('businessUniqueId'); 
    	$product = $this->servModel->getBusinessInfo($userid);	 
		if($product == 0){
			$response['status'] = false;
			$response['message'] = "Data not available";
		}else{
			$response['businessInfo'] =$product;
		}		
		echo json_encode($response);
	}
	
	public function forgotPassword(){
	    $mobile = $this->input->post('mobile');   
	    $response = array();
	    $user_key = $this->servModel->checkMobile($mobile);  
	    if($user_key == '0'){
			$response["status"] = false;      
			$response["message"] = "This Mobile does not exist.";
	    }else{
			//$result = $this->servModel->sendPasswordActivation($user_key, $email);
			$otp = $this->servModel->generateSMSKey();
			$result = $this->servModel->sendsms($mobile,$otp);
			$this->servModel->updateSms($mobile,$otp);
			$response['status'] = true;   
			$response['message'] = "Please use OTP";
			//$response['otp'] = $otp;
			$response['userId'] = $user_key;
	    }
    	echo json_encode($response); 
	}
    
    public function updateprofile() {
        $response = array();
        $form_data = array(         
            'firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'panNumber' => $this->input->post('panNumber'),
            'aadharNumber' => $this ->input->post('aadharNumber'),
            'address' => $this->input->post('address'),
            'passportNumber' => $this->input->post('passportNumber'),
            'userStatus' => $this->input->post('userStatus'),
            'gender' => $this->input->post('gender')
        );
        $userid = $this->input->post('userUniqueId');
        $update = $this->servModel->updateProfileDetails($form_data,$userid);
        if ($update==1) {
            $response["status"] = true;
            $response["message"] = "Profile updated successfully";
        } else {
            $response["status"] = true;
            $response['message'] = "Profile updated successfully";
    }
    echo json_encode($response);
    }
    
    public function businessUpdateProfile() {
        $response = array();
        
        $form_data = array(         
            'ownerFirstName' => $this->input->post('ownerFirstName'),
            'ownerLastName' => $this->input->post('ownerLastName'),
            'panNumber' => $this->input->post('panNumber'),
            'aadharNumber' => $this ->input->post('aadharNumber'),
            'address' => $this->input->post('address'),
            'passportNumber' => $this->input->post('passportNumber'),
            'businessStatus' => $this->input->post('businessStatus'),
            'gender' => $this->input->post('gender'),
            'businessName' => $this->input->post('businessName'),
            'businessUrl' => $this->input->post('businessUrl'),
            'businessStartDate' => $this->input->post('businessStartDate'),
            'businessEndDate' => $this ->input->post('businessEndDate'),
            //'businessStatus' => $this->input->post('businessStatus')
        );
        $businessId = $this->input->post('businessUniqueId');
        
        $update = $this->servModel->updateCompanyDetails($form_data,$businessId);
        
        if ($update==1) {
            $response["status"] = true;
            $response["message"] = "Profile updated successfully";
        } else {
            $response["status"] = true;
            $response['message'] = "Profile updated successfully";
    }
    echo json_encode($response);
    }
    
    public function getTypes(){
        $response = array();
        $type = $this->input->post('type'); 
    	$product = $this->servModel->getTypes($type);
		if($product == 0){
			$response['status'] = false;
			$response['message'] = "Data not available";
		}else{
			$response['data'] = $product;	
			$response['status'] = true;
		}		
		echo json_encode($response);
    }  
    
    public function deleteUser(){
        $userid = $this->input->post('userUniqueId');
        $delete = $this->servModel->deleteUser($userid);
        
        if($delete){
        $response["status"] = true;
            $response["message"] = "Profile deleted successfully";
        } else {
            $response["status"] = false;
            $response['message'] = "Failed to delete";
        }
    echo json_encode($response);
    }
    
    public function deleteBusiness(){
        $userid = $this->input->post('businessUniqueId');
        $delete = $this->servModel->deleteBusiness($userid);
        if($delete){
        $response["status"] = true;
            $response["message"] = "Profile deleted successfully";
        } else {
            $response["status"] = false;
            $response['message'] = "Failed to delete";
        }
    echo json_encode($response);
    }
    
    public function uploadProfilePic(){
    	$userid= $this->input->post('userUniqueId');
        if($_FILES['profilePic']['name'] != ""):
        	$profilepic = $_FILES["profilePic"]["name"];
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Profile pic has been uploaded.";
        $upload_dir = "assets/profilePics/";
		$profilepic = date('dmYHis').str_replace(" ", "", basename($_FILES["profilePic"]["name"]));	
        move_uploaded_file($_FILES["profilePic"]["tmp_name"],$upload_dir . $profilepic);        		
		$response['image']    =  base_url()."assets/profilePics/".$profilepic;
	
		    $form_data = array(
			'avatar' =>  $profilepic
		    );
		    $this->servModel->updateProfilePic($form_data,$userid);
		
        else:
        $response = array();
        $response['status'] = false;
        $response['message'] = "File mandatory";
        endif; 
        echo json_encode($response);
    }
    
     public function uploadBusinessLogo(){
    	$userid= $this->input->post('businessUniqueId');
        if($_FILES['businessLogo']['name'] != ""):
        	$profilepic = $_FILES["businessLogo"]["name"];
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Profile pic has been uploaded.";
        $upload_dir = "assets/businessLogos/";
		$profilepic = date('dmYHis').str_replace(" ", "", basename($_FILES["businessLogo"]["name"]));	
        move_uploaded_file($_FILES["businessLogo"]["tmp_name"],$upload_dir . $profilepic);        		
		$response['image']    =  base_url()."assets/businessLogos/".$profilepic;
	
		    $form_data = array(
			'businessLogo' =>  $profilepic
		    );
		    $this->servModel->updateBusinesslogo($form_data,$userid);
		
        else:
        $response = array();
        $response['status'] = false;
        $response['message'] = "File mandatory";
        endif; 
        echo json_encode($response);
    }
    
    
}