<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");

class Frontendcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(!empty($this->session->userdata('user_type'))){
    		//redirect(base_url('admin/dashboard'), 'Location');
    	}
    	$this->load->library('encryption');
    	$this->load->model('AdminDB_model', 'adminModel');
		$this->load->model('MainDB_model', 'mainModel');
		$this->load->helper('url');
		$this->load->library('session');
    }

    public function global_functions(){    	
    	$data['user_id'] = $this->session->userdata('user_id'); 
        $data['user_type'] = $this->session->userdata('user_type');
        //$data['user_id'] = $_COOKIE['patient_id']; 
        //$data['user_type'] = $_COOKIE['user_type'];
        $data['settings'] = $this->adminModel->getSettings();
        return $data;
    }

    public function patientLoginAction() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->adminModel->checkUser($mobile,$password);
		$userinfo = $this->adminModel->getUserInfo($checkUser);

		if($checkUser=='no_user'){
			$response['message']= "Please Check Credentials";
			$response['status'] = false; 
		}else{			
   			if($userinfo[0]['user_status'] == 1 && ($userinfo[0]['user_type']==4 || $userinfo[0]['user_type'] == 2)){
   				$this->session->set_userdata('user_type', $userinfo[0]['user_type']); 
				$this->session->set_userdata('user_id', $userinfo[0]['user_id']);
				setcookie("patient_id", $userinfo[0]['user_id'], time() + 3600 * 24);
				setcookie("user_type", $userinfo[0]['user_type'], time() + 3600 * 24);
				$_SESSION['user_id'] = $userinfo[0]['user_id'];
	   			$response['message'] = "Success";
				$response['status'] = true;
				$response['user_type'] = $userinfo[0]['user_type'];
				$response['user_id'] = $userinfo[0]['user_id'];
			}else if($userinfo[0]['user_status'] == 0 && ($userinfo[0]['user_type']==4 || $userinfo[0]['user_type'] == 2)){
				$response['message'] = "Your account is inactive";
				$response['status'] = false; 
			}else if($userinfo[0]['user_status'] == 2 && ($userinfo[0]['user_type']==4 || $userinfo[0]['user_type'] == 2)){
				$response['message'] = "Your account is disabled.";
				$response['status'] = false;
			}else{ 
				$response['message'] = "Invalid credentials";
				$response['status'] = false; 
			}

		}
		echo json_encode($response);
	}

    public function save_user(){
    	$response = array(); 
    	$first_name = $this->input->post('first_name');
    	$last_name = $this->input->post('last_name');
    	$email = $this->input->post('email');
    	$mobile = $this->input->post('mobile');
    	$password = $this->input->post('password');
    	$npi = $this->input->post('npi');
    	$address = $this->input->post('address');
    	$user_type = $this->input->post('user_type');
    	$licence = $this->input->post('licence');
    	$state = $this->input->post('state'); 
    	if(!empty($password)){
    		$password = md5($password);
    	}
        $form_data = array(
        	'first_name' =>  $first_name,
            'last_name' =>$last_name, 
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' =>  $password,
			'npi' =>  $npi,
			'address' =>  $address,
			'password' =>  $password,
			'state' =>  $state,
			'licence' =>  $licence,
			'user_status' => 1,
			'user_type' => "4"
		);
		 
		$uinfo = $this->adminModel->checkUserMobileEmail($email,$mobile); 
		if(count($uinfo) == 0) {			
			$insertUser = $this->adminModel->insertUser($form_data);
			$this->session->set_flashdata('message', "Successfully registered.");
			$this->session->set_userdata('user_type', "4"); 
			$this->session->set_userdata('user_id', $insertUser);
			$response['status'] = true;	
			$response['message'] = 'Successsfully registered';	
		}else if(isset($uinfo) && $uinfo[0]['email']==$email && $uinfo[0]['mobile']==$mobile){
			$response['status'] = false;
			$response['message'] = 'Email And Mobile Already Exist';
		}else if(isset($uinfo) && $uinfo[0]['email'] == $email){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if(isset($uinfo) && $uinfo[0]['mobile']== $mobile){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}
       echo json_encode($response);
	}

    public function login() {
    	if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type')=="4"){
    		redirect(base_url('dashboard'), 'Location');
    	}
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/login');	
		$this->load->view('templates/frontend/footer');
	}

    public function index() {
    	$data['categories'] = $this->adminModel->getCategories();
    	$data['testdata'] = $this->adminModel->getTestimonials();
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/landingpage',$data);	
		$this->load->view('templates/frontend/footer');
	}
	
	public function aboutus() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/aboutus');	
		$this->load->view('templates/frontend/footer');
	}

	public function pricing() {		
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/pricing',$data);	
		$this->load->view('templates/frontend/footer');
	}

	public function erectileDysfunction() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/erectile-dysfunction');	
		$this->load->view('templates/frontend/footer');
	}

	public function hair() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/hair');	
		$this->load->view('templates/frontend/footer');
	}

	public function howitworks() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/how-it-works');	
		$this->load->view('templates/frontend/footer');
	}

	public function contactus() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/contactus');	
		$this->load->view('templates/frontend/footer');
	}
	public function faqs() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/faqs');	
		$this->load->view('templates/frontend/footer');
	}
	public function terms() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/terms');	
		$this->load->view('templates/frontend/footer');
	}
	public function telehealth() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/telehealth');	
		$this->load->view('templates/frontend/footer');
	}
	
	public function privacy() {
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/privacy');	
		$this->load->view('templates/frontend/footer');
	}

	public function quiz() {
		$data['categories'] = $this->Homemodel->getCategories();
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/quiz');	
		$this->load->view('templates/frontend/footer');
	}



	//Get Ajax Calls
	public function get_ajax_strength_data(){
		$drug_id = $this->input->post('drug_id'); 
		$strenthData = $this->adminModel->get_ajax_strength_data($drug_id);
		//$tabletsData = $this->adminModel->get_ajax_tablets_data($drug_id,$drug_strength_id);
		$resData = array();
		if(count($strenthData)>0){
			$resData['status'] = true;
			$resData['strength_data'] = $strenthData;
		}else{
			$resData['status'] = false;
		}
		echo json_encode($resData);
	}

	public function get_ajax_tablets_data(){
		$data = $this->global_functions();
		$drug_id = $this->input->post('drug_id'); 
		$drug_strength_id = $this->input->post('drug_strength_id');  
		$category_id = $this->input->post('category_id');
		$patient_id = $this->input->post('user_id');
		if(empty($patient_id)){
			$patient_id = $data['user_id'];
		}
		$settings = $this->adminModel->getSettings();
		if($drug_strength_id){
		$tabletsData = $this->adminModel->get_ajax_tablet_prices($drug_id,$drug_strength_id);
		$cart_data = $this->mainModel->checkUserCartByCat($patient_id,$category_id);

		$resData = array();
		if(count($tabletsData)>0){
			$content = ' <nav aria-label="breadcrumb">';
			foreach ($tabletsData as $pdata) {  
			$cart_data = $this->mainModel->checkUserCartByCat($patient_id,$category_id);
			$sel = "";
			if(count($cart_data)>0 && $cart_data[0]['drug_price_id'] == $pdata['drug_price_id']): $sel = "checked"; endif;

			if($data['user_type'] == 2){
				$content.='<ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"> <label> 
                   <input style="margin-bottom: .5rem;" type="radio" '.$sel.'  class="radio drug_price_id" name="drug_price_id" value="'.$pdata['drug_price_id'].'" required> '.$pdata['tablet_count'].'</h6> </label>
                 </li>
              </ol>';
			}else{
				$content.='<ol class="breadcrumb slider-breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"> <label><h6 class="head">'.$pdata['tablet_count'].'</h6>
                   <input style="margin-bottom: .5rem;" type="radio" '.$sel.'  class="radio drug_price_id" name="drug_price_id" value="'.$pdata['drug_price_id'].'" required> $'.$pdata['price'].'</h6><h6 class="head">$'.$settings[0]['doctor_consultation_fee'].' Doctor Consultation Fee Required</h6></label>
                 </li>
              </ol>';
			}
            
          	}             
            $content.= '</nav>';
			$resData['status'] = true;
			$resData['price_data'] = $content;
		}else{
			$resData['status'] = false;
		}
		}else{
			$resData['status'] = false;
		}
		echo json_encode($resData);
	}



	public function hair_loss_questionnaire() {
		$data['category_id'] = 2;
		$data['categories'] = $this->adminModel->getCategories();
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/quiz',$data);	
		$this->load->view('templates/frontend/footer');
	}

	public function ed_questionnaire() { 
		$data['category_id'] = 1;
		$data['categories'] = $this->adminModel->getCategories();
    	$this->load->view('templates/frontend/header');
		$this->load->view('frontend/quiz',$data);	
		$this->load->view('templates/frontend/footer');
	} 


	public function logout() {	
		$this->session->unset_userdata('user_type'); 
		$this->session->unset_userdata('user_id');
		$this->session->sess_destroy();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
    	$this->output->set_header("Pragma: no-cache");
		redirect(base_url('/'), 'Location');
	}

	

	/*public function login() {
		$this->load->view('index');	
	}  

	public function forgotPassword() {
		$this->session->unset_userdata('userOtp');
		$this->session->unset_userdata('utype');
		$this->session->unset_userdata('userid');
		$this->load->view('forgotPassword');	
	} 

	public function submitForgotPassword(){
		$response = array();
	    $emailMobile = $this->input->post('emailMobile');   
	    $loginOtp = $this->input->post('loginOtp');
	    $password = $this->input->post('password');

	    if($password != ""){
	    	$userTypeCode = $this->session->userdata('utype');
	    	$userUniqueId = $this->session->userdata('userid');

	    	$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			'test_password' =>$password		
			);		
			if($userTypeCode == 2){
				$update = $this->mainModel->updateBusinessPassword($form_data,$userUniqueId);
			}else{
				$update = $this->mainModel->updateUserPassword($form_data,$userUniqueId);
			}
			$response["status"] = true; 
			$response["otpStatus"] = false; 
			$response["pStatus"] = true;      
			$response["message"] = "Password changed successfully";
			echo json_encode($response);
			$this->session->unset_userdata('userOtp');
			$this->session->unset_userdata('utype');
			$this->session->unset_userdata('userid');
	    	return false;
	    }

	    if($loginOtp != ""){ 
	    	if($loginOtp == $this->session->userdata('userOtp')){
	    		$response["status"] = true;    
	    		$response["otpStatus"] = true;     
				$response["message"] = "OTP validated. Please enter new password.";
	    	}else{
	    		$response["status"] = false;
	    		$response["otpStatus"] = false;      
				$response["message"] = "Invalid OTP";
	    	}
	    	echo json_encode($response);
	    	return false;
	    }

	    
	    $userUniqueId = $this->mainModel->checkUserMobileEmail($emailMobile); 
	    $businessUniqueId = $this->mainModel->checkBusinessMobileEmail($emailMobile);  
	    $response["otpStatus"] = false; 
	    if($userUniqueId != '0'){
			$userInfo = $this->mainModel->getUserInfo($userUniqueId);
			if($userInfo[0]['userStatus'] == 2){
				$response["status"] = false;      
				$response["message"] = "Your account is disabled. Please contact to admin.";
			}else if($userInfo[0]['userStatus'] == 0){
				$response["status"] = false;      
				$response["message"] = "Your account is Inactive. Please contact to admin.";
			}else if($userInfo[0]['userStatus'] == 1){
				$otp = $this->mainModel->generateSMSKey();
				$this->sendEmail($userInfo[0]['email'],$userInfo[0]['mobile'],$otp);
				$result = $this->mainModel->sendsms($userInfo[0]['mobile'],$otp);
				$this->mainModel->updateSms($userInfo[0]['mobile'],$otp);
				$response["status"] = true;      
				$response["message"] = "OTP sent to your email. Please enter here.";
				$this->session->set_userdata('userOtp', $otp);
				$this->session->set_userdata('utype', $userInfo[0]['userTypeCode']);
				$this->session->set_userdata('userid', $userInfo[0]['userUniqueId']);
			}else{
				$response["status"] = false;      
				$response["message"] = "System Error. Try again later";
			}
	    }else if($businessUniqueId  != '0'){
			//$result = $this->servModel->sendPasswordActivation($user_key, $email);
			$userInfo = $this->mainModel->getBusinessInfo($businessUniqueId);
			if($userInfo[0]['businessStatus'] == 2){
				$response["status"] = false;      
				$response["message"] = "Your account is disabled. Please contact to admin.";
			}else if($userInfo[0]['businessStatus'] == 0){
				$response["status"] = false;      
				$response["message"] = "Your account is Inactive. Please contact to admin.";
			}else if($userInfo[0]['businessStatus'] == 1){			
				$otp = $this->mainModel->generateSMSKey();	
				$this->sendEmail($userInfo[0]['email'],$userInfo[0]['mobile'],$otp);
				$result = $this->mainModel->sendsms($userInfo[0]['mobile'],$otp);
				$this->mainModel->updateSms($userInfo[0]['mobile'],$otp);
				$response["status"] = true;      
				$response["message"] = "OTP sent to your email. Please enter here.";
				$this->session->set_userdata('userOtp', $otp);
				$this->session->set_userdata('utype', $userInfo[0]['userTypeCode']);
				$this->session->set_userdata('userid', $userInfo[0]['userUniqueId']);
			}else{
				$response["status"] = false;      
				$response["message"] = "System Error. Try again later";
			}
	    }else{
	    	$response["status"] = false;      
			$response["message"] = "This Email/Mobile does not exist.";
	    }
    	echo json_encode($response); 
	}   

	public function sendWelcomeOtpEmail($email,$mobile,$otp){
		$config = Array(
			'protocol' => 'sendmail',
			'mailpath' => '/usr/sbin/sendmail',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);
	
		$query = $this->db->get_where('tbl_communications', array('commid' => '13'), 1);
		$row = $query->result_array();
		$email_message = $row[0]['message'];
		$subject =  $row[0]['subject'];
		$finalmessage = str_replace('MOBILE',$mobile,$email_message);
		$finalmessage = str_replace('ENTEROTP',$otp,$finalmessage);
		
		//$list = array($email, 'info@efileservices.in');
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL,'Aster IT');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($finalmessage);
		$this->email->send();
	}


	public function sendEmail($email,$mobile,$otp){
		$config = Array(
			'protocol' => 'sendmail',
			'mailpath' => '/usr/sbin/sendmail',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);
	
		$query = $this->db->get_where('tbl_communications', array('commid' => '2'), 1);
		$row = $query->result_array();
		$email_message = $row[0]['message'];
		$subject =  $row[0]['subject'];
		$finalmessage = str_replace('MOBILE',$mobile,$email_message);
		$finalmessage = str_replace('OTP',$otp,$finalmessage);
		
		//$list = array($email, 'info@efileservices.in');
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL,'Aster IT');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($finalmessage);
		$this->email->send();
	}

	public function loginAction() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->mainModel->checkUser($mobile,$password);
		$checkBusiness = $this->mainModel->checkBusiness($mobile,$password);
		$checkBusinessAvail = $this->mainModel->checkBusinessAvail($mobile,$password);
		
		$userinfo = $this->mainModel->getUserInfo($checkUser);
		$businessInfo = '';
		if($checkBusiness !='no_business'){
		    $businessInfo = $this->mainModel->getBusinessInfo($checkBusiness);
		}		
		if($checkBusinessAvail=='business_avail'){			
			if($businessInfo[0]['businessStatus'] == 1){
				$this->session->set_userdata('userTypeCode', $businessInfo[0]['userTypeCode']);
				$this->session->set_userdata('loginUniqueId', $businessInfo[0]['businessUniqueId']);
				$this->session->set_userdata('userType', $businessInfo[0]['userType']);
				$message = "success"; 
			}else if($businessInfo[0]['businessStatus'] == 0){
				$message= "Your account is inactive";
			}else if($businessInfo[0]['businessStatus'] == 2){
				$message= "Your account is disabled";
			}
			
		}else if($checkUser=='no_user'){
			$message= "Please Check Credentials";
		}else{			
   			if($userinfo[0]['userStatus'] == 1){
   				$this->session->set_userdata('userTypeCode', $userinfo[0]['userTypeCode']);
				$this->session->set_userdata('loginUniqueId', $userinfo[0]['userUniqueId']);
				$this->session->set_userdata('userType', $userinfo[0]['userType']);
	   			$message = "success";
			}else if($userinfo[0]['userStatus'] == 0){
				$message= "Your account is inactive";
			}else if($userinfo[0]['userStatus'] == 2){
				$message= "Your account is disabled";
			}

		}
		echo $message;
	}

	public function saveNewBusiness(){ //echo "ggg"; die();
		$response = array();
        $restkey = md5($this->mainModel->generateRESTKey());
        $otpverify = 0;
        $otpValue = $this->input->post('otpValue');
        $loginOtp = $this->input->post('loginOtp');
        $businessName = $this->input->post('businessName');        
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $password = $this->input->post('password');
        $address = $this->input->post('address');
        $businessUrl = $this->input->post('businessUrl');
        $ssnNumber = $this->input->post('ssnNumber');
        $businessId = $this->mainModel->generateBusinessID();
        $form_data = array(
			'userTypeCode' =>BUSINESS,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' =>  $businessName,
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($password),
			'address' => $address,
			'businessUrl' => $businessUrl,
			'ssnNumber' => $ssnNumber,
			'businessStatus' =>1
		);

        if(!empty($businessName) && !empty($email) && !empty($mobile) && !empty($password)){ 
	    	$check = $this->mainModel->checkBusinessUserAvailable($mobile,$email);
	    	if($check !=0){ 
    			$response["status"] = false; 
				$response["otpStatus"] = false; 
				$response["message"] = "Email/Mobile already exist.";
				echo json_encode($response);
	    		return false;
	    	}	    	
	    } 
    	if($this->session->userdata('userOtp') == ""){  
    		$otp = $this->mainModel->generateSMSKey();
			$this->sendWelcomeOtpEmail($email,$mobile,$otp); 
			$result = $this->mainModel->sendSmsEmail($mobile,$otp);
			//$this->mainModel->updateSms($mobile,$otp);
			$response["status"] = true;      
			$response["message"] = "OTP sent to your email. Please enter here.";
			$response["otpStatus"] = false; 
			$this->session->set_userdata('userOtp', $otp);			
			echo json_encode($response);
    		return false;
    	}	    	
	    

        if($loginOtp != ""){ 
	    	if($loginOtp == $this->session->userdata('userOtp')){
	    		$insert = $this->mainModel->insertBusiness($form_data);	
				if($insert>0) {
					$this->session->set_userdata('userid', $restkey);
					$response["status"] = true; 
					$response["otpStatus"] = true;   
					$response["message"] = "OTP Validated Successsfully";
					$this->session->unset_userdata('userOtp');
				}	
	    	}else{
	    		$response["status"] = true;
	    		$response["otpStatus"] = false;      
				$response["message"] = "Invalid OTP";
	    	}
	    	echo json_encode($response);
	    	return false;
	    }
	}

	public function businessSignup(){
		$this->session->unset_userdata('userOtp');
		$this->load->view('business-signup');
	}*/
    
}