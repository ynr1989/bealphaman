<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");
 
class Doctorcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id')) || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4){
    		redirect(base_url(), 'Location');
    	} 
		$this->load->model('AdminDB_model', 'adminModel');	
		$this->load->model('MainDB_model', 'mainModel');
		$this->load->helper('url');	
		$this->load->library('session');
    }  

    public function global_functions(){    	 
    	$data['user_type'] = $this->session->userdata('user_type'); 
    	$data['user_id'] = $this->session->userdata('user_id'); 
    	return $data;
    }

    public function schedule() {

    	if($this->input->post('submit')!=""){
            $shift_start  = date("H:i", strtotime($this->input->post('shift_start')));
            $shift_end  = date("H:i", strtotime($this->input->post('shift_end')));
    		$data = array(
    			'day' => date('l', strtotime($this->input->post('schedule_date'))),
		        'schedule_date' => $this->input->post('schedule_date'),
		        'is_availability' => $this->input->post('is_availability'),
		        'doctor_id' => $this->session->userdata('user_id'), 
		        'shift_start' => $shift_start,
		        'shift_end' => $shift_end
			); 
			$this->adminModel->insertDoctorSchdule($data);
    	}
		$data = $this->global_functions();
		$data['schedules'] = $this->adminModel->getDoctorSchdules($data['user_id']);
		$this->load->view('templates/admin/header',$data);	
		$this->load->view('admin/schedule/schedules',$data);	
		$this->load->view('templates/admin/footer',$data);	
	} 

	public function deleteschedule(){
    	$data = $this->global_functions();
        $id  = $this->input->post('id'); 
        $delete = $this->adminModel->deleteschedule($id);
        /*if($delete){
            $this->session->set_flashdata('message', 'Image deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Image delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');*/
    }
    	 
}