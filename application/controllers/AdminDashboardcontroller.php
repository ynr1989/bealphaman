<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");
 
class AdminDashboardcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id')) || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4){
    		redirect(base_url(), 'Location');
    	} 
		$this->load->model('AdminDB_model', 'adminModel');	
		$this->load->model('MainDB_model', 'mainModel');
		$this->load->helper('url');	
		$this->load->library('session');
    }  

    public function global_functions(){    	 
    	$data['user_type'] = $this->session->userdata('user_type'); 
    	$data['user_id'] = $this->session->userdata('user_id'); 
    	if($data['user_type'] == 2){
    		$data['userInfo'] = $this->adminModel->getUserInfo($data['user_id']);
    		$data['userName'] = $data['userInfo'][0]['first_name']." ".$data['userInfo'][0]['last_name'];
    		$data['email'] = $data['userInfo'][0]['email'];
    		$data['mobile'] = $data['userInfo'][0]['mobile'];
    		$data['user_status'] = $data['userInfo'][0]['user_status']; 
    		$data['user_id'] = $data['userInfo'][0]['user_id'];
    		$data['passwordFlag'] = $data['userInfo'][0]['passwordFlag']; 
    		//$data['profilePic'] = $data['userInfo'][0]['passwordFlag']; 
    		//$data['employeeId'] = ''; 
    	}else{
    		$data['userInfo'] = $this->adminModel->getUserInfo($data['user_type']);
    		$data['userName'] = $data['userInfo'][0]['first_name']." ".$data['userInfo'][0]['last_name'];
    		$data['email'] = $data['userInfo'][0]['email'];
    		$data['mobile'] = $data['userInfo'][0]['mobile'];
    		$data['user_status'] = $data['userInfo'][0]['user_status']; 
    		$data['user_id'] = $data['userInfo'][0]['user_id']; 
    	}
 
    	/*if($data['userTypeCode'] == 1){
    		$data['businessName'] = "Aster IT";
    		if($data['userInfo'][0]['profilePic']){
    			$pic =  base_url()."assets/profilePics/".$data['userInfo'][0]['profilePic'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		$data['businessLogo'] = $pic;
    	}else{
    		$data['bInfo'] = $this->adminModel->getBusinessInfo($data['loginUniqueId']);
    		$data['businessName'] = $data['bInfo'][0]['businessName'];

    		if($data['bInfo'][0]['businessLogo']){
    			$pic =  base_url()."assets/businessLogos/".$data['bInfo'][0]['businessLogo'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		
    		$data['businessLogo'] = $pic;
    	}*/ 
    	
    	return $data;
    }

    public function submitSettings(){
		$data = $this->global_functions(); 
		$oid = $this->input->get('oid');
		$form_data = array(
                'site_name' => $this->input->post('site_name'),
                'paypal_business_email' => $this->input->post('paypal_business_email'),
                'doctor_consultation_fee' => $this->input->post('doctor_consultation_fee') 
            );
		$check = $this->adminModel->checkSettingsInfo();
		if(count($check) == 0){
			$this->adminModel->insertSettings($form_data);
		}else{
			$id = $check[0]['id'];
			$this->adminModel->updateSettings($form_data,$id);
		}
        
        
	    $this->session->set_flashdata('message', 'Data updated successfully');
	    redirect(base_url('admin/settings'), 'Location');
			
	}

	public function settings() {
		$data = $this->global_functions(); 
		$data['sdata'] = $this->adminModel->getSettings();		
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/settings',$data);
		$this->load->view('templates/admin/footer',$data);
	}
	
	public function dashboard() {
		$data = $this->global_functions();
		$this->load->view('templates/admin/header',$data);	
		$this->load->view('admin/dashboard',$data);	
		$this->load->view('templates/admin/footer',$data);	
	}   

	public function changePassword(){
		$data = $this->global_functions();		
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/change-password',$data); 
		$this->load->view('templates/admin/footer');
	}

	public function updateProfile(){
		$data = $this->global_functions();		
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/update-profile',$data);
		$this->load->view('templates/admin/footer');
	}
	public function profileSubmit() {
		$data = $this->global_functions();
		if($_FILES["profilePic"]["name"]!='') {
			$target_dir = "assets/profilePics/";
			$target_file = $target_dir . basename($_FILES["profilePic"]["name"]);
			$file_name = basename($_FILES["profilePic"]["name"]);
			move_uploaded_file($_FILES["profilePic"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['profilePic'];
		}
		$form_data = array(
			'first_name' => $this->input->post('firstName'),
			'last_name' => $this->input->post('lastName'),
			'address' => $this->input->post('address'),
			'gender' => $this->input->post('gender'),
			'profile_pic' => $file_name,
			//'ssnNumber' => $this->input->post('ssnNumber'),
			//'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob')
		);		
		$update = $this->adminModel->updateProfileDetails($form_data,$data['loginUniqueId']);
		$this->session->set_flashdata('message', 'Profile updated successfully');		
		redirect(base_url('admin/updateProfile'), 'Location');
	}


	//Medicines
    public function add_drug(){
		$data = $this->global_functions();	
		$data['cats'] = $this->adminModel->getCategories();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/add_drug',$data);
		$this->load->view('templates/admin/footer');
	}

	public function drugStatusUpdate(){
    	$drug_status= $this->input->post('drug_status');
    	$drug_id = $this->input->post('drug_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'drug_status' =>$drug_status
		    );
	    $this->db->where('drug_id',$drug_id);
		$this->db->update('drugs',$form_data);
        echo json_encode($response);
    }

	public function drug_list(){
		$data = $this->global_functions();	
		$data['drugList'] = $this->adminModel->getDrugsList();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/drug_list',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function uploadDrugImages($drug_id){
		for($i = 0; $i <= count($this->input->post('attachment')); $i++) {
			$attachment = $this->input->post('attachment');
			$values = array($attachment[$i]);			
			if(count(array_unique($values))!=1)
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/uploads/drug_images/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"image" => $file_name,
				    "drug_id" => $drug_id,
				);
				$this->adminModel->inserDrugImages($documentsData);
			}
		}
	}

	public function update_drug(){
    	$response = array(); 
        $form_data = array(			
            'category_id' =>$this->input->post('category_id'), 
			'drug_code' =>  $this->input->post('drug_code'), 
			'drug_name' => $this->input->post('drug_name'),
			'drug_sub_name' => $this->input->post('drug_sub_name'),
			'drug_description' => $this->input->post('drug_description'),
		);
		$drug_id = $this->input->post('drug_id');
		$check = $this->adminModel->checkExistDrugAvailable($this->input->post('drug_name'),$drug_id);
		if($check=='0') {

			$this->uploadDrugImages($drug_id); 
			$updateUser = $this->adminModel->updateDrug($form_data,$drug_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['drug_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Data Name Already Exist';
		}
       echo json_encode($response);
	}

	public function delete_drug_image(){
    	$data = $this->global_functions();
        $drug_image_id  = $this->input->get('drug_image_id ');
        $img_key = $this->input->get('img_key');
        $delete = $this->adminModel->delete_drug_image($drug_image_id ,$img_key);
        if($delete){
            $this->session->set_flashdata('message', 'Image deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Image delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function delete_drug(){
        $drug_id  = $this->input->get('drug_id');
        $delete = $this->adminModel->deleteDrug($drug_id);
        if($delete){
            $this->session->set_flashdata('message', 'Drug deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Drug delete Failed');
        }
        redirect(base_url('admin/drug_list'), 'Location');
    }

	public function save_drug(){
    	$response = array();
    	$data = $this->global_functions(); 
        $form_data = array(
            'category_id' =>$this->input->post('category_id'), 
			'drug_code' =>  $this->input->post('drug_code'), 
			'drug_name' => $this->input->post('drug_name'),
			'drug_sub_name' => $this->input->post('drug_sub_name'),
			'drug_description' => $this->input->post('drug_description'),
			'drug_status' => 0 
		);
		 
		$check = $this->adminModel->checkDrugNameAvailable($this->input->post('drug_name'));
		if($check=='0') {			
			$insertUser = $this->adminModel->insertDrug($form_data);
			
			if($insertUser>0) {
			     $this->uploadDrugImages($insertUser); 
			}	
			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Drug Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_drug(){
		$data = $this->global_functions();	 
		$data['drug_id'] = $this->input->get('drug_id');
		$data['cats'] = $this->adminModel->getCategories();	
        $data['drugInfo'] = $this->adminModel->getDrugInfo($data['drug_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/edit_drug',$data);
		$this->load->view('templates/admin/footer');
	}

	//Drugs Types
	 public function add_drug_type(){
		$data = $this->global_functions();	
		$data['cats'] = $this->adminModel->getDrugsList();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/add_drug_strength',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function drugTypeStatusUpdate(){
    	$strength_status= $this->input->post('strength_status');
    	$drug_strength_id = $this->input->post('drug_strength_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'strength_status' =>$strength_status
		    );
	    $this->db->where('drug_strength_id',$drug_strength_id);
		$this->db->update('drug_types',$form_data);
        echo json_encode($response);
    }

	public function drug_types(){
		$data = $this->global_functions();	
		$data['drugList'] = $this->adminModel->getDrugStrength();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/drug_strength',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function update_drug_type(){
    	$response = array(); 
        $form_data = array(			
            'drug_id' =>$this->input->post('drug_id'), 
			'strength' =>  $this->input->post('strength') 
		);
		$drug_strength_id = $this->input->post('drug_strength_id');
		$check = $this->adminModel->checkExistDrugTypeAvailable($this->input->post('drug_id'),$this->input->post('strength'),$drug_strength_id,$this->input->post('drug_id'));
		if($check=='0') {  
			$updateUser = $this->adminModel->updateDrugType($form_data,$drug_strength_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['drug_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'This type Already Exist';
		}
       echo json_encode($response);
	} 

    public function delete_drug_type(){
        $drug_strength_id = $this->input->get('drug_strength_id');
        $delete = $this->adminModel->deleteDrugType($drug_strength_id);
        if($delete){
            $this->session->set_flashdata('message', 'Drug deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Drug delete Failed');
        }
        redirect(base_url('admin/drug_types'), 'Location');
    }

	public function save_drug_type(){
    	$response = array();
    	$data = $this->global_functions(); 
        $form_data = array(
            'drug_id' =>$this->input->post('drug_id'), 
			'strength' =>  $this->input->post('strength'),
			'strength_status' => 0 
		);
		 
		$check = $this->adminModel->checkDrugTypeNameAvailable($this->input->post('drug_id'),$this->input->post('strength'));
		if($check=='0') {			
			$insertUser = $this->adminModel->insertDrugType($form_data);

			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Drug Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_drug_type(){
		$data = $this->global_functions();	 
		$data['drug_strength_id'] = $this->input->get('drug_strength_id');
		$data['drugInfo'] = $this->adminModel->getDrugsList();	
        $data['drugTypeInfo'] = $this->adminModel->getDrugStrengthInfo($data['drug_strength_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/edit_drug_strength',$data);
		$this->load->view('templates/admin/footer');
	}

	//Drugs Doses
	 public function add_drug_dose(){
		$data = $this->global_functions();	
		$data['drugs'] = $this->adminModel->getDrugsList();
		$data['drugtypes'] = $this->adminModel->getDrugStrength();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/add_drug_tablet_set',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function drug_doses(){
		$data = $this->global_functions();	
		$data['drugList'] = $this->adminModel->getDrugTablets();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/drug_tablet_set',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function drugDoseStatusUpdate(){
    	$tablet_set_status= $this->input->post('tablet_set_status');
    	$tablet_set_id = $this->input->post('tablet_set_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'tablet_set_status' =>$tablet_set_status
		    );
	    $this->db->where('tablet_set_id',$tablet_set_id);
		$this->db->update('drug_tablets_set',$form_data);
        echo json_encode($response);
    }

	public function update_drug_dose(){
    	$response = array(); 
    	$drug_id = $this->input->post('drug_id');
    	//$drug_strength_id = $this->input->post('drug_strength_id');
    	$tablet_count = $this->input->post('tablet_count');
    	$tablet_set_id = $this->input->post('tablet_set_id');
        $form_data = array(			
            'drug_id' =>$drug_id,
			'tablet_count' =>  $tablet_count
		); 
		$check = $this->adminModel->checkExistDrugDoseAvailable($drug_id,$tablet_set_id,$tablet_count);
		if($check=='0') {  
			$updateUser = $this->adminModel->updateDrugDose($form_data,$tablet_set_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['drug_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'This type Already Exist';
		}
       echo json_encode($response);
	} 

    public function delete_drug_dose(){
        $tablet_set_id = $this->input->get('tablet_set_id');
        $delete = $this->adminModel->deleteDrugDose($tablet_set_id);
        if($delete){
            $this->session->set_flashdata('message', 'Dose deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Dose delete Failed');
        }
        redirect(base_url('admin/drug_doses'), 'Location');
    }

	public function save_drug_dose(){
    	$response = array();
    	$data = $this->global_functions(); 
    	$drug_id = $this->input->post('drug_id');
    	$tablet_count = $this->input->post('tablet_count');
        $form_data = array(
        	'drug_id' =>$drug_id,
			'tablet_count' =>  $tablet_count,
			'tablet_set_status' => 1
		);
		 
		$check = $this->adminModel->checkDrugDoseNameAvailable($drug_id,$tablet_set_id);
		if($check=='0') {			
			$insertUser = $this->adminModel->insertDrugDose($form_data);

			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Drug Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_drug_dose(){
		$data = $this->global_functions();	 
		$data['tablet_set_id'] = $this->input->get('tablet_set_id');
		$data['drugs'] = $this->adminModel->getDrugsList();
		$data['drugtypes'] = $this->adminModel->getDrugStrength();
        $data['doseInfo'] = $this->adminModel->getDrugDoseInfo($data['tablet_set_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/edit_drug_tablet_set',$data);
		$this->load->view('templates/admin/footer');
	}

	//Drug prices 
	 public function add_drug_price(){
		$data = $this->global_functions();	
		$data['drugs'] = $this->adminModel->getDrugsList();
		$data['drugtypes'] = $this->adminModel->getDrugStrength();
		$data['tablets'] = $this->adminModel->getDrugTablets();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/add_drug_price',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function drug_prices(){
		$data = $this->global_functions();	
		$data['drugList'] = $this->adminModel->getDrugPrices();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/drug_prices',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function drugPriceStatusUpdate(){
    	$tablet_set_status= $this->input->post('tablet_set_status');
    	$tablet_set_id = $this->input->post('tablet_set_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'tablet_set_status' =>$tablet_set_status
		    );
	    $this->db->where('tablet_set_id',$tablet_set_id);
		$this->db->update('drug_tablets_set',$form_data);
        echo json_encode($response);
    }

	public function update_drug_price(){
    	$response = array(); 
    	$drug_strength_id = $this->input->post('drug_strength_id');
    	$drug_id = $this->input->post('drug_id');
    	$tablet_set_id = $this->input->post('tablet_set_id');
    	$price = $this->input->post('price');
    	$drug_price_id = $this->input->post('drug_price_id');
        $form_data = array(			
            'drug_strength_id' =>  $drug_strength_id,
            'drug_id' =>$drug_id, 
			'tablet_set_id' =>  $tablet_set_id,
			'price' =>  $price
		); 
		$check = $this->adminModel->checkExistDrugPriceAvailable($drug_id,$drug_strength_id,$tablet_set_id,$price,$drug_price_id);
		if($check=='0') {  
			$updateUser = $this->adminModel->updateDrugPrice($form_data,$drug_price_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['drug_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'This type Already Exist';
		}
       echo json_encode($response);
	} 

    public function delete_drug_price(){
        $drug_price_id = $this->input->get('drug_price_id');
        $delete = $this->adminModel->deleteDrugPrice($drug_price_id);
        if($delete){
            $this->session->set_flashdata('message', 'Dose deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Dose delete Failed');
        }
        redirect(base_url('admin/drug_prices'), 'Location');
    }

	public function save_drug_price(){
    	$response = array();
    	$data = $this->global_functions(); 
    	$drug_strength_id = $this->input->post('drug_strength_id');
    	$drug_id = $this->input->post('drug_id');
    	$tablet_set_id = $this->input->post('tablet_set_id');
    	$price = $this->input->post('price');
        $form_data = array(
        	'drug_strength_id' =>  $drug_strength_id,
            'drug_id' =>$drug_id, 
			'tablet_set_id' =>  $tablet_set_id,
			'price' =>  $price,
			'price_status' => 1 
		);
		 
		$check = $this->adminModel->checkDrugPriceNameAvailable($drug_id,$drug_strength_id,$tablet_set_id,$price);
		if($check=='0') {			
			$insertUser = $this->adminModel->insertDrugPrice($form_data);

			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Drug Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_drug_price(){
		$data = $this->global_functions();	 
		$data['drug_price_id'] = $this->input->get('drug_price_id');
		$data['drugs'] = $this->adminModel->getDrugsList();
		$data['drugtypes'] = $this->adminModel->getDrugStrength();
        $data['priceInfo'] = $this->adminModel->getDrugPriceInfo($data['drug_price_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/drugs/edit_drug_price',$data);
		$this->load->view('templates/admin/footer');
	}

	//Doctors
	public function add_doctor(){
		$data = $this->global_functions();	 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/doctors/add_doctor',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function doctors(){
		$data = $this->global_functions();	
		$data['userList'] = $this->adminModel->getUsersByType(DOCTOR);	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/doctors/doctors',$data);
		$this->load->view('templates/admin/footer');
	}  


	public function edit_doctor(){
		$data = $this->global_functions();	 
		$data['user_id'] = $this->input->get('user_id');
        $data['userInfo'] = $this->adminModel->getUserInfo($data['user_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/doctors/edit_doctor',$data);
		$this->load->view('templates/admin/footer');
	}

	public function pharmacies(){
		$data = $this->global_functions();	
		$data['userList'] = $this->adminModel->getUsersByType(PHARMACY);	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pharmacies/pharmacies',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function add_pharmacy(){
		$data = $this->global_functions();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pharmacies/add_pharmacy',$data);
		$this->load->view('templates/admin/footer');
	}   

	public function edit_pharmacy(){
		$data = $this->global_functions();	 
		$data['user_id'] = $this->input->get('user_id');
        $data['userInfo'] = $this->adminModel->getUserInfo($data['user_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pharmacies/edit_pharmacy',$data);
		$this->load->view('templates/admin/footer');
	}

	public function patients(){
		$data = $this->global_functions();	
		$data['userList'] = $this->adminModel->getUsersByType(PATIENT);	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/patients/patients',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function patient_questionnaire() { 
		$data = $this->global_functions();	
		$category_id = $this->input->get('category_id');
		$user_id = $this->input->get('user_id');
        $data['categories'] = $this->adminModel->checkUserQuestionaire($category_id,$user_id);
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/patients/patient_questionnaire',$data);   
        $this->load->view('templates/admin/footer');
    }

    public function patient_treatment() { 
		$data = $this->global_functions();	
		$category_id = $this->input->get('category_id');
		$user_id = $this->input->get('user_id'); 
        $data['drugs'] = $this->adminModel->getDrugsListByCat($category_id);
        $data['drugtypes'] = $this->adminModel->getDrugStrength();
        $data['tablets'] = $this->adminModel->getDrugTablets();
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/patients/patient_treatment',$data);   
        $this->load->view('templates/admin/footer');
    }

	public function patient_update_profile() {
        $data = $this->global_functions();
        $data['rdata'] = $this->adminModel->getUserInfo($_GET['user_id']);
        $data['pdata'] = $this->adminModel->getPatientInfo($_GET['user_id']);
        $this->load->view('templates/admin/header',$data); 
        $this->load->view('admin/patients/patient_update_profile',$data);   
        $this->load->view('templates/admin/footer');
    }

	public function add_patient(){
		$data = $this->global_functions();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/patients/add_patient',$data);
		$this->load->view('templates/admin/footer');
	}  

	public function edit_patient(){
		$data = $this->global_functions();	 
		$data['user_id'] = $this->input->get('user_id');
        $data['userInfo'] = $this->adminModel->getUserInfo($data['user_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/patients/edit_patient',$data);
		$this->load->view('templates/admin/footer');
	}

	public function questionnaire(){
		$data = $this->global_functions();	
		$data['cats'] = $this->adminModel->getCategories();				
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/questionnaire/questionnaire',$data);
		$this->load->view('templates/admin/footer');
	}

	public function delete_questionnaire(){
        $question_id  = $this->input->get('question_id');
        $delete = $this->adminModel->deleteQuestion($question_id);
        if($delete){
            $this->session->set_flashdata('message', 'Drug deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Drug delete Failed');
        }
        redirect(base_url('admin/questionnaire'), 'Location');
    }

	public function questionStatusUpdate(){
    	$question_status= $this->input->post('question_status');
    	$question_id = $this->input->post('question_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'question_status' =>$question_status
		    );
	    $this->db->where('question_id',$question_id);
		$this->db->update('questions',$form_data);
        echo json_encode($response);
    }

	public function add_questionnaire(){
		$data = $this->global_functions();	
		$data['cats'] = $this->adminModel->getCategories();
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/questionnaire/add_questionnaire',$data);
		$this->load->view('templates/admin/footer');
	}

	public function save_questionnaire(){
    	$response = array();
    	$data = $this->global_functions(); 
        $form_data = array(
            'category_id' =>$this->input->post('category_id'), 
			'question_name' =>  $this->input->post('question_name'), 
			'question_description' => $this->input->post('question_description'), 
			'question_status' => 0 
		);
		 
		$check = $this->adminModel->checkQuestionNameAvailable($this->input->post('question_name'));
		if($check=='0') {			
			$question_id = $this->adminModel->insertQuestion($form_data);
			
			if($question_id>0) {
			     for($i = 0; $i <= count($this->input->post('ques_options')); $i++) {
					$ques_options = $this->input->post('ques_options');
					$values = array($ques_options[$i]);			
					if(count(array_unique($values))!=1)
					{	
						if($_FILES["attachment"]["name"]!='') {
							$target_dir = "assets/uploads/drug_images/";
							$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
							$file_name = basename($_FILES["attachment"]["name"][$i]);
							move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
						} 

						$documentsData = array(
							"question_option" => $ques_options[$i],
						    "question_id " => $question_id,
						);
						$this->adminModel->insertQuestionOptions($documentsData);
					}
				} 
			}	
			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Drug Name Already Exist';
		}
       echo json_encode($response);
	} 

	public function coupons(){
        $data = $this->global_functions();
        $coupons = $this->adminModel->getCoupons();
        $data['categories'] = $coupons;
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/coupons/coupons',$data);
        $this->load->view('templates/admin/footer',$data);
    }

    public function deleteCoupon(){
        $cat_id = $this->input->post('cat_id');
        $this->adminModel->deleteCoupon($cat_id);
        
            $succes = 'success';
            $success = 1;
            $message = 'Category deleted successfully';
                $this->session->set_flashdata($succes, $message);    

        echo $message;exit;
    }

    public function addCoupon(){
        $data = $this->global_functions();
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/coupons/edit_coupons',$data);
        $this->load->view('templates/admin/footer',$data);                
    }

    public function editCoupon(){
        $data = $this->global_functions();
        $cat_id = $this->uri->segment(3);
        $categories = $this->adminModel->getCoupons($cat_id);
        $data['categories'] = $categories[0];
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/coupons/edit_coupons',$data);
        $this->load->view('templates/admin/footer',$data);        
    }

    public function AddCouponAction(){   
        $insert_data = array(
           'couponCode' => $this->input->post('couponCode'),
           'couponPercentage' => $this->input->post('couponPercentage'),
           'startDate' => $this->input->post('startDate'),
           'endDate' => $this->input->post('endDate'),
           'maxAmount' => $this->input->post('maxAmount'),
           'couponDesc' => $this->input->post('couponDesc'),
           //'couponStatus' => $this->input->post('couponStatus')
        );
            $couponId = $this->input->post('couponId');
            if($couponId == '' || $couponId== 'undefined' || $couponId==NULL){
                $checkCat = $this->adminModel->checkCouponCode($this->input->post('couponCode'));

                if($checkCat==0){
                    $update = $this->adminModel->insertCoupon($insert_data);
                    /*if($update){
                        $updata_data = array('category_ref_id'=>'CAT_0'.$update);
                         $update = $this->Mainmodel->updateCoupon($updata_data,$update);
                    }*/
                }else{
                    $this->session->set_flashdata('failure', 'Coupon code already exist!..');
                    header("Location:".base_url('admin/coupons'));
                    die();
                }
                
            }else{                
                //$insert_data['cat_updatedat'] =  date('Y-m-d H:i:s');
                $update = $this->adminModel->updateCoupon($insert_data,$couponId);
            }
        if($update){
            $this->session->set_flashdata('success', 'Coupon details updated successfully!..');    
        }else{
            $this->session->set_flashdata('failure', 'Failed to update Coupon details!..');
        }
        header("Location:".base_url('admin/coupons'));
    }

	public function changeCouponStatus(){
        $cat_id = $this->input->post('cat_id');
        $status = $this->input->post('status');
        $update = $this->adminModel->changeCouponStatus($cat_id,$status);
        if($update){
            $succes = 'success';
            $success = 1;
            $message = 'Status changes successfully';
        }else{
            $succes = 'failure';
            $success = 0;
            $message = 'Filed to change status';
        }
        $this->session->set_flashdata($succes, $message); 
        echo $message;exit;
    }

	//Get Ajax Calls
	public function get_ajax_strength_data(){
		$drug_id = $this->input->post('drug_id'); 
		$strenthData = $this->adminModel->get_ajax_strength_data($drug_id);
		//$tabletsData = $this->adminModel->get_ajax_tablets_data($drug_id,$drug_strength_id);
		$resData = array();
		if(count($strenthData)>0){
			$resData['status'] = true;
			$resData['strength_data'] = $strenthData;
		}else{
			$resData['status'] = false;
		}
		echo json_encode($resData);
	}

	public function get_ajax_tablets_data(){
		$drug_id = $this->input->post('drug_id');  
		$tabletsData = $this->adminModel->get_ajax_tablets_data($drug_id);
		$resData = array();
		if(count($tabletsData)>0){
			$resData['status'] = true;
			$resData['tablets_data'] = $tabletsData;
		}else{
			$resData['status'] = false;
		}
		echo json_encode($resData);
	}


	public function update_user(){
    	$response = array(); 
    	$user_id = $this->input->post('user_id');
    	$first_name = $this->input->post('first_name');
    	$last_name = $this->input->post('last_name');
    	$email = $this->input->post('email');
    	$mobile = $this->input->post('mobile');
    	$password = $this->input->post('password');
    	$npi = $this->input->post('npi');
    	$address = $this->input->post('address');
    	$contact_name = $this->input->post('contact_name'); 
    	$licence = $this->input->post('licence');
    	$state = $this->input->post('state');
    	$old_doctor_signature =  $this->input->post('old_doctor_signature');
    	$userInfo = $this->adminModel->getUserInfo($user_id);	
    	$passwordField = $userInfo[0]['password'];
    	if($password!=""){
    		$passwordField = $this->input->post('password');
    	}

    	if($_FILES["attachment"]["name"]) {
			$target_dir = "assets/profilePics/";
			$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
			$doctor_signature = basename($_FILES["attachment"]["name"][$i]);
			move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
		}else{
			$doctor_signature = $old_doctor_signature;
		}

        $form_data = array(
        	'first_name' =>  $first_name,
            'last_name' =>$last_name, 
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' =>  $passwordField,
			'npi' =>  $npi,
			'address' =>  $address,
			'contact_name' =>$contact_name,
 			'state' =>  $state,
			'licence' =>  $licence,
			'doctor_signature' =>$doctor_signature
		);
		$check = $this->adminModel->checkExistUserAvailable($email,$mobile,$user_id);
		if($check=='0') {  
			$updateUser = $this->adminModel->updateUser($form_data,$user_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['drug_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}  

	public function save_user(){
    	$response = array();
    	$data = $this->global_functions(); 
    	$first_name = $this->input->post('first_name');
    	$last_name = $this->input->post('last_name');
    	$email = $this->input->post('email');
    	$mobile = $this->input->post('mobile');
    	$password = $this->input->post('password');
    	$npi = $this->input->post('npi');
    	$address = $this->input->post('address');
    	$user_type = $this->input->post('user_type');
    	$licence = $this->input->post('licence');
    	$state = $this->input->post('state'); 
        $form_data = array(
        	'first_name' =>  $first_name,
            'last_name' =>$last_name, 
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' =>  $password,
			'npi' =>  $npi,
			'address' =>  $address,
			'password' =>  $password,
			'state' =>  $state,
			'licence' =>  $licence,
			'user_status' => 0,
			'user_type' => $user_type 
		);
		 
		$uinfo = $this->adminModel->checkUserMobileEmail($email,$mobile);
		if(count($uinfo) == 0) {			
			if($user_type == 2){
				 $generatePw = $this->adminModel->generatePassword();
				$this->sendPwToEmail($email,$mobile,$generatePw);
			}
			$insertUser = $this->adminModel->insertUser($form_data);

			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if(isset($uinfo) && $uinfo[0]['email']==$email && $uinfo[0]['mobile']==$mobile){
			$response['status'] = false;
			$response['message'] = 'Email And Mobile Already Exist';
		}else if(isset($uinfo) && $uinfo[0]['email'] == $email){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if(isset($uinfo) && $uinfo[0]['mobile']== $mobile){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}
       echo json_encode($response);
	}

	public function sendPwToEmail($email,$mobile,$generatePw){
		$config = Array(
			'protocol' => 'sendmail',
			'mailpath' => '/usr/sbin/sendmail',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);
	
		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
		$row = $query->result_array();
		$email_message = $row[0]['message'];
		$subject =  $row[0]['subject'];
		$finalmessage = str_replace('EMAIL',$email,$email_message);
		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);

		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL,'Be Alpha Man');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($finalmessage);
		$this->email->send();
	}

	public function userStatusChange(){
    	$user_status= $this->input->post('user_status');
    	$user_id = $this->input->post('user_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'user_status' =>$user_status
		    );
	    $this->db->where('user_id',$user_id);
		$this->db->update('users',$form_data);
        echo json_encode($response);
    }

    public function delete_user(){
        $user_id = $this->input->get('user_id');
        $delete = $this->adminModel->delete_user($user_id);
        if($delete){
            $this->session->set_flashdata('message', 'Data deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Data delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    //Testimonials
    public function add_testimonial(){
		$data = $this->global_functions();	 	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/testimonials/add_testimonial',$data);
		$this->load->view('templates/admin/footer');
	}

	public function testimonial_status_update(){
    	$status= $this->input->post('status');
    	$testimonial_id = $this->input->post('testimonial_id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'status' =>$status
		    );
	    $this->db->where('testimonial_id',$testimonial_id);
		$this->db->update('testimonials',$form_data);
        echo json_encode($response);
    }

	public function testimonials(){
		$data = $this->global_functions();	
		$data['rdata'] = $this->adminModel->getTestimonials();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/testimonials/testimonials_list',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function update_testimonial(){
    	$response = array(); 
    	$time = time();
		$target_dir = "assets/uploads/testimonials/";
		if($_FILES["image"]["name"]!='') {
			$target_file = $target_dir . $time."_".basename($_FILES["image"]["name"]);
			$image = $time."_".basename($_FILES["image"]["name"]);
			move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
		}else{
			$image = $this->input->post('old_image');
		}
        $form_data = array(			
            'author_name' =>$this->input->post('author_name'), 
			'description' =>  $this->input->post('description'), 
			'image' => $image 
		);
		$testimonial_id = $this->input->post('testimonial_id');
		$check = 0;
		if($check=='0') {  
			$updateUser = $this->adminModel->updateTestimonial($form_data,$testimonial_id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['testimonial_id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Data Name Already Exist';
		}
       echo json_encode($response);
	} 

    public function delete_testimonial(){
        $testimonial_id  = $this->input->get('testimonial_id');
        $delete = $this->adminModel->deleteTestimonial($testimonial_id);
        if($delete){
            $this->session->set_flashdata('message', 'Data deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Data delete Failed');
        }
        redirect(base_url('admin/testimonials'), 'Location');
    }

	public function save_testimonial(){
    	$response = array();
    	$data = $this->global_functions(); 

    	$time = time();
		$target_dir = "assets/uploads/testimonials/";
		if($_FILES["image"]["name"]!='') {
			$target_file = $target_dir . $time."_".basename($_FILES["image"]["name"]);
			$image = $time."_".basename($_FILES["image"]["name"]);
			move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
		}else{
			$image = '';
		}

        $form_data = array( 
            'author_name' =>$this->input->post('author_name'), 
			'description' =>  $this->input->post('description'), 
			'image' => $image,
			'status' => 1
		);
		 
		$check = 0;
		if($check=='0') {			
			$insertUser = $this->adminModel->insertTestimonials($form_data); 
			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Testimonial Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_testimonial(){
		$data = $this->global_functions();	 
		$data['testimonial_id'] = $this->input->get('testimonial_id'); 	
        $data['rdata'] = $this->adminModel->getTestimonialInfo($data['testimonial_id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/testimonials/edit_testimonial',$data);
		$this->load->view('templates/admin/footer');
	}

	//Pages
	public function add_page(){
		$data = $this->global_functions();	 	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pages/add_page',$data);
		$this->load->view('templates/admin/footer');
	}

	public function page_status_update(){
    	$status= $this->input->post('status');
    	$id = $this->input->post('id');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'status' =>$status
		    );
	    $this->db->where('id',$id);
		$this->db->update('pages',$form_data);
        echo json_encode($response);
    }

	public function pages(){
		$data = $this->global_functions();	
		$data['rdata'] = $this->adminModel->getPages();	
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pages/pages',$data);
		$this->load->view('templates/admin/footer');
	} 

	public function update_page(){
    	$response = array();  
        $form_data = array(			
            'page_title' =>$this->input->post('page_title'), 
			'description' =>  $this->input->post('description') 
		);
		$id = $this->input->post('id');
		$check = 0;
		if($check=='0') {  
			$updateUser = $this->adminModel->updatePage($form_data,$id);			
			if($updateUser > 0 ) {
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
                $response['id'] = $updateUser;
			}else{
				$this->session->set_flashdata('message', "Data updated successsfully.");
				$response['message'] = "Data updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Data Name Already Exist';
		}
       echo json_encode($response);
	} 

    public function delete_page(){
        $id  = $this->input->get('id');
        $delete = $this->adminModel->deletePage($id);
        if($delete){
            $this->session->set_flashdata('message', 'Data deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Data delete Failed');
        }
        redirect(base_url('admin/pages'), 'Location');
    }

	public function save_page(){
    	$response = array();
    	$data = $this->global_functions();  
        $form_data = array( 
            'page_title' =>$this->input->post('page_title'), 
			'description' =>  $this->input->post('description'), 
			'status' => 1
		);
		 
		$check = 0;
		if($check=='0') {			
			$insertUser = $this->adminModel->insertPage($form_data); 
			$this->session->set_flashdata('message', "Data inserted successsfully.");
			$response['status'] = true;	
			$response['message'] = 'Data inserted successfully';	
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Page Name Already Exist';
		}
       echo json_encode($response);
	}

	public function edit_page(){
		$data = $this->global_functions();	 
		$data['id'] = $this->input->get('id'); 	
        $data['rdata'] = $this->adminModel->getPageInfo($data['id']);		 
		$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/pages/edit_page',$data);
		$this->load->view('templates/admin/footer');
	}

	//Old Code

	public function saveProject(){
    	$response = array();
    	$data = $this->global_functions();	
             
        $projectId = $this->adminModel->generateProjectID($data['businessUniqueId']);
        $form_data = array(
            'projectId' => $projectId,
            'projectName' =>$this->input->post('projectName'),
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->adminModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->adminModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->adminModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'projectStatus' => $this->input->post('projectStatus'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		 
		$check = $this->adminModel->checkProjectAvailable($this->input->post('projectName'));
		if($check=='0') {			
			$insertUser = $this->adminModel->insertProject($form_data);
			
			if($insertUser>0) {
			     $this->uploadProjectDocuments($insertUser);

			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		    
				$response['message'] = 'Project created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'projectList';
                $response['projectUniqueId'] = $insertUser;
                $this->session->set_flashdata('message', "Project added successsfully.");
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function updateProject(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $createUserType = $this->input->post('createUserType');
        $form_data = array(			
            'projectName' =>$this->input->post('projectName'),
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->adminModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->adminModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->adminModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		$projectUniqueId = $this->input->post('projectUniqueId');
		$check = $this->adminModel->checkExistProjectAvailable($this->input->post('projectName'),$projectUniqueId);
		if($check=='0') {

			$this->uploadProjectDocuments($projectUniqueId);
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateProject($form_data,$projectUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
                $response['projectUniqueId'] = $updateUser;
			}else{
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function projectStatusUpdate(){
    	$status= $this->input->post('projectStatus');
    	$projectUniqueId= $this->input->post('projectUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'projectStatus' =>$status
		    );
	    $this->adminModel->projectStatusUpdate($form_data,$projectUniqueId);
        echo json_encode($response);
    }

    public function editProject(){
		$data = $this->global_functions();	
		$data['vendorData'] = $this->adminModel->getVendors($data['businessUniqueId']);
		$data['clientData'] = $this->adminModel->getClients($data['businessUniqueId']);
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$data['projectUniqueId'] = $this->input->get('projectUniqueId');
        $data['projectInfo'] = $this->adminModel->getProjectInfo($data['projectUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('project/editProject',$data);
		$this->load->view('templates/footer');
	}

	 public function deleteProjectDocument(){
    	$data = $this->global_functions();
        $projectDocumentId = $this->input->get('projectDocumentId');
        $documentKey = $this->input->get('documentKey');
        $delete = $this->adminModel->deleteProjectDocument($projectDocumentId,$documentKey);
        if($delete){
            $this->session->set_flashdata('message', 'Document deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Document delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function deleteProject(){
        $projectUniqueId = $this->input->get('projectUniqueId');
        $delete = $this->adminModel->deleteProject($projectUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Project deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Project delete Failed');
        }
        redirect(base_url('projectList'), 'Location');
    }

    public function questionnaire_import(){
    	$data = $this->global_functions();
    	$this->load->view('templates/admin/header',$data);
		$this->load->view('admin/questionnaire_import',$data);
		$this->load->view('templates/admin/footer');
    }

	/*

	public function tickets(){
		$data = $this->global_functions();		
		$data['tickets'] = $this->adminModel->getTickets($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('tickets/tickets',$data); 
		$this->load->view('templates/footer');
	}

	public function createTicket(){
		$data = $this->global_functions();		
		$data['allemployees'] = $this->adminModel->getUsers("4",$data['businessUniqueId']);
		$data['tickets'] = $this->adminModel->getTickets($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('tickets/create-tickets',$data); 
		$this->load->view('templates/footer');
	}

	public function saveTicket(){
		$tdata = $this->input->post('tdata'); 
		$tdata['ticketId'] = $this->adminModel->generateTicketID();
		$ticketUniqueId = $this->adminModel->insertBusiness($form_data);

		$time = time();
		$target_dir = "assets/ticketDocuments/";
		if($_FILES["attachment1"]["name"][$i]!='') {
			$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
			$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
			move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
		}else{
			$attachment1 = $attachment11[$i];
		}

		$this->session->set_flashdata('message', 'Ticket created successfully.');
		redirect(base_url('tickets'), 'Location');
	}

	public function submitChangePassword(){
		$data = $this->global_functions();	
		$password = $this->input->post('password'); 
		$confirm_password = $this->input->post('confirm_password');
		if($password==$confirm_password) {
			$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			'test_password' =>$password,
			'passwordFlag' =>'1'	
			);		

			if($data['userTypeCode'] == 2){
				$update = $this->adminModel->updateBusinessPassword($form_data,$data['loginUniqueId']);
			}else{
				$update = $this->adminModel->updateUserPassword($form_data,$data['loginUniqueId']);
			}
			$this->session->set_flashdata('messageStatus', 'success');
		    $this->session->set_flashdata('message', 'Password updated successfully');
			redirect(base_url('change-password'), 'Location');
		}else{
		    $this->session->set_flashdata('message1', 'Confirm password wrong.');
			redirect(base_url('change-password'), 'Location');
		}
	}

 	public function userStatusUpdate(){
    	$status= $this->input->post('userStatus');
    	$userUniqueId= $this->input->post('userUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'userStatus' =>$status
		    );
		    $this->adminModel->userStatusUpdate($form_data,$userUniqueId);
        
        echo json_encode($response);
    }

    public function clientStatusUpdate(){
    	$clientStatus= $this->input->post('clientStatus');
    	$clientUniqueId= $this->input->post('clientUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'clientStatus' =>$clientStatus
		    );
		    $this->adminModel->clientStatusUpdate($form_data,$clientUniqueId);
        
        echo json_encode($response);
    }

    public function vendorStatusUpdate(){
    	$vendorStatus= $this->input->post('vendorStatus');
    	$vendorUniqueId= $this->input->post('vendorUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'vendorStatus' =>$vendorStatus
		    );
		    $this->adminModel->vendorStatusUpdate($form_data,$vendorUniqueId);
        
        echo json_encode($response);
    }

     public function businessStatusUpdate(){
    	$status= $this->input->post('businessStatus');
    	$userUniqueId= $this->input->post('businessUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'businessStatus' =>$status
		    );
		    $this->adminModel->businessStatusUpdate($form_data,$userUniqueId);
        echo json_encode($response);
    }
    

    public function personalDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}	
		$data['familyDetails'] = $this->adminModel->getFamilyDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/personalDetails',$data);
		$this->load->view('templates/footer');
	}

	public function educationDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}	
		$data['educationDetails'] = $this->adminModel->getEducationDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/educationDetails',$data);
		$this->load->view('templates/footer');
	}


	public function employmentDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['employmentDetails'] = $this->adminModel->getEmploymentDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employmentDetails',$data);
		$this->load->view('templates/footer');
	}

	public function visaDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['visaDetails'] = $this->adminModel->getVisaDetails($data['loginUniqueId']);
		$data['passportDetails'] = $this->adminModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/visaDetails',$data);
		$this->load->view('templates/footer');
	}

	public function myProjects(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['projectData'] = $this->adminModel->getMyProjects($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/myProjects',$data);
		$this->load->view('templates/footer');
	}

	public function viewTimesheet(){
		$data = $this->global_functions();
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/viewTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function viewWeeklyReviews(){
		$data = $this->global_functions();
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/viewWeeklyReviews',$data);
		$this->load->view('templates/footer');
    }

	public function addPersonalDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}	
		$data['familyDetails'] = $this->adminModel->getFamilyDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addPersonalDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addEducationDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}	
		$data['educationDetails'] = $this->adminModel->getEducationDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addEducationDetails',$data);
		$this->load->view('templates/footer');
	}


	public function addEmploymentDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['employmentDetails'] = $this->adminModel->getEmploymentDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addEmploymentDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addVisaDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['familyDetails'] = $this->adminModel->getFamilyDetails($data['loginUniqueId']);
		$data['visaDetails'] = $this->adminModel->getVisaDetails($data['loginUniqueId']);
		$data['passportDetails'] = $this->adminModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addVisaDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addPassportDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['familyDetails'] = $this->adminModel->getFamilyDetails($data['loginUniqueId']);		
		$data['passportDetails'] = $this->adminModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addPassportDetails',$data);
		$this->load->view('templates/footer');
	}

	public function personalDetailsSubmit() {
		$data = $this->global_functions();
		$userUniqueId = $this->input->post('userUniqueId');
		$postData = $this->input->post('pdetails'); 
		$update = $this->adminModel->updatePersonalDetails($postData,$userUniqueId);
		$employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
		
		for($i = 0; $i <= count($this->input->post('firstName')); $i++) {				
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$dob = $this->input->post('dob');
			$gender = $this->input->post('gender');
			$email = $this->input->post('email');
			$mobile = $this->input->post('mobile');
			$address = $this->input->post('address');
			$familyType = $this->input->post('familyType'); 
			$values = array($firstName[$i], $lastName[$i], $dob[$i], $gender[$i], $email[$i], $mobile[$i], $address[$i], $familyType[$i]);	
			$employeeFamilyId = $this->input->post('employeeFamilyId');	
			//if(!empty($firstName[$i]) && $firstName[$i]>0 &&(count(array_unique($values))!=1))	
			if((count(array_unique($values))!=1))
			{	
				$fData[] = [
					"employeeFamilyId" => ( isset( $employeeFamilyId[ $i ] ) ? $employeeFamilyId[ $i ] : '' ),
					"firstName" => $firstName[$i],
					"lastName" => $lastName[$i],
				    "dob"       => $dob[$i],
				    "gender"  => $gender[$i],
				    "email"       => $email[$i],
				    "mobile"       => $mobile[$i],
				    "userUniqueId"       => $userUniqueId,
				    "address"       => $address[$i],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "familyType" =>  $familyType[$i]
				];
			}
		}
		//echo "<hr><pre>"; print_r($fData); echo "</pre>"; die();
		$update = $this->adminModel->updateEmployeeFamilyDetails($userUniqueId,$fData);

		//Childrens Data
		

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('personalDetails'), 'Location');		 
	}

	public function passportDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];		
		$employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
		
		for($i = 0; $i <= count($this->input->post('firstName')); $i++) {				
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$passportNo = $this->input->post('passportNo');
			$issueDate = $this->input->post('issueDate');
			$placeOfIssue = $this->input->post('placeOfIssue');
			$expireDate = $this->input->post('expireDate'); 
			$country = $this->input->post('country');
			$employeeFamilyId = $this->input->post('employeeFamilyId'); 

			$values = array($firstName[$i], $lastName[$i], $passportNo[$i], $issueDate[$i], $placeOfIssue[$i], $expireDate[$i], $country[$i], $employeeFamilyId[$i]);	
			$passportUniqueId = $this->input->post('passportUniqueId');	

			$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');	
			if(!empty($firstName[$i]) && $firstName[$i]>0 &&(count(array_unique($values))!=1))	
			//if((count(array_unique($values))!=1))
			{	
				 $attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/passportDocuments/";
				if($_FILES["attachment1"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				}
				 
				if($_FILES["attachment2"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$i]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$i]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$i], $target_file);
				}else{
					$attachment2 = $attachment22[$i];
				}

				if($_FILES["attachment3"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$i]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$i]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$i], $target_file);
				}else{
					$attachment3 = $attachment33[$i];
				}

				$fData[] = [
					"passportUniqueId" => ( isset( $passportUniqueId[ $i ] ) ? $passportUniqueId[ $i ] : '' ),
					"firstName" => $firstName[$i],
					"lastName" => $lastName[$i],
				    "passportNo"       => $passportNo[$i],
				    "issueDate"  => $issueDate[$i],
				    "placeOfIssue"       => $placeOfIssue[$i],
				    "expireDate"       => $expireDate[$i],
				    "userUniqueId"       => $userUniqueId,
				    "country"       => $country[$i],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "employeeFamilyId" =>  $employeeFamilyId[$i]?$employeeFamilyId[$i]:'0',
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3,
				    "familyType" => $employeeFamilyId[$i]!=0?$this->adminModel->getFamilyTypeById($employeeFamilyId[$i]):'Self'
				];
				
			}
		}
		
		$update = $this->adminModel->updatePassportDetails($userUniqueId,$fData);

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('visaDetails'), 'Location');	
	}

	public function visaDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];		
		$employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
		//echo "<pre>"; print_r($this->input->post()); echo "</pre>"; die();
		for($j = 0; $j <= count($this->input->post('visaType')); $j++) {				
			$visaType = $this->input->post('visaType');
			$visaNumber = $this->input->post('visaNumber');
			$issueDate = $this->input->post('vissueDate');
			$placeOfIssue = $this->input->post('vplaceOfIssue');
			$expireDate = $this->input->post('vexpireDate');
			$country = $this->input->post('vcountry');
			$employeeFamilyId = $this->input->post('vemployeeFamilyId'); 
			$values = array($visaType[$j], $visaNumber[$j], $issueDate[$j], $placeOfIssue[$j], $expireDate[$j], $country[$j], $employeeFamilyId[$j]);	
			$visaUniqueId = $this->input->post('visaUniqueId');	

			$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');	

			//if(!empty($firstName[$j]) && $firstName[$j]>0 &&(count(array_unique($values))!=1))	
			if((count(array_unique($values))!=1))
			{	 
				$attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/visaDocuments/";
				if($_FILES["attachment1"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$j]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$j]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$j], $target_file);
				}else{
					$attachment1 = $attachment11[$j];
				}
				 
				if($_FILES["attachment2"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$j]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$j]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$j], $target_file);
				}else{
					$attachment2 = $attachment22[$j];
				}

				if($_FILES["attachment3"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$j]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$j]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$j], $target_file);
				}else{
					$attachment3 = $attachment33[$j];
				}

				$fData1[] = [
					"visaUniqueId" => ( isset( $visaUniqueId[ $j ] ) ? $visaUniqueId[ $j ] : '' ),
					"visaType" => $visaType[$j],
					"visaNumber" => $visaNumber[$j],
					"employeeFamilyId" => $employeeFamilyId[$j]?$employeeFamilyId[$j]:'0',
				    "issueDate"  => $issueDate[$j],
				    "placeOfIssue"       => $placeOfIssue[$j],
				    "expireDate"       => $expireDate[$j],
				    "userUniqueId"       => $userUniqueId,
				    "country"       => $country[$j],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3,
				    "familyType" => $employeeFamilyId[$j]!=0?$this->adminModel->getFamilyTypeById($employeeFamilyId[$j]):'Self'
				];
				
			}
		}
		echo "<pre>";
		//print_r($fData1); echo "</pre>";  die();	
		$update = $this->adminModel->updateVisaDetails($userUniqueId,$fData1);

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('visaDetails'), 'Location');	
	}

	public function educationDetailsSubmit(){
		$data = $this->global_functions();echo "<pre>";
		print_r($this->input->post()); echo "</pre>"; 
		$userUniqueId = $data['loginUniqueId'];
		for($i = 0; $i <= count($this->input->post('qualificationTitle')); $i++) {				
			$qualificationTitle = $this->input->post('qualificationTitle');
			$institution = $this->input->post('institution');
			$startYear = $this->input->post('startYear'); 
			$endYear = $this->input->post('endYear');
			$specialization = $this->input->post('specialization');
			$values = array($qualificationTitle[$i], $institution[$i], $startYear[$i], $endYear[$i],$specialization[$i]);		
			$educationId = $this->input->post('educationId');		
			$attachment11 = $this->input->post('attachment11');	
			echo "d===".$qualificationTitle[$i]; 
			if(!empty($qualificationTitle[$i]) &&(count(array_unique($values))!=1))
			{	
                $file_name = '';
				if($_FILES["attachment1"]["name"][$i]!='') {
					$time = time();
					$target_dir = "assets/educationDocuments/";
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$file_name = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				} 

				$cData[] = [
					"educationId" => ( isset( $educationId[ $i ] ) ? $educationId[ $i ] : '' ),
					"qualificationTitle" => $qualificationTitle[$i],
					"institution" => $institution[$i],
				    "startYear"       => $startYear[$i],
				    "endYear"  => $endYear[$i],
				    "specialization"  => $specialization[$i],
				    "userUniqueId"       => $userUniqueId,
				    "attachment1" =>$file_name
				];
			}
		}
	

		$update = $this->adminModel->updateEducationDetails($userUniqueId,$cData);
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('educationDetails'), 'Location');
	}

	public function employmentDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		for($i = 0; $i <= count($this->input->post('companyName')); $i++) {				
			$companyName = $this->input->post('companyName');
			$address = $this->input->post('address');
			$fromDate = $this->input->post('fromDate');
			$toDate = $this->input->post('toDate');
			$role = $this->input->post('role');
			$technology = $this->input->post('technology');
			$values = array($companyName[$i], $address[$i], $fromDate[$i], $toDate[$i], $role[$i], $technology[$i]);		
			$employmentId = $this->input->post('employmentId');		
				$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');
			if(!empty($companyName[$i]) && $companyName[$i]>0 &&(count(array_unique($values))!=1))
			{	
				 $attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/employmentDocuments/";
				if($_FILES["attachment1"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				}
				 
				if($_FILES["attachment2"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$i]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$i]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$i], $target_file);
				}else{
					$attachment2 = $attachment22[$i];
				} 

				if($_FILES["attachment3"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$i]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$i]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$i], $target_file);
				}else{
					$attachment3 = $attachment33[$i];
				} 

				$cData[] = [
					"employmentId" => ( isset( $employmentId[ $i ] ) ? $employmentId[ $i ] : '' ),
					"companyName" => $companyName[$i],
					"address" => $address[$i],
				    "fromDate"       => $fromDate[$i],
				    "toDate"  => $toDate[$i],
				    "role"  => $role[$i],
				    "technology"  => $technology[$i],
				    "userUniqueId"       => $userUniqueId,
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3
				];
			}
		}

		$update = $this->adminModel->updateEmploymentDetails($userUniqueId,$cData);
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('employmentDetails'), 'Location');
	}

	public function deleteChildren() {
		$data = $this->global_functions();
		$childrenId = $this->input->get('childrenId');
		$delete = $this->adminModel->deleteChildren($childrenId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteFamily() {
		$data = $this->global_functions();
		$employeeFamilyId = $this->input->get('employeeFamilyId');
		$delete = $this->adminModel->deleteFamily($employeeFamilyId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteEducation() {
		$data = $this->global_functions();
		$educationId = $this->input->get('educationId');
		$delete = $this->adminModel->deleteEducation($educationId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deletePassport() {
		$data = $this->global_functions();
		$passportUniqueId = $this->input->get('passportUniqueId');
		$delete = $this->adminModel->deletePassport($passportUniqueId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteUserVisa() {
		$data = $this->global_functions();
		$visaUniqueId = $this->input->get('visaUniqueId');
		$delete = $this->adminModel->deleteUserVisa($visaUniqueId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteEmployment() {
		$data = $this->global_functions();
		$employmentId = $this->input->get('employmentId');
		$delete = $this->adminModel->deleteEmployment($employmentId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function updateBusinessProfile(){
		$data = $this->global_functions();
        $data['businessInfo'] = $this->adminModel->getBusinessInfo($data['loginUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('business/updateBusinessprofile',$data);
		$this->load->view('templates/footer');
	}

	

	

	public function createBusiness(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('business/createBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function saveBusiness(){
    	$response = array();
    	if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = '';
		}

        $usertype = "2";
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->adminModel->generateBusinessID();
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
            'userTypeCode' =>BUSINESS,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'dob' => $this->input->post('dob'),
			'businessLogo' => $file_name
		);
		 
		$check = $this->adminModel->checkBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertBusines = $this->adminModel->insertBusiness($form_data);
			
			if($insertBusines>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Business created successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}			
		}else{
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function businessList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['businessData'] = $this->adminModel->getBusinessList();	
		$this->load->view('templates/header',$data);
		$this->load->view('business/businessList',$data);
		$this->load->view('templates/footer');
	} 

	public function deleteBusiness(){
        $userid = $this->input->get('businessUniqueId');
        $delete = $this->adminModel->deleteBusiness($userid);
        if($delete){
            $this->session->set_flashdata('message', 'Business deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Business delete Failed');
        }
        redirect(base_url('businessList'), 'Location');
    }

    public function deleteDocument(){
    	$data = $this->global_functions();
        $userDocumentId = $this->input->get('userDocumentId');
        $documentKey = $this->input->get('documentKey');
        $delete = $this->adminModel->deleteDocument($userDocumentId,$documentKey);
        if($delete){
            $this->session->set_flashdata('message', 'Document deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Document delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function editBusiness(){
		$data = $this->global_functions();	
		$data['businessUniqueId'] = $this->input->get('businessUniqueId');
        $data['businessInfo'] = $this->adminModel->getBusinessInfo($data['businessUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('business/editBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function updateBusiness(){
    	$response = array();

    	if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = $this->input->post('exbusinessLogo');
		}
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->adminModel->generateBusinessID();
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,			
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'dob' => $this->input->post('dob'),
			'businessLogo' => $file_name			
		);
		$businessUniqueId = $this->input->post('businessUniqueId');
		$check = $this->adminModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$insertBusines = $this->adminModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$response['message'] = 'Business updated successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function submitBusinessProfile(){
		$data = $this->global_functions();
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');

        if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['businessLogo'];
		}

        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			//'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'businessLogo' =>	$file_name, 
			'dob' => $this->input->post('dob')	
		);
		$businessUniqueId = $data['loginUniqueId'];
		$check = $this->adminModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$insertBusines = $this->adminModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$this->session->set_flashdata('message', 'Profile updated successfully');
			}else{
				$this->session->set_flashdata('message', 'Server Error');
			}			
		}else if($check=='1'){
			$this->session->set_flashdata('message', 'Mobile Already Exist');
		}else if($check=='2'){
			$this->session->set_flashdata('message', 'Email Already Exist');
		}else if($check=='3'){
			$this->session->set_flashdata('message', 'Mobile/Email Already Exist');
		}	
       redirect(base_url('updateBusinessprofile'), 'Location');
	}

	public function createEmployee(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'employeeList';	
		$data['createUserTypeCode'] = EMPLOYEE;
		$data['createUserType'] = "Employee";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveEmployee(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->adminModel->generatePassword();
        $employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
        $createUserType = $this->input->post('createUserType');
        $form_data = array(
            'userTypeCode' => $this->input->post('createUserTypeCode'),
            'userType' =>$this->input->post('createUserType'),
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate')
		);
		 
		$check = $this->adminModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertUser($form_data);
			
			if($insertUser>0) {
			     $this->uploadDocuments($insertUser);
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		    
				$response['message'] = 'Employee created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'employeeList';
                $response['userUniqueId'] = $insertUser;
                $this->session->set_flashdata('message', "$createUserType added successsfully.");
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function employeeList(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function timesheets(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/timesheets',$data);
		$this->load->view('templates/footer');
	} 

	public function approvalTimesshetsInfo(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/approvalTimesshetsInfo',$data);
		$this->load->view('templates/footer');
	}

	public function editEmployee(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Employee';
		$data['cancelUrl'] = 'employeeList';
        $data['userInfo'] = $this->adminModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateEmployee(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $createUserType = $this->input->post('createUserType');
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'userStatus' => $this->input->post('userStatus')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->adminModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		if($check=='0') {

			$this->uploadDocuments($userUniqueId);
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = "$createUserType updated successfully.";
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteEmployee(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->adminModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Employee deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Employee delete Failed');
        }
        redirect(base_url('employeeList'), 'Location');
    }

    //Project
    public function createProject(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'projectList';	
		$data['createUserTypeCode'] = EMPLOYEE;
		$data['createUserType'] = "Project";
		$data['vendorData'] = $this->adminModel->getVendors($data['businessUniqueId']);
		$data['clientData'] = $this->adminModel->getClients($data['businessUniqueId']);
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('project/createProject',$data);
		$this->load->view('templates/footer');
	}

	public function ProjectList(){
		$data = $this->global_functions();	
		$data['projectData'] = $this->adminModel->getProjects($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('project/projectList',$data);
		$this->load->view('templates/footer');
	} 

	public function saveProject(){
    	$response = array();
    	$data = $this->global_functions();	
             
        $projectId = $this->adminModel->generateProjectID($data['businessUniqueId']);
        $form_data = array(
            'projectId' => $projectId,
            'projectName' =>$this->input->post('projectName'),
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->adminModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->adminModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->adminModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'projectStatus' => $this->input->post('projectStatus'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		 
		$check = $this->adminModel->checkProjectAvailable($this->input->post('projectName'));
		if($check=='0') {			
			$insertUser = $this->adminModel->insertProject($form_data);
			
			if($insertUser>0) {
			     $this->uploadProjectDocuments($insertUser);

			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		    
				$response['message'] = 'Project created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'projectList';
                $response['projectUniqueId'] = $insertUser;
                $this->session->set_flashdata('message', "Project added successsfully.");
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function updateProject(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $createUserType = $this->input->post('createUserType');
        $form_data = array(			
            'projectName' =>$this->input->post('projectName'),
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->adminModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->adminModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->adminModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		$projectUniqueId = $this->input->post('projectUniqueId');
		$check = $this->adminModel->checkExistProjectAvailable($this->input->post('projectName'),$projectUniqueId);
		if($check=='0') {

			$this->uploadProjectDocuments($projectUniqueId);
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateProject($form_data,$projectUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
                $response['projectUniqueId'] = $updateUser;
			}else{
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function projectStatusUpdate(){
    	$status= $this->input->post('projectStatus');
    	$projectUniqueId= $this->input->post('projectUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'projectStatus' =>$status
		    );
	    $this->adminModel->projectStatusUpdate($form_data,$projectUniqueId);
        echo json_encode($response);
    }

    public function editProject(){
		$data = $this->global_functions();	
		$data['vendorData'] = $this->adminModel->getVendors($data['businessUniqueId']);
		$data['clientData'] = $this->adminModel->getClients($data['businessUniqueId']);
		$data['employeeData'] = $this->adminModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$data['projectUniqueId'] = $this->input->get('projectUniqueId');
        $data['projectInfo'] = $this->adminModel->getProjectInfo($data['projectUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('project/editProject',$data);
		$this->load->view('templates/footer');
	}

	 public function deleteProjectDocument(){
    	$data = $this->global_functions();
        $projectDocumentId = $this->input->get('projectDocumentId');
        $documentKey = $this->input->get('documentKey');
        $delete = $this->adminModel->deleteProjectDocument($projectDocumentId,$documentKey);
        if($delete){
            $this->session->set_flashdata('message', 'Document deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Document delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function deleteProject(){
        $projectUniqueId = $this->input->get('projectUniqueId');
        $delete = $this->adminModel->deleteProject($projectUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Project deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Project delete Failed');
        }
        redirect(base_url('projectList'), 'Location');
    }

    //HR
    public function createHr(){ 
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'hrList';	
		$data['createUserTypeCode'] = HR;
		$data['createUserType'] = "HR";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveHr(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->adminModel->generatePassword();
        $employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>HR,
            'userType' =>'HR',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->adminModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'HR created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'hrList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function hrList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'HR';
		$data['createurl'] = 'createHr';
		$data['editUrl'] = 'editHr';
		$data['deleteUrl'] = 'deleteHr';
		$data['employeeData'] = $this->adminModel->getUsers(HR,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editHr(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'HR';
		$data['cancelUrl'] = 'hrList';
        $data['userInfo'] = $this->adminModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateHr(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->adminModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'HR updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteHr(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->adminModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'HR deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'HR delete Failed');
        }
        redirect(base_url('hrList'), 'Location');
    }

    //Accountant

    public function createAccountant(){
		$data = $this->global_functions();
		$data['cancelUrl'] = 'accountantList';	
		$data['createUserTypeCode'] = ACCOUNTANT;
		$data['createUserType'] = "Accountant";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveAccountant(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->adminModel->generatePassword();
        $employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>ACCOUNTANT,
            'userType' =>'Accountant',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->adminModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			//$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertUser($form_data);
			
			if($insertUser>0) {				
			    $this->uploadDocuments($insertUser);
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Accountant created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'accountantList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function uploadDocuments($userUniqueId){
		for($i = 0; $i <= count($this->input->post('documentType')); $i++) {				
			$documentType = $this->input->post('documentType');
			$attachment = $this->input->post('attachment');
			$values = array($documentType[$i], $attachment[$i]);			
			if(!empty($documentType[$i]) && $documentType[$i]>0 &&(count(array_unique($values))!=1))
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/documents/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"documentKey" => $this->adminModel->generateEmailKey(),
					"documentTypeId" => $documentType[$i],
					"documentType" => $this->adminModel->getTypeName($documentType[$i],'DOCUMENT_TYPE'),
					"attachment" => $file_name,
				    "userUniqueId" => $userUniqueId,
				);
				$this->adminModel->userDocuments($documentsData);
			}
		}
	}

	public function uploadProjectDocuments($projectUniqueId){
		for($i = 0; $i <= count($this->input->post('docName')); $i++) {				
			$documentType = $this->input->post('docName');
			$attachment = $this->input->post('attachment');
			$values = array($documentType[$i], $attachment[$i]);			
			if(!empty($documentType[$i]) && $documentType[$i]>0 &&(count(array_unique($values))!=1))
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/projectDocuments/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"documentKey" => $this->adminModel->generateEmailKey(),
					"documentName" => $documentType[$i],
					"attachment" => $file_name,
				    "projectUniqueId" => $projectUniqueId,
				);
				$this->adminModel->insertProjectDocuments($documentsData);
			}
		}
	}

	public function accountantList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Accountant';
		$data['createurl'] = 'createAccountant';
		$data['editUrl'] = 'editAccountant';
		$data['deleteUrl'] = 'deleteAccountant';
		$data['createUrl'] = 'createAccountant';
		$data['employeeData'] = $this->adminModel->getUsers(ACCOUNTANT,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editAccountant(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Accountant';
		$data['cancelUrl'] = 'accountantList';
        $data['userInfo'] = $this->adminModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateAccountant(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->adminModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Accountant updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteAccountant(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->adminModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Accountant deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Accountant delete Failed');
        }
        redirect(base_url('accountantList'), 'Location');
    }

    //Visa
    public function createVisa(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'visaList';	
		$data['createUserTypeCode'] = VISA;
		$data['createUserType'] = "VISA";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveVisa(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->adminModel->generatePassword();
        $employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VISA,
            'userType' =>'Visa',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->adminModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Visa created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'visaList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function visaList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Visa';
		$data['createurl'] = 'createVisa';
		$data['editUrl'] = 'editVisa';
		$data['deleteUrl'] = 'deleteVisa';
		$data['employeeData'] = $this->adminModel->getUsers(VISA,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVisa(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Visa';
		$data['cancelUrl'] = 'visaList';
        $data['userInfo'] = $this->adminModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateVisa(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->adminModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Visa updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVisa(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->adminModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Visa deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Visa delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Bench
    public function createBench(){
		$data = $this->global_functions();	
		$data['cancelUrl'] = 'benchList';	
		$data['createUserTypeCode'] = BENCH;
		$data['createUserType'] = "Bench";	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveBench(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->adminModel->generatePassword();
        $employeeId = $this->adminModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>BENCH,
            'userType' =>'Bench',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->adminModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Bench created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'benchList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function benchList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createBench';
		$data['editUrl'] = 'editBench';
		$data['deleteUrl'] = 'deleteBench';
		$data['employeeData'] = $this->adminModel->getUsers(BENCH,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editBench(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Bench';
		$data['cancelUrl'] = 'benchList';
        $data['userInfo'] = $this->adminModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateBench(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->adminModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Bench updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteBench(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->adminModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Bench deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Bench delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Vendor
     public function createVendor(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/createVendor',$data);
		$this->load->view('templates/footer');
	}

	public function saveVendor(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $vendorId = $this->adminModel->generateVendorID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VENDOR,
            'userType' =>'Vendor',
            'vendorId' => $vendorId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'vendorFirm' => $this->input->post('vendorFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')
		);
		 
		$check = $this->adminModel->checkVendorAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertVendor($form_data);
			
			if($insertUser>0) {    		
				$this->session->set_flashdata('message', 'Client created successfully.');
				$response['message'] = 'Vendor created successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function vendorList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->adminModel->getVendors($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/vendorList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVendor(){
		$data = $this->global_functions();	
		$data['vendorUniqueId'] = $this->input->get('vendorUniqueId');
        $data['vendorInfo'] = $this->adminModel->getVendorInfo($data['vendorUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/editVendor',$data);
		$this->load->view('templates/footer');
	}

	public function updateVendor(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'vendorFirm' => $this->input->post('vendorFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')		
		);
		$vendorUniqueId = $this->input->post('vendorUniqueId');
		$check = $this->adminModel->checkExistVendorAvailable($this->input->post('mobile'),$this->input->post('email'),$vendorUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateVendor($form_data,$vendorUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Vendor updated successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVendor(){
        $userUniqueId = $this->input->get('vendorUniqueId');
        $delete = $this->adminModel->deleteVendor($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Vendor deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Vendor delete Failed');
        }
        redirect(base_url('vendorList'), 'Location');
    }

    //Clients
     public function createClient(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('client/createClient',$data);
		$this->load->view('templates/footer');
	}

	public function saveClient(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $clientrId = $this->adminModel->generateClientID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>CLIENT,
            'userType' =>'Client',
            'clientId' => $clientrId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'clientFirm' => $this->input->post('clientFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')
		);
		 
		$check = $this->adminModel->checkClientAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->adminModel->insertClient($form_data);
			
			if($insertUser>0) {    		
				$this->session->set_flashdata('message', 'Client created successfully.');
				$response['message'] = 'Client created successfully.';
				$response['status'] = true;
                $response['clinetUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function clientList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->adminModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('client/clientList',$data);
		$this->load->view('templates/footer');
	} 

	public function editClient(){
		$data = $this->global_functions();	
		$data['clientUniqueId'] = $this->input->get('clientUniqueId');
        $data['clientInfo'] = $this->adminModel->getClientInfo($data['clientUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('client/editClient',$data);
		$this->load->view('templates/footer');
	}

	public function updateClient(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->adminModel->generatePassword();
        $form_data = array(
			'clientFirm' => $this->input->post('clientFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')	 	
		);
		$clientUniqueId = $this->input->post('clientUniqueId');
		$check = $this->adminModel->checkExistClientAvailable($this->input->post('mobile'),$this->input->post('email'),$clientUniqueId);
		
		if($check=='0') {
			$otp = $this->adminModel->generateSMSKey();
			$updateUser = $this->adminModel->updateClient($form_data,$clientUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Client updated successfully.';
				$response['status'] = true;
                $response['clientUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteClient(){
        $clientUniqueId = $this->input->get('clientUniqueId');
        $delete = $this->adminModel->deleteClient($clientUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Client deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Client delete Failed');
        }
        redirect(base_url('clientList'), 'Location');
    }

    public function getUserDocuments(){
		if(isset($_POST["userUniqueId"]))  
	 	{  
 		$data = $this->global_functions();	
		$userUniqueId = $this->input->post('userUniqueId');
        $docInfo = $this->adminModel->getUserDocuments($userUniqueId);
	      $output = '';
	      $output .= '  
	      <div class="table-responsive">  
	           <table class="table table-bordered"><tr><th>Document Type</th><th>Attachment</th></tr>';  
	      foreach($docInfo as $docInfos)  
	      {   $path = base_url()."assets/documents/".$docInfos["attachment"];
	           $output .= '  
	                <tr> <td >'.$docInfos["documentType"].'</td>   
	                     <td><a href="'.$path.'" download><i class="fa fa-download" aria-hidden="true"></i></a></td>  
	                </tr> 
	                ';  
	      }  
	      $output .= "</table></div>";  
	      echo $output;  
 		}
    }

    public function addTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->adminModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/addTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function editTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->adminModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/editTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function checkTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->adminModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/checkTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function addWeeklyReview(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->adminModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/addWeeklyReview',$data);
		$this->load->view('templates/footer');
    } 

    public function saveWeeklyReview(){
    	$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		$description = $this->input->post('description');
		$weekDate = $this->input->post('weekDate');
		$projectUniqueId = $this->input->post('projectUniqueId');
		$check = $this->adminModel->checkWeeklyReviewByDate($userUniqueId,$weekDate);
		if($check>0){
			$form_data = array(
				'description' => $description,
				'projectUniqueId' =>$projectUniqueId,
				'supervisor' =>  $this->input->post('supervisor')
			);
			$update = $this->adminModel->updateWeeklyReview($userUniqueId,$weekDate,$form_data);
		}else{
			$form_data = array(
				'userUniqueId' => $data['loginUniqueId'],
				'description' => $description,
				'weekDate' => $weekDate,
				'projectUniqueId' =>$projectUniqueId,
				'supervisor' =>  $this->input->post('supervisor')
			);
			$this->db->insert('weekly_review', $form_data);
		}
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect($_SERVER['HTTP_REFERER'], 'Location');
    }
        
    public function saveTimesheet(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		for($i = 0; $i <= count($this->input->post('workDate')); $i++) {				
			$workDate = $this->input->post('workDate');
			$standardHours = $this->input->post('standardHours');
			$extraHours = $this->input->post('extraHours');
			$timesheetId = $this->input->post('timesheetId');
			$projectUniqueId = $this->input->post('projectUniqueId');
			$values = array($workDate[$i], $standardHours[$i]);	

			if(!empty($workDate[$i]) && $workDate[$i]>0 &&(count(array_unique($values))!=1))
			{
				$cData[] = [
					"timesheetId" => ( isset( $timesheetId[ $i ] ) ? $timesheetId[ $i ] : '' ),
					"workDate" => $workDate[$i],
					"standardHours" => $standardHours[$i],
				    "extraHours"       => $extraHours[$i],
				    "userUniqueId" => $userUniqueId,
				    "projectUniqueId" =>$projectUniqueId
				];
			}
		}

		$update = $this->adminModel->saveTimesheet($userUniqueId,$cData);

		if($_FILES["attachment1"]["name"]!='') {
			$target_dir = "assets/timesheetAttachments/";
			$target_file = $target_dir . basename($_FILES["attachment1"]["name"]);
			$attachment1 = basename($_FILES["attachment1"]["name"]);
			move_uploaded_file($_FILES["attachment1"]["tmp_name"], $target_file);
		} else{
			$attachment1 = $this->input->post('attachment1Old');
		}

		if($_FILES["attachment2"]["name"]!='') {
			$target_dir = "assets/timesheetAttachments/";
			$target_file = $target_dir . basename($_FILES["attachment2"]["name"]);
			$attachment2 = basename($_FILES["attachment2"]["name"]);
			move_uploaded_file($_FILES["attachment2"]["tmp_name"], $target_file);
		} else{
			$attachment2 = $this->input->post('attachment2Old');
		}
		$weekDate = $this->input->post('weekDate');

		$checkAttachment1 = $this->adminModel->checkWeekAttachment($weekDate,$userUniqueId);
		if($checkAttachment1>0){
			$form_data = array(
				'attachment1' => $attachment1,
				'attachment2' => $attachment2
			);
			$this->adminModel->updateWeekAttachment($weekDate,$form_data,$userUniqueId);
		}else{
			$form_data = array(
				'userUniqueId' => $data['loginUniqueId'],
				'employeeId' => $data['employeeId'],
				'attachment1' => $attachment1,
				'attachment1' => $attachment1,
				'attachment2' => $attachment2,
				'weekDate' =>$weekDate
			);
			$this->adminModel->insertTimesheetAttachments($form_data);
		}
		 
		
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect($_SERVER['HTTP_REFERER'], 'Location'); 
	}

	public function approveComment(){
		$data = $this->global_functions(); 
		$timeSheetAutoId = $this->input->get('timeSheetAutoId');
		$form_data = array(
                'weekStatus' => 1
            );
        $insert=$this->adminModel->approveComment($form_data,$timeSheetAutoId);
        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment approved successfully');
		}else{
		    $this->session->set_flashdata('message', 'Failed due to error.');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function RejectComment(){
		$data = $this->global_functions(); 
		$timeSheetAutoId = $this->input->get('timeSheetAutoId');
		$form_data = array(
                'weekStatus' => 0
            );
        $insert=$this->adminModel->approveComment($form_data,$timeSheetAutoId);

        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment approved successfully');
		}else{
		    $this->session->set_flashdata('message', 'Failed due to error.');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function approveReview(){
		$data = $this->global_functions(); 
		$weeklyReviewId = $this->input->get('weeklyReviewId');
		$form_data = array(
                'reviewStatus' => 1
            );
        $insert=$this->adminModel->approveReview($form_data,$weeklyReviewId);
        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}else{
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function RejectReview(){
		$data = $this->global_functions(); 
		$weeklyReviewId = $this->input->get('weeklyReviewId');
		$form_data = array(
                'reviewStatus' => 0
            );
        $insert=$this->adminModel->approveReview($form_data,$weeklyReviewId);

        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}else{
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}*/

	public function logout() {	
		$this->session->sess_destroy();
		redirect(base_url('/'), 'Location');
	}

    
}