<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	 
class MainDB_model extends CI_Model
{
	public function __construct()
	{ 
		parent::__construct();
	}

	public function checkOtpMobile($ph) {
		$sql = "select * from sms_history where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}

	 public function sendSmsEmail($mobile,$email){
        $sms = $this->generateSMSKey();
        $this->updateSms($mobile,$sms);
        $message = "Please use OTP: $sms for signup.";
        $this->sendSms($mobile,$message);
        return $message;
    }
    
    public function updateSms($mobile,$sms){
        $checkUser = $this->checkOtpMobile($mobile);
        if($checkUser == 0){
            $form_data = array(
		    'mobile' =>$mobile,
			'sms' =>  $sms
    		);
    		$this->db->insert('sms_history',$form_data);
        }else{
             $form_data = array(
			'sms' =>  $sms
    		);
            $this->db->where('mobile',$mobile);
            $this->db->update('sms_history',$form_data);
        }
    }

	public function getDoctorSchdulesByDate($date){
		$sql = "select * from doctor_schedules WHERE schedule_date='$date' AND is_availability=1";  
		$query = $this->db->query($sql);
        return $query->result_array();
	}
 
	public function validateLogin() {
		$uname = $this->input->post('mobile'); 
		$pwd = md5($this->input->post('password'));
		$sql = "select * from tbl_users_master where mobile='$uname' and password='$pwd' and status=1";
		$query = $this->db->query($sql);		
        $cnt = $query->num_rows();	
        if ($cnt == 1) {
			$row = $query->result_array();
			$uid = $row[0]['rest_verification_key'];
            return $uid;
        } else {
			return 'No_User';
		}
	} 
	
	public function generateBusinessID() {
		$sql = "select businessId from business where businessId LIKE '%BIN%' order by businessUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('BIN',$row[0]['businessId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'BIN'.$random_string;
		} else {
			$uniqueid = 'BIN001';
		}
		return $uniqueid;
	} 
	
	public function generateEmailKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ123456789',6)),0,6);
	}

	public function generateSMSKey() {
		return substr(str_shuffle(str_repeat('123456789',5)),0,5);
	}

	public function generateRESTKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',6)),0,6);
	}
	
	public function generatePassword() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',10)),0,10);
	}
	
	public function checkUserMobileEmail($emailMobile) {
		$sql = "select * from users where mobile='$emailMobile' or email='$emailMobile'"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		$row = $query->result_array();
		if($cnt>0) {
			return $row[0]['user_id'];
		} else {
			return 0;
		}		
	}

	public function getDrugsListByCat($category_id){
		$sql = "select * from drugs WHERE drug_status IN(0,1) AND category_id=$category_id Order BY drug_id ASC";
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getQuestionsByCat($category_id){
		$sql="select * from questions WHERE question_status IN(1) AND category_id=$category_id Order BY question_id ASC";
		$query = $this->db->query($sql);
        return $query->result_array();
	} 
	

	public function getQuestionById($category_id,$sn){
		$sql="select * from questions WHERE sn=$sn AND category_id=$category_id"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getOptionsByQyestion($question_id){
		$sql="select * from question_options WHERE question_id=$question_id"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getPharmacyInfo(){
		$sql="select * from users WHERE user_type=3 ORDER BY user_id DESC LIMIT 1"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	} 

	public function getUserInfoById($user_id){
		$sql="select * from users WHERE user_id=$user_id"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	} 

	public function getUserShipPharmacyAddress($cart_id){
		$sql="select * from ship_pharmacy_address WHERE cart_id=cart_id"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getUserQuestionnaireInfo($user_id){ 
		$sql="select * from users_questionnaire t1 INNER JOIN categories t2 ON t1.category_id=t2.category_id WHERE t1.user_id=$user_id AND t1.questionnaire_status IN(0,1,2) ORDER BY batch_id DESC"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getPaymentStatus($payment_type){
		switch ($payment_type) {
		  case "1":
		    echo "Partially Done";
		    break;
		  case "2":
		    echo "Payment Completed";
		    break; 
		  default:
		    echo "Not Done";
		}
	}

	public function getUserOrderQuestionnaireInfo($user_id){ 
		$sql="select * from users_questionnaire t1 INNER JOIN categories t2 ON t1.category_id=t2.category_id WHERE t1.user_id=$user_id AND t1.questionnaire_status IN(3,1,2) ORDER BY batch_id DESC"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getPaymentInfoByCartId($user_id){ 
		$sql="select * from payments t1 LEFT JOIN patient_cart t2 ON t1.cart_id=t2.cart_id INNER JOIN users_questionnaire t3 ON t2.batch_id=t3.batch_id WHERE t1.user_id=$user_id AND payment_for='Doctor Consultation Fee'";
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getPaymentInfoByCartIdAndUserID($user_id,$cart_id,$payment_type){ 
		if($payment_type == 2){
			$sql="select * from payments t1 LEFT JOIN patient_cart t2 ON t1.cart_id=t2.cart_id INNER JOIN users_questionnaire t3 ON t2.batch_id=t3.batch_id WHERE t1.user_id=$user_id AND t1.cart_id=$cart_id AND payment_for='Precription Amount'";
		}else{
			$sql="select * from payments t1 LEFT JOIN patient_cart t2 ON t1.cart_id=t2.cart_id INNER JOIN users_questionnaire t3 ON t2.batch_id=t3.batch_id WHERE t1.user_id=$user_id AND t1.cart_id=$cart_id AND payment_for='Doctor Consultation Fee'";
		}		
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getUserNonQuestionnaireInfo($user_id){ 
		$sql="select * from categories WHERE category_id NOT IN(SELECT category_id FROM users_questionnaire WHERE questionnaire_status IN(0,1,2) AND user_id=$user_id) ORDER BY category_id ASC"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getCurrentQuestionnairy(){
		$user_id = $_SESSION['user_id'];
		$category_id = $_SESSION['quiz_category'];
		$sql="select * from users_questionnaire WHERE user_id=$user_id ORDER BY batch_id DESC"; 
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getUserCurrentQuestionnaireInfo(){
		$user_id = $_SESSION['user_id'];
		$category_id = $_SESSION['quiz_category'];
		$sql="select * from users_questionnaire WHERE user_id=$user_id ORDER BY batch_id DESC"; 
		$query = $this->db->query($sql);
        $row =  $query->result_array();
        $batch_id = $row[0]['batch_id']; 
        $sql="select t1.*,t2.*,t3.*,t4.*,t1.batch_id as ubatch_id from users_questionnaire_history t1 INNER JOIN categories t2 ON t1.category_id=t2.category_id INNER JOIN questions t3 ON t1.question_id=t3.question_id INNER JOIN question_options t4 ON t1.option_id=t4.option_id WHERE t1.user_id=$user_id AND t1.category_id=$category_id AND t1.batch_id=$batch_id ORDER BY t1.id ASC"; 
      
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getUserQuestionnaireInfoByCat($category_id){
		$user_id = $_SESSION['user_id'];
		$sql="select * from users_questionnaire WHERE user_id=$user_id AND category_id=$category_id ORDER BY batch_id DESC";  
		$query = $this->db->query($sql);
        $row =  $query->result_array();
        $batch_id = $row[0]['batch_id']; 
        if(!empty($batch_id)):
        $sql="select * from users_questionnaire_history t1 INNER JOIN categories t2 ON t1.category_id=t2.category_id INNER JOIN questions t3 ON t1.question_id=t3.question_id INNER JOIN question_options t4 ON t1.option_id=t4.option_id WHERE t1.user_id=$user_id AND t1.category_id=$category_id AND t1.batch_id=$batch_id ORDER BY t1.id ASC"; 
		$query = $this->db->query($sql);
        return $query->result_array();
    	endif;
	}

	public function checkIsQuesByUser($category_id,$user_id = NULL){ 
		$sql="select * from users_questionnaire WHERE user_id=$user_id AND category_id=$category_id ORDER BY batch_id DESC";  
		$query = $this->db->query($sql);
        $row =  $query->result_array(); 
        return $row; 
	}

	public function updateUserPassword($data,$user_id) {
		$this->db->where('user_id',$user_id);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function insertUserQuestionnaire($category_id,$questionarray){
		$form_data = array(
	        'user_id' => $_SESSION['user_id'], 
	        'category_id' =>  $category_id 
      	);
		 $this->db->insert('users_questionnaire',$form_data);
		//echo "<pre>"; print_r($questionarray); echo "</pre>";
		$batch_id = $this->db->insert_id();
		for($i=0;$i<count($questionarray);$i++){
			$form_data = array(
		        'user_id' => $_SESSION['user_id'], 
		        'category_id' =>  $category_id,
		        'batch_id' =>  $batch_id,
		        'question_id' =>  $questionarray[$i]['question_id'],
		        'option_id' =>  $questionarray[$i]['option_id'] 
      		);
      		$this->db->insert('users_questionnaire_history',$form_data);
		} 
        return 1;
    }

    public function checkUserPersonalInfo($user_id) {
		$sql = "select * from patient_information where user_id=$user_id";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		return $cnt;	
	}

	public function getCartInfoById($cart_id) {
		$sql = "select * from patient_cart where cart_id=$cart_id";
		$query = $this->db->query($sql);
		$row = $query->result_array();
    	return $row;
	}

	public function getUserQuestionire($user_id) {
		$sql = "select * from users_questionnaire WHERE user_id=$user_id ORDER BY batch_id DESC LIMIT 1";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
    	return $row;
	}

	public function getUserAddresses($user_id) {
		$sql = "select * from user_addresses WHERE user_id=$user_id ORDER BY address_id DESC";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
    	return $row;
	}

	public function checkUserCartByCat($user_id,$category_id = NULL) {
		if($category_id == "" || $category_id == NULL){
			$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND payment_status IN(0,1) ORDER BY cart_id DESC LIMIT 1";
		}else{
			$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND t1.category_id=$category_id AND payment_status IN(0,1) ORDER BY cart_id DESC LIMIT 1";
		}
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function checkUserCheckoutData($user_id,$batch_id,$category_id) {		 
			$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND t1.category_id=$category_id AND payment_status IN(0,1) AND batch_id=$batch_id ORDER BY cart_id DESC LIMIT 1"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function getCartInfoByBatchId($user_id,$batch_id = NULL) {
		 $sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND payment_status IN(0,1) AND t1.batch_id=$batch_id"; 		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function getCartInfoByBatchIdCartId($user_id,$cart_id,$batch_id = NULL) {
		 $sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND t1.cart_id =$cart_id AND t1.batch_id=$batch_id"; 		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function getUserQuestionnaireInfoByBatch($user_id,$batch_id,$category_id){ 
		$sql="select * from users_questionnaire t1 INNER JOIn categories t2 ON t1.category_id=t2.category_id WHERE t1.user_id=$user_id AND t1.batch_id=$batch_id AND t1.category_id=$category_id"; 
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	public function getUserCheckoutInformation($user_id,$category_id,$batch_id) { 
			$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND t1.category_id=$category_id AND  payment_status IN(0,1)  AND batch_id=$batch_id";		 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	} 

	public function checkUserCartById($user_id,$cart_id) {
		$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id AND t1.cart_id=$cart_id AND payment_status IN(0,1) ORDER BY cart_id DESC LIMIT 1";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function getUserCart($user_id) {
		$sql = "select * from patient_cart t1 INNER JOIN drug_strengh t2 ON t1.drug_id=t2.drug_id AND t1.drug_strength_id=t2.drug_strength_id INNER JOIN drugs t3 ON t1.drug_id=t3.drug_id INNER JOIN drug_prices t4 ON t1.drug_price_id=t4.drug_price_id where t1.user_id=$user_id ORDER BY cart_id DESC LIMIT 1";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $row;	
	}

	public function checkPaymentTxnId($txn_id) {
		$sql = "select * from payments WHERE txn_id='$txn_id'";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		$row = $query->result_array();
        return $cnt;	
	}

	public function getUserCartByCat($user_id,$category_id) {
		$sql = "select * from patient_cart where user_id=$user_id AND category_id=$category_id AND payment_status IN(0,1)";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        return $row;	
	}

	public function getUserAddressById($user_id,$address_id) {
		$sql = "select * from user_addresses where user_id=$user_id AND address_id=$address_id";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        return $row;	
	}

	public function insertPersonalInfo($form_data){
		$this->db->insert('patient_information',$form_data);
	}

	public function insertAddress($form_data){
		$this->db->insert('user_addresses',$form_data);
		return $this->db->insert_id();
	}

	public function updateAddress($data,$address_id,$user_id) {
		$this->db->where('address_id',$address_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('user_addresses',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function insertCart($form_data){
		$this->db->insert('patient_cart',$form_data);
		return $this->db->insert_id();
	}

	public function insertPayment($form_data){
		$this->db->insert('payments',$form_data);
		return $this->db->insert_id();
	}

	public function insertShipPharmacy($form_data){
		$this->db->insert('ship_pharmacy_address',$form_data);
		return $this->db->insert_id();
	}

	public function updatePaymentStatus($data,$cart_id,$user_id) {
		$this->db->where('cart_id',$cart_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('payments',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function updatePaymentStatusCart($data,$cart_id,$user_id) {
		$this->db->where('cart_id',$cart_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('patient_cart',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function updateQuestionnaireStatus($data,$batch_id,$user_id) {

		$this->db->where('batch_id',$batch_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('users_questionnaire',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function updateCart($data,$category_id,$user_id) {
		$this->db->where('category_id',$category_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('patient_cart',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function updatePersonalInfo($data,$user_id) {
		$this->db->where('user_id',$user_id);
		$this->db->update('patient_information',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function getTypes($type) {
        $sql = "select * from tbl_codes where type='$type' ORDER BY code ASC";
        $query = $this->db->query($sql);
        $cnt = $query->num_rows();
        if ($cnt > 0) {
            $row = $query->result_array();
            return $row;
        }
    }

}