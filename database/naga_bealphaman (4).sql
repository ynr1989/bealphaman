-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2022 at 06:17 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naga_bealphaman`
--

-- --------------------------------------------------------

--
-- Table structure for table `alergies`
--

CREATE TABLE `alergies` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alergies`
--

INSERT INTO `alergies` (`id`, `name`, `status`) VALUES
(1, 'None', 1),
(2, 'Latex', 1),
(3, 'Penicillin', 1),
(4, 'Aspirin', 1),
(5, 'Iodine', 1),
(6, 'Shellfish', 1),
(7, 'Other', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `short_description` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `category_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `short_description`, `image`, `category_status`, `created_at`) VALUES
(1, 'Erectile Dysfunction', '', 'ED.png', 1, '2021-02-28 16:18:43'),
(2, 'Hair Loss Treatment', '', 'HL.png', 1, '2021-02-28 16:18:43');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `couponId` int(11) NOT NULL,
  `couponCode` varchar(100) NOT NULL,
  `couponPercentage` varchar(5) DEFAULT NULL COMMENT '%',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `maxAmount` int(11) DEFAULT NULL,
  `couponDesc` text DEFAULT NULL,
  `couponStatus` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`couponId`, `couponCode`, `couponPercentage`, `startDate`, `endDate`, `maxAmount`, `couponDesc`, `couponStatus`, `created_at`) VALUES
(1, 'SUMM30', '15', '2020-07-01', '2020-07-20', 15, 'Test', 1, '2020-07-01 21:49:43'),
(2, 'SUMM44', '5', '2020-06-29', '2020-07-30', 10, 'fsvgsgs', 1, '2020-06-29 10:24:37'),
(8, 'BAM005', '29', '0000-00-00', '0000-00-00', 50, '77777', 0, '2021-05-13 18:40:59'),
(6, 'Test2021', '5', '2021-04-15', '2021-04-20', 10, 'Test Coupon', 0, '2021-04-11 16:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_schedules`
--

CREATE TABLE `doctor_schedules` (
  `doctor_schedule_id` int(11) NOT NULL,
  `day` varchar(9) NOT NULL,
  `schedule_date` varchar(20) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `shift_start` text NOT NULL,
  `shift_end` text NOT NULL,
  `is_availability` int(11) NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_schedules`
--

INSERT INTO `doctor_schedules` (`doctor_schedule_id`, `day`, `schedule_date`, `doctor_id`, `shift_start`, `shift_end`, `is_availability`, `insert_date`, `update_date`) VALUES
(65, 'Monday', '12/27/2021', 202, '14:39', '20:39', 1, '2021-12-26 08:39:40', '2021-12-26 08:39:40'),
(66, 'Thursday', '12/30/2021', 202, '08:39', '20:40', 1, '2021-12-26 08:40:09', '2021-12-26 08:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE `drugs` (
  `drug_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `drug_code` varchar(155) DEFAULT NULL,
  `drug_name` varchar(255) NOT NULL,
  `drug_sub_name` varchar(255) DEFAULT NULL,
  `drug_description` text DEFAULT NULL,
  `drug_status` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drugs`
--

INSERT INTO `drugs` (`drug_id`, `category_id`, `drug_code`, `drug_name`, `drug_sub_name`, `drug_description`, `drug_status`, `created_at`) VALUES
(1, 1, '', 'Sildenafil 20mg', '', '', 1, '2021-02-28 16:32:01'),
(2, 1, NULL, 'Sildenafil', 'Generic of Viagra', NULL, 1, '2021-02-28 16:32:01'),
(3, 1, NULL, 'Tadalafil Daily Dose', 'Generic of Cialis', NULL, 1, '2021-02-28 16:32:52'),
(4, 1, NULL, 'Tadalafil', 'Generic of Cialis', NULL, 1, '2021-02-28 16:32:52'),
(5, 1, NULL, 'Brand Viagra\r\n', NULL, NULL, 0, '2021-02-28 16:33:23'),
(6, 1, NULL, 'Brand Cialis', NULL, NULL, 1, '2021-02-28 16:33:23'),
(7, 2, '', 'Finasteride', '', '', 1, '2021-02-28 16:34:15'),
(8, 2, NULL, 'Minoxidil', NULL, NULL, 2, '2021-02-28 16:34:15'),
(9, 2, '', 'Finasteride and Minoxidil', 'Combo', '', 1, '2021-02-28 16:34:29'),
(10, 1, '5435', 'ccccc', '5435', '543543', 2, '2021-03-01 07:14:03'),
(11, 2, 'ds', 'ccccccc', 'sad', 'sadsadsa', 2, '2021-03-01 07:14:24'),
(12, 1, 'ewe', 'wewqe', 'ewqewq', 'ewqewqe', 2, '2021-03-01 07:16:07'),
(13, 1, 'dsadsa', 'dsad', 'dsad', 'sadsadsa', 2, '2021-03-01 07:16:32'),
(14, 1, 'dsds', 'adsad', 'dsad', 'sadsadsa', 2, '2021-03-01 07:17:39'),
(15, 1, 'dg1', 'Sample drug1 234', 'Sample drug1', 'Sample drug1', 2, '2021-03-03 19:53:03'),
(16, 1, '', 'ddddddcvcvcv', '', '', 2, '2021-03-05 05:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `drug_images`
--

CREATE TABLE `drug_images` (
  `drug_image_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `image` varchar(555) NOT NULL,
  `img_key` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `drug_prices`
--

CREATE TABLE `drug_prices` (
  `drug_price_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `drug_strength_id` int(11) NOT NULL,
  `tablet_set_id` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `price_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drug_prices`
--

INSERT INTO `drug_prices` (`drug_price_id`, `drug_id`, `drug_strength_id`, `tablet_set_id`, `price`, `price_status`, `created_at`) VALUES
(1, 1, 1, 1, '37', 1, '2021-03-15 07:29:41'),
(2, 1, 1, 2, '30', 1, '2021-03-15 07:30:09'),
(3, 1, 1, 3, '90', 1, '2021-03-15 07:30:29'),
(4, 1, 2, 1, '6', 1, '2021-03-15 07:30:50'),
(5, 1, 2, 2, '40', 1, '2021-03-15 07:31:05'),
(6, 1, 2, 3, '96', 1, '2021-03-15 07:31:28'),
(7, 1, 3, 1, '9', 1, '2021-03-15 07:31:55'),
(8, 1, 3, 2, '60', 1, '2021-03-15 07:32:24'),
(9, 1, 3, 3, '144', 1, '2021-03-15 07:32:39'),
(10, 1, 4, 1, '12', 1, '2021-03-15 07:32:58'),
(11, 1, 4, 2, '80', 1, '2021-03-15 07:33:20'),
(12, 1, 4, 3, '192', 1, '2021-03-15 07:33:38'),
(13, 1, 5, 1, '15', 1, '2021-03-15 07:34:01'),
(14, 1, 5, 2, '100', 1, '2021-03-15 07:34:15'),
(15, 1, 5, 3, '240', 1, '2021-03-15 07:34:35'),
(16, 2, 6, 4, '10', 1, '2021-03-15 07:35:12'),
(17, 2, 6, 5, '80', 1, '2021-03-15 07:37:53'),
(18, 2, 6, 6, '200', 1, '2021-03-15 07:38:27'),
(19, 2, 7, 4, '10', 1, '2021-03-15 07:39:40'),
(20, 2, 7, 5, '80', 1, '2021-03-15 07:40:14'),
(21, 2, 7, 6, '200', 1, '2021-03-15 07:40:38'),
(22, 2, 8, 4, '10', 1, '2021-03-15 07:42:01'),
(23, 2, 8, 5, '80', 1, '2021-03-15 07:42:42'),
(24, 2, 8, 6, '200', 1, '2021-03-15 07:43:15'),
(25, 3, 9, 7, '3', 1, '2021-03-15 07:43:54'),
(26, 3, 9, 8, '90', 1, '2021-03-15 07:44:08'),
(27, 3, 9, 9, '216', 1, '2021-03-15 07:44:25'),
(28, 3, 21, 7, '3', 1, '2021-03-15 07:45:16'),
(29, 3, 21, 8, '90', 1, '2021-03-15 07:45:30'),
(30, 3, 21, 9, '216', 1, '2021-03-15 07:45:43'),
(31, 4, 11, 10, '6', 1, '2021-03-15 07:46:39'),
(32, 4, 11, 11, '60', 1, '2021-03-15 07:46:54'),
(33, 4, 11, 12, '144', 1, '2021-03-15 07:47:08'),
(34, 4, 12, 10, '10', 1, '2021-03-15 07:47:24'),
(35, 4, 12, 11, '100', 1, '2021-03-15 07:48:19'),
(36, 4, 12, 12, '240', 1, '2021-03-15 07:48:41'),
(37, 5, 13, 13, '10', 1, '2021-03-15 07:49:25'),
(38, 5, 14, 13, '10', 1, '2021-03-15 07:49:43'),
(39, 5, 15, 13, '10', 1, '2021-03-15 07:49:58'),
(40, 6, 16, 14, '17', 1, '2021-03-15 07:50:25'),
(41, 6, 17, 14, '17', 1, '2021-03-15 07:50:47'),
(42, 6, 18, 14, '72', 1, '2021-03-15 07:51:12'),
(43, 6, 19, 14, '72', 1, '2021-03-15 07:51:31'),
(44, 7, 20, 15, '20', 1, '2021-03-15 07:52:07'),
(45, 7, 20, 16, '45', 1, '2021-03-15 07:52:24'),
(47, 2, 22, 17, '250', 1, '2021-04-05 19:54:01'),
(48, 2, 22, 4, '250', 1, '2021-04-05 19:57:23'),
(49, 2, 22, 5, '2500', 1, '2021-04-05 19:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `drug_strengh`
--

CREATE TABLE `drug_strengh` (
  `drug_strength_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `strength` varchar(255) NOT NULL,
  `strength_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drug_strengh`
--

INSERT INTO `drug_strengh` (`drug_strength_id`, `drug_id`, `strength`, `strength_status`, `created_at`) VALUES
(1, 1, '20mg', 0, '2021-03-15 07:11:34'),
(2, 1, '40mg(2 tablets)', 0, '2021-03-15 07:12:19'),
(3, 1, '60mg(3 tablets)', 0, '2021-03-15 07:12:36'),
(4, 1, '80mg(4 tablets)', 0, '2021-03-15 07:12:52'),
(5, 1, '100mg(5 tablets)', 0, '2021-03-15 07:13:07'),
(6, 2, '25mg', 0, '2021-03-15 07:13:47'),
(7, 2, '50mg', 0, '2021-03-15 07:14:01'),
(8, 2, '100mg', 0, '2021-03-15 07:14:15'),
(9, 3, '2.5mg (Daily)', 0, '2021-03-15 07:14:52'),
(10, 4, '5mg (Daily)', 2, '2021-03-15 07:15:04'),
(11, 4, '10mg', 0, '2021-03-15 07:15:20'),
(12, 4, '20mg', 0, '2021-03-15 07:15:36'),
(13, 5, '25mg', 0, '2021-03-15 07:15:51'),
(14, 5, '50mg', 0, '2021-03-15 07:16:06'),
(15, 5, '100mg', 0, '2021-03-15 07:16:17'),
(16, 6, '2.5mg', 0, '2021-03-15 07:16:35'),
(17, 6, '5mg', 0, '2021-03-15 07:16:47'),
(18, 6, '10mg', 0, '2021-03-15 07:16:59'),
(19, 6, '20mg', 0, '2021-03-15 07:17:12'),
(20, 7, '1mg', 0, '2021-03-15 07:17:26'),
(21, 3, '5mg (Daily)', 0, '2021-03-15 07:44:52'),
(22, 2, '200 mg', 0, '2021-04-05 19:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `drug_tablets_set`
--

CREATE TABLE `drug_tablets_set` (
  `tablet_set_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `drug_strength_id` int(11) NOT NULL,
  `tablet_count` varchar(100) NOT NULL,
  `tablet_set_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drug_tablets_set`
--

INSERT INTO `drug_tablets_set` (`tablet_set_id`, `drug_id`, `drug_strength_id`, `tablet_count`, `tablet_set_status`, `created_at`) VALUES
(1, 1, 0, '1 Dose', 1, '2021-03-15 07:23:08'),
(2, 1, 0, '10 Doses', 1, '2021-03-15 07:23:27'),
(3, 1, 0, '30 Doses', 1, '2021-03-15 07:23:40'),
(4, 2, 0, '1 Dose', 1, '2021-03-15 07:23:58'),
(5, 2, 0, '10 Doses', 1, '2021-03-15 07:24:14'),
(6, 2, 0, '30 Doses', 1, '2021-03-15 07:24:28'),
(7, 3, 0, '1 Dose', 1, '2021-03-15 07:24:44'),
(8, 3, 0, '30 Doses 30 Day', 1, '2021-03-15 07:24:59'),
(9, 3, 0, '90 Doses 90 Day', 1, '2021-03-15 07:25:13'),
(10, 4, 0, '1 Dose', 1, '2021-03-15 07:25:27'),
(11, 4, 0, '10 Doses', 1, '2021-03-15 07:25:39'),
(12, 4, 0, '30', 1, '2021-03-15 07:25:54'),
(13, 5, 0, '1 Dose', 1, '2021-03-15 07:26:10'),
(14, 6, 0, '1 Dose', 1, '2021-03-15 07:26:27'),
(15, 7, 0, '30 Day Supply', 1, '2021-03-15 07:26:42'),
(16, 7, 0, '90 Day Supply $45.00', 1, '2021-03-15 07:27:02'),
(17, 2, 0, '1', 2, '2021-04-05 19:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `health_issues`
--

CREATE TABLE `health_issues` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `health_issues`
--

INSERT INTO `health_issues` (`id`, `name`, `status`) VALUES
(1, 'Bleeding Problems', 1),
(2, 'Anemia', 1),
(3, 'High Blood Pressure', 1),
(4, 'Heart disease/Heart attack', 1),
(5, 'Atrial Fibrilliation', 1),
(6, 'Pacemaker', 1),
(7, 'Lung Disease', 1),
(8, 'Copd/emphysema', 1),
(9, 'Kidney Disease', 1),
(10, 'Liver Disease', 1),
(11, 'Hepatitis _A _B _C', 1),
(12, 'Colitis', 1),
(13, 'Diverticulitis', 1),
(14, 'Diverticulitis', 1),
(15, 'Ulcers/Gerd', 1),
(16, 'Thyroid problems', 1),
(17, 'Lupus', 1),
(18, 'Arthritis', 1),
(19, 'Gout', 1),
(20, 'Parkinson’s', 1),
(21, 'Stroke /TIA', 1),
(22, 'Seizures', 1),
(23, 'Phlebitis/DVT', 1),
(24, 'Venereal Disease', 1),
(25, 'Cancer', 1),
(26, 'Asthma', 1),
(27, 'Diabetes', 1),
(28, 'HIV or AIDS', 1),
(29, 'High Cholesterol', 1),
(30, 'Other', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_title` varchar(555) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_title`, `description`, `status`, `created_at`) VALUES
(1, 'fdsfds', '<p>fdsfdsfd</p>\n', 2, '2021-03-26 07:56:05'),
(2, 'dsadsa czxf fffffffff', '<p>dsadsadsa ds dsa dsa dsa dsadsad</p>\n', 1, '2021-03-26 07:58:08'),
(3, 'gfdgfdgfdgfdgfdg', '<p><u>fdgfdg gdf fd&nbsp; &nbsp;</u>&nbsp;f gfdg fdg <strong>gfd </strong>fd</p>\n', 1, '2021-03-26 08:12:07'),
(4, 'about s', '<p>dsdsadsadsad</p>\n\n<p><strong>dsadsa</strong></p>\n\n<p>dsa</p>\n\n<p>dsa</p>\n', 1, '2021-03-27 20:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `patient_cart`
--

CREATE TABLE `patient_cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `drug_strength_id` int(11) NOT NULL,
  `drug_price_id` int(11) NOT NULL,
  `drug_price` int(11) DEFAULT NULL,
  `no_of_refills` varchar(10) DEFAULT NULL,
  `doctor_consultation_fee` int(11) NOT NULL,
  `user_address_id` int(11) DEFAULT NULL,
  `ship_address_type` int(11) DEFAULT NULL COMMENT '1-Default,2-Customer Pharmacy',
  `appointment_date` varchar(25) DEFAULT NULL,
  `appointment_time` varchar(25) DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-No Payment, 1-Partial Payment, 2-Payment Done',
  `doctor_approve_status` int(11) NOT NULL DEFAULT 0,
  `send_to_pharmacy` int(11) DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_cart`
--

INSERT INTO `patient_cart` (`cart_id`, `user_id`, `batch_id`, `address_id`, `category_id`, `drug_id`, `drug_strength_id`, `drug_price_id`, `drug_price`, `no_of_refills`, `doctor_consultation_fee`, `user_address_id`, `ship_address_type`, `appointment_date`, `appointment_time`, `payment_status`, `doctor_approve_status`, `send_to_pharmacy`, `updated_at`, `created_at`) VALUES
(1, 207, 5, 2, 2, 7, 20, 44, 20, NULL, 40, NULL, 1, '06-18-2021', '09:45', 2, 0, NULL, '2021-06-13 10:55:26', '2021-06-13 10:55:26'),
(2, 207, 5, 2, 2, 7, 20, 44, 20, NULL, 40, NULL, 1, '11-30-2021', '01:00', 1, 0, NULL, '2021-06-20 11:28:03', '2021-06-20 11:28:03'),
(3, 207, 4, 2, 1, 6, 16, 40, 17, NULL, 40, NULL, 1, '11-30-2021', '01:00', 1, 0, NULL, '2021-07-25 22:59:42', '2021-07-25 22:59:42'),
(4, 206, 1, 0, 2, 7, 20, 44, 20, NULL, 40, NULL, NULL, NULL, NULL, 0, 0, NULL, '2021-12-26 08:46:25', '2021-12-26 08:46:25'),
(5, 206, 1, 1, 1, 6, 16, 40, 17, NULL, 40, NULL, 1, '12-30-2021', '09:19', 1, 0, NULL, '2021-12-29 03:49:50', '2021-12-29 03:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `patient_information`
--

CREATE TABLE `patient_information` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `height_feet` varchar(100) DEFAULT NULL,
  `height_inches` varchar(20) DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL,
  `living_situation` varchar(255) DEFAULT NULL,
  `smoking_status` varchar(155) DEFAULT NULL,
  `alcohol_status` varchar(155) DEFAULT NULL,
  `drug_status` varchar(155) DEFAULT NULL,
  `blood_pressure` varchar(100) DEFAULT NULL,
  `reason_for_visit` varchar(100) DEFAULT NULL,
  `what_is_the_problem` varchar(100) DEFAULT NULL,
  `when_did_it_start` varchar(100) DEFAULT NULL,
  `how_often` varchar(100) DEFAULT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `medication_taken` varchar(100) DEFAULT NULL,
  `operations_surgeries` varchar(100) DEFAULT NULL,
  `alergies` varchar(100) DEFAULT NULL,
  `health_issues` varchar(100) DEFAULT NULL,
  `family_history` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_information`
--

INSERT INTO `patient_information` (`id`, `user_id`, `height_feet`, `height_inches`, `weight`, `living_situation`, `smoking_status`, `alcohol_status`, `drug_status`, `blood_pressure`, `reason_for_visit`, `what_is_the_problem`, `when_did_it_start`, `how_often`, `notes`, `message`, `medication_taken`, `operations_surgeries`, `alergies`, `health_issues`, `family_history`, `created_at`) VALUES
(1, 201, '5', '1', '25', '2', '2', '2', '2', '120-80', '1', 'rdrew', '2', 'Testing', '', NULL, '2', '1', '1', '1,2,3', '2,4', '2021-03-31 12:11:45'),
(3, 200, '', '', '', '1', '1', '2', '2', '', '', '', '', '', '', NULL, '1', '2', '', '', '', '2021-04-05 05:58:08'),
(4, 203, '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '1,2', '2021-04-11 19:44:33'),
(5, 207, '', '', '', '2', '1', '3', '2', '', '', '', '', '', '', 'Teesss', '1', '1', '2,3,4', '6,7', '1,2', '2021-06-13 13:42:51');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `payment_for` varchar(255) NOT NULL,
  `payment_fee` varchar(20) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `txn_type` varchar(100) DEFAULT NULL,
  `payment_string` text DEFAULT NULL,
  `payment_status` varchar(50) NOT NULL,
  `verify_sign` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`payment_id`, `user_id`, `cart_id`, `amount`, `payer_email`, `payment_for`, `payment_fee`, `txn_id`, `txn_type`, `payment_string`, `payment_status`, `verify_sign`, `created_at`) VALUES
(1, 206, 5, '40.00', 'atulramesht-buyer@gmail.com', 'Doctor Consultation Fee', '2.19', '61P05912EJ7889649', NULL, 'payer_email=atulramesht-buyer%40gmail.com, payer_id=W5TE2WL9NASQS, payer_status=VERIFIED, first_name=test, last_name=buyer, txn_id=61P05912EJ7889649, mc_currency=USD, mc_fee=2.19, mc_gross=40.00, protection_eligibility=ELIGIBLE, payment_fee=2.19, payment_gross=40.00, payment_status=Completed, payment_type=instant, handling_amount=0.00, shipping=0.00, item_name=Doctor+Consultation+Fee, item_number=5_206_12-30-2021_09%3A19, quantity=1, txn_type=web_accept, payment_date=2021-12-29T12%3A40%3A07Z, business=ynreddy1989nthwall%40gmail.com, receiver_id=P9932YLLCL8D4, notify_version=UNVERSIONED, verify_sign=AB7.8XxLblmrjlOMDGRqqdPSpHr7ASn6gzMAzGgLYpXaaK9cE1-hWJIa', 'Completed', 'AB7.8XxLblmrjlOMDGRqqdPSpHr7ASn6gzMAzGgLYpXaaK9cE1-hWJIa', '2021-12-29 12:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `sn` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `batch_id` varchar(25) DEFAULT NULL,
  `question_name` text NOT NULL,
  `question_description` text DEFAULT NULL,
  `question_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `sn`, `category_id`, `batch_id`, `question_name`, `question_description`, `question_status`, `created_at`) VALUES
(1, 1, 1, 'BAT001', 'How often are you suffering from ED?', NULL, 1, '2021-03-16 07:08:10'),
(2, 2, 1, 'BAT001', 'What sort of results are you looking for?', NULL, 1, '2021-03-16 07:08:11'),
(3, 3, 1, 'BAT001', 'Do you ever have a problem getting or maintaining an erection that is rigid enough for sex?', NULL, 1, '2021-03-16 07:08:11'),
(4, 4, 1, 'BAT001', 'How did your erectile dysfunction begin? Select the one that best describes your ED. ', NULL, 1, '2021-03-16 07:08:12'),
(5, 5, 1, 'BAT001', 'Do you get erections …', NULL, 1, '2021-03-16 07:08:12'),
(6, 6, 1, 'BAT001', 'Which best describes your sex drive or desire to have sex (libido)?', NULL, 1, '2021-03-16 07:08:12'),
(7, 7, 1, 'BAT001', 'Have you ever been formally treated for ED or tried any medication, vitamins, or supplements to treat it?', NULL, 1, '2021-03-16 07:08:12'),
(8, 8, 1, 'BAT001', 'Have you had a physical exam with a healthcare provider in the past 3 years?', NULL, 1, '2021-03-16 07:08:12'),
(9, 9, 1, 'BAT001', 'Have you had your blood pressure measured in the past 6 months?', 'You need to know your blood pressure (BP) within the last 6 months to receive treatment. It is an important factor in deciding whether it is safe for you to use ED medication. It is essential that you are accurate and 	honest about your BP. ', 1, '2021-03-16 07:08:13'),
(10, 10, 1, 'BAT001', 'What was your blood pressure (BP) at your last reading measured within the last 6 months?', 'Note: BP is read as 2 numbers – the top number is always higher than the bottom number. Example: “115/72” 115 (systolic) 72 (diastolic)', 1, '2021-03-16 07:08:13'),
(11, 11, 1, 'BAT001', 'Do you take any medication, vitamins, or supplements regularly?', NULL, 1, '2021-03-16 07:08:13'),
(12, 12, 1, 'BAT001', 'Please list any current medicines, vitamins, or dietary supplements you take regularly, including the dosage.\n	Include any medicines (e.g. Lipitor, Zyrtec, Ibuprofen) or any supplement taken in the past 2 weeks, even if you are not taking them daily. ', NULL, 1, '2021-03-16 07:08:13'),
(13, 13, 1, 'BAT001', 'Do you take any of the following medication? Select all that apply. ', NULL, 1, '2021-03-16 07:08:13'),
(14, 14, 1, 'BAT001', 'Do you have any allergies? \n	Include any allergies to food, dyes, prescriptions, over –the-counter medicines (e.g. antibiotics, allergy medications), herbs, vitamins, supplements, or anything else. \n', NULL, 1, '2021-03-16 07:08:14'),
(15, 15, 1, 'BAT001', 'Do any of the following cardiovascular risk factors apply to you?', NULL, 1, '2021-03-16 07:08:14'),
(16, 16, 1, 'BAT001', 'Do you experience any of the following cardiovascular symptoms? ', NULL, 1, '2021-03-16 07:08:14'),
(17, 17, 1, 'BAT001', 'Do you have or have you previously been diagnosed with any of the following? Select all that apply. ', NULL, 1, '2021-03-16 07:08:15'),
(18, 18, 1, 'BAT001', ' In the last 2 weeks, have you been troubled by any of the following?', NULL, 1, '2021-03-16 07:08:15'),
(19, 19, 1, 'BAT001', 'Do you use any of the following recreational drugs?\n	Severe reactions may result if ED meds are used in conjunction with recreational drugs. ', NULL, 1, '2021-03-16 07:08:16'),
(20, 20, 1, 'BAT001', 'Do you have, or have you ever had any of the following conditions? \n	This information helps your doctor provide both effective and safe 	dosages of medication if appropriate. ', NULL, 1, '2021-03-16 07:08:16'),
(21, 21, 1, 'BAT001', 'Do you have any of the following conditions? Select all that apply. ', NULL, 1, '2021-03-16 07:08:18'),
(22, 22, 1, 'BAT001', 'Do you have any other medical conditions or history or prior surgeries?', NULL, 1, '2021-03-16 07:08:18'),
(23, 1, 2, 'BAT002', 'Have you experienced any of the following?', NULL, 1, '2021-03-16 07:08:18'),
(24, 2, 2, 'BAT002', 'Have you ever been formally treated for hair loss (including any medical procedures) or tried any medicines. Vitamins, or supplements to treat it?', NULL, 1, '2021-03-16 07:08:18'),
(25, 3, 2, 'BAT002', 'Have you experienced any of the following conditions, events, or symptoms?', NULL, 1, '2021-03-16 07:08:19'),
(26, 4, 2, 'BAT002', 'Do you have any medical conditions or a history of prior surgeries?', NULL, 1, '2021-03-16 07:08:19'),
(27, 5, 2, 'BAT002', 'Do you have a problem getting or maintaining an erection that is satisfying enough for sex?', NULL, 1, '2021-03-16 07:08:19'),
(28, 6, 2, 'BAT002', 'Are you currently suffering from depression or anxiety?', NULL, 1, '2021-03-16 07:08:19'),
(29, 7, 2, 'BAT002', 'Which of the following apply to you ?\n	Sometimes, lifestyle habits are associated with, contribute to, or worsen hair loss. Understanding your sleep, exercise, and other lifestyle habits can help your doctor make the most appropriate recommendations for you.', NULL, 1, '2021-03-16 07:08:20'),
(30, 8, 2, 'BAT002', 'Is there anything else you want your doctor to know about your condition or health?', NULL, 1, '2021-03-16 07:08:20');

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

CREATE TABLE `question_options` (
  `option_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_option` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question_options`
--

INSERT INTO `question_options` (`option_id`, `question_id`, `question_option`, `created_at`) VALUES
(1, 1, 'Every time ', '2021-03-16 07:08:11'),
(2, 1, ' Half the time ', '2021-03-16 07:08:11'),
(3, 1, ' On occasion ', '2021-03-16 07:08:11'),
(4, 1, ' Rarely \n', '2021-03-16 07:08:11'),
(5, 2, 'Get hard and stay hard ', '2021-03-16 07:08:11'),
(6, 2, ' Increase my libido ', '2021-03-16 07:08:11'),
(7, 2, '  All of the above', '2021-03-16 07:08:11'),
(8, 3, 'Yes – every time ', '2021-03-16 07:08:11'),
(9, 3, ' Yes – more than half the time ', '2021-03-16 07:08:11'),
(10, 3, ' Yes – on occasion ', '2021-03-16 07:08:11'),
(11, 3, ' Yes – but rarely ', '2021-03-16 07:08:12'),
(12, 3, ' I never have a problem getting or maintaining an erection for as long as I want \n', '2021-03-16 07:08:12'),
(13, 4, 'Gradually but has worsened over time ', '2021-03-16 07:08:12'),
(14, 4, ' Suddenly, but not with a new partner ', '2021-03-16 07:08:12'),
(15, 4, ' Suddenly, with a new partner ', '2021-03-16 07:08:12'),
(16, 4, ' I do not recall how it began \n', '2021-03-16 07:08:12'),
(17, 5, 'When masturbating ', '2021-03-16 07:08:12'),
(18, 5, ' When you wake up ', '2021-03-16 07:08:12'),
(19, 5, ' Neither\n', '2021-03-16 07:08:12'),
(20, 6, 'Less that it was ', '2021-03-16 07:08:12'),
(21, 6, ' Less than it was because I have trouble with erections ', '2021-03-16 07:08:12'),
(22, 6, ' Less and it started before I had trouble with erections ', '2021-03-16 07:08:12'),
(23, 6, ' Less but I don’t know which came first (less desire or trouble with erections) ', '2021-03-16 07:08:12'),
(24, 6, ' Unchanged ', '2021-03-16 07:08:12'),
(25, 7, 'No ', '2021-03-16 07:08:12'),
(26, 7, ' Yes', '2021-03-16 07:08:12'),
(27, 8, 'Yes, it was normal – continue ', '2021-03-16 07:08:12'),
(28, 8, ' Yes, but there were issues ', '2021-03-16 07:08:12'),
(29, 8, ' No', '2021-03-16 07:08:12'),
(30, 9, 'Continue, I know my blood pressure reading ', '2021-03-16 07:08:13'),
(31, 9, ' I do not remember a recent blood pressure reading ', '2021-03-16 07:08:13'),
(32, 10, 'Systolic (mmHg)   -     (      ) 	Diastolic (mmHg) -     (      )	', '2021-03-16 07:08:13'),
(33, 11, 'No ', '2021-03-16 07:08:13'),
(34, 11, ' Yes', '2021-03-16 07:08:13'),
(35, 13, 'Nitroglycerin spray, ointment, patches or tablets (Nitro – Dur, Nitrolingual, Nitrostat, Nitromist, Nitro – Bid, Transderm – Nitro, Nitro – Time, Deponit, Minitran, Nitrek, Nitrodisc, Nitrogard, Nitroglycn, Nitrol ointment, Nitrong, Nitro – Par) ', '2021-03-16 07:08:13'),
(36, 13, ' Isosorbide mononitrate, or isosorbide dinitrate (Isordil, Dilatrate, Sorbitrate, Imdur, Ismo, Monoket) ', '2021-03-16 07:08:13'),
(37, 13, ' Other medicines containing nitrates ', '2021-03-16 07:08:13'),
(38, 13, ' Nitric Oxide supplements / booster ', '2021-03-16 07:08:13'),
(39, 13, ' Nitroglycerin spray, ointment, patches or tablets (Nitro – Dur, Nitrolingual, Nitrostat, Nitromist, Nitro – Bid, Transderm – Nitro, Nitro – Time, Deponit, Minitran, Nitrek, Nitrodisc, Nitrogard, Nitroglycn, Nitrol ointment, Nitrong, Nitro – Par) ', '2021-03-16 07:08:13'),
(40, 13, ' Isosorbide mononitrate, or isosorbide dinitrate (Isordil, Dilatrate, Sorbitrate, Imdur, Ismo, Monoket) ', '2021-03-16 07:08:13'),
(41, 13, ' Other medicines containing nitrates ', '2021-03-16 07:08:13'),
(42, 13, ' Nitric Oxide supplements / boosters ', '2021-03-16 07:08:13'),
(43, 13, ' Non-uroselective alpha blockers, doxazosin (Cardura), prazosin (Minipress), terazosin (Hytrin) ', '2021-03-16 07:08:13'),
(44, 13, ' Other medicines containing nitrates ', '2021-03-16 07:08:13'),
(45, 13, ' Nitric Oxide supplements / boosters ', '2021-03-16 07:08:14'),
(46, 13, ' Non – uroselective alpha blockers, doxazosin (Cardura), prazosin (Minipress), terazosin (Hytrin) ', '2021-03-16 07:08:14'),
(47, 13, ' Uroselective alpha blockers (i.e. Flomax (tamsulosin), Uroxatral (alfuzosin), and Rapaflo (silodosin) ', '2021-03-16 07:08:14'),
(48, 13, ' Riociguat (Adempas) ', '2021-03-16 07:08:14'),
(49, 13, ' None Apply', '2021-03-16 07:08:14'),
(50, 14, 'No ', '2021-03-16 07:08:14'),
(51, 14, ' Yes', '2021-03-16 07:08:14'),
(52, 15, 'Diabetes ', '2021-03-16 07:08:14'),
(53, 15, ' High cholesterolHigh blood pressure ', '2021-03-16 07:08:14'),
(54, 15, ' My father had a heart attack or heart disease at 55 years or younger ', '2021-03-16 07:08:14'),
(55, 15, ' My mother had a heart attack or heart disease at 65 years or younger ', '2021-03-16 07:08:14'),
(56, 15, ' None apply to me ', '2021-03-16 07:08:14'),
(57, 16, 'Chest pain or shortness of breath when climbing 2 flights of stairs or walking 4 blocks ', '2021-03-16 07:08:14'),
(58, 16, ' Chest pain or shortness of breath with sexual activity ', '2021-03-16 07:08:14'),
(59, 16, ' Unexplained fainting or dizziness ', '2021-03-16 07:08:14'),
(60, 16, ' Prolonged cramping of the legs with exercise ', '2021-03-16 07:08:15'),
(61, 16, ' Abnormal heart beats or rhythms ', '2021-03-16 07:08:15'),
(62, 16, ' None of these apply to me ', '2021-03-16 07:08:15'),
(63, 17, 'Prostate cancer ', '2021-03-16 07:08:15'),
(64, 17, ' Enlarged prostate (BPH) ', '2021-03-16 07:08:15'),
(65, 17, ' Kidney transplant or any condition affecting the kidney ', '2021-03-16 07:08:15'),
(66, 17, ' Liver disease ', '2021-03-16 07:08:15'),
(67, 17, ' Multiple Sclerosis (MS) or similar disease Spinal injuries and / or paralysis ', '2021-03-16 07:08:15'),
(68, 17, ' Neurological diseases ', '2021-03-16 07:08:15'),
(69, 17, ' Stomach, intestinal, bowel ulcers ', '2021-03-16 07:08:15'),
(70, 17, ' Life threatening heart arrhythmia ', '2021-03-16 07:08:15'),
(71, 17, ' Any acquired, congenital, or developmental abnormalities of the heart including heart murmurs ', '2021-03-16 07:08:15'),
(72, 17, ' COVID – 19 ', '2021-03-16 07:08:15'),
(73, 17, ' None of these apply to me ', '2021-03-16 07:08:15'),
(74, 18, 'Little interest or pleasure in doing things ', '2021-03-16 07:08:15'),
(75, 18, ' Feeling down, depressed, or hopeless ', '2021-03-16 07:08:15'),
(76, 18, ' Feeling nervous, anxious, or on edge (enough that it impairs your ability to function at work or at home) ', '2021-03-16 07:08:15'),
(77, 18, ' Worrying too much about different things (enough that it impairs your ability to function at work or at home) ', '2021-03-16 07:08:15'),
(78, 18, ' No, I have not felt down, anxious, nervous, etc. in the last 2 weeks. ', '2021-03-16 07:08:16'),
(79, 19, 'Poppers or Rush ', '2021-03-16 07:08:16'),
(80, 19, ' Amyl Nitrate or Butyl Nitrate ', '2021-03-16 07:08:16'),
(81, 19, ' Cocaine ', '2021-03-16 07:08:16'),
(82, 19, ' Cigarettes ', '2021-03-16 07:08:16'),
(83, 19, ' Marijuana ', '2021-03-16 07:08:16'),
(84, 19, ' Other ', '2021-03-16 07:08:16'),
(85, 19, ' No I don’t use any of these', '2021-03-16 07:08:16'),
(86, 20, 'Priapism (erection lasting longer than 4 hours) ', '2021-03-16 07:08:16'),
(87, 20, ' Retinitis Pigmentosa ', '2021-03-16 07:08:17'),
(88, 20, ' Anterior Ischemic Optic Neuropathy (AION) ', '2021-03-16 07:08:17'),
(89, 20, ' Blood clotting disorder, abnormal bleeding or bruising, or coagulopathy ', '2021-03-16 07:08:17'),
(90, 20, ' Stomach or intestinal ulcer ', '2021-03-16 07:08:17'),
(91, 20, ' A prior heart attack ', '2021-03-16 07:08:17'),
(92, 20, ' Heart failure, or narrowing of the arteries ', '2021-03-16 07:08:17'),
(93, 20, ' Stroke, TIA, or severe insufficiency of the autonomic nervous system ', '2021-03-16 07:08:17'),
(94, 20, ' Peripheral Vascular Disease ', '2021-03-16 07:08:17'),
(95, 20, ' Any history of QT prolongation in you - even in your family ', '2021-03-16 07:08:17'),
(96, 20, ' Sickle Cell Anemia, Myeloma, LeukemiaIdiopathic Hypertrophic Subaortic Stenosis ', '2021-03-16 07:08:17'),
(97, 20, ' Use blood thinners ', '2021-03-16 07:08:17'),
(98, 20, ' None apply to me ', '2021-03-16 07:08:17'),
(99, 21, 'A marked curve or bend in the penis that interferes with sex, or Peyronie’s Disease ', '2021-03-16 07:08:18'),
(100, 21, ' Pain with erections or with ejaculation ', '2021-03-16 07:08:18'),
(101, 21, ' A foreskin that is too tight ', '2021-03-16 07:08:18'),
(102, 21, ' Fibrous tissue in the penis (lumps and bumps under the skin that feel hard) ', '2021-03-16 07:08:18'),
(103, 21, ' Any history of QT prolongation in you – or even in your family ', '2021-03-16 07:08:18'),
(104, 21, ' Sickle Cell Anemia, Myeloma, LeukemiaIdiopathic Hypertrophic Stenosis ', '2021-03-16 07:08:18'),
(105, 21, ' Use of blood thinners ', '2021-03-16 07:08:18'),
(106, 21, ' None apply to me ', '2021-03-16 07:08:18'),
(107, 22, 'No ', '2021-03-16 07:08:18'),
(108, 22, ' Yes', '2021-03-16 07:08:18'),
(109, 23, 'Dandruff ', '2021-03-16 07:08:18'),
(110, 23, ' A sudden increase in hair loss ', '2021-03-16 07:08:18'),
(111, 23, ' Losing body hair ', '2021-03-16 07:08:18'),
(112, 23, ' Pain, itching, burning, or bumps on the scalp ', '2021-03-16 07:08:18'),
(113, 23, ' Red rings or other rashes on the scalp ', '2021-03-16 07:08:18'),
(114, 23, ' Hair loss other than on your head ', '2021-03-16 07:08:18'),
(115, 23, ' A diagnosis of scalp psoriasis or scalp eczema ', '2021-03-16 07:08:18'),
(116, 23, 'No, I have not experienced any of these symptoms ', '2021-03-16 07:08:18'),
(117, 24, 'No ', '2021-03-16 07:08:18'),
(118, 24, ' Yes', '2021-03-16 07:08:18'),
(119, 25, 'Kidney disease ', '2021-03-16 07:08:19'),
(120, 25, ' Liver disease ', '2021-03-16 07:08:19'),
(121, 25, ' Thyroid disease ', '2021-03-16 07:08:19'),
(122, 25, ' Prostate enlargement (e.g., a weak stream, feeling an urgent need to urinate ', '2021-03-16 07:08:19'),
(123, 25, ' Cancer ', '2021-03-16 07:08:19'),
(124, 25, ' HIV ', '2021-03-16 07:08:19'),
(125, 25, ' A weakened immune system due to other causes ', '2021-03-16 07:08:19'),
(126, 25, ' Difficult and recurrent  yeast or fungal infections ', '2021-03-16 07:08:19'),
(127, 25, ' Rheumatological disorders or autoimmune disease (e.g., Lupus, discoid lupus, sarcoidosis, psoriatic arthritis)\n ', '2021-03-16 07:08:19'),
(128, 25, ' No, I have not experienced any of these conditions, events, or symptoms', '2021-03-16 07:08:19'),
(129, 26, 'No ', '2021-03-16 07:08:19'),
(130, 26, ' Yes', '2021-03-16 07:08:19'),
(131, 27, 'Yes, every time ', '2021-03-16 07:08:19'),
(132, 27, ' Yes, more than a half time ', '2021-03-16 07:08:19'),
(133, 27, ' Yes, on occasion ', '2021-03-16 07:08:19'),
(134, 27, ' Yes, but rarely\n', '2021-03-16 07:08:19'),
(135, 27, ' I NEVER have a problem getting or maintaining an erection for as long as I want', '2021-03-16 07:08:19'),
(136, 28, 'No ', '2021-03-16 07:08:20'),
(137, 28, ' Yes', '2021-03-16 07:08:20'),
(138, 29, 'I get less than 2 hours of exercise per week ', '2021-03-16 07:08:20'),
(139, 29, ' I do not eat as healthy as I would like ', '2021-03-16 07:08:20'),
(140, 29, ' I smoke or use tobacco (e.g., chewing tobacco, snuff) ', '2021-03-16 07:08:20'),
(141, 29, ' I use other nicotine containing products (e.g., vaping) ', '2021-03-16 07:08:20'),
(142, 29, ' I drink more than 2 alcoholic drinks per day ', '2021-03-16 07:08:20'),
(143, 29, ' I get less than 7 hours of sleep per night, on average ', '2021-03-16 07:08:20'),
(144, 29, ' I am 20+ pounds overweight ', '2021-03-16 07:08:20'),
(145, 29, ' I am frequently under a lot of stress ', '2021-03-16 07:08:20'),
(146, 29, 'None apply to me ', '2021-03-16 07:08:20'),
(147, 30, 'No ', '2021-03-16 07:08:20'),
(148, 30, ' Yes', '2021-03-16 07:08:20');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `paypal_business_email` varchar(255) DEFAULT NULL,
  `doctor_consultation_fee` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `paypal_business_email`, `doctor_consultation_fee`) VALUES
(1, 'Be Alpha Man', 'ynreddy1989@gmail.com', '40');

-- --------------------------------------------------------

--
-- Table structure for table `ship_pharmacy_address`
--

CREATE TABLE `ship_pharmacy_address` (
  `pharmacy_address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `pharmacy_name` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `fax` varchar(155) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ship_pharmacy_address`
--

INSERT INTO `ship_pharmacy_address` (`pharmacy_address_id`, `user_id`, `cart_id`, `pharmacy_name`, `phone`, `street_address`, `city`, `state`, `zip`, `fax`, `created_at`) VALUES
(7, 206, 2, 'XYG', 999999999, '350 Lemarc st', 'Fremont', 'Ascension Island', '94539', '', '2021-05-22 16:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `sms_history`
--

CREATE TABLE `sms_history` (
  `id` int(11) NOT NULL,
  `mobile` varchar(155) NOT NULL,
  `sms` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_codes`
--

CREATE TABLE `tbl_codes` (
  `tbl_codes_id` int(4) UNSIGNED NOT NULL,
  `type` varchar(40) COLLATE utf8_bin NOT NULL,
  `code` int(6) NOT NULL,
  `value` varchar(40) COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_codes`
--

INSERT INTO `tbl_codes` (`tbl_codes_id`, `type`, `code`, `value`, `status`) VALUES
(1, 'MARITAL_STATUS', 1, 'Single', 1),
(2, 'MARITAL_STATUS', 2, 'Married', 1),
(3, 'MARITAL_STATUS', 3, 'Divorce', 1),
(4, 'LIVING_SITUATION', 1, 'Alone', 1),
(5, 'LIVING_SITUATION', 2, 'W/Spouse', 1),
(6, 'LIVING_SITUATION', 3, 'W/Family', 1),
(7, 'SMOKING_STATUS', 1, 'Most/Everyday', 1),
(8, 'SMOKING_STATUS', 2, 'Somedays', 1),
(9, 'ALCOHOLIC_TYPE', 1, 'Frequent', 1),
(10, 'ALCOHOLIC_TYPE', 2, 'Occasional', 1),
(11, 'ALCOHOLIC_TYPE', 3, 'Never', 1),
(12, 'DRUG_STATUS', 1, 'None', 1),
(13, 'DRUG_STATUS', 2, 'Other', 1),
(14, 'ALERGIES', 1, 'None', 1),
(15, 'ALERGIES', 2, 'Latex', 1),
(16, 'ALERGIES', 3, 'Penicillin', 1),
(17, 'ALERGIES', 4, 'Aspirin', 1),
(18, 'ALERGIES', 5, 'Iodine', 1),
(19, 'ALERGIES', 6, 'Shellfish', 1),
(20, 'ALERGIES', 7, 'Other', 1),
(21, 'HEALTH_ISSUES', 1, 'Bleeding Problems', 1),
(22, 'HEALTH_ISSUES', 2, 'Anemia', 1),
(23, 'HEALTH_ISSUES', 3, 'High Blood Pressure', 1),
(24, 'HEALTH_ISSUES', 4, 'Heart disease/Heart attack', 1),
(25, 'HEALTH_ISSUES', 5, 'Atrial Fibrilliation', 1),
(26, 'HEALTH_ISSUES', 6, 'Pacemaker', 1),
(27, 'HEALTH_ISSUES', 7, 'Lung Disease', 1),
(28, 'FAMILY_HISTORY', 1, 'Diabetes', 1),
(29, 'FAMILY_HISTORY', 2, 'Heart Diasease', 1),
(30, 'FAMILY_HISTORY', 3, 'High Blood Pressure', 1),
(31, 'FAMILY_HISTORY', 4, 'Teting family issue', 1),
(32, 'REASON_FOR_VISIT', 1, 'Erectile Dysfunction', 1),
(33, 'REASON_FOR_VISIT', 2, 'Hair Loss', 1),
(34, 'WHEN_DID_IT_START', 1, 'Less than 3 Months', 1),
(35, 'WHEN_DID_IT_START', 2, 'Less than 6 Months', 1),
(36, 'WHEN_DID_IT_START', 3, '1 Year or Longer', 1),
(37, 'SURGERIES', 1, 'Yes', 1),
(38, 'SURGERIES', 2, 'No', 1),
(39, 'MEDICATION_TAKEN', 1, 'Yes', 1),
(40, 'MEDICATION_TAKEN', 2, 'No', 1),
(41, 'SHIP_ADDRESS_TYPE', 1, 'Ship With Our Pharmacy', 1),
(42, 'SHIP_ADDRESS_TYPE', 2, 'Select Different Pharmacy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_communications`
--

CREATE TABLE `tbl_communications` (
  `commid` int(11) NOT NULL,
  `category_name` varchar(30) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_communications`
--

INSERT INTO `tbl_communications` (`commid`, `category_name`, `subject`, `message`, `created_date_time`) VALUES
(1, 'Be Alpha Man', 'Account Logins', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n<style>\r\ntable {\r\n  font-family: arial, sans-serif;\r\n  border-collapse: collapse;\r\n  width: 100%;\r\n}\r\n\r\ntd, th {\r\n  border: 1px solid #dddddd;\r\n  text-align: left;\r\n  padding: 8px;\r\n}\r\n\r\ntr:nth-child(even) {\r\n  background-color: #dddddd;\r\n}\r\n</style>\r\n</head>\r\n<body>\r\n\r\n<h2>Login Information</h2>\r\n\r\n<table>  \r\n  <tr>\r\n    <th>Email</th>\r\n    <td>EMAIL</td>\r\n  </tr>\r\n  <tr>\r\n    <th>Mobile Number</th>\r\n    <td>MOBILE</td>\r\n  </tr>\r\n  <tr>\r\n    <th>Password</th>\r\n    <td>PASSWORD</td>\r\n  </tr>\r\n  <tr><td colspan=\"2\"><strong>Note: </strong>Please use Email/Mobile as username for login.</td></tr>\r\n</table>\r\n\r\n</body>\r\n</html>\r\n', '2021-04-04 17:36:22'),
(2, 'Forgot Password', 'Forgot Password', '<!DOCTYPE html>\n<html>\n<head>\n<style>\ntable {\n  font-family: arial, sans-serif;\n  border-collapse: collapse;\n  width: 100%;\n}\n\ntd, th {\n  border: 1px solid #dddddd;\n  text-align: left;\n  padding: 8px;\n}\n\ntr:nth-child(even) {\n  background-color: #dddddd;\n}\n</style>\n</head>\n<body>\n\n<h2>Please use below OTP</h2>\n\n<table>\n  <tr>\n    <th>Mobile Number</th>\n    <td>MOBILE</td>\n  </tr>\n  <tr>\n    <th>OTP</th>\n    <td>OTP</td>\n  </tr>\n</table>\n\n</body>\n</html>\n', '2022-02-25 21:20:31'),
(13, 'OTP Verification', 'OTP Verification', '\r\n<!doctype html>\r\n<html lang=\"en-US\">\r\n\r\n<head>\r\n    <meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />\r\n    <title>Aster IT - OTP Verification</title>\r\n    <meta name=\"description\" content=\"Nandus - OTP for Registration\">\r\n    <style type=\"text/css\">\r\n        a:hover {text-decoration: underline !important;}\r\n    </style>\r\n</head>\r\n\r\n<body marginheight=\"0\" topmargin=\"0\" marginwidth=\"0\" style=\"margin: 0px; background-color: #f2f3f8;\" leftmargin=\"0\">\r\n    <!--100% body table-->\r\n    <table cellspacing=\"0\" border=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#f2f3f8\"\r\n        style=\"@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: \'Open Sans\', sans-serif;\">\r\n        <tr>\r\n            <td>\r\n                <table style=\"background-color: #f2f3f8; max-width:670px;  margin:0 auto;\" width=\"100%\" border=\"0\"\r\n                    align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\r\n                    <tr>\r\n                        <td style=\"height:80px;\">&nbsp;</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td style=\"text-align:center;\">\r\n                          <a href=\"#\" title=\"logo\" target=\"_blank\">\r\n                            <img width=\"200\" src=\"https://smydatademo.com/assets/img/Logo.png\" title=\"logo\" alt=\"logo\">\r\n                          </a>\r\n                        </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td style=\"height:20px;\">&nbsp;</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>\r\n                            <table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"\r\n                                style=\"max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);\">\r\n                                <tr>\r\n                                    <td style=\"height:40px;\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td style=\"padding:0 35px;\">\r\n                                        <h1 style=\"color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:\'Rubik\',sans-serif;\">E-mail Confirmation</h1>\r\n                                        <span\r\n                                            style=\"display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;\"></span>\r\n                                        <p style=\"color:#455056; text-align:left;font-size:15px;line-height:24px; margin:0;\"> Hey, <br><br>It looks like you just signed up for The Aster IT, that’s awesome! Can we ask you for email confirmation? Just enter OTP in Signup Screen.\r\n                                        </p>\r\n                                        <a href=\"\"\r\n                                            style=\"background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;\">ENTEROTP</a>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td style=\"height:40px;\">&nbsp;</td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    <tr>\r\n                        <td style=\"height:20px;\">&nbsp;</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td style=\"text-align:center;\">\r\n                            <p style=\"font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;\">&copy; <strong>www.asterit.com</strong></p>\r\n                        </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td style=\"height:80px;\">&nbsp;</td>\r\n                    </tr>\r\n                </table>\r\n            </td>\r\n        </tr>\r\n    </table>\r\n    <!--/100% body table-->\r\n</body>\r\n\r\n</html>', '2020-12-23 14:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `author_name` varchar(155) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(555) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `author_name`, `description`, `image`, `status`, `created_at`) VALUES
(1, 'Danny', 'I was embarrassed to talk to my doctor about ED, but BeAlphaMan made everything very simple and discreet.  \r\n', NULL, 1, '2021-03-26 06:52:07'),
(2, 'Alix', 'I really enjoyed how easy it was to speak to an actual doctor and not having to wait in a office to be seen. Also was surprised how quickly I received my medications. ', '1616741721_GettyImages-971582964-ee0f28aa66b04fb1a54171fa4bdee7a6.jpg', 1, '2021-03-26 06:55:21'),
(6, 'Robert', 'Today I received my package and I could not be more satisfied ! It was delivered in a discreet manner. I am very impressed with the delivery speed. I am looking forward to a positive outcome. Thank you Be Alpha Man !', NULL, 1, '2021-07-01 18:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_code` varchar(40) COLLATE utf8_bin NOT NULL,
  `user_type` int(5) DEFAULT NULL COMMENT '1-Admin,2-Docotr,3-Pharmacy,4-Patient',
  `first_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(155) COLLATE utf8_bin NOT NULL,
  `mobile` varchar(15) COLLATE utf8_bin NOT NULL,
  `fax` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `password` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `gender` int(2) DEFAULT NULL,
  `aadhar_number` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `pan_number` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `doctor_signature` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `marital_status` int(10) DEFAULT NULL,
  `npi` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `state` text COLLATE utf8_bin DEFAULT NULL,
  `licence` text COLLATE utf8_bin DEFAULT NULL,
  `contact_name` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `address` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_code`, `user_type`, `first_name`, `last_name`, `email`, `mobile`, `fax`, `dob`, `age`, `password`, `gender`, `aadhar_number`, `pan_number`, `profile_pic`, `doctor_signature`, `marital_status`, `npi`, `state`, `licence`, `contact_name`, `user_status`, `address`, `created_at`, `updated_at`) VALUES
(1, 'BAM001', 1, 'Nga', 'Reddy', 'admin@gmail.com', '9676326502', NULL, '0000-00-00', 0, 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1, '', '2021-04-05 05:49:14', '2021-04-05 05:49:14'),
(197, '', 3, 'Continental Drugs', '', 'parkavecontinental@gmail.com', '201-854-3535', '201-854-6770', NULL, NULL, '6565656', NULL, NULL, NULL, NULL, NULL, NULL, '798798', 'ssss', '65656', 'Naga', 0, '6419 Park Ave, West New York, NJ 07093 ', '2021-05-26 19:49:41', '2021-05-26 19:49:41'),
(202, '', 2, 'Dr. Mahesh', 'Patel', 'drmaheshpatel49@gmail.com', '(973) 534-8196', NULL, NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, '543543', 'T/T', '5435435', '', 1, '350 Lemarc st', '2021-12-26 16:19:21', '2021-12-26 16:19:21'),
(206, '', 4, 'Bob', 'Smith', 'smith@gmail.com', '1111111111', NULL, NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 1, '', '2021-04-24 16:21:16', '2021-04-24 16:21:16'),
(207, '', 4, 'John 5454777', 'Peter 4', 'peter@gmail.com', '9879879655', NULL, '0000-00-00', 26, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, 1, '', '', '', NULL, 1, 'ewweewew', '2021-11-27 16:18:53', '2021-11-27 16:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `users_questionnaire`
--

CREATE TABLE `users_questionnaire` (
  `batch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `questionnaire_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-Ques Done, 1-Partial Pay, 2-Doctor Approved, 3-Full Pay Done',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_questionnaire`
--

INSERT INTO `users_questionnaire` (`batch_id`, `user_id`, `category_id`, `questionnaire_status`, `created_at`) VALUES
(1, 206, 1, 1, '2021-12-29 11:37:20'),
(2, 207, 2, 0, '2022-02-23 04:15:32');

-- --------------------------------------------------------

--
-- Table structure for table `users_questionnaire_history`
--

CREATE TABLE `users_questionnaire_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_questionnaire_history`
--

INSERT INTO `users_questionnaire_history` (`id`, `user_id`, `batch_id`, `category_id`, `question_id`, `option_id`, `created_at`) VALUES
(1, 206, 1, 1, 1, 1, 0),
(2, 206, 1, 1, 2, 6, 0),
(3, 206, 1, 1, 3, 9, 0),
(4, 206, 1, 1, 4, 14, 0),
(5, 206, 1, 1, 5, 17, 0),
(6, 206, 1, 1, 6, 20, 0),
(7, 206, 1, 1, 7, 26, 0),
(8, 206, 1, 1, 8, 27, 0),
(9, 206, 1, 1, 9, 30, 0),
(10, 206, 1, 1, 10, 32, 0),
(11, 206, 1, 1, 11, 33, 0),
(12, 206, 1, 1, 12, NULL, 0),
(13, 206, 1, 1, 13, 35, 0),
(14, 206, 1, 1, 14, 50, 0),
(15, 206, 1, 1, 15, 52, 0),
(16, 206, 1, 1, 16, 57, 0),
(17, 206, 1, 1, 17, 63, 0),
(18, 206, 1, 1, 18, 74, 0),
(19, 206, 1, 1, 19, 79, 0),
(20, 206, 1, 1, 20, 86, 0),
(21, 206, 1, 1, 21, 99, 0),
(22, 206, 1, 1, 22, 107, 0),
(23, 207, 2, 2, 23, 109, 0),
(24, 207, 2, 2, 24, 117, 0),
(25, 207, 2, 2, 25, 119, 0),
(26, 207, 2, 2, 26, 129, 0),
(27, 207, 2, 2, 27, 131, 0),
(28, 207, 2, 2, 28, 136, 0),
(29, 207, 2, 2, 29, 139, 0),
(30, 207, 2, 2, 30, 148, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(15) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`address_id`, `user_id`, `address_line1`, `address_line2`, `city`, `state`, `zip`, `created_at`) VALUES
(6, 207, '350 Lemarc st', 'cc', 'Fremont', 'California', '94539', '2022-02-23 04:01:48'),
(7, 207, '350 Lemarc st', '', 'Fremont', 'T/T', '94539', '2022-02-23 04:03:38'),
(8, 207, '350 Lemarc st', '', 'Fremont', 'California', '94539', '2022-02-23 04:03:52'),
(9, 207, '350 Lemarc stccccc', '', 'Fremont', 'California', '94539', '2022-02-23 04:04:03'),
(10, 207, '350 Lemarc stccccc ccxxx', '', 'Fremont', 'California', '94539', '2022-02-23 04:06:13'),
(11, 207, '350 Lemarc ad111', 'ad22', 'Fremont', 'California', '94539', '2022-02-23 04:06:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alergies`
--
ALTER TABLE `alergies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`couponId`);

--
-- Indexes for table `doctor_schedules`
--
ALTER TABLE `doctor_schedules`
  ADD PRIMARY KEY (`doctor_schedule_id`),
  ADD KEY `doctor_id` (`doctor_id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
  ADD PRIMARY KEY (`drug_id`);

--
-- Indexes for table `drug_images`
--
ALTER TABLE `drug_images`
  ADD PRIMARY KEY (`drug_image_id`);

--
-- Indexes for table `drug_prices`
--
ALTER TABLE `drug_prices`
  ADD PRIMARY KEY (`drug_price_id`);

--
-- Indexes for table `drug_strengh`
--
ALTER TABLE `drug_strengh`
  ADD PRIMARY KEY (`drug_strength_id`);

--
-- Indexes for table `drug_tablets_set`
--
ALTER TABLE `drug_tablets_set`
  ADD PRIMARY KEY (`tablet_set_id`);

--
-- Indexes for table `health_issues`
--
ALTER TABLE `health_issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_cart`
--
ALTER TABLE `patient_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `patient_information`
--
ALTER TABLE `patient_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `question_options`
--
ALTER TABLE `question_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship_pharmacy_address`
--
ALTER TABLE `ship_pharmacy_address`
  ADD PRIMARY KEY (`pharmacy_address_id`);

--
-- Indexes for table `sms_history`
--
ALTER TABLE `sms_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_codes`
--
ALTER TABLE `tbl_codes`
  ADD PRIMARY KEY (`tbl_codes_id`);

--
-- Indexes for table `tbl_communications`
--
ALTER TABLE `tbl_communications`
  ADD PRIMARY KEY (`commid`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_questionnaire`
--
ALTER TABLE `users_questionnaire`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `users_questionnaire_history`
--
ALTER TABLE `users_questionnaire_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`address_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alergies`
--
ALTER TABLE `alergies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `couponId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `doctor_schedules`
--
ALTER TABLE `doctor_schedules`
  MODIFY `doctor_schedule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `drug_images`
--
ALTER TABLE `drug_images`
  MODIFY `drug_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drug_prices`
--
ALTER TABLE `drug_prices`
  MODIFY `drug_price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `drug_strengh`
--
ALTER TABLE `drug_strengh`
  MODIFY `drug_strength_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `drug_tablets_set`
--
ALTER TABLE `drug_tablets_set`
  MODIFY `tablet_set_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `health_issues`
--
ALTER TABLE `health_issues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `patient_cart`
--
ALTER TABLE `patient_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patient_information`
--
ALTER TABLE `patient_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `question_options`
--
ALTER TABLE `question_options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ship_pharmacy_address`
--
ALTER TABLE `ship_pharmacy_address`
  MODIFY `pharmacy_address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sms_history`
--
ALTER TABLE `sms_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_codes`
--
ALTER TABLE `tbl_codes`
  MODIFY `tbl_codes_id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_communications`
--
ALTER TABLE `tbl_communications`
  MODIFY `commid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `users_questionnaire`
--
ALTER TABLE `users_questionnaire`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_questionnaire_history`
--
ALTER TABLE `users_questionnaire_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
